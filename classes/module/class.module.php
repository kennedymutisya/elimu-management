<?php


 interface module
 {
    function _list();
	function manage();
	function save();
	function delete();
}

 abstract  class system_module implements  module
 {
    protected   static $id;
	protected   static $table;
	protected   static $view;
	protected   static $userid;
	protected   static $record;

 public  function __construct()
 {
 }

 public static function autocomplete()
  {
 	$table   = filter_input(INPUT_POST,'table',FILTER_SANITIZE_STRING);
 	$column1 = filter_input(INPUT_POST,'column1',FILTER_SANITIZE_STRING);
 	$column2 = filter_input(INPUT_POST,'column2',FILTER_SANITIZE_STRING);
 	$search  = filter_input(INPUT_POST,'search',FILTER_SANITIZE_STRING);

 	if(!empty($table) && !empty($column1) && !empty($column2) && !empty($search)){
      return system::search($table,"{$column1},{$column2}",$search);
 	}else{
 	  return null;
 	}

  }

 public static function  bid_column($column,$default_value_while_adding='')
  {
 	$column = strtolower($column);

 	$return = $default_value_while_adding;
 	if(isset($this->record) && is_object($this->record)){

 		if(array_key_exists($column,$this->record)){
 			$return = $this->record->$column;
 		}
 	}
 	return $return;
  }

 public static function output_json($array=array())
 {
 	if(!empty($array)){
 		return  json_encode($array);
 	}elseif (count($this->json_data)){
 	    return json_encode($array);
 	}
 }

 public  function __destruct()
 {
 }

}

 final class sysDB
 {
	private static  $cache_id;
    private static  $query_sql;
    private static  $cache_expire = 43200;//12 hours
    private static  $queryMethod;
    public  static  $force_database  = false;

    public  $cache_replace  = false;
    public  $ErrorMsg;

    public  $SelectLimit_numrows;
    public  $SelectLimit_offset;

    public  $debug=false;

 public function __construct()
 {
 }

 public static function GetOne($sql,$force_database=false)
 {
 	global $db,$root_path;

   	self::$cache_id       = md5($sql);
    self::$query_sql      = $sql;
    self::$force_database  = $force_database;
   	self::$queryMethod    = 1;
   	return self::get();
 }

 public static function GetRow($sql,$force_database=false)
 {
 	global $db,$root_path;

   	self::$cache_id        = md5($sql);
    self::$query_sql       = $sql;
    self::$force_database  = $force_database;
   	self::$queryMethod     = 2;
   	return self::get();
 }

 public static function GetArray($sql,$force_database=false)
 {
 	global $db,$root_path;

   	self::$cache_id        = md5($sql);
    self::$query_sql       = $sql;
    self::$force_database  = $force_database;
   	self::$queryMethod     = 3;
   	return self::get();
 }

 public static function GetAssoc($sql,$force_database=false)
 {
 	global $db,$root_path;

   	self::$cache_id        = md5($sql);
    self::$query_sql       = $sql;
    self::$force_database  = $force_database;
   	self::$queryMethod     = 4;
   	return self::get();
 }

 public static function Select($sql,$force_database=false)
 {
 	global $db,$root_path;

   	self::$cache_id        = md5($sql);
    self::$query_sql       = $sql;
    self::$force_database  = $force_database;
   	self::$queryMethod     = 5;
   	return self::get();
 }

 public static function SelectLimit($sql,$numrows=-1,$offset=-1,$force_database=false)
 {
 	global $db,$root_path;

   	$cache_id                  = "{$sql}.{$numrows}.{$offset}";
   	self::$cache_id            = md5($cache_id);
    self::$query_sql           = $sql;
    $this->SelectLimit_numrows = $numrows;
    $this->SelectLimit_offset  = $offset;
    self::$force_database      = $force_database;
   	self::$queryMethod         = 6;
   	return self::get();
 }

 public static function Execute($sql)
 {
 	global $db,$root_path;
//var_dump($db);exit();
   	 if(!is_object($db)){
   	   include_once($root_path.'config/db.php');
   	 }
	  if($db->IsConnected()) {
	  	$execute = $db->Execute($sql);
	  	if(isset($execute)) {
	  		return true;
	  	}else{
	  		return false;
	  	}
	  }else{
	  	return false;
	  }
 }

 public static function generateID($table,$column)
 {
	global $db,$root_path;

	if(!is_object($db)){
   	   include_once($root_path.'core/db.php');
   	 }
	  if($db->IsConnected()) {
	  		$column = !empty($column) ? $column : 'ID';
	        $return ='';
            $record_id  = $db->GetRow("SELECT MAX(".$column.") AS RID FROM ".$table);
           if (!$record_id){
       	    return null;
           }else{
       	    if($record_id['RID']<=0)$record_id['RID']=0;
             $return = $record_id['RID']+1;
           }
	  		return $return;
	  }else{
	  	return null;
	  }
}

 public static function AutoExecute($table,$recordsArray,$mode='INSERT')
 {
 	global $db,$root_path;

   	 if(!is_object($db)){
   	   include_once($root_path.'core/db.php');
   	 }
	  if($db->IsConnected()) {

	  	if($db->AutoExecute($table,$recordsArray,$mode)) {
	  		return true;
	  	}else{
	  		$this->ErrorMsg = $db->ErrorMsg();
	  		return false;
	  	}
	  }else{
	  	return false;
	  }
 }

 public static function MetaColumns($tablename,$force_database='')
 {
    self::$cache_id        = md5($type.'metacolumns');
    self::$query_sql       = $tablename;
    self::$force_database  = 1;
   	self::$queryMethod     = 7;
   	return self::get();
 }

 public static function cacheAdd($cache_key,$data,$cache_expire=300)
 {
 	global $memcache;
 	  $cached_data = @$memcache->get($cache_key);
 	 if(!$cached_data)
 	 {
 	  @$memcache->add($cache_key,$data,MEMCACHE_COMPRESSED,$cache_expire);
 	 }else
 	 {
 	  @$memcache->replace($cache_key,$data,MEMCACHE_COMPRESSED,$cache_expire);
 	 }
 }

 public static function cacheGet($cache_key)
 {
 	global $memcache;
 	 $data = @$memcache->get($cache_key);
 	 if($data)
 	 {
 	  return $data;
 	 }else
 	 {
 	  return null;
 	 }
 }

 public static function cache_delete($sql)
 {
  global $memcache,$memcache_config;
    self::$cache_id  = md5($sql);
//    self::$query_sql = $sql;
if($memcache_config['debug']) print('memory: delete ('.self::$cache_id.')<hr>');
    $status =  false;
   # Remove from memory
    if($memcache->delete(self::$cache_id)) {
     $status = true;
    } else {
     $status =  false;
    }
    return $status;
  }

 public static function key_delete($cache_id)
 {
  global $memcache,$memcache_config;
//    self::$query_sql = $sql;
if($memcache_config['debug']) print('memory: delete ('.$cache_id.')<hr>');
    $status =  false;
   # Remove from memory
    if(is_object($memcache) && @$memcache->delete($cache_id)) {
     $status = true;
    } else {
     $status =  false;
    }
    return $status;
  }

 private  static function get()
 {

	# Connect to database
	global $db,$root_path,$memcache_config;
	 require_once($root_path.'config/db.php');
	  if(is_object($db)){
	   if($db->IsConnected()) {
//	   	$db->debug=1;
	  # Create object
	  if($memcache_config['debug']) print('get: database ('.self::$cache_id.')<hr>');
	  if($memcache_config['debug']==1) $db->debug=1;

	  $data = array();

	 switch (self::$queryMethod)
	  {
	  	case 1://GetOne
	  	 try {
	  	  $rs   = $db->GetOne(self::$query_sql);
	  	  $data = $rs;
	  	 }catch (Exception $e){
	  	 	$this->ErrorMsg = $e->getMessage();
	  	 }
	  	break;
	  	case 2://GetRow
	  	 try {
	  	   $rs   = $db->GetRow(self::$query_sql);
	  	  $data = $rs;
	  	 }catch (Exception $e){
	  	 	$this->ErrorMsg = $e->getMessage();
	  	 }
	  	break;
	  	case 3://GetArray
	  	  try {
	  	  $rs   = $db->GetArray(self::$query_sql);
	  	  $data = $rs;
	  	  }catch (Exception $e){
	  	 	$this->ErrorMsg = $e->getMessage();
	  	  }
	  	break;
	  	case 4://GetAssoc
	  	 try {
	  	  $rs   = $db->GetAssoc(self::$query_sql);
	  	  if(isset($rs) && is_array($rs) && sizeof($rs)>0){
	  	  	foreach ($rs as $index=>$array_data){
	  	  	 $data[$index] = $array_data;
	  	  	}
	  	  }
	  	 }catch (Exception $e){
	  	 	$this->ErrorMsg = $e->getMessage();
	  	 }
	  	break;
	  	case 5://Select
	  	 try {
	  	  $rs   = $db->Execute(self::$query_sql);
	  	 if(isset($rs->fields) && count($rs->fields)){
	  	 	$count = 0;
	  	    while(!$rs->EOF) {
	         $data[] = $rs->fields;
	         $rs->MoveNext();
	         ++$count;
	  	     }
	  	   }
	  	  }catch (Exception $e){
	  	 	$this->ErrorMsg = $e->getMessage();
	  	  }
	  	break;
	  	case 6://SelectLimit
//	  		$db->debug=1;
          try {
	  	  $data = array();
	  	  $rs   = $db->SelectLimit(self::$query_sql,$this->SelectLimit_numrows,$this->SelectLimit_offset);
	  	 if(isset($rs)){
	  	  if($rs->RecordCount()>0) {
	  	  	$count = 0;
	  	   while(!$rs->EOF) {
	        $data[] = $rs->fields;
	        $rs->MoveNext();
	        ++$count;
	  	    }
	  	   }
	  	  }
	  	 }catch (Exception $e){
	  	 	$this->ErrorMsg = $e->getMessage();
	  	 }
	  	break;
	  	case 7://MetaColumns
	  	 try {
	  	  $rs   = $db->MetaColumns(self::$query_sql);
	  	  if(isset($rs)){
	  	  	foreach ($rs as $column_name=>$column_details){
	  	  	  $data[] = $column_name;
	  	  	}
	  	  }
	  	 }catch (Exception $e){
	  	 	$this->ErrorMsg = $e->getMessage();
	  	 }
	  	break;
	  }

	  # Close
	  if(isset($rs) && is_object($rs)) $rs->Close();
	  $rs = null;



	  # Return recently grabbed records
	  return $data;
	 } else {
	  echo 'main_module: not connected to database ';
	  return false;
	 }
	} else {
	  echo 'main_module: not connected to database ';
	  return false;
	}

  }

 private static function set($data)
 {
   global $memcache,$memcache_config;

//   echo self::$appname;
    # Write to memory
//     if($memcache->add(self::$cache_id,$data,self::$cache_expire)) {

     if($this->cache_replace){
     # Replace information
      @$memcache->replace(self::$cache_id,$data,MEMCACHE_COMPRESSED,self::$cache_expire);
     if($memcache_config['debug']) print('set: replacing ('.self::$cache_id.')<hr>');
      return true;
     } else {
      @$memcache->add(self::$cache_id,$data,MEMCACHE_COMPRESSED,self::$cache_expire);
      if($memcache_config['debug']) print('set: memory ('.self::$cache_id.')<hr>');
       return  true;
     }
  }

 public  function __destruct()
 {
 }

}


?>
