<?php

final class leavetype { 
	public $lvtname; 
	public $lvtdesc; 
	public $eligible_genders = array(); 
	public $eligible_employment_terms = array(); 
	public $eligible_job_titles = array();
	public $apply_in_advance_of_days; 
	public $apply_days_min; 
	public $apply_days_max; 
	public $apply_max_spells_per_year; 
	public $allow_leave_clubbing; 
	public $allow_scheduing; 
	public $allow_accumulation; 
	public $accumulation_percentage; 
	public $accumulation_laspe_at; 
	public $include_holidays; 
	public $include_weekends; 
	public $clubb_with = array();
	public $allow_apply_past_days; 
	public $leave_cancellation_after_days; 
	public $waiting_period; 
	public $allowed_current_year_negative_balance; 
	public $allowed_previous_year_negative_balance; 
	public $allow_encashment; 
	public $encashment_minimum_days; 
	public $encashment_maximum_days; 
	public $encashment_retain_days; 
	public $encashment_frequency; 
	public $require_certificates_validation; 
	
	public function __construct( $lvtcode ){
	global $db;
	
	$record = $db->GetRow("select * from HMLVT where LVTCODE='{$lvtcode}' ");
	
	$this->lvtname = valueof($record, 'LVTNAME'); 
	$this->lvtdesc = valueof($record, 'LVTDESC'); 
	
	if(isset($record['ELGGND']) && !empty($record['ELGGND'])){
	 $this->eligible_genders =unserialize( base64_decode($record['ELGGND']));
	}
	
	if(isset($record['ELGEMT']) && !empty($record['ELGEMT'])){
	 $this->eligible_employment_terms =unserialize( base64_decode($record['ELGEMT']));
	}
	
	if(isset($record['ELGJTT']) && !empty($record['ELGJTT'])){
	 $this->eligible_job_titles =unserialize( base64_decode($record['ELGJTT']));
	}
	
	$this->allow_leave_clubbing = valueof($record, 'ALWCLB',0); 
	
	if( $this->allow_leave_clubbing && isset($record['CLBWTH']) && !empty($record['CLBWTH'])){
	 $this->clubb_with =unserialize( base64_decode($record['CLBWTH']));
	}
	 
	$this->waiting_period = valueof($record, 'WAITPRD'); 
	$this->apply_in_advance_of_days = valueof($record, 'APDDAYS',0); 
	$this->allow_apply_past_days = valueof($record, 'ALWAPPD',0); 
	$this->apply_days_min = valueof($record, 'AVLMNDAYS',1); 
	$this->apply_days_max = valueof($record, 'AVLMXDAYS'); 
	$this->apply_max_spells_per_year = valueof($record, 'AVLMXSPL'); 
	
	$this->include_holidays = valueof($record, 'INCHLD',0); 
	$this->include_weekends = valueof($record, 'INCWKD',0); 
	
	$this->allow_scheduing = valueof($record, 'ALWSCH',0); 
	
	$this->allow_accumulation = valueof($record, 'ALWACM',0); 
	$this->accumulation_percentage = valueof($record, 'ACMPCG'); 
	$this->accumulation_laspe_at = valueof($record, 'ACMXPA'); 
	
	$this->leave_cancellation_after_days = valueof($record, 'ALWLCPD'); 
	
	$this->allowed_current_year_negative_balance = valueof($record, 'NGBYC',0); 
	$this->allowed_previous_year_negative_balance = valueof($record, 'NGBYP',0); 
	
	$this->allow_encashment = valueof($record, 'ALWENC',0); 
	$this->encashment_minimum_days = valueof($record, 'ENCLMTMN',0); 
	$this->encashment_maximum_days = valueof($record, 'ENCLMTMX'); 
	$this->encashment_retain_days = valueof($record, 'ENCRTNLMT',0); 
	$this->encashment_frequency = valueof($record, 'ENCFRQM',12); 
	$this->require_certificates_validation = valueof($record, 'RQCTVD',0); 

   } 
 
 } 
