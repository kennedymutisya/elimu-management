<?php


 class auths {
 	private $_appid;
 	private $_modid;
 	private $_groupcode;
 	private $menu_parents = array();
 	private $menu_children = array();
 	private $menu_saved = array();
 	
	public function __construct( $appid , $modid , $groupcode){
		$this->_appid = $appid;
		$this->_modid = $modid;
		$this->_groupcode = $groupcode;
	}
	
	public function list_menu(){
		global $db;
     
	 $modulename       = $db->GetOne("SELECT MODNAME FROM SYSMODS WHERE APPID='{$this->_appid}' AND MODID='{$this->_modid}' ");
	 $menus_saved       = $db->GetArray("SELECT MNUID FROM SYSMNUAUTH WHERE APPID='{$this->_appid}' AND MODID='{$this->_modid}' AND  GROUPCODE='{$this->_groupcode}' ");
	 
	 if(count($menus_saved)>=1){
	 	foreach ($menus_saved as $menu_saved){
	 	 $menu_saved_id = valueof($menu_saved, 'MNUID');
	 	 $this->menu_saved[$menu_saved_id] = 1;
	 	}
	 }
	 
	 $menu       = $db->GetAssoc("
	   SELECT MNUID,MNUNAME,PARENTID,MODPATH 
	   FROM SYSMODMNU 
	   WHERE APPID='{$this->_appid}' 
	   AND MODID='{$this->_modid}' 
	   ORDER BY PARENTID,POSITION
	  ");
	  
	 $num_parents = array();
	 
	 if(count($menu)>0){
	  foreach ($menu as $mnuid=>$menuitem){
 	   
 	   	$mnuname   =  valueof($menuitem, 'MNUNAME');
 	   	$modpath   =  valueof($menuitem, 'MODPATH');
 	   	$parentid  =  valueof($menuitem, 'PARENTID');
 	   	
 	   	if(empty($parentid)){
 	   	 $this->menu_parents[$mnuid]  = array($mnuname,$modpath);
 	   	}else{
 	   	 $this->menu_children[$parentid][$mnuid] = array($mnuname,$modpath);	
        }
 	   	
      }
     }
  
      $num_parents = count($this->menu_parents);
      
      $return =  "[";
      
     if($num_parents>0)
     {
     	$count = 1;
     	
     	 foreach ($this->menu_parents as $parentid=>$parent){
     	 	
     	   $parentname = valueof($parent,0);
     	   $modpath    = valueof($parent,1);
     	   
     	    $return .=  "{\r\n";
     	     $return .=  "\"id\":{$parentid},\r\n";
     	     $return .= "\"text\": \"{$parentname}\"\r\n";
     	     
     	     if(array_key_exists($parentid, $this->menu_children)){
     	     	$return .=  ","; 
     	     }else{
     	       if(array_key_exists($parentid, $this->menu_saved)){
     	       }
     	    
     	     }
     	     
     	    //children
     	    if(array_key_exists($parentid, $this->menu_children)){
     	    	$return .= "\"state\":\"closed\", \r\n";
     	    	$return .= "\"children\":[\r\n";
     	         $return .= self::list_menu_children($parentid);
     	        $return .= "]\r\n";
     	        
     	     }else{
     	     	
			  $vars['appid']    = $this->_appid;
			  $vars['modid']    = $this->_modid;
			  $vars['mnuid']    = $parentid;
			  $vars['modpath']  = $modpath;
			  $vars_str         = packvars($vars);
     	     
             }
     	   
     	    $return .=  "}";//close leaf
     	   
     	   if($count<$num_parents) {
     	   	 $return .=  ",\r\n";
     	   }
     	   
     	   ++$count;
         }
         
       }
       
     $return .=  "]\r\n";
     
     return $return;
     
	}
	
	public function list_menu_children( $parentid ){
		
      $num_children = count( $this->menu_children[$parentid] );
      
     if($num_children>0){
     	$count = 1;
     	$tree = "";
     	
     	 foreach ($this->menu_children[$parentid] as $childid=>$child){
     	 	
     	   $childname      = valueof($child,0);
     	   $modpath        = valueof($child,1);
     	    
     	    if(!array_key_exists($childid, $this->menu_children)){
     	     $state = 'open';	
     	    }else{
     	     $state = 'closed';	
     	    }
     	    
		   $vars['appid']    = $this->_appid;
		   $vars['modid']    = $this->_modid;
		   $vars['mnuid']    = $childid;
		   $vars['modpath']  = $modpath;
		   $vars_str         = packvars($vars);
     	      
     	   $tree .= "{\r\n";
     	    $tree .=  "\"id\":{$childid},\r\n";
     	    $tree .=  "\"text\":\"{$childname}\",\r\n";
     	    if(!array_key_exists($childid, $this->menu_children) && array_key_exists($childid, $this->menu_saved)){
     	     $tree .= "\"checked\":\"true\", \r\n";
     	    }
     	    
     	    $tree .= "\"state\":\"{$state}\" \r\n";
     	    //children
     	    if(array_key_exists($childid, $this->menu_children)){
     	    	$tree .= ",\r\n";
     	    	$tree .=  "\"children\":[\r\n";
     	         $tree .=  self::list_menu_children( $childid );
     	        $tree .=  "]\r\n";
     	    }
     	    
     	     $tree .=  "}";//close leaf
     	   
     	   if($count<$num_children) {
     	   	$tree .= ",\r\n";
     	   }
     	   
     	   ++$count;
         }
         
      }
      
     return $tree;
     
	}
	
 }
 
