<?php

 final class company {
	public $id;
	public $companyname;
	public $addresspys;
	public $addresspst;
	public $postcode;
	public $town;
	public $email;
	public $phone1;
	public $phone2;
	public $phone3;
	public $phone4;
	public $adminemail;
	public $adminname;
	public $mailertype;
	public $smtphost;
	public $smtpport;
	public $smtpuser;
	public $smtppwd;
	public $smtpstl;
	public $smtpsize;
	public $logo;

  public function __construct(  ){
  global $db;
	$record             = $db->GetRow("select * from HMCMP  ");
	$this->companyname  = valueof($record, 'COMPANYNAME');
	$this->addresspys   = valueof($record, 'ADDRESSPYS');
	$this->addresspst   = valueof($record, 'ADDRESSPST');
	$this->postcode     = valueof($record, 'POSTCODE');
	$this->town         = valueof($record, 'TOWN');
	$this->email        = valueof($record, 'EMAIL');
	$this->phone1       = valueof($record, 'PHONE1');
	$this->phone2       = valueof($record, 'PHONE2');
	$this->phone3       = valueof($record, 'PHONE3');
	$this->phone4       = valueof($record, 'PHONE4');
	$this->adminemail   = valueof($record, 'ADMINEMAIL');
	$this->adminname    = valueof($record, 'ADMINNAME');
	$this->mailertype   = valueof($record, 'MAILERTYPE');
	$this->smtphost     = valueof($record, 'SMTPHOST');
	$this->smtpport     = valueof($record, 'SMTPPORT');
	$this->smtpuser     = valueof($record, 'SMTPUSER');
	$this->smtppwd      = valueof($record, 'SMTPPWD');
	$this->smtpstl      = valueof($record, 'SMTPSTL');
	$this->smtpsize     = valueof($record, 'SMTPSIZE');
	$this->logo         = valueof($record, 'LOGO');

  }

 } 
