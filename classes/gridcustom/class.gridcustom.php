<?php


/*
 *@author : kenmsh@gmail.com
 **/
 
	class gridcustom {
	 public $name;
	 public $title;
     public $grid_width  =   510;
	 public $grid_height  =   300;
     public $form_width  =   510;
	 public $form_height  =   300;
	 public $grid_id;
	 public $form_id;
	 public $dialog_id;
	 public $toolbar_id;
	 public $data_handler;
	 public $save_handler;
	 public $delete_handler;
	 public $columns       = array();
	 public $url           =   'endpoints/crud/';
	 public $pagination    =   'true';
	 public $rownumbers    =   'true';
	 public $fitColumns    =   'true';
	 public $singleSelect  =   'true';
	 public $idField       =   'id';
	 public $toolbar;
	 public $queryParams   =   array();
	 public $remoteFilter  =   'true';
	 public $multiSort     =   'true';
	 public $fit           =   'true';
	 public $type          =   'post';
		
		
  public function __construct( $name ){
		
	$this->name           =   $name;
		
	$this->grid_id         =   'dg_'.$this->name;
	$this->form_id         =   'fm_'.$this->name;
	$this->dialog_id       =   'dlg_'.$this->name;
	$this->toolbar_id      =   'toolbar_'.$this->name;
	$this->data_handler    =   'data_'.$this->name;
	$this->save_handler    =   'save_'.$this->name;
	$this->delete_handler  =   'delete_'.$this->name;
	
  }
  
  public function render_grid( $auto_load = true ){
	  global $vars;
	  
	$queryParams = array();
	$queryParams_get = array();
	$queryParams_str = '';
	
	if(sizeof($this->queryParams)>0){
	  foreach($this->queryParams as $qp_key => $qp_val ){
		 $queryParams[] = "{$qp_key}:{$qp_val}";
	  }
	}
	
	if(sizeof($queryParams)>0){
	 $queryParams_str =  ',' . implode(',', $queryParams);
	}
	
	//class=\"easyui-datagrid\" 
	 echo "<table id=\"{$this->grid_id}\" style=\"width:{$this->grid_width}px;height:{$this->grid_height}px;border:0\" >";
	 echo '<thead>';
	  echo '<tr>';
	  
	   if(isset($this->columns) && sizeof($this->columns)>0){
		  foreach($this->columns as $colid=>$col_properties){
		  	
		  	$visible        =  valueof($col_properties,'visible',1);
		  	$sortable_flg   =  valueof($col_properties,'sortable',1);
		  	$title          =  valueof($col_properties,'title',$colid);
			$width          =  valueof($col_properties,'width',50);
			$width          =  is_numeric($width) ? $width : 50;
			$sortable       =  $sortable_flg==1 ? 'true' : 'false';
			
			if($visible==1 || $visible==2){
			 echo "<th field=\"{$colid}\" width=\"{$width}\" sortable=\"{$sortable}\">{$title}</th>";
		    }
		    
		  }
	   }
	  echo '</tr>';
	 echo '</thead>';
	echo '</table>';
	
	echo '<div id="'.$this->toolbar_id.'">';
	echo "<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-add\" plain=\"true\" onclick=\"custom_crud.add('{$this->dialog_id}','{$this->form_id}','Add {$this->title}');\">New</a>";
	echo "<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-edit\" plain=\"true\" onclick=\"custom_crud.edit('{$this->grid_id}','{$this->dialog_id}','{$this->form_id}','Edit {$this->title}');\">Edit</a>";
	echo "<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-remove\" plain=\"true\" onclick=\"custom_crud.remove('{$this->grid_id}','{$this->dialog_id}','{$this->form_id}','{$this->title}','{$this->delete_handler}');\">Remove</a>";
	echo '</div>';
	
	
	if($auto_load){
	 $opt_url = " url:'{$this->url}',";
	}else{
	 $opt_url = "";
	}
	
	echo <<<SCRIPT
	<script>
	  
	   $(function(){
			var {$this->grid_id} = $('#{$this->grid_id}').datagrid({
		     url:'{$this->url}',
             pagination:{$this->pagination},
             rownumbers:{$this->rownumbers},
             fitColumns:{$this->fitColumns},
             singleSelect:{$this->singleSelect},
             idField:'{$this->idField}',
             toolbar:'#{$this->toolbar_id}',
             queryParams:{  modvars:'{$vars}',function:'{$this->data_handler}' {$queryParams_str} },
             remoteFilter:{$this->remoteFilter},
             multiSort:{$this->multiSort},
             fit:{$this->fit},
             type:'{$this->type}',
		   });
		   {$this->grid_id}.datagrid('enableFilter');
		});
		
	</script>
SCRIPT;
	
  }
  
  public function render_form() {
		global $cfg, $vars, $combogrid_array;
		
	echo  "<div id=\"{$this->dialog_id}\" class=\"easyui-dialog\" style=\"width:{$this->form_width}px;height:{$this->form_height}px;padding:10px 20px\" closed=\"true\" buttons=\"#dlg_buttons_{$this->name}\">";	
    echo "<form id=\"{$this->form_id}\" method=\"post\" novalidate>";
    echo "<input type=\"hidden\" id=\"id\" name=\"id\" value=\"0\">";
 
    if(isset($this->columns) && sizeof($this->columns)>0){
		  foreach($this->columns as $colid=>$col_properties){
			
			$visible           =  valueof($col_properties,'visible',1);
			$title             =  isset($col_properties['title']) ? $col_properties['title'] : $col;
			$validation_class  =  isset($col_properties['validation_class']) ? $col_properties['validation_class'] : '';
			$required          =  isset($col_properties['required']) && $col_properties['required']==1 ? "required=\"true\" " : '';
			$validType         =  isset($col_properties['validType']) ? $col_properties['validType'] : '';
			$inputType         =  isset($col_properties['inputType']) ? $col_properties['inputType'] : 'text';
			
			if($visible==1  || $visible==3 ){
			switch($inputType) {
				case 'text':
				default:
				 $input  = "<input type=\"text\" id=\"{$colid}\" name=\"{$colid}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
				 break;	
				case 'textarea':
				 $input  = "<textarea  id=\"{$colid}\" name=\"{$colid}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\"></textarea>";
				 break;	
				case 'password':
				 $input  = "<input type=\"password\" id=\"{$colid}\" name=\"{$colid}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
				 break;	
				case 'date':
				 $input  = "<input type=\"text\" id=\"{$colid}\" name=\"{$colid}\" class=\"easyui-datebox {$validation_class}\" {$required} validType=\"{$validType}\" data-options=\"formatter:dateYmdFormatter,parser:dateYmdParser\" >";
				 break;	
				case 'checkbox':
				 $input  = "<input type=\"checkbox\" id=\"{$colid}\" name=\"{$colid}\" value=\"1\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
				break;
				case 'select':
			       $selectsrc         =  isset($col_properties['selectsrc']) ? $col_properties['selectsrc'] : 'text';
			 
			        if(strstr($selectsrc,'|')){
					  $selectsrc_array            = explode('|', $selectsrc);
					  $table_col_datasrc          = valueof($selectsrc_array, 0);
	                  $linked_tbl_col_code        = valueof($selectsrc_array, 1);
	                  $linked_tbl_col_name        = valueof($selectsrc_array, 2);
	                  
	                  if(!empty($table_col_datasrc) && !empty($linked_tbl_col_code) && !empty($linked_tbl_col_name) ){
	                  	 $input    = ui::form_select($colid , $table_col_datasrc , $linked_tbl_col_code , $linked_tbl_col_name , '', '');
	                  }
			        }
			   break;
			   case 'combogrid':
			   $input  = "<input type=\"text\" id=\"{$colid}\" name=\"{$colid}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
			    echo '<script>';
                 echo ui::ComboGrid($combogrid_array,$colid);
                echo '</script>';
			   break;
			 }
			
			
			echo "<div class=\"fitem\">
                   <label>{$title}:</label>
                   {$input}
                  </div>
                  ";

         }//if visible
	   }
	 
    }
		
	echo '</form>';
	echo '</div>';
	echo "<div id=\"dlg_buttons_{$this->name}\">";
	 echo "<a href=\"#\" class=\"easyui-linkbutton\" iconCls=\"icon-ok\" onclick=\"custom_crud.save('{$this->grid_id}','{$this->dialog_id}','{$this->form_id}','{$this->title}','{$this->save_handler}');\">Save</a>";
     echo "<a href=\"#\" class=\"easyui-linkbutton\" iconCls=\"icon-cancel\" onclick=\"custom_crud.close('{$this->dialog_id}','{$this->form_id}');\">Cancel</a>";
	echo '</div>';
	
  }//fn
	
  public function render_reload_js( $_params='reload=1' ){
	  global $vars;
	  return "\$('#{$this->grid_id}').datagrid('load',{ {$_params},modvars:'{$vars}',function:'{$this->data_handler}' });";
  }
 
 }//class
