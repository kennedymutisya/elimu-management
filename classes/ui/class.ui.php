<?php

/**
 * user interface controls
 *
 * this class contanins methods to create commonly used controls
 * @author Erastus Kariuki <kenmsh@gmail.com>
 * @version 5.0
 * @subpackage User Interface Controls
 *
 */

 class ui 
 {
 
 /**
  * combobox
  */
  
 static function combobox( $id, $source, $colValue,$colText, $onSelectHandler='',$editable=false, $disabled=false ,$readonly=false, $default_value='' ){
	global $vars;
	
 $idField = 'id';
 
  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	 $id = "{$id}_".MNUID; 
    }
  }

 $textField = 'text';
 $onSelectHandlerFn = '';
 $editableState = $editable ? 'true' : 'false';
 $disabledState = $disabled ? 'true' : 'false';
 $readonlyState = $readonly ? 'true' : 'false';
 
 $vars2['source']    = $source;
 $vars2['IdField']   = $colValue;
 $vars2['TextField'] = $colText;
 $vars2_str = packvars($vars2);
 
 if(!empty($onSelectHandler)) {
  $onSelectHandlerFn = "{$onSelectHandler}(option.{$idField},option.{$textField});";
 }
 
 $combobox = '<input id="'.$id.'"  name="'.$id.'" class="easyui-combobox" value="'.$default_value.'" data-options="
    valueField: \''.$idField.'\',
    textField: \''.$textField.'\',
    url: \'./endpoints/cba/\',
    editable: '.$editableState.',
    disabled: '.$disabledState.',
    readonly: '.$readonlyState.',
    delay: 100,
    mode:\'remote\',
    queryParams: {vars:\''.$vars2_str.'\'},
    onSelect: function(option){
     '.$onSelectHandlerFn.'
    }">';
    
  return  self::compress_html( $combobox );
    
}

 static function ComboGrid($combogrid_array,$form_field,$onSelectHandler='', $queryParams= array() ,$multiple=false){

	$js_combogrid_columns     = array();
	$js_combogrid_columns_str = array();
	$queryParams_array = array();
	$queryParams_extra = '';
	$return_cols = array();
	$IdField    = '';
	$TextField = '';
	$panelWidth = 0;
	
	if(sizeof($queryParams)>0){
	  foreach($queryParams as $qp_key => $qp_val ){
		 $queryParams_array[] = "{$qp_key}:{$qp_val}";
	  }
	}
	
	if(sizeof($queryParams_array)>0){
	 $queryParams_extra =  ',' . implode(',', $queryParams_array);
	}
	
	if(isset($combogrid_array[$form_field]['columns'])){
		foreach ($combogrid_array[$form_field]['columns'] as $db_column => $properties){
			
		   $field   = valueof($properties, 'field');
           $title   = valueof($properties, 'title');
           $width   = valueof($properties, 'width',50);
           
           $panelWidth += $width;
           
           $return_cols[$db_column] = $field;
           
           if(isset($properties['isIdField']) && ($properties['isIdField']===true)){
           	$IdField = $field;
           	$IdFieldRemote = $db_column;
           }
           
           if(isset($properties['isTextField']) && ($properties['isTextField']===true)){
           	$TextField = $field;
           	$TextFieldRemote = $db_column;
           }
           
           $js_combogrid_columns[] = "{field:'{$field}',title:'{$title}',width:{$width}}";
           
		}
	}
	
	$source = isset($combogrid_array[$form_field]['source']) ? $combogrid_array[$form_field]['source'] : 'invalid';
	
	$vars['source']       = $source;
	$vars['IdField']      = $IdFieldRemote;
	$vars['TextField']    = $TextFieldRemote;
	
	$vars = array_merge($vars,$return_cols);
	
	$vars_str = packvars($vars);
	
	if(sizeof($js_combogrid_columns)>0){
		$js_combogrid_columns_str = implode(',', $js_combogrid_columns);
	}
	
	if(!empty($onSelectHandler)){
	  $onSelectHandlerJS = "{$onSelectHandler}(row)";
	}else{
	  $onSelectHandlerJS = $onSelectHandler;
	}
	
	if($multiple){
	  $isMultiple = 'true';
	  $checkbox = "{field:'ck',checkbox:true},";
	}else{
	  $isMultiple = 'false';
	  $checkbox = '';
	}
	
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $form_field = "{$form_field}_".MNUID; 
     }
    }
    
	return "
	 $('#{$form_field}').combogrid({
      panelWidth:{$panelWidth},
      multiple: {$isMultiple},
      url: './endpoints/cga/',
      idField:'{$IdField}',
      textField:'{$TextField}',
      queryParams:{ vars:'{$vars_str}' {$queryParams_extra} },
      delay: 100,
      mode:'remote',
      fitColumns:true,
      columns:[[{$checkbox}{$js_combogrid_columns_str}]],
      onSelect:function(i,row){
         {$onSelectHandlerJS}
       }
      });
      
	";
	
	}
	
 static function compress_html($buffer){
 	return $buffer;
  $search = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
  $replace = array('>','<','\\1');
  $compr_buffer = preg_replace($search, $replace, $buffer);
  return $compr_buffer;
 }


/**
 * html input field of type text , textarea , password , hidden...
 *
 * @param string $type
 * @param string $id
 * @param string $size
 * @param string $default_value
 * @param string $javascript
 * @param string $disabled
 * @param string $options
 * @param string $help_text
 * @return string
 */
 static public function form_input($type, $id, $size=30, $default_value='', $javascript='', $disabled='', $options="", $help_text='', $class='' ,$data_options=''){

    if(strstr($size,'px')){
 	 $width =" style=\"width:{$size}\" ";
    }else{
     $width =" size=\"".$size."\" ";
    }
    
  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	 $id = "{$id}_".MNUID; 
    }
  }
  
  switch ($type){
  	case "text":
  		$input = "<input type=\"text\" autocomplete=\"off\" name=\"".$id."\" id=\"".$id."\" ".$width." value=\"".$default_value."\" ".$javascript." ".$disabled."  ".$options." data-options=\"{$data_options}\" class=\"{$class}\" > \n";
  	break;
  	case "password":
  		$input =   "<input type=\"password\" name=\"".$id."\" id=\"".$id."\" ".$width." value=\"".$default_value."\" ".$javascript."> \n ";
  	break;
  	case "hidden":
  		$input =   "<input type=\"hidden\" name=\"".$id."\" id=\"".$id."\" value=\"".$default_value."\"> \n";
  	break;
  	case "select":
  	 $select = "<select name=\"".$id."\" id=\"".$id."\" ".$javascript."  data-options=\"{$data_options}\" class=\"{$class}\" > \n";
      if(is_array($default_value)){
      foreach ($default_value as $value=>$desc) {
      		$select.="<option value=\"".$value."\">".$desc."</option> \n";
      	}
      }
     $select.="</select> \n";
       $input =	$select;
  	break;
  	case "textarea":
  	  $input = "<textarea name=\"".$id."\" id=\"".$id."\" cols=\"".$size."\" {$javascript} rows=\"2\" data-options=\"{$data_options}\" class=\"{$class}\">".$default_value."</textarea>";
  	break;
  	default:
  	  $input = "<input type=\"text\" name=\"".$id."\" id=\"".$id."\" ".$width." value=\"".$default_value."\" ".$disabled."  ".$options." data-options=\"{$data_options}\" class=\"{$class}\"  > \n";
  	break;
  }//switch

  return $input;
  
}//function input

/**
 * html input id...
 *
 * @param string $id
 */
 
 static public function get_input_id( $field_id, $underscore ){
	 
   $underscore_str =  $underscore? '_' : '';
  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	$field_id = "{$field_id}{$underscore_str}".MNUID; 
  }
  return $field_id;
  
 }
 
 /**
  *synonymous to self::get_input_id()
  **/
  
 static public function fi( $field_id, $underscore=true ){
  return self::get_input_id( $field_id,$underscore );
 }
 
 static public function form_input_unique($type, $id, $size=30, $default_value='', $javascript='', $disabled='', $options="", $help_text='', $class='' ,$data_options=''){

    if(strstr($size,'px')){
 	 $width =" style=\"width:{$size}\" ";
    }else{
     $width =" size=\"".$size."\" ";
    }
    
  if(defined('MNUID')){
	$id = "{$id}_".MNUID; 
  }
  
  switch ($type){
  	case "text":
  		$input = "<input type=\"text\" autocomplete=\"off\" name=\"".$id."\" id=\"".$id."\" ".$width." value=\"".$default_value."\" ".$javascript." ".$disabled."  ".$options." data-options=\"{$data_options}\" class=\"{$class}\" > \n";
  	break;
  	case "password":
  		$input =   "<input type=\"password\" name=\"".$id."\" id=\"".$id."\" ".$width." value=\"".$default_value."\" ".$javascript."> \n ";
  	break;
  	case "hidden":
  		$input =   "<input type=\"hidden\" name=\"".$id."\" id=\"".$id."\" value=\"".$default_value."\"> \n";
  	break;
  	case "select":
  	 $select = "<select name=\"".$id."\" id=\"".$id."\" ".$javascript."  data-options=\"{$data_options}\" class=\"{$class}\" > \n";
      if(is_array($default_value)){
      foreach ($default_value as $value=>$desc) {
      		$select.="<option value=\"".$value."\">".$desc."</option> \n";
      	}
      }
     $select.="</select> \n";
       $input =	$select;
  	break;
  	case "textarea":
  	  $input = "<textarea name=\"".$id."\" id=\"".$id."\" cols=\"".$size."\" {$javascript} rows=\"2\" data-options=\"{$data_options}\" class=\"{$class}\">".$default_value."</textarea>";
  	break;
  	default:
  	  $input = "<input type=\"text\" name=\"".$id."\" id=\"".$id."\" ".$width." value=\"".$default_value."\" ".$disabled."  ".$options." data-options=\"{$data_options}\" class=\"{$class}\"  > \n";
  	break;
  }//switch

  $input .= isset($help_text) && strlen($help_text)>2 ? self::form_help($help_text) : '';
  return $input;
}//function input

 static public function form_checkbox($id,$checked='',$javascript='',$class='',$value='', $help_text=''){
	
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $id = "{$id}_".MNUID; 
     }
    }

 	$checked = isset($checked)&&!empty($checked)?'checked':"";
	
	if( isset($value)&&!empty($value)){
	 $checkbox = "<input type=\"checkbox\" name=\"{$id}\" id=\"{$id}\"  class=\"{$class}\" value=\"{$value}\" {$checked} {$javascript} >";
	}else{
	 $checkbox = "<input type=\"checkbox\" name=\"{$id}\" id=\"{$id}\"  class=\"{$class}\" {$checked} {$javascript} >";
	}
	
	$checkbox .= isset($help_text) && strlen($help_text)>2 ? "<label for=\"{$id}\">".self::form_help($help_text)."</label>" : '';
	
  return $checkbox;
}

 static public function form_radiobutton($id,$name,$checked=false,$javascript='',$disabled='',$value=''){

    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $id = "{$id}_".MNUID; 
     }
    }

 	$status = '';

    if($checked){
     $status = 'checked';
    }

	return "<input type=\"radio\" name=\"{$name}\" id=\"{$id}\" value=\"{$value}\" {$status} {$javascript} {$disabled}>";
}

 static public function form_button($type,$id,$label,$javascript,$disabled='',$icon='',$class=''){
	 global $root_path;
	
	 switch ($type)
	 {
	 	case 'button':
	 		$class .= ' button ';
	 		switch ($id)
	 		{
	 			case 'btnSave':
	 				$class .= ' primary icon approve';
	 			break;
	 			case 'btnAppFlt':
	 				$class .= ' primary icon search';
	 			break;
	 			case 'btnDelete':
	 				$class .= ' danger icon trash';
	 			break;
	 			case 'btnCancel':
	 				$class .= ' icon reload';
	 			break;
	 		}
	 	break;
	 	case 'reset':
	 		$class = 'button icon reload';
	 	break;	
	 }
	 
 	switch ($type){
 	 case "submit":
 	 	$button = "<input type=\"submit\" name=\"".$id."\" id=\"".$id."\"  class=\"".$class."\" value=\"".$label."\" ".$javascript." ".$disabled." >";
 	 break;
 	 case "reset":
 	 	$button = "<input type=\"reset\" name=\"".$id."\" id=\"".$id."\" class=\"".$class."\" ".$javascript." value=\"".$label."\"  ".$disabled." >";
 	 break;
 	 case "button":
 	 	$button = "<button name=\"".$id."\" id=\"".$id."\" class=\"".$class."\" ".$javascript."  ".$disabled." > ".$label."</button>";
 	 break;
 	 default:
 	 	$button = "";
 	}//switch
 	
 	return $button;
 }//function form_button

 
 static public function form_select($select_name,$table,$value_column,$text_column,$sql_where='',$default_value='',$options='',$show_value_column='',$ordercolumn=''){
    global $db;
    
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $select_name = "{$select_name}_".MNUID; 
     }
    }

   $sql_where =  trim($sql_where);

   $cachesecs  = !empty($cachesecs) && $cachesecs> 0 ? $cachesecs : null;

   $select_columns = array();

   if(!empty($value_column))
   {
    $select_columns[] = $value_column;
   }

   if(!empty($text_column))
   {
    $select_columns[] = $text_column;
   }

   if(!empty($ordercolumn) && $ordercolumn!=$text_column && $ordercolumn!=$value_column){
    $select_columns[] = $ordercolumn;
   }

   $select_columns = array_unique($select_columns);

   $sql_columns = join(',', $select_columns);

    $select_sql  = "SELECT DISTINCT {$sql_columns} FROM {$table} ";

   $select_sql .= isset($sql_where) && !empty($sql_where)?" WHERE ".$sql_where:'';

   if(!empty($ordercolumn)){
     $select_sql .= " ORDER BY ".$ordercolumn;
   }else{
	   if(!empty($value_column)){
	   	$select_sql .= " ORDER BY ".$value_column;
	   }else if(!empty($text_column)){
	    $select_sql .= " ORDER BY ".$text_column;
	   }
   }

   try  {

    $select_record_set = $db->GetArray($select_sql);
   
   $select  = "<select name=\"".$select_name."\" id=\"".$select_name."\" ".$options.">";
   $select .= "<option value=\"\">[select one]</option>";
    if($select_record_set && sizeof($select_record_set)>0){

     foreach ($select_record_set as $select_record){

      $value = $select_record[$value_column];
      
      if (isset($text_column) && !empty($text_column)) {
      $text  = $select_record[$text_column];
      }

      $select .= "<option value=\"".$value."\"";
      $select .= isset($default_value) && !empty($default_value) && trim($default_value)==trim($value)?' selected':'';
      $select .= " >";
      $select .= isset($show_value_column)&&$show_value_column==true?$value:'';
      $select .= isset($show_value_column)&&$show_value_column==true?' : ':'';
      $select .= !empty($text) ? $text : $value;
      $select .= "</option>";

     }//foreach
    }
   $return = $select .= "</select>";
  }catch (Exception $e){
   $return = $e->getMessage();
  }
  return self::compress_html($return);

 }

 /**
  * make a form drop-down from an Array.
  *
  * @param string $select_name
  * @param array $array
  * @param string $default_value
  * @param string $options
  * @param boolean $show_value_column
  * @param int $width
  * @param string $use_filter_after_records
  * @param boolean $force_use_filter
  * @return unknown
  */
  
 static public function form_select_fromArray($select_name,$array= array(),$default_value='',$options='',$show_value_column='' , $width='100', $use_filter_after_records=50 , $force_use_filter=false)
{
	
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $select_name = "{$select_name}_".MNUID; 
     }
    }
    
     $width = str_replace('px','', $width);

	 $numOptions = count($array);
	 $select   = "<select name=\"".$select_name."\" id=\"".$select_name."\"  style=\"width:{$width}px\" ".$options.">";
     $select_options = "<option value=\"\">... </option>";

     if($numOptions>0){

       foreach ($array as $k=>$v){

       	 $value =  !is_array($k) ? trim($k) : null;
       	 $text  =  !is_array($v) ? trim($v) : null;

          $select_options .= "<option value=\"".$value."\"";
          $select_options .= isset($default_value) && trim($default_value)==trim($value)?' selected':'';
          $select_options .= " >";
          $select_options .= isset($show_value_column)&&$show_value_column==true? $value :'';
          $select_options .= isset($show_value_column)&&$show_value_column==true?' : ':'';
          $select_options .= !is_null($text) ? $text : $value;
          $select_options .= "</option>";

       }
     }
     
      $select .= "{$select_options}</select>";
      return self::compress_html($select);

}


 public function loadjs($name, $cache = true , $park = true){
 	 return self::get_cached_js_file($name, $cache, $park);
 }

 public function get_cached_js_file($jsfile){
 	global $root_path;


if(!function_exists('cacheFileName')){
 require_once($root_path.'core/functions.filecache.php');
}

$jsfile_fullname = "js/".$jsfile.'.js';

    ob_start();
    echo "<!--Expires ".date('jS F Y H:i', time()+CACHE_TIME)."-->";


  echo "<script type=\"text/javascript\" src=\"".STATIC_CONTENT_URL."".$jsfile_fullname."?version=".mt_rand()."\"></script> \n";

 ob_end_flush();

}//function get_js_file

 public  function  loadjs_combined( $jsfiles = array()  , $refresh = false)
 {
 	global $root_path;

 	$unique_filename = '';

 	if(count($jsfiles)<0) return ;

 	 foreach ($jsfiles as $index=>$jsfile)
 	 {
 	  if(isset($jsfile['name']))
 	  {
 	   $unique_filename  .= $jsfile['name'];
 	  }
 	 }

    $filename_md5     = md5($unique_filename);

    $filename_combined  =  $root_path.'cache/'.$filename_md5.'.js';

    if($refresh && is_readable($filename_combined)) unlink($filename_combined);

 	if(!is_readable($filename_combined))
 	{

     $output = '';

 	 foreach ($jsfiles as $index=>$jsfile_properties)
   	  {

 	  $jsfile    = isset($jsfile_properties['name']) ? $jsfile_properties['name'] : '';
 	  if(!empty($jsfile))
 	  {
 	  $compress  = isset($jsfile_properties['compress']) ? $jsfile_properties['compress'] : '';
 	  $pack      = isset($jsfile_properties['pack']) ? $jsfile_properties['pack'] : '';

 	  $jsfile =   ROOT_PATH.'js/'.$jsfile.'.js';

 		if(is_readable($jsfile))
 		{
 			$contents = file_get_contents($jsfile);

           if($compress)
           {
 			 $parker  = new jspacker($contents, $pack ? 62 : 0  );
 			 $output .= PHP_EOL;
 			 $output .= $parker->pack();
           }else{
           	 $output .= $contents;
           }

 		}
 	   }
 	  }//foreach

 	  if(!file_put_contents($filename_combined, $output))
 	  {
 	  	die("Unable to create Javascript Cache Files. Make sure it has httpd write rights");
 	  }

 	}

    ob_start();
    echo "<!--Expires ".date('jS F Y H:i', time()+CACHE_TIME)."-->";
    echo "<script type=\"text/javascript\" src=\"".STATIC_CONTENT_URL."cache/".$filename_md5.".js\"></script> \n";
    ob_end_flush();

 }

 public function importCss($name){
// 	return self::get_cached_css_file($name);

 	global $root_path;
 	echo "<style type=text/css>@import url( ".$root_path."themes/default/".$name.".css?version=1);</style> \n";

 }

 public function get_cached_css_file($css_file){
 	global $root_path;

$css_file_fullname = 'themes/default/'.$css_file.'.css';



    ob_start();
    echo "<!--Expires ".date('jS F Y H:i', time()+CACHE_TIME)."-->";

  echo "<style type=text/css>@import url( ".STATIC_CONTENT_URL.$css_file_fullname."?version=".mt_rand().");</style> \n";


 ob_end_flush();


}//function get_cached_css_file

 public function loadcss_combined($cssfiles = array() , $force_reload = false )
 {
 	global $root_path;

 	if(count($cssfiles)<0) return ;

      $unique_filename  = '/';
      $search           = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
      $replace          = array('>','<','\\1');

 	 foreach ($cssfiles as $index=>$cssfile)
 	 {
 	  if(isset($cssfile['name'])){
 	   $unique_filename  .= $cssfile['name'];
 	  }
 	 }

    $filename_md5       = md5($unique_filename);

    $filename_combined  =  $root_path.'cache/'.$filename_md5.'.css';

    $output =  "/******Cached ".date('jS F Y H:i')."*****/\r\n";

    if(!is_readable($filename_combined) || $force_reload)
    {
 	 foreach ($cssfiles as $index=>$cssfile_properties)
   	 {
   	  if(isset($cssfile_properties['name']))
   	  {

 	  $cssfile_name    = $cssfile_properties['name'];
 	  $compress        = $cssfile_properties['compress'];

 	  $cssfile         =  ROOT_PATH.'themes/'.$cssfile_name.'.css';

 		if(is_readable($cssfile) )
 		{
 			$contents = file_get_contents($cssfile);

           if($compress)
           {
 			 $output .= PHP_EOL;

             $output  .= preg_replace($search, $replace, $contents);

           }else{
           	 $output .= $contents;
           }

 		}else{
 			print ("Unable to load stylesheet {$cssfile} ");
 		}
   	   }
 	  }//foreach

 	  if(!file_put_contents($filename_combined, $output))
 	  {
 	  	die("Unable to create CSS Cache Files. Make sure it has httpd write rights");
 	  }

    }

    ob_start();
    echo "<!--Expires ".date('jS F Y H:i', time()+CACHE_TIME)."-->";

    echo "<style type=text/css>@import url( ".STATIC_CONTENT_URL."cache/".$filename_md5.".css);</style> \n";

    ob_end_flush();

 }

 public function icon($name) {
 	return self::sprite($name);
 }

 /**
  * use Combined  background icons into a single image , use the CSS background-image (gui/sprites.silk-sprite.png)
  *  and background-position properties to display the desired image segment
  * @param string $name
  * @param boolean $hover_effect
  * @return unknown
  */
 public function sprite($name , $hover_effect = false) {
 	 $hover  = $hover_effect? 'hover' : '';
 	 $name   = str_replace('.png', '', $name); 
 	 $name   = str_replace('_', '-', $name);
 	 
 	return "<span class=\"ui-silk inline {$hover} ui-silk-{$name}\"></span>";
 }

 public function get_cached_icon( $icon_name, $dimesions='', $align='', $id='', $ob_start_flush=false, $options=''){
 	global $root_path;

if(!function_exists('cacheFileName')){
 require_once($root_path.'core/functions.filecache.php');
}

//--------------------------check cache file--------------------------------
$icon_name_fullname = 'gui/icons/'.$icon_name;


$cachefile  = cacheFileName($icon_name_fullname);


if(getCache( $cachefile , CACHE_TIME)) {
	 return file_get_contents($cachefile);

}else{
    if ($ob_start_flush) {
    	ob_start();
    }

  $icon_file        = ($root_path.'gui/icons/'.$icon_name);
  if(file_exists($icon_file)){
  $icon_info        = getimagesize($root_path.$icon_name_fullname);

  if(is_array($dimesions)){
   $icon_dimensions =" width=\"{$dimesions[0]}\" height=\"{$dimesions[1]}\" ";
  }else{
   $icon_dimensions = ($icon_info[3]);
  }
   return "<img src=\"".STATIC_CONTENT_URL.$icon_name_fullname."\" id=\"".$id."\" {$icon_dimensions} align=\"".$align."\" border=\"0\" {$options} >";
  }else{
  	return null;
  }


 if($ob_start_flush){
 $contents = ob_get_contents();
 }
 createCache( $contents , $cachefile );

}//else no cache file

}//function get_cached_icon

 public function get_cached_anim_icon($icon_name,$dimesions='',$align='',$id='',$ob_start_flush=false){
 	global $root_path;


if(!function_exists('cacheFileName')){
 require_once($root_path.'core/functions.filecache.php');
}

$icon_name_fullname = 'gui/indicators/'.$icon_name;
$cachefile  = cacheFileName($icon_name_fullname);

if(getCache( $cachefile , CACHE_TIME)) {
	 return file_get_contents($cachefile);
}else{
    if ($ob_start_flush) {
    	ob_start();
    }
  $icon_file        = ($root_path.'gui/indicators/'.$icon_name);
  if(file_exists($icon_file)){
  $icon_info        = getimagesize($root_path.$icon_name_fullname);

  if(is_array($dimesions)){
   $icon_dimensions =" width=\"{$dimesions[0]}\" height=\"{$dimesions[1]}\" ";
  }else{
   $icon_dimensions = ($icon_info[3]);
  }

   return "<img src=\"".STATIC_CONTENT_URL.$icon_name_fullname."\" id=\"".$id."\" {$icon_dimensions} align=\"".$align."\" border=\"0\" >";
  }else{
  	return null;
  }
 if($ob_start_flush){
 $contents = ob_get_contents();
 }
 createCache( $contents , $cachefile );
}//else no cache file

}//function get_cached_icon

 static public function href($onclick,$text,$title='',$id='',$class=''){
    $link_id = mt_rand();
	$href ="<a href=\"javascript:;\" ";
	$href .= " id=\"l_{$link_id}\" ";
	$href.= " onclick=\"{$onclick}\" ";
	$href.= " title=\"".$title."\"  class=\"{$class}\" >";
	$href.=  $text;
	$href.="</a>";
	return $href;
 }


 static  public function year_select($name,$cur_year,$end_year,$selected_year='',$options='',$fiscal=true){
	 
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $name = "{$name}_".MNUID; 
     }
    }
  
  	$return = "<select name=\"{$name}\"  id=\"{$name}\" {$options} >";
  	$return .= "<option value=\"\">[Year]</option>";
  	 while ($cur_year < $end_year){
  	 	$return .= "<option value=\"{$cur_year}\"";
  	 	$return .= $cur_year==$selected_year? ' selected' : '';

  	 	if($fiscal){
   	 	 $return .= ">{$cur_year}/".($cur_year+1);
  	 	}else{
  	 	 $return .= ">{$cur_year}";
  	 	}

  	 	$return .= "</option>";
  	  ++$cur_year;
  	 }
  	$return .= "</select>";
  	return $return;
 }

 static public function month_select($name,$selected_month='',$format='text'){
    
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $name = "{$name}_".MNUID; 
     }
    }
  
 	$months = array(
 	 1=>'Jan',
 	 2=>'Feb',
 	 3=>'Mar',
 	 4=>'Apr',
 	 5=>'May',
 	 6=>'Jun',
 	 7=>'Jul',
 	 8=>'Aug',
 	 9=>'Sep',
 	 10=>'Oct',
 	 11=>'Nov',
 	 12=>'Dec'
 	);

  	$return  = "<select name=\"{$name}\"  id=\"{$name}\" >";
  	$return .= "<option value=\"\">[Month]</option>";
  	$cur_month = 1;
  	$end_month = 12;
  	 while ($cur_month <= $end_month){
  	 	$return .= "<option value=\"{$cur_month}\"";
  	 	$return .= $cur_month==$selected_month? ' selected' : '';
  	 	$return .= ">";
  	 	$return .= $format=='text' && isset($months[$cur_month])? $months[$cur_month] : $cur_month;
  	 	$return .= "</option>";
  	  ++$cur_month;
  	 }
  	$return .= "</select>";
  	return $return;
 }

 /**
  * months drop down
  *
  * @deprecated See form class
  * @param string $name
  * @param int $selected_day
  * @return string
  * @see form::month_days_select
  */
 public function month_days_select($name,$selected_day){
  	$return = "<select name=\"{$name}\"  id=\"{$name}\" >";
  	$return .= "<option value=\"\">[day]</option>";
  	$cur_day = 1;
  	$end_day = 31;
  	 while ($cur_day <= $end_day){
  	 	$return .= "<option value=\"{$cur_day}\"";
  	 	$return .= $cur_day==$selected_day? ' selected' : '';
  	 	$return .= ">{$cur_day}";
  	 	$return .= "</option>";
  	  ++$cur_day;
  	 }
  	$return .= "</select>";
  	return $return;
 }
 
 
  public function calendar_combo($name, $selected_date='', $options='', $default_today_date=false) {
 	
 	if(!empty($selected_date)){
 		$selected_year   = date("Y",strtotime($selected_date));
 		$selected_month  = date("m",strtotime($selected_date));
 		$selected_day    = date("d",strtotime($selected_date));
 	}else{
 		$selected_year   =  $default_today_date ? date("Y") :  date("Y");
 		$selected_month  =  $default_today_date ? date("m") :  '';
 		$selected_day    =  $default_today_date ? date("d") :  '';
 	}
 	
 	$year   = self::year_select($name.'_year',date('Y'),date('Y')+2,$selected_year,$options,false);
 	$month  = self::month_select($name.'_month', $selected_month, 'text', $options);
 	$day    = self::month_days_select($name.'_day', $selected_day, $options);
 	
 	return  $day.'-'.$month.'-'.$year;
 	
 }
 
 public static function numbers_select($name, $startno=1, $endno=10 , $step=1 , $selected_no='' , $postfix_label='' , $options='' , $width=100, $use_filter_after_records=50, $force_use_filter= false, $help_text=''){
 	    
 	     if($startno>$endno) return "gui::numbers_select({$name}... End Number {$endno} must be greator than then Starting number".$startno;
 	     
 	     
 	     
 	     $numbers_array  = array();
 	     $range          = range($startno, $endno, $step);
 	 	 
 	 	 foreach ($range as $number)
 	 	 {
 	 	  if($number==1){
 	 	   $postfix_label_s         = depluralize($postfix_label);
 	 	    $numbers_array[$number] = !empty($postfix_label_s) ? "{$number} {$postfix_label_s}" : $number;
 	 	  }else{
 	 	    $numbers_array[$number] = !empty($postfix_label) ? "{$number} {$postfix_label}" : $number;
 	 	  }
 	 	 }
 	 	 
 	 	if(count($numbers_array)>0){ 
 	 		$help_text = isset($help_text) && strlen($help_text)>2 ? self::form_help($help_text) : '';
 	 	    return self::form_select_fromArray($name, $numbers_array, $selected_no, $options , false , $width, $use_filter_after_records, $force_use_filter=false ).$help_text;
 	 	}else{
 	 		return "gui::numbers_select({$name}... the numbers array is empty";
 	 	}
 }

}

?>
