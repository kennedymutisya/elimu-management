<?php


 class menu {
 	private $_appid;
 	private $_modid;
 	private $menu_parents = array();
 	private $menu_children = array();
 	private $menu_saved = array();
 	
	public function __construct( $appid , $modid){
		$this->_appid = $appid;
		$this->_modid = $modid;
	}
	
	public function list_menu(){
		global $db;
		
     $user         = new user();
	 $modulename   = $db->GetOne("SELECT MODNAME FROM SYSMODS WHERE APPID='{$this->_appid}' AND MODID='{$this->_modid}' order by MODPOS ");
	 
	 if(strtolower($user->userid)=='admin'){
	  $menu         = $db->GetAssoc("SELECT MNUID,MNUNAME,PARENTID,MODPATH,MODKEY,MODTYPE,ATTRIBS,POSITION FROM SYSMODMNU WHERE APPID='{$this->_appid}' AND MODID='{$this->_modid}' AND ACTIVE=1 ORDER BY POSITION ASC ");
	 }else{
	  $menu         = $db->GetAssoc("
	  SELECT MNUID,MNUNAME,PARENTID,MODPATH,MODKEY,MODTYPE,ATTRIBS,POSITION FROM SYSMODMNU 
	  WHERE APPID='{$this->_appid}' 
	  AND MODID='{$this->_modid}'
	  AND ACTIVE=1
	  AND MNUID IN (
	     SELECT MNUID FROM SYSMNUAUTH WHERE GROUPCODE='{$user->groupcode}' AND  APPID='{$this->_appid}' AND MODID='{$this->_modid}' 
	     )
	  ORDER BY POSITION ASC
	   ");
	 }
	 
	 $num_parents  = array();
	 
	 if(count($menu)>0){
	  foreach ($menu as $mnuid=>$menuitem){
 	   
 	   	$mnuname   =  valueof($menuitem, 'MNUNAME');
 	   	$modpath   =  valueof($menuitem, 'MODPATH');
 	   	$modkey    =  valueof($menuitem, 'MODKEY');
 	   	$parentid  =  valueof($menuitem, 'PARENTID');
 	   	$modtype   =  valueof($menuitem, 'MODTYPE');
 	   	$attribs   =  valueof($menuitem, 'ATTRIBS');
 	   	$attribsd  = json_decode($attribs);
 	   	$attributes  = array();
 	   	
 	   	if(sizeof($attribsd)>0){
 	   	 foreach($attribsd as $k=>$v){
		  $attributes[$k] = $v;
		 }
		}
 	   	
 	   	if(empty($parentid)){
 	   	 $this->menu_parents[$mnuid]  = array($mnuname, $modpath, $modkey, $modtype ,$attributes);
 	   	}else{
 	   	 $this->menu_children[$parentid][$mnuid] = array($mnuname, $modpath, $modkey, $modtype ,$attributes);	
        }
 	   	
      }
     }
  
      $num_parents = count($this->menu_parents);
      
      $return =  "[";
      
     if($num_parents>0)
     {
     	$count = 1;
     	
     	 foreach ($this->menu_parents as $parentid=>$parent){
     	 	
     	   $parentname = valueof($parent,0);
     	   $modpath    = valueof($parent,1);
     	   $modkey     = valueof($modkey,2);
     	   $modtype    = valueof($modkey,3);
     	   $attributes = valueof($modkey,4);
     	   
     	    $return .=  "{\r\n";
     	     $return .=  "\"id\":{$parentid},\r\n";
     	     $return .= "\"text\": \"{$parentname}\",\r\n";
     	     $return .= "\"state\":\"closed\", \r\n";
     	     
     	    //children
     	    if(array_key_exists($parentid, $this->menu_children)){

     	    	$return .= "\"children\":[";
     	         $return .= self::list_menu_children($parentid);
     	        $return .= "]";
     	        
     	     }else{
     	     	
			  $vars['appid']    = $this->_appid;
			  $vars['modid']    = $this->_modid;
			  $vars['mnuid']    = $parentid;
			  $vars['modpath']  = $modpath;
			  $vars['modkey']   = $modkey;
			  $vars_str         = packvars($vars);
				 	
     	      $return .=  "\"attributes\":{ \"modvars\":\"{$vars_str}\", \"modkey\":\"{$modkey}\", \"modtype\":\"{$modtype}\",\"win\":\"".valueof($attributes,'win')."\",\"hei\":\"".valueof($attributes,'hei')."\",\"min\":\"".valueof($attributes,'min')."\",\"max\":\"".valueof($attributes,'max')."\",\"col\":\"".valueof($attributes,'col')."\",\"clo\":\"".valueof($attributes,'clo')."\",\"res\":\"".valueof($attributes,'res')."\",\"bta\":\"".valueof($attributes,'bta')."\",\"bte\":\"".valueof($attributes,'bte')."\",\"btd\":\"".valueof($attributes,'btd')."\",\"bti\":\"".valueof($attributes,'bti')."\",\"btx\":\"".valueof($attributes,'btx')."\"}";
     	     
             }
     	   
     	    $return .=  "}";
     	   
     	   if($count<$num_parents) {
     	   	 $return .=  ",\r\n";
     	   }
     	   
     	   ++$count;
         }
         
       }
       
     $return .=  "]\r\n";
     
     return $return;
     
	}
	
	public function list_menu_children( $parentid ){
		
      $num_children = count( $this->menu_children[$parentid] );
      
     if($num_children>0){
     	$count = 1;
     	$tree = "";
     	
     	 foreach ($this->menu_children[$parentid] as $childid=>$child){
     	 	
     	   //$childname      = '<i class=\"fa fa-edit\"></i>&nbsp;'.valueof($child,0);
     	   $childname      = valueof($child,0);
     	   $modpath        = valueof($child,1);
     	   $modkey         = valueof($child,2);
     	   $modtype        = valueof($child,3);
     	   $attributes     = valueof($child,4);
     	   
     	    if(!array_key_exists($childid, $this->menu_children)){
     	     $state = 'open';	
     	    }else{
     	     $state = 'closed';	
     	    }
     	    
		   $vars['appid']    = $this->_appid;
		   $vars['modid']    = $this->_modid;
		   $vars['mnuid']    = $childid;
		   $vars['modpath']  = $modpath;
		   $vars['modkey']   = $modkey;
		   $vars_str         = packvars($vars);
     	      
     	   $tree .= "{\r\n";
     	    $tree .=  "\"id\":{$childid},\r\n";
     	    $tree .=  "\"text\":\"{$childname}\",\r\n";
     	    $tree .= "\"state\":\"{$state}\", \r\n";
     	    //$tree .= "\"iconCls\":\"\", \r\n";
     	    $tree .=  "\"attributes\":{ \"modvars\":\"{$vars_str}\", \"modkey\":\"{$modkey}\", \"modtype\":\"{$modtype}\",\"win\":\"".valueof($attributes,'win')."\",\"hei\":\"".valueof($attributes,'hei')."\",\"min\":\"".valueof($attributes,'min')."\",\"max\":\"".valueof($attributes,'max')."\",\"col\":\"".valueof($attributes,'col')."\",\"clo\":\"".valueof($attributes,'clo')."\",\"res\":\"".valueof($attributes,'res')."\",\"bta\":\"".valueof($attributes,'bta')."\",\"bte\":\"".valueof($attributes,'bte')."\",\"btd\":\"".valueof($attributes,'btd')."\",\"bti\":\"".valueof($attributes,'bti')."\",\"btx\":\"".valueof($attributes,'btx')."\"}";
     	    
     	    if(array_key_exists($childid, $this->menu_children)){
     	    	$tree .= ",\r\n";
     	    	$tree .=  "\"children\":[\r\n";
     	         $tree .=  self::list_menu_children( $childid );
     	        $tree .=  "]\r\n";
     	    }
     	    
     	     $tree .=  "}";
     	   
     	   if($count<$num_children) {
     	   	$tree .= ",\r\n";
     	   }
     	   
     	   ++$count;
         }
         
      }
      
     return $tree;
     
	}
	
 }
 
