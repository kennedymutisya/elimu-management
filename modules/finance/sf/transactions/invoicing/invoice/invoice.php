<?php

/**
* auto created config file for /modules/finance/sf/transactions/invoicing/invoice
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-06 17:08:32
*/

  class invoice{
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function list_items(){
		global $db;
        //$db->debug=1;//remove 
		//print_pre($_POST);//remove 
		
    $formcode     = filter_input(INPUT_POST , ui::fi('form')); 
    $yearcode     = filter_input(INPUT_POST , ui::fi('year')); 
    $termcode     = filter_input(INPUT_POST , ui::fi('term'));
    
    if(strlen($formcode)>1){
    $formcode = substr($formcode,0,1);
    }
   
    $records = $db->GetAssoc("
    SELECT F.FITEMCODE ,F.ID, I.FITEMNAME , F.AMOUNT
     FROM FINFSTR F
     INNER JOIN FINITEMS I ON I.FITEMCODE = F.FITEMCODE
     WHERE F.YEARCODE='{$yearcode}' 
     AND F.TERMCODE='{$termcode}' 
     AND F.FORMCODE='{$formcode}' 
    ");
    //AND (F.ACTIVE=1)
    
    echo "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"easyui-datagrid\">";
    
     if(count($records)>0){
		
		 echo "<tr>";
		  echo "<td colspan=\"2\"><b>Item</b></td><td><b>Amount</b></td><td>&nbsp;</td>";
		 echo "</tr>";
		 
		 $total = 0;
		 
		 foreach ($records as $itemcode=>$item){
		 	
		  $amount       = $item['AMOUNT'];
		  $checkbox_id  = "items[{$itemcode}]";
		  $checkbox     = ui::form_checkbox($checkbox_id, 1 , '', '', $amount);
		 	
		  echo "<tr>";
		   echo "<td>{$checkbox}</td><td>{$item['FITEMNAME']}</td><td>{$item['AMOUNT']}</td><td></td>";
		  echo "</tr>";
		  
		  $total += $item['AMOUNT'];
		 }
		 
		 echo "<tr>";
		  echo "<td colspan=\"2\" align=\"right\">Total</td>";
		  echo "<td colspan=\"2\"><b>".number_format($total,2)."</b></td>";
		 echo "</tr>";
		 
     }else{
		 echo "<tr>";
		  echo "<td colspan=\"4\">No Items</td>";
		 echo "</tr>";
     }
		
     echo "</table>";
     
	}
	
	public function list_students(){
		global $db,$cfg;
        //$db->debug=1;//remove 
		//print_pre($_POST);//remove 
		
    $form_stream_code  = filter_input(INPUT_POST , ui::fi('form')); 
    $yearcode          = filter_input(INPUT_POST , ui::fi('year')); 
    $termcode          = filter_input(INPUT_POST , ui::fi('term'));
      
    if(strlen($form_stream_code)==1){
     $form_stream = 'FORMCODE';
    }else{
     $form_stream = 'STREAMCODE';
    }

    $records = $db->GetAssoc("
    SELECT ADMNO,FULLNAME,ACCOUNTNO,STREAMCODE
     FROM VIEWSP 
     WHERE {$form_stream}='{$form_stream_code}' 
     order by ADMNO
    ");

    echo "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"easyui-datagrid\">";
    
     if(count($records)>0){
		
		 $checkbox_toggle     = ui::form_checkbox('inv_checkall', 1 , "onchange=\"{$cfg['appname']}.toggle_student();\"", 'students', 1);
		 
		 echo "<tr>";
		  echo "<td colspan=\"2\">{$checkbox_toggle} <label for=\"".ui::fi('inv_checkall')."\">Select All/None </label></td><td>&nbsp;</td><td><b>&nbsp;</b></td>";
		 echo "</tr>";
		
		 echo "<tr>";
		  echo "<td colspan=\"2\"><b>Student Number</b></td><td><b>Full Name</b></td><td><b>Stream</b></td>";
		 echo "</tr>";
		 
		 $total = 0;
		 
		 foreach ($records as $admno=>$student){
		 	
          $accountno    = valueof($student,'ACCOUNTNO');
		  $streamcode   = valueof($student,'STREAMCODE');
		  $checkbox_id  = "students[{$admno}]";
		  $checkbox     = ui::form_checkbox($checkbox_id, 1 , '', 'inv_students', 1);
		 	
		  echo "<tr>";
		   echo "<td width=\"30px\">{$checkbox}</td><td><label for=\"{$checkbox_id}\">{$admno}</label></td><td><label for=\"{$checkbox_id}\">{$student['FULLNAME']}</label></td><td>{$streamcode}</td>";
		  echo "</tr>";
		  
		 }
		 
		 echo "<tr>";
		  echo "<td colspan=\"4\"><hr></td>";
		 echo "</tr>";
		 
		 echo "<tr>";
		  echo "<td colspan=\"2\" align=\"right\">Total</td>";
		  echo "<td colspan=\"2\"><b>".number_format( count($records))."</b></td>";
		 echo "</tr>";
		 
     }else{
     	
		 echo "<tr>";
		  echo "<td colspan=\"4\">No students</td>";
		 echo "</tr>";
     }
		
     echo "</table>";
     
	}
	
	public function save(){
		global $db,$cfg;
		
	$invdesc            = filter_input(INPUT_POST ,  ui::fi('invdesc'));
    $invdate            = filter_input(INPUT_POST ,  ui::fi('invdate'));
    $yearcode           = filter_input(INPUT_POST ,  ui::fi('year')); 
    $termcode           = filter_input(INPUT_POST ,  ui::fi('term'));
    $form_stream_code   = filter_input(INPUT_POST ,  ui::fi('form')); 

    if(strlen($form_stream_code)==1){
     $form_stream = 'FORMCODE';
     $formcode    = $form_stream_code;
    }else{
     $form_stream = 'STREAMCODE';
     $formcode    = substr($form_stream_code,0,1);
    }

    $invdesc      = "FEES PAYABLE :Y{$yearcode} F{$formcode} T{$termcode}";
    $students     = isset($_POST['students']) && is_array($_POST['students']) && sizeof($_POST['students'])>0 ? $_POST['students'] : array(); 

    $fyear        = self::get_fyear();
    $user         = new user();
       
    $students_invoiced       = 0;
      
    $records = $db->GetAssoc("
    SELECT ADMNO,FULLNAME,ACCOUNTNO,STREAMCODE
     FROM VIEWSP 
     WHERE {$form_stream}='{$form_stream_code}' 
     order by ADMNO
    ");
    
      $items = $db->CacheGetAssoc(1200,"SELECT FITEMCODE ,FITEMNAME FROM FINITEMS ");
    
      $num_students = count($students);
      
      $inv_header= new ADODB_Active_Record('FININVH', array('YEARCODE','TERMCODE','FORMCODE'));
    
      $inv_header->Load("
      YEARCODE='{$yearcode}' 
      AND TERMCODE='{$termcode}' 
      AND FORMCODE='{$formcode}' 
     ");
      
     $inv_header_saved = true;
    
    if(empty($inv_header->_original)){
     $inv_header->id           = generateID( $inv_header->_tableat );// 	
     $inv_header->invdesc      = $invdesc;
     $inv_header->invdate      = $invdate;
     $inv_header->yearcode     = $yearcode;
     $inv_header->termcode     = $termcode;
     $inv_header->formcode     = $formcode;
     $inv_header->auditdate    = date('Y-m-d');
     $inv_header->audittime    = time();
     $inv_header->audituser    = $user->userid;
      
     if(!$inv_header->Save()){
     	$inv_header_saved = false;
     }
     
    }
    
    if(!$inv_header_saved){
    	return json_response(0,'saving of invoice header failed');
    }
    
    if(isset($_POST['students']) && isset($_POST['items']) ){
    	if(count($_POST['students'])>0 && count($_POST['items'])>0){
    		foreach ($_POST['students'] as $admno=>$state){
    		   foreach ($_POST['items'] as $itemcode => $amount){
                $inv_detail_line= new ADODB_Active_Record('FININVD', array('YEARCODE','TERMCODE','FORMCODE','ADMNO','FITEMCODE'));
    
                $accountno   = valueof($students , $admno , $admno);
                $itemname    = valueof($items , $itemcode , $itemcode);
                
                $inv_detail_line->Load("
                YEARCODE='{$yearcode}' 
                AND TERMCODE='{$termcode}' 
                AND FORMCODE='{$formcode}' 
                AND ADMNO='{$admno}' 
                AND FITEMCODE='{$itemcode}' 
               ");
      
               $inv_detail_line_saved = true;
    
                if(empty($inv_detail_line->_original)){
                 	
                 $inv_detail_line->id           = generateID('FININVD');// 	
                 $inv_detail_line->headerid     = $inv_header->id;
                 $inv_detail_line->entrytype    = 'INV';
                 $inv_detail_line->invno        = self::get_inv_number( $inv_detail_line->entrytype ); //admno
                 $inv_detail_line->invdesc      = $invdesc;
                 $inv_detail_line->invdate      = $invdate;
                 $inv_detail_line->yearcode     = $yearcode;
                 $inv_detail_line->termcode     = $termcode;
                 $inv_detail_line->formcode     = $formcode;
                 
                 $inv_detail_line->admno        = $admno;
                 
                 $inv_detail_line->fitemcode    = $itemcode;
                 $inv_detail_line->fitemname    = $itemname;
                 $inv_detail_line->amount       = $amount;
                 
                 $inv_detail_line->fyrprcode    = $fyear;
                 $inv_detail_line->crcode       = 'KES';
     
                 $inv_detail_line->auditdate    = date('Y-m-d');
                 $inv_detail_line->audittime    = time();
                 $inv_detail_line->audituser    = $user->userid;

      // print_pre($inv_detail_line);
      
                 if(!$inv_detail_line->Save()){
     	            $inv_detail_line_saved = false;
                 }
     
                 if($inv_detail_line_saved){
//                 	++$students_invoiced;
                 }
                 
               }//if not invoiced yet
               // exit;
              }//each item
              ++$students_invoiced;
    		}//each student
    	}
    }
    
    if($inv_detail_line_saved){
     return json_response(1,"Invoiced {$students_invoiced} of {$num_students} Students");
    }else{
     return json_response(0,'Invoicing Failed');
    }

  }
	
    private function get_fyear(){
  	 
 	  $periods      = array();
 	  $periods[7] = 1;
 	  $periods[8] = 2;
 	  $periods[9] = 3;
 	  $periods[10] = 4;
 	  $periods[11] = 5;
 	  $periods[12] = 6;
 	  $periods[1] = 7;
 	  $periods[2] = 8;
 	  $periods[3] = 9;
 	  $periods[4] = 10;
 	  $periods[5] = 11;
 	  $periods[6] = 12;
 	 
      $year        =  date('Y');
  	  $year_prev   =  date('Y')-1;
  	  $year_next   =  date('Y')+1;
  	  
 	  $month  =  date('n');
 	  $day	  =  date('d');
 	  $fiscal_period =  $periods[$month];
 	  
      if($month>=7){
 	  	$fiscal_year   = "{$year_next}";
 	  }else{
 	  	$fiscal_year  = "{$year}";
 	  }
 	  
	  // echo "\$month={$month} <br>";
	  // echo "\$day={$day} <br>";
	  // echo "\$fiscal_period={$fiscal_period} <br>";
	  // echo "\$fiscal_year={$fiscal_year} <br>";
 	  
      return  	$fiscal_year .'-' . $fiscal_period;
        
  }
    
    private function get_inv_number($type){
    	global $db;
    	
    	$appno = $db->GetOne("SELECT MAX(NXT{$type})  FROM FINDOCS ");
    	
        if(empty($appno)){
          if($db->Execute("INSERT INTO FINDOCS (NXT{$type}) VALUES(1)")){
          	$appno = 1;
          }
        }
        
        $appno_new = $appno+1;
        
		  $db->Execute("UPDATE FINDOCS SET NXT{$type}={$appno_new}");

	    return   generateUniqueCode($type,$appno,6);
	    
    }
    
    public function delete(){
		global $db, $cfg;
		
		$id  = filter_input(INPUT_POST , 'id');
		$del = $db->Execute("UPDATE FINFSTR SET ACTIVE=0 WHERE ID={$id}");
		
		return  self::list_items();
		
		if($del){
		 json_response(1,'deleted');
		}else{
		  json_response(0,'failed');	
		}
		
	}
	
	
}
