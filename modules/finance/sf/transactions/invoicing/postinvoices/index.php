<?php

/**
* auto created index file for /modules/finance/sf/transactions/invoicing/postinvoices
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-06 17:07:14
*/


 $batches  = $db->GetAssoc("
 SELECT H.ID, H.INVDESC, H.INVDATE, COUNT(DISTINCT(D.ADMNO)) NUM , SUM(D.AMOUNT) AMOUNT
 FROM FININVH H
 inner join FININVD D on D.YEARCODE = H.YEARCODE AND D.TERMCODE = H.TERMCODE AND D.FORMCODE = H.FORMCODE  
 WHERE  (D.POSTED IS NULL OR  D.POSTED=0)
 GROUP BY H.ID, H.INVDESC, H.INVDATE
 ");
 
 $arr = array();
 
 if(count($batches)){
 	foreach ($batches as $batchid=> $batch){
 		$arr[$batchid]  = $batch['INVDESC'] .' ('. $batch['INVDATE'] .') '. $batch['NUM'] .' students' ;
 	}
 }
?>
<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		
<table cellpadding="2" cellspacing="0" width="100%">

	<tr>
		<td  colspan="3">&nbsp;</td>
	</tr>
	
	<tr>
		<td  width="180px">Invoice:</td>
		<td  colspan="2"><div id="<?php echo ui::fi('div_invoices'); ?>"><?php echo ui::form_select_fromArray('batchid', $arr , '' , "",'', 400); ?></div></td>
	</tr>
	
	<tr>
		<td  colspan="3">&nbsp;</td>
	</tr>
	
   <tr>
		<td >&nbsp;</td>
		<td colspan="2">
		<a href="javascript:void(0)" class="easyui-linkbutton"   onclick="<?php echo $cfg['appname']; ?>.post_invoices();"><i class="fa fa-check-circle-o"></i> Post Invoices</a>
		|
		<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.list_invoices()"><i class="fa fa-refresh"></i> Reset</a>
		</td>
	</tr>
		
</table>
</form>

 <script>
		
	
	var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		list_invoices:function (){
		 ui.cc('list_invoices',$('#<?php echo ui::fi('ff'); ?>').serialize() + '&modvars=<?php echo $vars; ?>','<?php echo ui::fi('div_invoices'); ?>');
		},
		post_invoices:function (){
			var batchid =  $('#<?php echo ui::fi('batchid'); ?>').val();
			if(batchid===''){
			 $.messager.alert('Error','Select Invoice Batch','error');
			 return;	
			}else{
			 $.messager.confirm('Invoicing Posting...', "Post selected Invoices?", function(r) {
 	 	     if (r){
			 $.messager.progress();
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=post_invoices';
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
		     <?php echo $cfg['appname']; ?>.list_invoices();
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		  });
		 }
		},
	  }
		
		<?php echo $cfg['appname']; ?>.list_invoices();
	</script>
