<?php

/**
* auto created index file for /modvarss/finance/sf/transactions/receipting/approve
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:10:22
*/
 
?>
    <table id="<?php echo ui::fi('dg'); ?>" >
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'YEARCODE',width:50,align:'left',sortable:true">Year</th>
				<th data-options="field:'TERMCODE',width:50,align:'left',sortable:true">Term</th>
				<th data-options="field:'FORMCODE',width:50,align:'left',sortable:true">Form</th>
				<th data-options="field:'RECEIPTNO',width:80,sortable:true,sortable:true">Receipt</th>
				<th data-options="field:'ADMNO',width:70,align:'left',sortable:true">Stud. No</th>
				<th data-options="field:'FULLNAME',width:180,align:'left'">Stud. Name</th>
				<th data-options="field:'DATERECEIPT',width:85,align:'center',sortable:true">Date</th>
				<th data-options="field:'DOCUMENTNO',width:80,align:'left',sortable:true">Doc. Ref</th>
				<th data-options="field:'DOCUMENTAMOUNT',width:80,align:'center'">Amount</th>
			</tr>
		</thead>
	</table>
	
	<div id="<?php echo ui::fi('tb'); ?>" style="padding:3px">
		<a href="#" class="easyui-linkbutton"   data-options="iconCls:'icon-ok'" onclick="<?php echo $cfg['appname']; ?>.approve()">Approve Selected</a>
	</div>
	
  <script>
  $(function(){
			$('#<?php echo ui::fi('dg'); ?>').datagrid({
				rownumbers: true,
				url: './endpoints/crud/',
				method: 'post',
				queryParams: { 'modvars':'<?php echo $vars; ?>', function:'data'},
				loadMsg: 'Retrieving Receipts..Please Wait',
				idField:'RECEIPTNO',
				toolbar:'#<?php echo ui::fi('tb'); ?>',
				width: 700,
				height: 300,
				fit: true,
				pagination: true,
				multiSort: true,
				remoteSort: true,
				remoteFilter: true,
				filterDelay: 200,
				fitColumns: true,
				singleSelect: false,
				checkOnSelect: true,
			});
		});
		
		$('#<?php echo ui::fi('dg'); ?>').datagrid('enableFilter');
		
		var <?php echo $cfg['appname']; ?> = {
		 approve:function (){
			
			  var receiptno_str,rows,ids = [];
              var rows = $('#<?php echo ui::fi('dg'); ?>').datagrid('getSelections');
              
              if(rows.length<=0){
              	$.messager.alert( 'Error', 'Select Receipt(s) to Approve','error');
              }else{
              
              for(var i=0; i<rows.length; i++){
                ids.push(rows[i].RECEIPTNO);
              }
              
             receiptno_str= 'receiptnos[]='+(ids.join('&receiptnos[]='));	
             var s = rows.length>1 ? 's' : '';
			 $.messager.confirm('Approving Receipts...', "Approve the "+rows.length+" Selected Receipt"+s+"?", function(r) {
 	 	     if (r){
			 $.messager.progress();
			 var fdata = receiptno_str + '&modvars=<?php echo $vars; ?>&function=save';
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
				$('#<?php echo ui::fi('dg'); ?>').datagrid('clearSelections');
				$('#<?php echo ui::fi('dg'); ?>').datagrid('clearChecked');
				$('#<?php echo ui::fi('dg'); ?>').datagrid('reload');
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		  });
         }
		},
	   }
		
  </script>
