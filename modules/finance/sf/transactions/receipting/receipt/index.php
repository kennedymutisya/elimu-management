<?php

/**
* auto created index file for /modvarss/finance/sf/transactions/receipting/receipt
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:09:46
*/

 $school  = new school();
 $user    = new user();
 $forms   = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS');
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $streams = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years   = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms   = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 
 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
 

?>
<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    	    
	    	    <tr>
	    		  <td width="100px">Student</td>
				   <td><?php echo ui::form_input( 'text', 'find_admno', 20, '', '', '', '', '', '' , ""); ?> </td>
				   <td><a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.loadProfile();"><i class="fa fa-chevron-circle-down"></i> View</a></td>
			    </tr>
	    		
	    	    <tr>
	    			<td>Registration No:</td>
	    			<td colspan="2"><?php echo ui::form_input( 'text', 'admno', 20, '', '', '', ' disabled="disabled"  readonly "', '', '' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Year:</td>
	    			<td  colspan="2" title="receipt year"><?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode," onchange=\"\" ");  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Term:</td>
	    			<td  colspan="2" title="receipt term"><?php echo ui::form_select_fromArray('term', $terms,$school->active_termcode,"onchange=\"\"");  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td width="180px">Stream</td>
	    			<td  colspan="2" title="current form"><?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"\"");  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td colspan="3"><hr></td>
	    		</tr>
	    		
	    		<tr>
	    			<td title="Where Student Paid">Pay Bank</td>
	    			<td colspan="2" title="Where Student Paid">
	    			<?php echo dropdown::bank('paybank');  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td  title="How Student Paid" class="easyui-tooltip">Pay Mode</td>
	    			<td colspan="2" title="How Student Paid">
	    			<?php echo dropdown::paymode('paymode','BD');  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td >Date Paid:</td>
	    			<td colspan="2" title="Date Student Paid"><?php echo ui::form_input( 'text', 'paydate', 15, '', '', '', 'data-options="required:true,formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td title="Bank slip Reference Number">Pay Ref:</td>
	    			<td colspan="2"><?php echo ui::form_input( 'text', 'payref', 20, '', '', '', 'data-options="required:true"', '', 'easyui-validatebox easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td title="amount paid">Pay Amount:</td>
	    			<td colspan="2" title="Amount Paid"><?php echo ui::form_input( 'text', 'payamount', 20, '', '', '', 'data-options="required:true,min:0,max:1000000,groupSeparator:\',\',precision:2"', '', 'easyui-numberbox ' , ""); ?></td>
	    		</tr>
	    		
	    	   <tr>
	    			<td >&nbsp;</td>
	    			<td colspan="2">
	    			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="<?php echo $cfg['appname']; ?>.receipt();">Generate Receipt</a>
	    			|
	    			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="<?php echo $cfg['appname']; ?>.clearForm()">Reset</a>
	    			</td>
	    		</tr>
	    			
	    	</table>
 
     </form>
     
     <script>
		
		var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		loadProfile:function (){
		 var admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
		 $.post('./endpoints/crud/',   'admno='+admno+'&modvars=<?php echo $vars; ?>&function=data_precheck'  , function(data){
          if(data.success===1){
		   $('#<?php echo ui::fi('ff'); ?>').form('load', './endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&admno='+admno);
		  }else{
		  	$.messager.alert('Error',data.message,'error');
		  }
		 }, "json");
		},
		receipt:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
				$.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 $.messager.confirm('Receipting...', "Save Receipt now?", function(r) {
 	 	     if (r){
			 $.messager.progress(); 
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save';
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close'); 
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message });
				<?php echo $cfg['appname']; ?>.print(data.receiptno);
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		  });
		 }
		},
		print:function (r){
            var w=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&r='+r,'printout','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
            w.focus();
		},
		list_items:function (){
		 ui.cc('list_items',$('#<?php echo ui::fi('ff'); ?>').serialize() + '&modvars=<?php echo $vars; ?>','div_list_items');
		},
	   }
 
	   <?php
        echo ui::ComboGrid($combogrid_array,'find_admno',"{$cfg['appname']}.loadProfile();");
	   ?>
		
	</script>
