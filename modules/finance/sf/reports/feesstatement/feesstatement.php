<?php

/**
* auto created config file for /modules/finance/sf/reports/feesstatement
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 16:17:43
*/

  class feesstatement{
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->admno        = filter_input(INPUT_POST , 'admno');
	}
	
	
	public function get_profile(){
		global $db,$cfg;
		
         $student    = new student( $this->admno );
         
         if(!empty($student->fullname)){
			 
			 $balance = $db->GetOne("SELECT SUM(AMOUNT) FROM FINSTH WHERE ADMNO='{$this->admno}' ");
			 $success = 1;
			 $message = 'ok';
			 
			 $data['admno']       =  $student->admno;
			 $data['fullname']    =  $student->fullname;
			 $data['streamname']  =  $student->streamcode;
			 $data['formname']    =  $student->formcode;
			 $data['balance']     =  number_format($balance,2);
			 
	     }else{
		     $success = 1;
			 $message = 'Student Not Found';
	     }
	     
          $data['success']    =  $success;
          $data['message']    =  $message;
         
         return json_encode($data);
	}
	
	
}
