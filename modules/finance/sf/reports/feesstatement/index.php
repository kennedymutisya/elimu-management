<?php
?>
    <div style="padding:10px">
	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		
	 <table cellpadding="4" cellspacing="0" width="100%">
	 
		 <tr>
		  <td width="100px">Student</td>
		  <td><?php echo ui::form_input( 'text', 'find_admno', 20, '', '', '', '', '', '' , ""); ?> </td>
<!--
		  <td><a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.loadProfile();"><i class="fa fa-chevron-circle-down"></i> View</a></td>
-->
		  <td>&nbsp;</td>
		 </tr>
		 
		<tr>
		 <td>Admission No</td>
		 <td colspan="2"><div id="<?php echo ui::fi('div_no') ?>"></div></td>
		</tr>
		
		<tr>
		 <td>Name</td>
		 <td colspan="2"><div id="<?php echo ui::fi('div_name') ?>"></div></td>
		</tr>
		
		<tr>
		 <td>Stream</td>
		 <td colspan="2"><div id="<?php echo ui::fi('div_stream') ?>"></div></td>
		</tr>
		
		
		<tr>
		 <td>Balance</td>
		 <td colspan="2"><div id="<?php echo ui::fi('div_balance') ?>"></div></td>
		</tr>
		
		<tr>
		 <td >&nbsp;</td>
		 <td  colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
		 <td >&nbsp;</td>
		 <td colspan="2"><a href="javascript:void(0)"  class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.view_report();"><i class="fa fa-print"></i> View Report</a></td>
		</tr>
	    		
	</table>
   </form>
   </div>
   
	<script>
		
		var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('find_admno'); ?>').val();
			$('.combo-text').val();
		},
		loadProfile :function(){
		  var admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
		  $.post('./endpoints/crud/', 'modvars=<?php echo $vars; ?>&function=get_profile&admno='+admno, function(data) {
            if (data.success === 1) {
              $('#<?php echo ui::fi('div_no'); ?>').html(data.admno);
              $('#<?php echo ui::fi('div_name'); ?>').html(data.fullname);
              $('#<?php echo ui::fi('div_stream'); ?>').html(data.streamname);
              $('#<?php echo ui::fi('div_balance'); ?>').html(data.balance);
            }
          }, "json");
		},
		view_report:function (){
         var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
  	     var <?php echo ui::fi('win'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&'+fdata,'p','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('win'); ?>.focus();
        },
	   }
		
	<?php
      echo ui::ComboGrid($combogrid_array,'find_admno',"{$cfg['appname']}.loadProfile();");
	?>
	</script>
	
