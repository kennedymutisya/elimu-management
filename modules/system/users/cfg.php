<?php
/**
* auto created config file for modules/system/users
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-16 11:21:23
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Users';//user-readable formart
 $cfg['appname']       = 'users';//lower cased one word
 $cfg['datasrc']       = 'VIEWUSERS';//where to get data
 $cfg['datatbl']       = 'USERS';//base data src [for updates & deletes]
 $cfg['form_width']    = 430;
 $cfg['form_height']   = 300;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'USERID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['vtipy'] = array(
                      'dbcol'=>'GROUPCODE',
                      'title'=>'Group',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'USERGROUPS|GROUPCODE|GROUPNAME|GROUPCODE',
                      );
                      



                      
$cfg['columns']['z0fhg'] = array(
                      'dbcol'=>'GROUPNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['r4d1d'] = array(
                      'dbcol'=>'IST',
                      'title'=>'Ist',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> '',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['yxgis'] = array(
                      'dbcol'=>'USERID',
                      'title'=>'Userid',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['n1ike'] = array(
                      'dbcol'=>'USERNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['37xji'] = array(
                      'dbcol'=>'DEPTCODE',
                      'title'=>'Department',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SADEPT|DEPTCODE|DEPTNAME|DEPTCODE',
                      );
                      



                      
$cfg['columns']['1kgfv'] = array(
                      'dbcol'=>'DEPTNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['gqpcc'] = array(
                      'dbcol'=>'EMAIL',
                      'title'=>'Email',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> 'email',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['s7pdx'] = array(
                      'dbcol'=>'PASSWORD',
                      'title'=>'Password',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'password',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['vtipy']['columns']['GROUPCODE']  = array( 'field'=>'GROUPCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['vtipy']['columns']['GROUPNAME']  = array( 'field'=>'GROUPNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['vtipy']['source'] ='USERGROUPS';
	 
$combogrid_array['37xji']['columns']['DEPTCODE']  = array( 'field'=>'DEPTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['37xji']['columns']['DEPTNAME']  = array( 'field'=>'DEPTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['37xji']['source'] ='SADEPT';
  