<?php

/**
* auto created index file for modules/lb/transactions/lendreceive
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-24 16:54:06
*/

?>
<style>
		
#<?php echo ui::fi('dlg'); ?> .fitem label{
	display:inline-block;
	min-width:150px;
}

#<?php echo ui::fi('dlg'); ?> .fitem input{
	
}
</style>
	<table id="<?php echo ui::fi('dg',false); ?>" style="width:676px;height:354px;border:0" >
        <thead>
            <tr>
				<th field="<?php echo ui::fi('fullname'); ?>"    width="100"  sortable="true" >Member</th>
                <th field="<?php echo ui::fi('mtpname'); ?>"     width="70"  sortable="true" >Type</th>
                <th field="<?php echo ui::fi('booktitle'); ?>"   width="150" sortable="true" >Book</th>
                <th field="<?php echo ui::fi('dateissue'); ?>"   width="80"  sortable="true" >Issued</th>
                <th field="<?php echo ui::fi('datedue'); ?>"     width="80"  sortable="true" >Due</th>
                <th field="<?php echo ui::fi('returned'); ?>"    width="60"  sortable="true" >Returned</th>
            </tr>
        </thead>
    </table>
    
    <div id="<?php echo ui::fi('tb'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="<?php echo $cfg['appname']; ?>.lend()">Lend</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="<?php echo $cfg['appname']; ?>.receive()">Receive</a>
<!--
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="crud.remove(<?php echo MNUID; ?>,'<?php echo $cfg['apptitle']; ?>','<?php echo $vars; ?>')">Remove</a>
-->
    </div>
    
  <div 
     id="<?php echo ui::fi('dlg'); ?>" 
     class="easyui-dialog" 
     style="width:460px;height:280px;padding:0" 
     closed="true" 
     buttons="#<?php echo ui::fi('dlg-buttons'); ?>"
     modal="true"
     >
         
	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate  onsubmit="return false;">
	
	    	<table cellpadding="5" cellspacing="0" width="100%">
				
	    		<tr>
	    		 <td colspan="4" >&nbsp;</td>
	    		</tr>
	    		
	    		<tr>
				 <td>Member:</td>
				 <td colspan="3"><?php echo ui::form_input( 'text', 'find_member',20); ?></td>
				</tr>
				
				<tr>
				 <td>Book:</td>
				 <td colspan="3"><?php echo ui::form_input( 'text', 'find_book',20); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Lend Days</td>
	    		 <td><?php echo ui::form_input( 'text', 'days', 5, '', '', '', 'data-options="required:true, min:0,max:10000,precision:0"', '', 'easyui-numberspinner' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Due Date</td>
	    		 <td><?php echo ui::form_input( 'text', 'datertn', 10, '', '', '', 'data-options="formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>&nbsp;</td>
	    		 <td colspan="3" >&nbsp;</td>
	    		</tr>
	    		
	    	</table>
	
   </form>
  </div>
  
   <div id="<?php echo ui::fi('dlg-buttons'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save();" >Lend Book</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg'); ?>').dialog('close');<?php echo $cfg['appname']; ?>.clearForm();" style="width:90px">Cancel</a>
   </div>
 
 
  <div 
     id="<?php echo ui::fi('dlg-receive'); ?>" 
     class="easyui-dialog" 
     style="width:430px;height:330px;padding:5px" 
     closed="true" 
     buttons="#<?php echo ui::fi('dlg-buttons-receive'); ?>"
     modal="true"
     >
         
	<form id="<?php echo ui::fi('ff_receive'); ?>" method="post" novalidate  onsubmit="return false;">
	 
	       <div class="fitem">
                <label>Member:</label>
                <?php echo ui::form_input( 'text', 'return_member', 40, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Book:</label>
                <?php echo ui::form_input( 'text', 'return_book', 50, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Lend Date</label>
                <?php echo ui::form_input( 'text', 'return_date_lend', 30, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Due Date</label>
                <?php echo ui::form_input( 'text', 'return_date_due', 30, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Days Past</label>
                <?php echo ui::form_input( 'text', 'return_days_past', 10, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Fine</label>
                <?php echo ui::form_input( 'text', 'return_fine', 10, '', '', '', '', '', 'easyui-numberbox' , ""); ?>
            </div>
            
            <div class="fitem">
                <label>Damages Charge</label>
                <?php echo ui::form_input( 'text', 'return_damage_charge', 10, '', '', '', '', '', 'easyui-numberbox' , ""); ?>
            </div>
            
            <div class="fitem">
                <label>Remarks</label>
                <?php echo ui::form_input( 'text', 'return_remarks', 10, '', '', '', '', '', 'easyui-textbox' , ""); ?>
            </div>
            
   </form>
  </div>
  
   <div id="<?php echo ui::fi('dlg-buttons-receive'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save_receive();" >Receive Book</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg-receive'); ?>').dialog('close');<?php echo $cfg['appname']; ?>.clearForm();" style="width:90px">Cancel</a>
   </div>
 
 
	<script>
		
	  $(function(){
			
	    $('#<?php echo ui::fi('dg',false); ?>').datagrid({
		     url:'./endpoints/crud/',
             pagination:'true',
             rownumbers:'true',
             fitColumns:'true',
             singleSelect:'true',
             idField:'id',
             toolbar:'#<?php echo ui::fi('tb'); ?>',
             queryParams:{ modvars:'<?php echo $vars; ?>',function:'data' },
             remoteFilter:true,
             collapsible:false,
             filterBtnIconCls:'icon-filter',
             multiSort:true,
             fit:true,
             type:'post'
		   });
	   });
	   
	   $('#<?php echo ui::fi('dg',false); ?>').datagrid('enableFilter');
	   
       var <?php echo $cfg['appname']; ?> = {
		 clearForm :function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		checkMember :function (id){
		 console.log(id);
		},
		setMemberOpts :function (r){
		    var params   = 'modvars=<?php echo $vars; ?>&function=get_member_opts&memberid='+ r.memberid;
			$.post('./endpoints/crud/', params, function(data) {
             if(data.success === 1){
			  $('#<?php echo ui::fi('days'); ?>').numberspinner('setValue', data.maxdays);
			  <?php echo $cfg['appname']; ?>.calc_end_date();
             }else{
              $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		},
		lend:function (){
			$('#<?php echo ui::fi('dlg'); ?>').dialog('open').dialog('setTitle','Lend Item');
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('datefrom'); ?>').val('<?php echo date('Y-m-d'); ?>');
		},
		receive:function (){
		var row = $('#<?php echo ui::fi('dg',false); ?>').datagrid('getSelected');
		 if (row){
			
			if(row.<?php echo ui::fi('returned'); ?>=='Yes'){
			 $.messager.alert('Error','Book Already Returned','error');
			 return;
			}
			$('#<?php echo ui::fi('dlg-receive'); ?>').dialog('open').dialog('setTitle','Receive Item');
			$('#<?php echo ui::fi('ff_receive'); ?>').form('clear');
			var params   = 'modvars=<?php echo $vars; ?>&function=data_return&id='+ row.id;
			$.post('./endpoints/crud/', params, function(data) {
             if(data.success === 1){
			  $('#<?php echo ui::fi('ff_receive'); ?>').form('load',data);
             }else{
              $.messager.alert('Error',data.message,'error');
             }
            }, "json");
                 
		 }else{
			$.messager.alert('Error','Select a Record First','error'); 
		 }
		},
		calc_end_date:function(){
		    var days      = $('#<?php echo ui::fi('days'); ?>').numberspinner('getValue');
		    var datefrom  = '<?php echo date('Y-m-d'); ?>';
		  if(datefrom!='' && days>0){
            var end     = moment(datefrom, "YYYY-MM-DD").add( days,'days');
            var enddate = end.format('YYYY') +'-'+ end.format('MM')+'-'+ end.format('DD');
		    $('#<?php echo ui::fi('datertn'); ?>').datebox('setValue', enddate);
		  }
		  
		},
		calc_lend_days:function(){
		  var datefrom = '<?php echo date('Y-m-d'); ?>';
		  var dateto   = $('#<?php echo ui::fi('datertn'); ?>').datebox('getValue');
		  var start    = moment( datefrom );
          var end      = moment( dateto );
          if( (end>0 && start>0) && (end < start) ){
		   alert("Invalid Due Date");
		  }else{
		   var duration = moment.duration(end.diff(start));
		   var days = duration.asDays();
		   $('#<?php echo ui::fi('days'); ?>').numberspinner('setValue', days);
		  }
		}, 
		save:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 
			var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&module=<?php echo $vars; ?>&function=save&';
		     $.post('./endpoints/crud/', fdata, function(data) {
            if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
				$('#<?php echo ui::fi('dg',false); ?>').datagrid('reload');
				$('#<?php echo ui::fi('dlg'); ?>').dialog('close');
            } else {
                $.messager.alert('Error',data.message,'error');
            }
           }, "json");
           
		 }
		},
		save_receive:function (){
			var row = $('#<?php echo ui::fi('dg',false); ?>').datagrid('getSelected');
			if(!row){
			  $.messager.alert('Error','Select a Record to Receive','error');
			 return;	
			}else{
			 
			var fdata = $('#<?php echo ui::fi('ff_receive'); ?>').serialize() +'&module=<?php echo $vars; ?>&function=save_receive&id='+row.id;
		     $.post('./endpoints/crud/', fdata, function(data) {
            if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
			    $('#<?php echo ui::fi('ff_receive'); ?>').form('clear');
				$('#<?php echo ui::fi('dg',false); ?>').datagrid('reload');
				$('#<?php echo ui::fi('dlg-receive'); ?>').dialog('close');
            } else {
                $.messager.alert('Error',data.message,'error');
            }
           }, "json");
           
		 }
		},
		
	  }
		
		
    <?php
      echo ui::ComboGrid($combogrid_array,'find_member',"{$cfg['appname']}.setMemberOpts");
      echo ui::ComboGrid($combogrid_array,'find_book',"{$cfg['appname']}.checkMember");
	?>
	$('#<?php echo ui::fi('days'); ?>').numberspinner({ onChange: function(d){<?php echo $cfg['appname']; ?>.calc_end_date();}});
	$('#<?php echo ui::fi('datertn'); ?>').datebox({ onSelect: function(date){<?php echo $cfg['appname']; ?>.calc_lend_days();}});
	</script>
