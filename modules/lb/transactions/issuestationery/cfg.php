<?php
/**
* auto created config file for modules/lb/transactions/issuestationery
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-25 13:47:01
*/

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Issue Stationery';//user-readable formart
 $cfg['appname']       = 'issuestationery';//lower cased one word
 $cfg['datasrc']       = '';//where to get data
 $cfg['datatbl']       = '';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'STCODE';//the primary key
 
$combogrid_array   = array();
 
$combogrid_array['find_member']['columns']['MTPNAME']   = array( 'field'=>'mtpname', 'title'=>'Type', 'width'=>80);
$combogrid_array['find_member']['columns']['MEMBERID']  = array( 'field'=>'memberid', 'title'=>'Member ID', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['find_member']['columns']['FULLNAME']  = array( 'field'=>'fullname', 'title'=>'Member Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['find_member']['source'] ='VIEWLIBMEMBERS';

$combogrid_array['find_stationery']['columns']['STCODE']      = array( 'field'=>'stcode', 'title'=>'Code', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['find_stationery']['columns']['STNAME']      = array( 'field'=>'stname', 'title'=>'Stationery Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['find_stationery']['source'] ='SLST';
	
 
