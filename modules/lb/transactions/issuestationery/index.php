<?php

/**
* auto created index file for modules/lb/transactions/issuestationery
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-25 13:47:01
*/

$stationery    = $db->GetAssoc("select STCODE ,STNAME from  SLST  order by SCTCODE ");

?>
<style>
		
#<?php echo ui::fi('dlg'); ?> .fitem label{
	display:inline-block;
	min-width:150px;
}

#<?php echo ui::fi('dlg'); ?> .fitem input{
	
}
</style>
	<table id="<?php echo ui::fi('dg',false); ?>" style="width:676px;height:354px;border:0" >
        <thead>
            <tr>
				<th field="<?php echo ui::fi('memberid'); ?>"   width="100" sortable="true" >Member ID</th>
				<th field="<?php echo ui::fi('fullname'); ?>"   width="100" sortable="true" >Member Name</th>
                <th field="<?php echo ui::fi('dateissue'); ?>"  width="85"  sortable="true" >Date</th>
                <th field="<?php echo ui::fi('qty'); ?>"        width="35" sortable="true" >Qty</th>
                <th field="<?php echo ui::fi('descr'); ?>"      width="300"  >Description</th>
                
            </tr>
        </thead>
    </table>
    
    <div id="<?php echo ui::fi('tb'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="<?php echo $cfg['appname']; ?>.add()">Issue</a>
<!--
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="<?php echo $cfg['appname']; ?>.edit()">Edit</a>
-->
<!--
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="crud.remove(<?php echo MNUID; ?>,'<?php echo $cfg['apptitle']; ?>','<?php echo $vars; ?>')">Remove</a>
-->
    </div>
    
  <div 
     id="<?php echo ui::fi('dlg'); ?>" 
     class="easyui-dialog" 
     style="width:440px;height:280px;padding:0" 
     closed="true" 
     buttons="#<?php echo ui::fi('dlg-buttons'); ?>"
     modal="true"
     >
         
	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate  onsubmit="return false;">
	
	    	<table cellpadding="5" cellspacing="0" width="100%">
				
	    		<tr>
				 <td>Member:</td>
				 <td colspan="3"><?php echo ui::form_input( 'text', 'find_member',20); ?></td>
				</tr>
				
				<?php
				foreach($stationery as $code=>$name){
				?>
				<tr>
				 <td><?php echo Camelize($name); ?>:</td>
				 <td colspan="3">
				 <?php
				  echo "<input type=\"text\" autocomplete=\"off\" size=\"5\" name=\"st_issue_quantity[{$code}]\" id=\"st_issue_quantity[{$code}]\"  value=\"0\"  data-options=\"min:0,max:10000,precision:0\" class=\"easyui-numberspinner\" >";
				 ?>
				 </td>
	    		</tr>
	    		<?php
			    }
				?>
	    		
	    		<tr>
	    		 <td>Issue Date</td>
	    		 <td><?php echo ui::form_input( 'text', 'dateissue', 10, '', '', '', 'data-options="formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>&nbsp;</td>
	    		 <td colspan="3" >&nbsp;</td>
	    		</tr>
	    		
	    	</table>
	
   </form>
  </div>
  
   <div id="<?php echo ui::fi('dlg-buttons'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save();" >Issue Stationery</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg'); ?>').dialog('close');<?php echo $cfg['appname']; ?>.clearForm();" style="width:90px">Cancel</a>
   </div>
 
 
  <div 
     id="<?php echo ui::fi('dlg-receive'); ?>" 
     class="easyui-dialog" 
     style="width:430px;height:330px;padding:5px" 
     closed="true" 
     buttons="#<?php echo ui::fi('dlg-buttons-receive'); ?>"
     modal="true"
     >
         
	<form id="<?php echo ui::fi('ff_receive'); ?>" method="post" novalidate  onsubmit="return false;">
	 
	       <div class="fitem">
                <label>Member:</label>
                <?php echo ui::form_input( 'text', 'return_member', 40, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Book:</label>
                <?php echo ui::form_input( 'text', 'return_book', 50, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Lend Date</label>
                <?php echo ui::form_input( 'text', 'return_date_lend', 30, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Due Date</label>
                <?php echo ui::form_input( 'text', 'return_date_due', 30, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Days Past</label>
                <?php echo ui::form_input( 'text', 'return_days_past', 10, '', '', '', 'readonly'); ?>
            </div>
            
            <div class="fitem">
                <label>Fine</label>
                <?php echo ui::form_input( 'text', 'return_fine', 10, '', '', '', '', '', 'easyui-numberbox' , ""); ?>
            </div>
            
            <div class="fitem">
                <label>Damages Charge</label>
                <?php echo ui::form_input( 'text', 'return_damage_charge', 10, '', '', '', '', '', 'easyui-numberbox' , ""); ?>
            </div>
            
            <div class="fitem">
                <label>Remarks</label>
                <?php echo ui::form_input( 'text', 'return_remarks', 10, '', '', '', '', '', 'easyui-textbox' , ""); ?>
            </div>
            
   </form>
  </div>
  
   <div id="<?php echo ui::fi('dlg-buttons-receive'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save_receive();" >Receive Book</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg-receive'); ?>').dialog('close');<?php echo $cfg['appname']; ?>.clearForm();" style="width:90px">Cancel</a>
   </div>
 
 
	<script>
		
	  $(function(){
			
	    $('#<?php echo ui::fi('dg',false); ?>').datagrid({
		     url:'./endpoints/crud/',
             pagination:'true',
             rownumbers:'true',
             fitColumns:'true',
             singleSelect:'true',
             idField:'id',
             toolbar:'#<?php echo ui::fi('tb'); ?>',
             queryParams:{ modvars:'<?php echo $vars; ?>',function:'data' },
             remoteFilter:true,
             collapsible:false,
             filterBtnIconCls:'icon-filter',
             multiSort:true,
             fit:true,
             type:'post'
		   });
	   });
	   
	   $('#<?php echo ui::fi('dg',false); ?>').datagrid('enableFilter');
	   
       var <?php echo $cfg['appname']; ?> = {
		 clearForm :function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		add:function (){
			$('#<?php echo ui::fi('dlg'); ?>').dialog('open').dialog('setTitle','Issue Stationery');
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('datefrom'); ?>').val('<?php echo date('Y-m-d'); ?>');
		},
		edit:function (){
		var row = $('#<?php echo ui::fi('dg',false); ?>').datagrid('getSelected');
		 if (row){
			//load to edit
		 }else{
			$.messager.alert('Error','Select a Record First','error'); 
		 }
		},
		save:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 
			var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&module=<?php echo $vars; ?>&function=save&';
		     $.post('./endpoints/crud/', fdata, function(data) {
            if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
				$('#<?php echo ui::fi('dg',false); ?>').datagrid('reload');
				$('#<?php echo ui::fi('dlg'); ?>').dialog('close');
            } else {
                $.messager.alert('Error',data.message,'error');
            }
           }, "json");
           
		 }
		},
	  }
		
		
    <?php
      echo ui::ComboGrid($combogrid_array,'find_member',"");
	?>
	</script>
