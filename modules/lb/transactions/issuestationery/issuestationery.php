<?php

/**
* auto created config file for modules/lb/transactions/issuestationery
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-25 13:47:01
*/

 final class issuestationery {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 
    public function data(){
    global $db, $cfg;
   //$db->debug=1;
   
	$page     = isset($_POST['page']) ? intval($_POST['page']) : 1;
    $rows     = isset($_POST['rows']) ? intval($_POST['rows']) : 200;
    $offset   = ($page-1)*$rows;
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array  = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		 $sort_col_name  = strtoupper($sort_col_name);
		 $sort_col_name  = str_replace("_".MNUID,'',$sort_col_name);
		 $sort_col_order = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]      = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = 'order by ' .implode(',', $sortbys);
	 }
   }else{
	 $sortby   =  'order by ID asc';  
   }
	
	 $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
		
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleCol = str_replace("_".MNUID,'',$filterRuleCol);
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = @$filterRule->value;
	  
	  $filterRuleDBCol = strtoupper($filterRuleCol);
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
         default  : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	 
	}else{
	    $filters_conditions_str =  '';
	}
	
    $sql_where   = " WHERE 1=1 {$filters_conditions_str}";
	
	$numRecords    = $db->GetOne("select count(*) from VIEWLIBSTI {$sql_where} ");
	
	$result["total"] = $numRecords;
	   
	$sql = "
	SELECT * FROM VIEWLIBSTI
    {$sql_where}
    {$sortby}
    ";
	
	$rs = $db->SelectLimit($sql,$rows, $offset);
		
	$items = array();
	
	if($rs){
	 
	 while(!$rs->EOF){
	 	
	  foreach ($rs->fields as $dbCol=>$val){	
		  
	    $formCol = strtolower($dbCol);
	    
	    if($formCol=='returned'){
		 $val  = $val==1 ? 'Yes' : 'No';
		}
		
	    $rs->fields[$formCol]  = $val;
	    unset($rs->fields[$dbCol]);
	  }
	  
	  	
	  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($rs->fields as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  if($k!='ID' && $k!='id'){
		   unset($rs->fields[$k]);
	      }
		  $rs->fields[$field_id] = $v;
		}
	  }
	  
	  array_push($items, $rs->fields);
	  
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
		
 }
		

	public function save(){
		global $db,$cfg;

    $user          = new user();		
	$memberid      = filter_input(INPUT_POST, ui::fi('find_member'));
	$dateissue     = filter_input(INPUT_POST, ui::fi('dateissue'));
	$fid_items     = ('st_issue_quantity');
	$items         = isset($_POST[$fid_items]) ?  $_POST[$fid_items] :  array();
	$qty           = array_sum( $items );
	$num_items     = sizeof( $items );
    $dateissue     = date('Y-m-d'); 
    $saved_lines   = 0; 
    $descr         = array(); 
	
	if(empty($qty)){
     return json_response(0,'Enter Quatity Issued Per Item');
    }
    
    $stationery    = $db->GetAssoc("select STCODE ,STNAME from  SLST");
    
    if(sizeof($stationery)>0){
     foreach($items as $itemcode=>$item_qty){
		$itemname          = valueof($stationery, $itemcode);
		$descr[$itemcode]  = Camelize("{$item_qty} {$itemname}");
	 }
	}
	
	$descr_str = implode(',', $descr);
	
	$issue_header  = new ADODB_Active_Record('SLSIH', array('ID'));
    
    if(empty($issue_header->_original)){
     $issue_header->id       = generateID($issue_header->_tableat);
     $issue_header->issueno  = generateUniqueCode('STI',$issue_header->id,6);
    }
	
	$issue_header->memberid       =  $memberid;
	$issue_header->dateissue	  =  $dateissue;
	$issue_header->qty	          =  $qty;
	$issue_header->descr	      =  $descr_str;
	$issue_header->audituser      =  $user->userid;
	$issue_header->auditdate      =  date('Y-m-d'); 
	$issue_header->audittime      =  time();

    if(!$issue_header->Save()){
     return json_response(0,'Save Failed');
    }
    
    foreach($items as $itemcode=>$item_qty){
	$issue_detail_line = new ADODB_Active_Record('SLSID', array('ID'));
    
    if(empty($issue_detail_line->_original)){
     $issue_detail_line->id       = generateID($issue_header->_tableat);
     $issue_detail_line->issueno  = $issue_header->issueno;
    }
	
	$issue_detail_line->memberid      =  $memberid;
	$issue_detail_line->stcode	      =  $itemcode;
	$issue_detail_line->qty	          =  $item_qty;
	$issue_detail_line->dateissue	  =  $dateissue;
	
	$issue_detail_line->audituser     =  $user->userid;
	$issue_detail_line->auditdate     =  date('Y-m-d'); 
	$issue_detail_line->audittime     =  time();
	
     if($issue_detail_line->Save()){
	  ++$saved_lines;
	 }
	 
    }
    
     if($saved_lines==$num_items){
      return json_response(1,'Stationery Issues Saved');
     }else{
      return json_response(0,'Save Failed');
     }


  }
	
	public function save_receive(){
		global $db,$cfg;

    $user         = new user();		
	$id           = filter_input(INPUT_POST, 'id');
	$fine         = filter_input(INPUT_POST, ui::fi('return_fine'));
	$damage       = filter_input(INPUT_POST, ui::fi('return_damage_charge'));
	$remarks      = filter_input(INPUT_POST, ui::fi('return_remarks', FILTER_SANITIZE_STRING));
	
    $book = new ADODB_Active_Record('SLTRN', array('ID'));
    $book->Load("ID={$id}");
    
    if(!empty($book->_original)) {
	
	$book->returned     =  1;
	$book->fine         =  $fine;
	$book->damage       =  $damage;
	$book->remarks      =  $remarks;
	$book->datertn      =  date('Y-m-d'); 
	$book->userrcv      =  $user->userid; 

     if($book->Save()){
      return json_response(1,'Book Returned');
     }else{
      return json_response(0,'Book Return Failed');
     }

    }
    
  }
	
	public function remove(){
		global $db, $cfg;
		json_response(0,'failed');
        $grid = new grid();
        return $grid->grid_remove_row();
	}
	
}
