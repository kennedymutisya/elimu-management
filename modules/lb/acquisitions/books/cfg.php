<?php
/**
* auto created config file for modules/lb/acquisitions/books
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-25 17:47:15
*/


if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Book Acquisition';//user-readable formart
 $cfg['appname']       = 'book_acquisition';//lower cased one word
 $cfg['datasrc']       = 'SLAQB';//where to get data
 $cfg['datatbl']       = 'SLAQB';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
$combogrid_array   = array();
 
$combogrid_array['find_spl']['columns']['SPLCODE']      = array( 'field'=>'SPLCODE', 'title'=>'Supplier Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['find_spl']['columns']['SPLNAME']      = array( 'field'=>'splname', 'title'=>'Supplier Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['find_spl']['source'] ='SLSPL';
	
$combogrid_array['find_book']['columns']['SUBJECTCODE']   = array( 'field'=>'subjectcode', 'title'=>'Subject', 'width'=>80);
$combogrid_array['find_book']['columns']['BOOKCODE']      = array( 'field'=>'bookcode', 'title'=>'Book Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['find_book']['columns']['BOOKTITLE']     = array( 'field'=>'booktitle', 'title'=>'Book Title', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['find_book']['source'] ='SLBK';
	
