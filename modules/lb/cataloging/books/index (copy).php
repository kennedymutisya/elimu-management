<?php

?>
<style>
		
#<?php echo ui::fi('dlg'); ?> .fitem label{
	display:inline-block;
	min-width:150px;
}

#<?php echo ui::fi('dlg'); ?> .fitem input{
	
}
</style>
	<table id="<?php echo ui::fi('dg'); ?>" style="width:676px;height:354px;border:0" >
        <thead>
            <tr>
                <th field="ISBN"    width="70"  sortable="true" >ISBN</th>
                <th field="TITLE"    width="150"  sortable="true" >Title</th>
                <th field="BCTCODE" width="50" sortable="true" >Category</th>
                <th field="COPIES"  width="60"  sortable="true" >Copies</th>
            </tr>
        </thead>
    </table>
    
    <div id="<?php echo ui::fi('tb'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="<?php echo $cfg['appname']; ?>.add()">New</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="<?php echo $cfg['appname']; ?>.edit()">Edit</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="crud.remove(<?php echo MNUID; ?>,'<?php echo $cfg['apptitle']; ?>','<?php echo $vars; ?>')">Remove</a>
    </div>
    
<div 
     id="<?php echo ui::fi('dlg'); ?>" 
     class="easyui-dialog" 
     style="width:555px;height:380px;padding:0" 
     closed="true" 
     buttons="#<?php echo ui::fi('dlg-buttons'); ?>"
     modal="true"
     >
         
	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate  onsubmit="return false;">
	
		<table cellpadding="2" cellspacing="1" width="100%">
		
		<tr>
		 <td>Book Code:</td>
		 <td><input id="bookcode"  name="bookcode" class="easyui-validatebox textbox" type="text" style="width:100px"   data-options=" validType:'minLength[2]'" ></input></td>
		</tr>
		
		<tr>
		 <td>Book Title:</td>
		 <td><input id="booktitle"  name="booktitle" class="easyui-validatebox textbox" type="text" style="width:300px"  ></input></td>
		</tr>
	    		
	   </table>
	   
	   <div id="<?php echo ui::fi('tabs'); ?>" class="easyui-tabs" data-options="tabWidth:110,plain:true,justified:true,border:false" style="width:541px;height:251px">
		     <div title="General" style="padding:10px">
	 
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    		 <td>Book Category:</td>
	    		 <td><?php  echo dropdown::bookcategory('bctcode'); ?></td>
	    		 <td>Book ISBN:</td>
	    		 <td><?php echo ui::form_input( 'text', 'isbn', 10, '', '', '', 'data-options=" validType:\'minLength[10]\'"', '', 'easyui-numberbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Book Subject</td>
	    		 <td><?php  echo dropdown::subject('subjectcode'); ?></td>
	    		 <td>Form</td>
	    		 <td><?php  echo dropdown::form('formcode'); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Book Series</td>
	    		 <td><?php echo ui::form_input( 'text', 'series', 10, '', '', '', 'data-options=" validType:\'minLength[1]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		 <td>Edition</td>
	    		 <td><?php echo ui::form_input( 'text', 'edition', 10, '', '', '', 'data-options=" validType:\'minLength[1]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
				 <td>Date Received</td>
	    		 <td><?php echo ui::form_input( 'text', 'datercv', 10, '', '', '', 'data-options="required:true,formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    		 <td>Copy Right Year</td>
	    		 <td><?php  echo dropdown::year('yrcpr'); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>No.Of Copies</td>
	    		 <td><?php echo ui::form_input( 'text', 'author1', 5, '', '', '', 'data-options=" min:0,max:10000,precision:0"', '', 'easyui-numberspinner' , ""); ?></td>
	    		 <td>Location</td>
	    		 <td><?php  echo dropdown::book_shelves('slvcode'); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Book Publisher</td>
	    		 <td colspan="2" ><?php  echo dropdown::book_publisher('pblcode'); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Book Author</td>
	    		 <td colspan="2" ><?php echo ui::form_input( 'text', 'author1', 30, '', '', '', 'data-options=" validType:\'minLength[5]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>&nbsp;</td>
	    		 <td colspan="2" >&nbsp;</td>
	    		</tr>
	    		
	    	</table>
	    	
	    	 </div>
	      
	      <div title="Payslips" style="padding:10px">
			
	    	<table cellpadding="5" cellspacing="0" width="100%">
	    
	    		<tr>
	    		 <td>Display On Payslip:</td>
	    		 <td><input type="checkbox" value="1" id="psshow" name="psshow" ></input></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Display Index</td>
	    		 <td><input id="posdisp"  name="posdisp" class="easyui-numberspinner"  style="width:80px;" data-options="min:0,max:356,precision:0"></input></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Display Year to Date:</td>
	    		 <td><input type="checkbox" value="1" id="ytdshow" name="ytdshow"></input></td>
	    		</tr>
	    		
	        </table>
	     </div>
	
	</div>	
	
   </form>
  </div>
  
   <div id="<?php echo ui::fi('dlg-buttons'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save();" >Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg'); ?>').dialog('close');<?php echo $cfg['appname']; ?>.clearForm();" style="width:90px">Cancel</a>
   </div>
 
	<script>
		
	  $(function(){
			
	    $('#<?php echo ui::fi('dg'); ?>').datagrid({
		     url:'./endpoints/crud/',
             pagination:'true',
             rownumbers:'true',
             fitColumns:'true',
             singleSelect:'true',
             idField:'id',
             toolbar:'#<?php echo ui::fi('tb'); ?>',
             queryParams:{ modvars:'<?php echo $vars; ?>',function:'data' },
             remoteFilter:true,
             collapsible:false,
             filterBtnIconCls:'icon-filter',
             multiSort:true,
             fit:true,
             type:'post'
		   });
	   });
	   
	   $('#<?php echo ui::fi('dg'); ?>').datagrid('enableFilter');
	   
       var <?php echo $cfg['appname']; ?> = {
		 clearForm :function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		loadData :function (){
		 $('#<?php echo ui::fi('ff'); ?>').form('load', './endpoints/crud/?module=<?php echo $vars; ?>&function=data&id=1&'+$('#ff').serialize());
		},
		add:function (){
			$('#<?php echo ui::fi('dlg'); ?>').dialog('open').dialog('setTitle','Add <?php echo valueof($cfg,'apptitle'); ?>');
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		edit:function (){
		var row = $('#<?php echo ui::fi('dg'); ?>').datagrid('getSelected');
		 if (row){
			$('#<?php echo ui::fi('dlg'); ?>').dialog('open').dialog('setTitle','Edit <?php echo valueof($cfg,'apptitle'); ?>');
			$('#<?php echo ui::fi('ff'); ?>').form('load',row);
		 }
		},
		save:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 
			var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&module=<?php echo $vars; ?>&function=save&';
		     $.post('./endpoints/crud/', fdata, function(data) {
            if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
				$('#<?php echo ui::fi('dg'); ?>').datagrid('reload');
				$('#<?php echo ui::fi('dlg'); ?>').dialog('close');
            } else {
                $.messager.show({title: 'Error', msg: data.message});
            }
           }, "json");
           
		 }
		},
		
	  }
		
		//$('#itypecode').combobox({ onChange: function(val){<?php echo $cfg['appname']; ?>.decide_make_extra_tab();}});
		
	</script>
