<?php

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Books';//user-readable formart
 $cfg['appname']       = 'books';//lower cased
 $cfg['datasrc']       = 'SLBK';//where to get data
 $cfg['datatbl']       = 'SLBK';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 280;
 
 $cfg['pkcol']         = 'bookno';//the primary key

$combogrid_array   = array();
$combogrid_array['find_bkacno']['columns']['BOOKNO']       = array( 'field'=>'bookno', 'title'=>'Acc. No', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['find_bkacno']['columns']['BKTITLE']      = array( 'field'=>'bktitle', 'title'=>'Book Title', 'width'=> 100, 'isTextField'=>true);
$combogrid_array['find_bkacno']['columns']['SUBJECTCODE']  = array( 'field'=>'subjectcode', 'title'=>'Subject', 'width'=>100);
$combogrid_array['find_bkacno']['columns']['CLASS']        = array( 'field'=>'class', 'title'=>'Class', 'width'=> 80);
$combogrid_array['find_bkacno']['source'] ='SLBK';
	
