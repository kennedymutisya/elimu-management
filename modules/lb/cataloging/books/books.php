<?php

  
/**
* auto created config file for modules/lb/cataloging/books
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-23 20:24:42
*/

 final class books {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
	
    public function data(){
    global $db, $cfg;
   
	$page     = isset($_POST['page']) ? intval($_POST['page']) : 1;
    $rows     = isset($_POST['rows']) ? intval($_POST['rows']) : 200;
    $offset   = ($page-1)*$rows;
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		 $sort_col_name  = strtoupper($sort_col_name);
		 $sort_col_order = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]       = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = 'order by ' .implode(',', $sortbys);
	 }
   }else{
	 $sortby   =  'order by ID asc';  
   }
	
	 $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
		
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = @$filterRule->value;
	  
	  $filterRuleDBCol = strtoupper($filterRuleCol);
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
         default  : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	 
	}else{
	    $filters_conditions_str =  '';
	}
	
    $sql_where   = " WHERE 1=1 {$filters_conditions_str}";
	
	$numRecords    = $db->GetOne("select count(*) from SLBK {$sql_where} ");
	
	$result["total"] = $numRecords;
	   
	$sql = "
	SELECT * FROM SLBK
    {$sql_where}
    {$sortby}
    ";
	
	$rs = $db->SelectLimit($sql,$rows, $offset);
		
	$items = array();
	
	if($rs){
	 
	 while(!$rs->EOF){
	 	
	  foreach ($rs->fields as $dbCol=>$val){	
		  
	    $formCol = strtolower($dbCol);
	    $rs->fields[$formCol]  = $val;
	    unset($rs->fields[$dbCol]);
	  }
	  
	  	
	  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($rs->fields as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  if($k!='ID' && $k!='id'){
		   unset($rs->fields[$k]);
	      }
		  $rs->fields[$field_id] = $v;
		}
	  }
	  
	  array_push($items, $rs->fields);
	  
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
		
 }
		
	public function save(){
		global $db,$cfg;
//$db->debug=1;
//foreach($_POST as $k=>$v){
 //$ks = str_replace(MNUID,'', $k);
 //echo "\${$ks}              = filter_input(INPUT_POST, ui::fi('{$ks}')); <br>";
 //echo '$book->'.$ks.'		  =  $'.$ks.';'."\r\n";
//}
//exit();

    $user         = new user();		
	$bookcode     = filter_input(INPUT_POST, ui::fi('bookcode'), FILTER_SANITIZE_STRING );
	$isbn         = filter_input(INPUT_POST, ui::fi('isbn'));
	$booktitle    = filter_input(INPUT_POST, ui::fi('booktitle'), FILTER_SANITIZE_STRING );
	$subjectcode  = filter_input(INPUT_POST, ui::fi('subjectcode'));
	$formcode     = filter_input(INPUT_POST, ui::fi('formcode'));
	$bctcode      = filter_input(INPUT_POST, ui::fi('bctcode'));
	$series       = filter_input(INPUT_POST, ui::fi('series'), FILTER_SANITIZE_STRING );
	$edition      = filter_input(INPUT_POST, ui::fi('edition'), FILTER_SANITIZE_STRING );
	$datercv      = filter_input(INPUT_POST, ui::fi('datercv'));
	$yrcpr        = filter_input(INPUT_POST, ui::fi('yrcpr'));
	$author1      = filter_input(INPUT_POST, ui::fi('author1'), FILTER_SANITIZE_STRING );
	$slvcode      = filter_input(INPUT_POST, ui::fi('slvcode'));
	$pblcode      = filter_input(INPUT_POST, ui::fi('pblcode')); 
	$copies       = filter_input(INPUT_POST, ui::fi('copies')); 
	$cost         = filter_input(INPUT_POST, ui::fi('cost')); 
	
    $book = new ADODB_Active_Record('SLBK', array('BOOKCODE'));
    $book->Load("BOOKCODE='{$bookcode}' ");
    
    if(empty($book->_original)){
     $book->id       = generateID('SLBK');
     $book->bookcode =  $bookcode;
    }
	
	$book->isbn		      =  $isbn;
	$book->booktitle	  =  $booktitle;
	$book->subjectcode	  =  $subjectcode;
	$book->formcode		  =  $formcode;
	$book->bctcode		  =  $bctcode;
	$book->series		  =  $series;
	$book->edition		  =  $edition;
	$book->datercv		  =  $datercv;
	$book->yrcpr		  =  $yrcpr;
	$book->author1		  =  $author1;
	$book->slvcode		  =  $slvcode;
	$book->pblcode		  =  $pblcode;
	$book->copies		  =  $copies;
	$book->cost		      =  $cost;
	
	$book->audituser      = $user->userid;
	$book->auditdate      = date('Y-m-d'); 
	$book->audittime      = time();

    if($book->Save()){
     return json_response(1,'record saved');
    }else{
     return json_response(0,'save failed');
    }

	}
	
	public function remove(){
		global $db, $cfg;
		//json_response(0,'failed');
        $grid = new grid();
        return $grid->grid_remove_row();
	}
	
}
