<?php
/**
* auto created config file for modules/lb/settings/suppliers
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-23 15:36:31
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Suppliers';//user-readable formart
 $cfg['appname']       = 'suppliers';//lower cased one word
 $cfg['datasrc']       = 'SLSPL';//where to get data
 $cfg['datatbl']       = 'SLSPL';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 550;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'SPLCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['u0mdp'] = array(
                      'dbcol'=>'SPLCODE',
                      'title'=>'Supplier Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['r3mes'] = array(
                      'dbcol'=>'SPLNAME',
                      'title'=>'Supplier Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['vg168'] = array(
                      'dbcol'=>'MOBILEPHONE',
                      'title'=>'Mobile',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberbox',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['rjf3a'] = array(
                      'dbcol'=>'EMAIL',
                      'title'=>'Email',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> 'email',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['4f68c'] = array(
                      'dbcol'=>'TOWN',
                      'title'=>'Town',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
  