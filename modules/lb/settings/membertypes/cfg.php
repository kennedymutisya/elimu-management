<?php
/**
* auto created config file for modules/lb/settings/membertypes
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-25 12:23:03
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Member Types';//user-readable formart
 $cfg['appname']       = 'membertypes';//lower cased one word
 $cfg['datasrc']       = 'SLMTP';//where to get data
 $cfg['datatbl']       = 'SLMTP';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 550;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'MTPCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['wgatb'] = array(
                      'dbcol'=>'MTPCODE',
                      'title'=>'Type Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['iokp8'] = array(
                      'dbcol'=>'MTPNAME',
                      'title'=>'Type  Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['qewvp'] = array(
                      'dbcol'=>'ISSTUD',
                      'title'=>'Is Students?',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'checkbox',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['v4gbt'] = array(
                      'dbcol'=>'MAXBOOKS',
                      'title'=>'Max Borrow',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberspinner',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['sdfnx'] = array(
                      'dbcol'=>'MAXDAYS',
                      'title'=>'Max Lend Days',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberspinner',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
  