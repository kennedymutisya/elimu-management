<?php

?>
<style>
td.label {   font-size: 12px; }
div#<?php echo ui::fi('logo'); ?> { cursor:pointer; border:1px solid #DEDEDE;min-width: 162px; min-height: 112px; max-width: 162px; max-height: 112px; }
</style>

	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
	<div class="easyui-tabs" data-options="tabWidth:123" style="width:486px;height:250px">
		
		<div title="Identification" style="padding:5px">
	   
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    		 <td width="125px">Library Name:</td>
	    		 <td><?php echo ui::form_input( 'text', 'libname', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		 <td rowspan="5">
					 <div id="<?php echo ui::fi('logo'); ?>" title="click to replace logo" onclick="<?php echo $cfg['appname']; ?>.initiate_upload_logo();"><img src="./public/images/logo-default.gif" border="0"></div>
				 </td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Opening Time:</td>
	    			<td><?php echo ui::form_input( 'text', 'timeopen', 5, '', '', '', 'data-options="required:true"', '', 'easyui-timespinner' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Closing Time:</td>
	    			<td><?php echo ui::form_input( 'text', 'timeclose', 5, '', '', '', 'data-options="required:true"', '', 'easyui-timespinner' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Max Hold Length</td>
	    			<td><?php echo ui::form_input( 'text', 'maxhlgn', 5, '', '', '', 'data-options="required:true"', '', 'easyui-numberspinner' , ""); ?>*days</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Check Out</td>
	    			<td><?php echo ui::form_checkbox( 'blcfd'); ?><label for="<?php echo ui::fi('blcfd'); ?>">Block When Fines Due</label></td>
	    		</tr>
	    		
	    		
	    	</table>
	
		</div>
		
		<div title="Contacts" style="padding:5px">
			
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    			<td>Physical Address:</td>
	    			<td><?php echo ui::form_input( 'text', 'addressphys', 20, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Mobile Phone:</td>
	    			<td><?php echo ui::form_input( 'text', 'mobile', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Email:</td>
	    			<td><?php echo ui::form_input( 'text', 'email', 15, '', '', '', ' data-options="required:true, validType:[\'email\',\'length[0,20]\']" ', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		
	    	</table>
	    	
		</div>
		
		
	</div>
   </form>
   
	<div style="margin:10px 0;"></div>
	<table cellpadding="2" cellspacing="0" width="300px">
	 <tr>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="<?php echo $cfg['appname']; ?>.saveSchool();">Save</a></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onclick="<?php echo $cfg['appname']; ?>.clearForm()">Reset</a></td>
	 </tr>
	</table>
	
 
 <div id="<?php echo ui::fi('dlg_upload'); ?>" class="easyui-dialog" title="Logo Upload" style="width:300px;height:120px;padding:5px" closed="true" >
 <form id="ff_logo<?php echo MNUID; ?>" class="form-horizontal" action="./" method="POST" enctype="multipart/form-data">
  <input  type="file" name="<?php echo ui::fi('library_logo'); ?>" id="<?php echo ui::fi('library_logo'); ?>" onchange="<?php echo $cfg['appname']; ?>.do_upload_logo()">
 </form>
 </div>
 
	<script>
		
	  $( document ).ready(function() {
       setTimeout("<?php echo $cfg['appname']; ?>.loadData();",1000);
      });

	  
       var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('logo'); ?>').html('<img src="./public/images/logo-default.gif" border="0">');
			<?php echo $cfg['appname']; ?>.loadData();
		},
		loadData:function (){
		 $('#<?php echo ui::fi('ff'); ?>').form('load', './endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&id=1&'+$('#<?php echo ui::fi('ff'); ?>').serialize());
		  <?php echo $cfg['appname']; ?>.load_logo();
		},
		load_logo :function (){
		   var rdata = 'modvars=<?php echo $vars; ?>&function=logo';	
		   $.post('./endpoints/crud/', rdata, function(data) {
		   if (data.success === 1 && (typeof data.logo!=undefined)) {
			$('#<?php echo ui::fi('logo'); ?>').html(data.logo);
		   }else{
			$.messager.alert('Error',data.message,'error');
		   }
		  }, "json");
		},
		initiate_upload_logo:function (){
	   	$.messager.confirm('Confirm','Do you want to upload a logo Now?',function(r){
         if (r){
	  	  $('#<?php echo ui::fi('dlg_upload'); ?>').dialog('open');
         }
	   	});
	   },
	   do_upload_logo:function(){
		var data = new FormData($('#ff_logo<?php echo MNUID; ?>')[0]);
		$.ajax({
        url : './endpoints/crud/?modvars=<?php echo $vars; ?>&function=upload_logo',
        type: 'POST',
        data: data,
        cache: false,
        async:'false',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR){
         <?php echo $cfg['appname']; ?>.load_logo();
         $('#<?php echo ui::fi('dlg_upload'); ?>').dialog('close');
        },
        error: function(jqXHR, textStatus, errorThrown){

        }
        });
       },
	   saveSchool:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 $.messager.progress();
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&modvars=<?php echo $vars; ?>&function=save';	
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
             } else {
				$.messager.alert('Error',data.message,'error');
             }
           }, "json");
		 }
		},
	  }
		
	<?php
       
       if(isset($combogrid_array)){
       	if(sizeof($combogrid_array)>0){
       		foreach ($combogrid_array as $ac_source=>$details){
       		  echo ui::ComboGrid($combogrid_array,$ac_source);	
       		}
       	}
       }
	?>
		
	
	</script>
	
