<?php
/**
* auto created config file for modules/students/exams/reports/subjectsummary
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-10-06 19:14:51
*/


$scriptname  = end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Subject Summary';//user-readable formart
 $cfg['appname']       = 'subject summary';//lower cased one word
 $cfg['datasrc']       = 'SASTUDSUB';//where to get data
 $cfg['datatbl']       = 'SASTUDSUB';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 
 $cfg['pkcol']         = 'ADMNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 

$cfg['columns']['8rkqb'] = array(
                      'dbcol'=>'ADMNO',
                      'title'=>'Admno',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['qvoei'] = array(
                      'dbcol'=>'SUBJECTCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['ivgqq'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['fhtsd'] = array(
                      'dbcol'=>'STREAMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['21zxe'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['v4yzk'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      