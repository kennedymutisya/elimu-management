<?php

require_once('cfg.php');
//require_once(BASEPATH.'init.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");	

 if(!class_exists($class)){
 	die('class--' .$classfile. 'not found');
 }
 
 $_class = new $class();
 $grid   = new grid();
 
 $module = array();
 $module['url']    =  DIR;
 $module['class']  =  $class;
 $module_packed    =  packvars($module);

 $forms = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');

// print_pre($streams_data);
 
 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
 
// print_pre($forms);
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>Term Grading</title>
	
	<link rel="stylesheet" type="text/css" href="../../public/css/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/themes/icon.css">
	<script type="text/javascript" src="../../public/js/jquery.min.js"></script>
	<script type="text/javascript" src="../../public/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../public/js/ui.js"></script>
	
</head>
<body>
	<script>
	
	</script>
  <div class="easyui-panel" title="" style="width:600px;height:300px">
    <div style="padding:10px">
	<form id="frmMain" method="post" novalidate>
		
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    	
	    		<tr>
	    			<td>Year:</td>
	    			<td>
	    				<?php echo ui::form_select_fromArray('year', $years, date('Y')," onchange=\"list_subjects();\" ");  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Term:</td>
	    			<td>
	    				<?php echo ui::form_select_fromArray('term', $terms,'',"onchange=\"list_subjects();\"");  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td width="180px">Form</td>
	    			<td>
	    			<?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"list_subjects();\"");  ?>
	    			</td>
	    		</tr>
	    		
	    		
	    		
	    		<!--<tr>
	    			<td>Subject:</td>
	    			<td>
	    			   <div id="div_subjects">
	    				
	    				</div>
	    			</td>
	    		</tr>
	    		-->
	    		<tr>
	    			<td>&nbsp;</td>
	    			<td>
	    			 <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print'" onclick="print_report();">Open Report</a>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td colspan="2"><div id="div_grade"></div></td>
	    		</tr>
	    		
	    	</table>
 
     </form>
    </div>
   </div>
	
	<style scoped="scoped">
		.textbox{
			height:20px;
			margin:0;
			padding:0 2px;
			box-sizing:content-box;
		}
	</style>
	<script>
		
		function clearForm(){
			$('#frmMain').form('clear');
		}
		
		
		function list_subjects(){
            $.post('../../endpoints/crud/',   'module=<?php echo $module_packed; ?>&function=list_subjects&'+$('#frmMain').serialize()   , function( html ){
  		 	$('#div_subjects').html(html);
  		 });
		}
		
		
        function print_report(){
         var fdata = $('#frmMain').serialize();
  	     var w=window.open('../../endpoints/print/?module=<?php echo $module_packed; ?>&'+fdata,'p','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          w.focus();
        }
			
	</script>
	
</body>
</html>
