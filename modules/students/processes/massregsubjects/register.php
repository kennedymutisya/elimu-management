<?php

@ignore_user_abort(true); 
@set_time_limit(0); 
@ini_set("max_execution_time", 600000000);

$register = new register();
$register->subjects();

  class register{
	private $id;
	private $datasrc;
	private $primary_key;
	private $max_id;
	
	
	public function __construct(){
		global $cfg;
		
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function subjects() {
		global $db,$cfg;
		
        $yearcode     = filter_input(INPUT_GET , ui::fi('year'));
        $formstream   = filter_input(INPUT_GET , ui::fi('form'));

		if(strlen($formstream)>1){
		 $formcode            = $formstream;
		 $formstream_code_col = "STREAMCODE";
		}else{
		 $formcode            = substr($formstream,0,1);
		 $formstream_code_col = "FORMCODE";
		}
		
      
       if(empty($yearcode)){
        echo "<script>parent.$.messager.alert('Error','Select Year','error');</script>";	
       }elseif(empty($formcode)){
        echo "<script>parent.$.messager.alert('Error','Select Form','error');</script>";	
       }
	   
       
		$students = $db->Execute("
		SELECT DISTINCT ADMNO,FULLNAME,FORMCODE,STREAMCODE 
        FROM VIEWSP 
        WHERE YEARCODE='{$yearcode}'
        AND {$formstream_code_col}='{$formcode}'
        ORDER BY ADMNO
        ");
        
        if(!$students){
       	$err = $db->ErrorMsg();
       	 echo "<script>parent.$.messager.alert('Error','".$err."','error');</script>";
        }
        
        if(!$students){
       	$err = $db->ErrorMsg();
       	 echo "<script>parent.$.messager.alert('Error','".$err."','error');</script>";
        }
        
        $offered_subjects  = $db->CacheGetAssoc(1200,"SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS WHERE OFFERED = 1 ");
        
        if(!$offered_subjects){
       	$err = $db->ErrorMsg();
       	 echo "<script>parent.$.messager.alert('Error','".$err."','error');</script>";
        }
        
        $this->max_id  = generateID('SASTUDSUB');
        $num_records   = $students->RecordCount();
        $count         = 1;
         
         foreach($students as $student) {
			
		 $progress         = ($count/$num_records)*100;
		 $progress         = round($progress,0);
		 $admno            = valueof($student,'ADMNO');
		 $formcode         = valueof($student,'FORMCODE');
		 $streamcode       = valueof($student,'STREAMCODE');
		 $fullname         = valueof($student,'FULLNAME');
		 $success          = false;
		 
	     $reg = self::register_subjects( $admno, $formcode, $streamcode, $yearcode, $offered_subjects);
	     
		  echo "
		  <script>
           parent.\$('#".ui::fi('prg_regsub')."').progressbar({ value: {$progress}});
           parent.\$('#".ui::fi('div_form')."').html('F{$formcode} : <b>{$count}</b>/<b>{$num_records}</b>');
          </script>
		 ";
		
		  sleep(.9);
		  flush();
		  ++$count; 
			 
		 }
			
        echo "
		<script>
		  parent.alert('Done Registering Subjects');
          parent.\$('#btnRegister').linkbutton('enable');
          parent.\$('#".ui::fi('div_spinner_register')."').html('&nbsp;');
          /*parent.{$cfg['appname']}.print();*/
        </script>
		";

	}
    
    private function register_subjects( $admno, $formcode, $streamcode, $yearcode, $offered_subjects) 
	{
	global $db;
	//print_pre($offered_subjects);exit();
	 $user    = new user();
  	 $numSub  = sizeof($offered_subjects);
  	 $count  = 1;
     $terms  = array(1,2,3);
  	  if($numSub>0){
        foreach($offered_subjects as $subjectcode => $subjectname){
			foreach($terms as $termcode){
				
            $subject = new ADODB_Active_Record('SASTUDSUB',array('ADMNO','SUBJECTCODE','FORMCODE','STREAMCODE','YEARCODE','TERMCODE'));
            
        	$subject->Load("
        	ADMNO='{$admno}'
        	AND SUBJECTCODE='{$subjectcode}'
        	AND FORMCODE='{$formcode}'
        	AND STREAMCODE='{$streamcode}'
        	AND YEARCODE='{$yearcode}'
        	AND TERMCODE='{$termcode}'
        	");
        	
			 if(empty($subject->_original)){
			  $subject->id           = $this->max_id;
			  $subject->admno        = $admno;
			  $subject->subjectcode  = $subjectcode;
			  $subject->formcode     = $formcode;
			  $subject->streamcode   = $streamcode;
			  $subject->yearcode     = $yearcode;
			  $subject->termcode     = $termcode;
			  }
			
			  if($subject->Save()){
			   ++$count;
			   ++$this->max_id;
			  }
			  
           }//each term
	      }
	  	 }
	   
	return true;
		
	}
	
	
  }
      
