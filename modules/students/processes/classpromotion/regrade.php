<?php

require_once('../../../../init.php');
require_once('../../../../config/db.php');

/*
echo '<pre>';
 print_r($_GET);
echo '</pre>';
*/

$function = filter_input(INPUT_GET , 'function');


$regrademarks = new regrademarks();
$regrademarks->regrade();

  //classes
 function copy_classes() {
 global $source,$db;
 
 $db->Execute("DELETE FROM SASTREAMS");
 $data = $source->Execute("SELECT FORM,CLASS FROM CLASSES ORDER BY FORM");
  if(($data)){
  	
  	 $RecordCount = $data->RecordCount();
  	 $count=1;
  
  	  if($RecordCount>0){
        while (!$data->EOF){
        
            $formcode   = isset($data->fields['FORM']) ? $data->fields['FORM'] : '';
            $streamcode = isset($data->fields['CLASS']) ? $data->fields['CLASS'] : '';
            
            $record = new ADODB_Active_Record('SASTREAMS',array('STREAMCODE'));
        	$record->Load("STREAMCODE='{$streamcode}'");
        	
			if(empty($record->_original)){
			 $record->id          = generateID($record->_tableat, 'ID', $db);;
			 $record->formcode    = $formcode;
			 $record->streamcode  = $streamcode;
			 $record->streamname  = $streamcode;
			}
		
			 $record->Save();
        	
        ++$count;
            
		$data->MoveNext();
		
		$progress= ($count/$RecordCount)*100;
	
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_copy_classes').progressbar({
            value: {$progress}
            });
</script>
		";
        }
  	  }
  	}
  }	

  class regrademarks{
	private $id;
	private $datasrc;
	private $primary_key;
	private $regno;
	private $positioning = array();
	
	private $_formcode;
	private $_streamcode;
	private $_yearcode;
	private $_termcode;
	private $_exam;
	private $_position_by_marks_col;
	private $_position_by_points_col;
	private $_numsubjects;
	private $_minsubjects;
	
	private $_cat_required   = array();
	private $_cat_done       = array();
	private $_cat_picked     = array();
	private $_cat_dropped    = array();
	private $_cat_mandatory  = array();
	private $_cat_mandatory_missing  = array();
	private $_comnination_codes  = array();
	
	private $_setupmax_col;
	private $_setupmax;
	private $_setup  = array();
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_GET , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function regrade() {
		global $db;
//		$db->debug=1;
//		print_pre($_GET);
       $yearcode  = filter_input(INPUT_GET , 'year');
       $termcode  = filter_input(INPUT_GET , 'term');
       $form_stream_code  = filter_input(INPUT_GET , 'form');
       $formcode     = filter_input(INPUT_GET , 'form');
       $exam         = filter_input(INPUT_GET , 'exam');
       $disp_score   = filter_input(INPUT_GET , 'subjscore');
       
       if(empty($exam)){
        echo "<script>parent.$.messager.alert('Error','Please Exam Global Settings Form {$formcode} , {$yearcode} - Term {$termcode} ','error');</script>";	
        die;
       }

       switch ($exam){
       	case 'CAT1':
       	 $this->_setupmax_col = 'CATMAX1';
       	 $exam_name_col = 'CATNAME1';
       	break;	
       	case 'CAT2':
       	 $this->_setupmax_col = 'CATMAX2';
       	 $exam_name_col = 'CATNAME2';
       	break;	
       	case 'CAT3':
       	 $this->_setupmax_col = 'CATMAX3';
       	 $exam_name_col = 'CATNAME3';
       	break;	
       	case 'CAT4':
       	 $this->_setupmax_col = 'CATMAX4';
       	 $exam_name_col = 'CATNAME4';
       	break;	
       	case 'CAT5':
       	 $this->_setupmax_col = 'CATMAX5';
       	 $exam_name_col = 'CATNAME5';
       	break;	
       	case 'PAPER1':
       	 $this->_setupmax_col = 'EXAMMAX1';
       	 $exam_name_col = 'EXAMNAME1';
       	break;	
       	case 'PAPER2':
       	 $this->_setupmax_col = 'EXAMMAX2';
       	 $exam_name_col = 'EXAMNAME2';
       	break;	
       	case 'PAPER3':
       	 $this->_setupmax_col = 'EXAMMAX3';
       	 $exam_name_col = 'EXAMNAME3';
       	break;	
       	case 'PAPER4':
       	 $this->_setupmax_col = 'EXAMMAX4';
       	 $exam_name_col = 'EXAMNAME4';
       	break;	
       	case 'PAPER5':
       	 $this->_setupmax_col = 'EXAMMAX5';
       	 $exam_name_col = 'EXAMNAME5';
       	break;	
       	case 'TOTAL':
       	 $this->_setupmax_col = 'EXAMMAX1';
       	 $exam_name_col = 'TOTAL';
       	break;	
       }
       
       
	   if(strlen($form_stream_code)==1){
	 	$form_stream = 'FORMCODE';
	   }else{
	   	$form_stream = 'STREAMCODE';
	   	$formcode = substr($formcode,0,1);
	   }
	   
       $this->_exam  = $exam;
       
       if(strstr($exam,'CAT') || strstr($exam,'PAPER')){
        $pointscol = "{$exam}POINTS";
        $this->_position_by_marks_col  = $exam;
        $this->_position_by_points_col = $pointscol;
       }else{
       	$pointscol = "POINTS";
       	$this->_position_by_marks_col = "TOTALM";
       	$this->_position_by_points_col = "TOTALP";
       	
       }
       
       $pointscol = strtoupper($pointscol);
       
       $settings = $db->GetAssoc("select SBCCODE,REQUIRED,MANDATORY from SAFCS 
       WHERE YEARCODE='{$yearcode}' 
       AND TERMCODE='{$termcode}' 
       AND FORMCODE='{$formcode}'
       ");
      
       if(empty($settings)){
        echo "<script>parent.$.messager.alert('Error','Please Setup Required Subjects for Form {$formcode} , {$yearcode} - Term {$termcode} ','error');</script>";	
        	die;
       }

       $this->_setup =  $db->GetRow("SELECT * FROM SATSSG WHERE FORMCODE='{$formcode}' AND YEARCODE='{$yearcode}' AND TERMCODE='{$termcode}'");
       
       if(empty($this->_setup)){
        echo "<script>parent.$.messager.alert('Error','Please Do Exam Global Setup for Form {$formcode} , {$yearcode} - Term {$termcode} ','error');</script>";	
        	die;
       }
       
       if($exam=='TOTAL'){
       $this->_setupmax         = (100);
//       $this->_setupmax         = valueof($this->_setup, $this->_setupmax_col,100);
       }else{
       $this->_setupmax         = valueof($this->_setup, $this->_setupmax_col,100);
       }
       
       $exam_name               = valueof($this->_setup, $exam_name_col,$exam_name_col);
       $exams_config_gsyscode   = valueof($this->_setup, 'GSYSCODE');

       $data_students = $db->GetAssoc("SELECT DISTINCT SUBJECTCODE,SUBJECTNAME
        FROM VIEWSTUDENTSUBJECTS 
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}' 
        AND {$form_stream}='{$form_stream_code}'
        AND {$exam}>0
        ORDER BY SUBJECTCODE
        ");
       
       if(!$data_students){
       	$err = $db->ErrorMsg();
       	echo "<script>parent.$.messager.alert('Error','".$err."','error');</script>";
       	die;
       }
       
       foreach ($data_students as $subjectcode=>$subjectname) {
       	 $subjectname = Camelize($subjectname);
       
       $data = $db->Execute("SELECT ID,ADMNO,SUBJECTCODE,{$exam} FROM VIEWSTUDENTSUBJECTS 
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}' 
        AND {$form_stream}='{$form_stream_code}'
        AND {$exam}>0
        AND SUBJECTCODE='{$subjectcode}'
        ORDER BY STREAMCODE, ADMNO, SORTPOS,SUBJECTCODE
        ");
       
	  	  
       if($data) {
       	    $num_records = $data->RecordCount();
       	if($num_records>0){
        	$num_regraded  =0;	
        	
  	      $count=1;
  
       	 while (!$data->EOF) {
       	 	
       	 	$id            = valueof($data->fields, 'ID');	
       	 	$admno         = valueof($data->fields, 'ADMNO');	
       	 	$subjectcode   = valueof($data->fields, 'SUBJECTCODE');	
       	 	$score         = valueof($data->fields, $exam);	
       	 	$column        = $exam;
       	 	
       	 	if($score>$this->_setupmax){
       	 	 echo "<script>parent.$.messager.alert('Error','Student {$admno} has {$subjectcode}={$score} and the {$exam_name} Exam Setup has a max of {$this->_setupmax}','error');</script>";	
       	 	}
       	 	
       	 	
       	 	$save_score  = self::save_score($id, $score, $exam);
       	 	
       	 	if(!is_bool($save_score)){
       	 	 $save_score_json = json_decode($save_score);
       	 	 echo "<script>parent.$.messager.alert('Error','{$save_score_json->message}','error');</script>";	
       	 	}else{
       	 	 ++$num_regraded;	
       	 	}
       	 	
       	 	
       	   $data->MoveNext();
       	   
       	   
		$progress = ($count/$num_records)*100;
		$progress = round($progress,0);
		
		echo "
		<script>
          parent.\$('#prg_regrade').progressbar({ value: {$progress}});
          parent.\$('#div_grade').html('{$subjectname} : <b>{$count}</b>/<b>{$num_records}</b>');
        </script>
		";
		
		sleep(.7);
		flush();
		++$count;

       	  }//while
       	  
         }
       }
      }
      
        echo "
		<script>
          parent.print_marks();
          parent.\$('#btnProc').linkbutton('enable');
          parent.\$('#div_spinner').html('&nbsp;');
        </script>
		";
          

	}
    
    private function Get_Grade( $gradesyscode='KN',$total ){
    	global $db;
      $total         = $total>0 ? $total : 0;
      return $db->CacheGetRow(1200,"SELECT GRADECODE,POINTS,COMMENTS FROM SAXGRD WHERE GSYSCODE='{$gradesyscode}' AND MIN<={$total} AND MAX>={$total}", 0);
    }
    
	private  function Get_Comments($subjectcode, $gradecode ){
	  	 global $db;
	  	 return $db->CacheGetOne(120,"SELECT COMMENTS FROM SASUBREMARKS WHERE SUBJECTCODE='{$subjectcode}' AND GRADECODE='{$gradecode}'");
	}
	  
    private function save_score($id, $score, $column){
		global $db,$cfg;
//	$db->debug=1;
	
    $column = strtolower($column);
    
	$record = new ADODB_Active_Record('SASTUDSUB', array('ID'));
	$record->Load("ID={$id}");
	
	if(empty($record->_original)){
	 return json_response(0,"Record ID {$id} not found");
	}
	
	$formcode     =  $record->formcode;
	$yearcode     =  $record->yearcode;
	$termcode     =  $record->termcode;
	$subjectcode  =  $record->subjectcode;
	$streamcode   =  $record->streamcode;
    
       $count_cats   = 0;
       $count_exams   = 0;
       
       for ($c=1;$c<=5;++$c){
        if(isset($this->_setup["CATMAX{$c}"])){
      	 if($this->_setup["CATMAX{$c}"]>0){
      		++$count_cats;
      	 }
        }
       }
     
     for ($x=1;$x<=5;++$x){
      if(isset($this->_setup["EXAMMAX{$x}"])){
      	if($this->_setup["EXAMMAX{$x}"]>0){
      		++$count_exams;
      	}
      }
     }
    
    $catmax1          =  valueof($this->_setup, 'CATMAX1');
    $catmax2          =  valueof($this->_setup, 'CATMAX2');
    $catmax3          =  valueof($this->_setup, 'CATMAX3');
    $catmax4          =  valueof($this->_setup, 'CATMAX4');
    $catmax5          =  valueof($this->_setup, 'CATMAX5');
    
    $exammax1         =  valueof($this->_setup, 'EXAMMAX1');
    $exammax2         =  valueof($this->_setup, 'EXAMMAX2');
    $exammax3         =  valueof($this->_setup, 'EXAMMAX3');
    $exammax4         =  valueof($this->_setup, 'EXAMMAX4');
    $exammax5         =  valueof($this->_setup, 'EXAMMAX5');
    
    $contc         =  valueof($this->_setup, 'CONTC');
    $conte         =  valueof($this->_setup, 'CONTE');
    $cont          =  $contc + $conte;
    $gradesyscode  =  valueof($this->_setup, 'GSYSCODE');
    
    $special_subjects = array('CHM','PHY','BIO');
    $subject_is_special = false;
    
	if($formcode==3 || $formcode==4){
	 if(array_search($subjectcode, $special_subjects) || in_array($subjectcode, $special_subjects)){
	 	$subject_is_special = true;
	 }
	}
	
	if(empty($formcode)){
	 return json_response(0," missing variable 'formcode'");	
	}	
	
	if(empty($gradesyscode)){
	 return json_response(0," missing grading system for {$streamcode}");	
	}
	
    if($contc==100 && $conte==100){
    	
    }else{
     if($cont!=100){
      return json_response(0,"CAT & EXAM contributions should total 100");
     }
    }
    
	if(!empty($column)){
	  $record->$column = $score;	
	}
	
	if(!empty($column)){
		
	  switch($column){
	  	case 'cat1':	
	     $avg_this             =  ($score/$catmax1) * 100;
	     break;
	     case 'cat2':	
	     $avg_this             =  ($score/$catmax2) * 100;
	     break;
	     case 'cat3':	
	     $avg_this             =  ($score/$catmax3) * 100;
	     break;
	     case 'cat4':	
	     $avg_this             =  ($score/$catmax4) * 100;
	     break;
	     case 'cat5':	
	     $avg_this             =  ($score/$catmax5) * 100;
	     break;
	     case 'paper1':	
	     $avg_this             =  ($score/$exammax1) * 100;
	     break;
	     case 'paper2':	
	     $avg_this             =  ($score/$exammax2) * 100;
	     break;
	     case 'paper3':	
	     $avg_this             =  ($score/$exammax3) * 100;
	     break;
	     case 'paper4':	
	     $avg_this             =  ($score/$exammax4) * 100;
	     break;
	     case 'paper5':	
	     $avg_this             =  ($score/$exammax5) * 100;
	     break;
	     case 'total':	
	     $avg_this             =  ($score/100) * 100;
	     break;
	  }
	 	
//	  echo "\$column={$column} <br>";
//	  echo "\$score={$score} <br>";
//	  echo "\$avg_this={$avg_this} <br>";
//	  exit();
	  
	  $avg_this             = round($avg_this,2);
	  $exam_per_column      = "{$column}p";
	  $gradecode_column     = "{$column}gradecode";
	  $points_column        = "{$column}points";
	  $comments_column      = "{$column}comments";
	  
	  $record->$column      = $score;
	  
	  $Get_Grade_Exam       = self::Get_Grade( $gradesyscode , $avg_this);	
	  
//	  echo "\$avg_this={$avg_this} <br>";
//	  echo "\$exam_per_column={$exam_per_column} <br>";
//	  echo "\$gradecode_column={$gradecode_column} <br>";
//	  echo "\$points_column={$points_column} <br>";
//	  echo "\$column={$column} <br>";
//	  print_pre($Get_Grade_Exam);
	  
//	  exit();
	  
	  if(is_array($Get_Grade_Exam)){
	   $record->$exam_per_column    = $avg_this;
	   $record->$gradecode_column   = valueof($Get_Grade_Exam, 'GRADECODE');
	   $record->$points_column      = valueof($Get_Grade_Exam, 'POINTS');
//	   $record->$comments_column    = valueof($Get_Grade_Exam, 'COMMENTS');
	  }
	  
	  $subject_Comments            = self::Get_Comments( $record->subjectcode , $record->$gradecode_column);
	  $record->$comments_column    = $subject_Comments;	

	}
	
   if($contc==100 && $conte==100) {	
   	
	if($count_cats>0){
	 $catcont1        =  $record->cat1>0 && $catmax1>0 ? $record->cat1  : 0;
	 $catcont2        =  $record->cat2>0 && $catmax2>0 ? $record->cat2  : 0;
	 $catcont3        =  $record->cat3>0 && $catmax3>0 ? $record->cat3  : 0;
	 $catcont4        =  $record->cat4>0 && $catmax4>0 ? $record->cat4  : 0;
	 $catcont5        =  $record->cat5>0 && $catmax5>0 ? $record->cat5  : 0;
	 $avgcat          =  ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) / $count_cats;
	 $record->avgcat  =  round($avgcat,0);
	}
	
	if($count_exams>0){
		
	  $examcont1        =  $record->paper1>0 && $exammax1>0 ? $record->paper1  : 0;
	  $examcont2        =  $record->paper2>0 && $exammax2>0 ? $record->paper2  : 0;
	  $examcont3        =  $record->paper3>0 && $exammax3>0 ? $record->paper3  : 0;
	  $examcont4        =  $record->paper4>0 && $exammax4>0 ? $record->paper4  : 0;
	  $examcont5        =  $record->paper5>0 && $exammax5>0 ? $record->paper5  : 0;
	  	
	 if(!$subject_is_special) {	
	  $avgexam          =  ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5) / $count_exams;
	  $record->avgexam  =  round($avgexam,0);
	  $total            = ( ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5)  + ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5)) / ($count_cats+$count_exams);
	 }else{
	  $exammaxs         = ($exammax1 + $exammax2);
	  $examcont_avg     = ($examcont1 + $examcont2 ) / ($exammax1+$exammax2);
	  $kamaa            = ($examcont_avg * 60) + $examcont3;
	  $njooro           = $kamaa * ( $conte/100);
	  $avgexam          =  $njooro;
	  $record->avgexam  =  round($avgexam,0); 	
	  $total            = ( ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5)  + ($record->avgexam)) / ($count_cats+1);	  
	  
	 }
	 
	}
	
	$record->total = round($total);
	
   }else{//else not 100-100
   	
	if($count_cats>0){
	 $catcont1        =  $record->cat1>0 && $catmax1>0 ? $record->cat1/$catmax1 * $contc : 0;
	 $catcont2        =  $record->cat2>0 && $catmax2>0 ? $record->cat2/$catmax2 * $contc : 0;
	 $catcont3        =  $record->cat3>0 && $catmax3>0 ? $record->cat3/$catmax3 * $contc : 0;
	 $catcont4        =  $record->cat4>0 && $catmax4>0 ? $record->cat4/$catmax4 * $contc : 0;
	 $catcont5        =  $record->cat5>0 && $catmax5>0 ? $record->cat5/$catmax5 * $contc : 0;
	 $avgcat          =  ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) / $count_cats;
	 $record->avgcat  =  round($avgcat,0);
	}
	
	if($count_exams>0){
	  
	 if( (!$subject_is_special) || ($count_exams==1) ) {	
	   $examcont1        =  $record->paper1>0 && $exammax1>0 ? $record->paper1/$exammax1 * $conte : 0;
	   $examcont2        =  $record->paper2>0 && $exammax2>0 ? $record->paper2/$exammax2 * $conte : 0;
	   $examcont3        =  $record->paper3>0 && $exammax3>0 ? $record->paper3/$exammax3 * $conte : 0;
	   $examcont4        =  $record->paper4>0 && $exammax4>0 ? $record->paper4/$exammax4 * $conte : 0;
	   $examcont5        =  $record->paper5>0 && $exammax5>0 ? $record->paper5/$exammax5 * $conte : 0;
	 	
	   $avgexam          =  ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5) / $count_exams;
	   $record->avgexam  =  round($avgexam,0);
	   $total            = $record->avgcat + $record->avgexam;
	   
	 }else{
	   
	  $examcont1        =  $record->paper1>0 && $exammax1>0 ? $record->paper1  : 0;
	  $examcont2        =  $record->paper2>0 && $exammax2>0 ? $record->paper2  : 0;
	  $examcont3        =  $record->paper3>0 && $exammax3>0 ? $record->paper3  : 0;
	  $examcont4        =  $record->paper4>0 && $exammax4>0 ? $record->paper4  : 0;
	  $examcont5        =  $record->paper5>0 && $exammax5>0 ? $record->paper5  : 0;
	  	
	  $exammaxs         =  ($exammax1 + $exammax2);
	  $examcont_avg     =  ($examcont1 + $examcont2 ) / ($exammax1+$exammax2);
	  $kamaa            =  ($examcont_avg * 60) + $examcont3;
	  $njooro           =  $kamaa * ( $conte/100);
	  $avgexam          =  $njooro;
	  $record->avgexam  =  round($avgexam,0); 	
	  $total            =  $record->avgcat + $record->avgexam;

	 }
	 
	 $record->total     = round($total);
	 
	 }
	
    }

	$Get_Grade             = self::Get_Grade( $gradesyscode , $record->total);
	
	if(is_array($Get_Grade)){
	 $record->gradecode     = valueof($Get_Grade, 'GRADECODE');
	 $record->points        = valueof($Get_Grade, 'POINTS');
	}
	
	 $subject_Comments     = self::Get_Comments( $record->subjectcode , $record->gradecode);
	 $record->comments     = $subject_Comments;	
	  
    if( $record->Save() ){
     return true;
    }else{
     return false;
    }
    
   }
  }
      