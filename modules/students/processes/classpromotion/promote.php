<?php

@ignore_user_abort(true); 
@set_time_limit(0); 
@ini_set("max_execution_time", 600000000);

$promotion = new promotion();
$promotion->promote();

  class promotion{
	private $id;
	private $datasrc;
	private $primary_key;
	private $max_id;
	private $promoted = array();
	
	
	public function __construct(){
		global $cfg;
		
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function promote() {
		global $db,$cfg;
       
       $yearcode          = filter_input(INPUT_GET , ui::fi('year'));
      
       if(empty($yearcode)){
        echo "<script>parent.$.messager.alert('Error','Select Year','error');</script>";	
       }
        
        $to_yearcode = $yearcode+1;
        $to_termcode = 1;
        
        $forms  = $db->GetAssoc(" SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE DESC ");
       
        if(!$forms){
       	$err = $db->ErrorMsg();
       	 echo "<script>parent.$.messager.alert('Error','".$err."','error');</script>";
        }
        
        $this->max_id        = generateID('SASTUDSUB');
        
        foreach($forms as $formcode => $formname){
		
		$students = $db->Execute("
		SELECT DISTINCT ADMNO,FULLNAME,STREAMCODE 
        FROM VIEWSP 
        WHERE YEARCODE='{$yearcode}'
        AND FORMCODE='{$formcode}'
        ORDER BY ADMNO
        ");
       
         if(!$students){
       	  $err = $db->ErrorMsg();
       	  echo "<script>parent.$.messager.alert('Error','".$err."','error');</script>";
         }
       
        
         $num_records = $students->RecordCount();
         $count       = 1;
         
         foreach($students as $student) {
			
		 $progress         = ($count/$num_records)*100;
		 $progress         = round($progress,0);
		 $admno            = valueof($student,'ADMNO');
		 $streamcode       = valueof($student,'STREAMCODE');
		 $success          = false;
		 
		   if($formcode==4){
			$success  = $db->Execute("update SASTUDENTS set COMPLETED=1 where ADMNO='{$admno}' "); 
	       }else{
		    $to_formcode    = $formcode+1;
		    $to_streamcode  = str_replace($formcode, $to_formcode, $streamcode);
		    $success        =  self::promote_student( $admno, $formcode, $yearcode, $to_yearcode, $to_termcode, $to_formcode, $to_streamcode  );
	       }
	       
	       if($success){
			   // :)
		   }
	       
		   
		  echo "
		  <script>
           parent.\$('#".ui::fi('prg_promotion')."').progressbar({ value: {$progress}});
           parent.\$('#".ui::fi('div_form')."').html('{$formname} : <b>{$count}</b>/<b>{$num_records}</b>');
          </script>
		 ";
		
		  sleep(.7);
		  flush();
		  ++$count; 
			 
		 }
			
		}
      
        echo "
		<script>
		  parent.alert('Done Promoting Students');
          parent.\$('#btnPromote').linkbutton('enable');
          parent.\$('#".ui::fi('div_spinner_promotion')."').html('&nbsp;');
          parent.{$cfg['appname']}.print_class_list();
        </script>
		";

	}
    
    function finals_student_move_profile( $admno ) 
	{
		global $db;
		
		$student = $db->GetRow("select * from  SASTUDENTS where ADMNO='{$admno}' ");
		
		if(!empty($student)){
			
		 $move = $db->AutoExecute('SASTUDENTSC',$student,'INSERT');
		 
		 if($move){
			 
		  $remove = $db->Execute("delete from  SASTUDENTS where ADMNO='{$admno}' "); 
		  
		  if($remove) return true;
		  
		 }
		 
		}
		
		return false;
		
	}
    
    function finals_student_move_exams( $admno  ) 
	{
		global $db;
		
		$data = $db->Execute("select * from  SASTUDSUB where ADMNO='{$admno}' ");
		
		if(!empty($data)){
			
		  $rows        = $data->RecordCount();
		  $rows_moved  = 0;
		  
		  foreach($data as $row){
			  
		   $move = $db->AutoExecute('SASTUDSUBC',$row,'INSERT');
		   
		   if($move){
			  ++$rows_moved;
		   }
		 }
		}
		
		if($rows==$rows_moved){
			$remove = $db->Execute("delete from  SASTUDSUB where ADMNO='{$admno}' "); 
		}
		
		return true;
		
	}
	
    
    function promote_student( $admno, $formcode, $from_yearcode, $to_yearcode, $to_termcode, $to_formcode, $to_streamcode  ) 
	{
		global $db;
		$terms     = array(1,2,3);
		$user      = new user();
		$subjects  = $db->GetAssoc("select ID,SUBJECTCODE from  SASTUDSUB where ADMNO='{$admno}' AND FORMCODE='{$formcode}' AND YEARCODE='{$from_yearcode}' AND TERMCODE='3' ");
		
		
		if(!empty($subjects)) {
			
		  $rows         = sizeof($subjects);
		  $rows_copied  = 0;
		  $subject      = array();
		  $sql_inserts  = array();
		  
		  $db->Execute("delete from  SASTUDSUB where ADMNO='{$admno}' AND FORMCODE='{$to_formcode}' AND YEARCODE='{$to_yearcode}' ");
		  
		  foreach($subjects as $subjectcode){
		   foreach($terms as $termcode){
		    $sql_inserts[] = array($this->max_id, $admno, $subjectcode , $to_formcode, $to_streamcode, $to_yearcode, $termcode, $user->userid , date("Y-m-d") , time() );
		    ++$this->max_id;
		   }
		  }
		 
		   $db->StartTrans();
  	  
           $sql_statement = $db->Prepare('INSERT INTO SASTUDSUB
           (ID,ADMNO,SUBJECTCODE,FORMCODE,STREAMCODE,YEARCODE,TERMCODE,AUDITUSER,AUDITDATE,AUDITTIME)
  	        values 
  	        ('.$db->Param('0').','.$db->Param('1').','.$db->Param('2').','.$db->Param('3').','.$db->Param('4').','.$db->Param('5').','.$db->Param('6').','.$db->Param('7').','.$db->Param('8').','.$db->Param('9').')');
  	 
		    foreach ($sql_inserts as $sql_insert){
		     $db->Execute($sql_statement, $sql_insert);
		    }
		 
		    $db->CompleteTrans();
  	
		}
		
		$promote = $db->Execute("
		 update SASTUDENTS set STREAMCODE='{$to_streamcode}',YEARCODE='{$to_yearcode}' where ADMNO ='{$admno}' and ADMNO in
		  (
		  select distinct (ADMNO) from SASTUDSUB where ADMNO='{$admno}' AND FORMCODE='{$to_formcode}' AND YEARCODE='{$to_yearcode}'   group by ADMNO
		  )
		"); 
		
		if($promote) return true;
		
		return false;
		
	}
	
	
  }
      
