<?php

 $school = new school();
 $years  = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 
 ?>
<style>
div#<?php echo ui::fi('div_grade'); ?> { color:#500;}
</style>

   <div class="easyui-panel" title=""  style="font-size:12px;font-family:Verdana;"  fit="true">
    <div style="padding:10px">
	 <form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		
	    	<table cellpadding="2" cellspacing="0" width="100%" >
	    	
	    		<tr>
	    			<td>Promote from Year:</td>
	    			<td><?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode,"");  ?></td>
	    		</tr>
	    			    		
	    		<tr>
	    			<td>Status</td>
	    			<td><div id="<?php echo ui::fi('div_form'); ?>">&nbsp;</div></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Progress</td>
	    			<td><div id="<?php echo ui::fi('prg_promotion'); ?>" class="easyui-progressbar" data-options="value:0" style="width:200px;"></div></td>
	    		</tr>
	    		
	    		<tr>
	    			<td><div id="<?php echo ui::fi('div_spinner_promotion'); ?>">&nbsp;</div></td>
	    			<td><a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.promote();" id="btnPromote"><i class="fa fa-spinner"></i>  Promote</a></td>
	    		</tr>
	    		
	    	</table>
 
     </form>
    </div>
   </div>
	<iframe id="<?php echo ui::fi('fr'); ?>" frameborder="0" height="0px" width="100%" src="" scrolling="no"></iframe>
	<script>
		
		var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		promote:function (){
		   $.messager.confirm('Confirm','Are you sure you run Class Promotion?',function(r){
			if (r){
			 $.messager.progress();
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize()  + '&modvars=<?php echo $vars; ?>&function=pre_run';
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
             if (data.success === 1) {
                $('#<?php echo ui::fi('prg_promotion'); ?>').progressbar({ value: 0 });
                $('#btnPromote').linkbutton('disable');
                $('#<?php echo ui::fi('div_spinner_promotion'); ?>').html('<img src="./public/images/spinner.gif" >');
                $('#<?php echo ui::fi('fr'); ?>').attr('src', './endpoints/go/?to=promote.php&modvars=<?php echo $vars; ?>&'+$('#<?php echo ui::fi('ff'); ?>').serialize());
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		  });
		},
        print_class_list:function (){
         var fdata      = $('#<?php echo ui::fi('ff'); ?>').serialize();
		 var <?php echo ui::fi('w'); ?>=window.open('../../endpoints/print/?modvars=<?php echo $vars; ?>&'+fdata,'p','height=800,width=900,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          if(<?php echo ui::fi('w'); ?>){
			 <?php echo ui::fi('w'); ?>.focus();
		  }else{
			  alert('Allow Popups to Open from this Web Address');
		  }
	   },
     }
	</script>
