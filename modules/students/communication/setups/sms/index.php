<?php

/**
* auto created index file for modules/students/communication/setups/sms
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-08 09:07:46
*/

 class IgnorantRecursiveDirectoryIterator extends RecursiveDirectoryIterator {

  function getChildren() {
    try {
     return new IgnorantRecursiveDirectoryIterator($this->getPathname());
    }catch(UnexpectedValueException $e) {
     return new RecursiveArrayIterator(array());
   }
  }

 }
 
$gateways_source =  (ROOT .'classes/sms/');

if(!is_readable($gateways_source)) echo ("Directory <b>'{$gateways_source}'</b> does not exist or is not readable!");

   $gateways_classes = array();
   $replace = array('.php','class.');

   try {
    $it = new RecursiveIteratorIterator(new IgnorantRecursiveDirectoryIterator($gateways_source), RecursiveIteratorIterator::SELF_FIRST );
    foreach( $it as $fullFileName => $fileSPLObject )
         if(!is_dir($fullFileName))
         {
          if (@eregi("^\.{1,2}$|\.(php)$", $fullFileName)) {
             $pathinfo    =  pathinfo($fullFileName);

             $filename    =  $pathinfo['basename'];
             $pretty_name = str_replace($replace,'',$filename);

             if($filename !='class.sms.php'){
              $gateways_classes[$filename] = $pretty_name;
		     }
          }
         }
   }
    catch (UnexpectedValueException $e) {
     printf("Directory [%s] contained a directory we can not recurse into", $dir);
   }

?>
<div id="content<?php echo MNUID; ?>"  class="easyui-panel" title="" fit="true" >
	  <form id="<?php echo ui::fi('ff'); ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="120px">SMS Gateway:</td>
			<td>
			 <?php echo ui::form_select_fromArray('gateway', $gateways_classes, ''," onchange=\"{$cfg['appname']}.list_params()\" ",false,200);  ?>
			</td>
		</tr>
		
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td colspan="2">
			 <div id="<?php echo ui::fi('div_params'); ?>">
			 GateWay Parameters
			 </div>
			</td>
		</tr>

	 </table>
	</form>
  </div>

<script>
 var <?php echo $cfg['appname']; ?> = {
   list_params:function (){
	$.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_params&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  	 $('#<?php echo ui::fi('div_params'); ?>').html(html);
  	});
   },
   save:function(){
  	    var fdata = 'modvars=<?php echo $vars; ?>&function=save&'+$('#<?php echo ui::fi('ff'); ?>').serialize();
  	    $.messager.progress();
		$.post('./endpoints/crud/', fdata, function(data) {
		if (data.success === 1) {
		  $.messager.progress('close');
		  $.messager.alert('Status',data.message,'info');
          <?php echo $cfg['appname']; ?>.list_params();
         } else {
          $.messager.alert('Error',data.message,'error');
         }
        }, "json");
   },
 }
</script>
