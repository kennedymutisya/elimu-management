<?php

$dateNow     = textDate(null, true);
$school      = new school();

$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$sortcol     = 'ADMNO';

$students_in = array();

if(isset($_GET['sms_general_streams'])){
 foreach($_GET['sms_general_streams'] as $streamcode){
  $students_in[] = $streamcode;
 }
}

$students_in_sql = "('" . implode("','", $students_in) ."')";

$data  = $db->Execute("
        SELECT ADMNO,FULLNAME,STREAMNAME,MOBILEPHONE FROM VIEWSP 
        where STREAMCODE in {$students_in_sql} AND (MOBILEPHONE IS NULL OR MOBILEPHONE='' OR {$db->length}(MOBILEPHONE)<10)  
        ORDER BY STREAMCODE, {$sortcol}
        ");


 ?>
 <html>
  <head>
   <title>Students Without Contact Mobilephone No</title>
  <script language="JavaScript" type="text/javascript">
    setTimeout("window.print();", 10000);
</script>
<style>
 body {
 padding : 10px;
 margin : 10px;
 font-size:9px;
 }
    table {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    td {
        border:1px solid #ccc;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:10px;
    }

    td.bold {
        font-weight:bold;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

  </head>
 <body>
 <?php
 if($data) {
 	
 	 	  echo "
 	 	  <table width=\"100%\"  style=\"border:0;\"  cellspacing=\"0\" cellpadding=\"3\" class=\"data\">
 	 	  <tr>
 	 	   <td colspan=\"5\">
 	 	 
 	 	   <table width=\"100%\"  style=\"border:0;\"  cellspacing=\"0\" cellpadding=\"1\" class=\"data\">
 	 	   
 	 	    <tr>
 	 	     <td rowspan=\"6\" style=\"border:0;\" ><img src=\"{$school->logo_path}\" ></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td valign=\"top\"  colspan=\"3\" style=\"border:0;\" ><span class=\"title\">{$school->name}</span></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"3\" style=\"border:0;\" ><b>Address : {$school->address}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"3\" style=\"border:0;\" ><b>Tel :{$school->telephone}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"3\" style=\"border:0;\" ><b>Vision :{$school->vision}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"3\" style=\"border:0;\" >&nbsp;</td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0;\" ></td>
 	 	     <td colspan=\"2\"  style=\"border:0;\" align=\"right\"><b>Date : </b>{$dateNow}</td>
 	 	    </tr>
 	 	    
 	 	   </table>
 	 	 </td>
 	 	 </tr>";
 	 	
 	   if($data->RecordCount()>0) {
 	   	$count= 1;
 	   			 	
 	 	 echo "<tr>";
 	 	  echo "<td colspan=\"5\"><h4>STUDENTS WITHOUT CONTACT PHONE NUMBER</h4></td>";
 	 	 echo "</tr>";
 	 	 
 	 	 echo "<tr>";
 	 	  echo "<td>No</td>";
 	 	  echo "<td>STREAM</td>";
 	 	  echo "<td>ADM.NO</td>";
 	 	  echo "<td>NAME</td>";
 	 	  echo "<td>Mobile Phone</td>";
 	 	 echo "</tr>";
 	 	 
 		while (!$data->EOF) {
       	 	
       	 	$admno        = valueof($data->fields, 'ADMNO');	
       	 	$fullname     = valueof($data->fields, 'FULLNAME');	
       	 	$formcode     = valueof($data->fields, 'FORMCODE');
       	 	$streamcode   = valueof($data->fields, 'STREAMNAME');
       	 	$mobilephone  = valueof($data->fields, 'MOBILEPHONE');
       	 	
 	 	 echo "<tr>";
 	 	  echo "<td>{$count}</td>";
 	 	  echo "<td>{$streamcode}</td>";
 	 	  echo "<td>{$admno}</td>";
 	 	  echo "<td>{$fullname}</td>";
 	 	  echo "<td>{$mobilephone}</td>";
 	 	 echo "</tr>";
 	 	 
 	 	 ++$count;
    
       	  $data->MoveNext();
       	 }

       	 --$count; 
 	 	 echo "<tr>";
 	 	  echo "<td colspan=\"5\"><b>Total : {$count}</b></td>";
 	 	 echo "</tr>";
 	 	 
 	   }
 	  }
 	 	
 	 	echo "</table>";
 	 	
 ?>
 </body>
  </html>
 
