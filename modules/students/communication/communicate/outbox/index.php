<?php

/**
* auto created index file for modules/students/communication/communicate/outbox
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2017-02-19 07:08:14
*/

require_once('cfg.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");

 if(!class_exists($class)){
    die(' ' .$classfile. ' Not Found');
 }

 $_class = new $class();
 $grid   = new grid();
 $grid->draw_page_header( );
 $grid->draw_grid( $module_packed );
 $grid->draw_form_simple();
