<?php

/**
* auto created config file for modules/students/communication/communicate/masssms
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-22 08:38:00
*/

 final class masssms {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 public function data_streams(){
		global $db, $cfg;

	$rs        =  $db->SelectLimit("select STREAMCODE CODE,STREAMNAME NAME from VIEWSTREAMS order by FORMCODE,STREAMCODE ",20);
	
	$checked_codes_array =  array();
	
	$json_cols = array('code','name','ck_form');
	
	$str = '';
	if($rs){
		$numRecs = $rs->RecordCount();
		$numCols = count($json_cols);
		if($numRecs>0){
			$count = 1;
			$count_cols = 0;

			$str = '';
			while (!$rs->EOF){
				
				 $code    = isset($rs->fields['CODE']) ? $rs->fields['CODE'] : null;
				 $checked = 0;
			     
			     if( in_array( $code, $checked_codes_array) || array_search( $code, $checked_codes_array)){
				  $checked = 1;
				 }
			     
			     $rs->fields['ck_form'] = $checked;
			     
				 $str .= "{";
				 
				 $count_cols = 1;
				 foreach ($json_cols as $json_col ){
					$db_col = strtoupper($json_col);
					$json_col_value = isset($rs->fields[$db_col]) ? $rs->fields[$db_col] : '';
					$str .= "\"{$json_col}\":\"{$json_col_value}\"";
					$str .=  $count_cols<$numCols ? ',' : '';
					
					++$count_cols;
				 }
			 
				 $str .= "}";
				 
				$str .=  $count <$numRecs ? ",\r\n" : '';			 
				
			 ++$count;	
			 $rs->MoveNext();
			}
		}
	}
 
	 header('Content-type: text/json');
	 
	 echo '['.$str.']';
	 
  }
 
 public function pre_run() {
		global $db;
		
       $students_in = array();
       
       if(isset($_POST['sms_general_streams'])){
         foreach($_POST['sms_general_streams'] as $streamcode){
          $students_in[] = $streamcode;
		 }
	   }
	   
	   $students_in_sql = "('" . implode("','", $students_in) ."')";
	   
	   $missing_phones  = $db->Execute("select ADMNO,FULLNAME,STREAMCODE from VIEWSP where STREAMCODE in {$students_in_sql} AND (MOBILEPHONE IS NULL OR MOBILEPHONE='' OR {$db->length}(MOBILEPHONE)<10)");
       $num             = @$missing_phones->RecordCount();
       
       if($num>0){
        return json_response(0,"{$num} Student Are Missing Phone");
	   }else{
        return json_response(1,"ok");
	   }

  }


  public function send(){
   global $db;
   
	$message      =  filter_input(INPUT_POST , 'sms_general', FILTER_SANITIZE_STRING);
	$message      =  centerTrim($message);

	if(empty($message)){
      return json_response(0, 'Enter SMS Message');
	}
	
	//if(strlen($message)>160){
     // return json_response(0, 'SMS is too long');
	//}
     
	$SMSGateway   =  new sms();
     
    if(!empty($SMSGateway->error)){
      return json_response(0, $SMSGateway->error);
	}

	 $students_in = array();
	 $recipients_array = array();
   
    if(isset($_POST['sms_general_streams'])){
	 foreach($_POST['sms_general_streams'] as $streamcode){
	  $students_in[] = $streamcode;
	 }
    }
	   
    $students_in_sql = "('" . implode("','", $students_in) ."')";
	   
	$students        = $db->Execute("select ADMNO,MOBILEPHONE from VIEWSP where STREAMCODE in {$students_in_sql} AND (MOBILEPHONE IS NOT NULL OR MOBILEPHONE!='' AND {$db->length}(MOBILEPHONE)>=10)");
    $num             = @$students->RecordCount();
    $next_id         = generateID('SYSQSMS');
    $queuedate       =  date('Y-m-d H:i:s');
       
	foreach($students as $student){
     $mobilephone         = valueof($student, 'MOBILEPHONE');
     $recipients_array[]  = $mobilephone;
	}

	$recipients = implode(',', $recipients_array);
	
	$results    = $SMSGateway->sendMessage($recipients, $message);

    foreach($recipients_array as $mobilephone){
	$sql_sms   = "insert into SYSQSMS(ID,MOBILE,SMS,QUEUEDATE)values( {$next_id},'{$mobilephone}','{$message}','{$queuedate}')";

    if($db->Execute($sql_sms))
    {
     ++$next_id;
    }
    
    }

	return json_response(1, "Sent SMS to {$num} Recipients");

  }
    
 }
