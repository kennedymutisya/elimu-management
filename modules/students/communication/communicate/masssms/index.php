<?php

/**
* auto created index file for modules/students/communication/communicate/viasms
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-03-22 08:38:00
*/

 $school = new school();
 
 $forms = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 
 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }

 $sortCols = array();
 $sortCols['admno']    = 'Admission Number';
 $sortCols['fullname'] = 'Student Name';
 
?>

	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		<table cellpadding="2" cellspacing="0" width="100%">
		    <tr>
			 <td>&nbsp;</td>	
			 <td>&nbsp;</td>	
	    	</tr>
	    	
			<tr>
	    		 <td>Send to</td>
	    		 <td>
				<select id="sms_general_streams" name="sms_general_streams[]" class="easyui-combogrid" style="width:350px" data-options="
						panelWidth: 350,
						multiple: true,
						idField: 'code',
						textField: 'name',
						url: './endpoints/crud/?module=<?php echo $vars; ?>&function=data_streams',
						method: 'post',
						columns: [[
							{field:'ck',checkbox:true},
							{field:'code',title:'Code',width:100},
							{field:'name',title:'Name',width:200}
						]],
						fitColumns: true,
                        checkOnSelect: true,
                        onLoadSuccess:function(data){
				        if(data.rows.length>0){ 
				        for(i=0;i<data.rows.length;++i){
				 	      if(typeof data.rows[i].ck_form!='undefined' && data.rows[i].ck_form==='1'){
				 	       $(this).combogrid('grid').datagrid('checkRow', i);
				 	      }
				         }
				        }
				       }
					">
				</select>
	    		 </td>
	    		</tr>

	    	<tr>
			 <td>&nbsp;</td>	
			 <td>&nbsp;</td>	
	    	</tr>
	    	
		    <tr>
			 <td>SMS:</td>
			 <td><textarea name="sms_general" id="sms_general" cols="50"  rows="5"   class=""></textarea></td>
			</tr>
			
		    <tr>
			 <td>&nbsp;</td>
			 <td>
			 <a href="javascript:void(0)"  id="btnSendSMS" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.pre_run();" style="min-height:30px;vertical-align:center"><i class="fa fa-paper-plane"></i> Send</a>
			 </td>
			</tr>
			
		</table>
     </form>
   
	<script>
		
		var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		list_rcpt:function (){
		 ui.cc('list_rcpt', $('#<?php echo ui::fi('ff'); ?>').serialize() ,'<?php echo ui::fi('div_rcpt'); ?>');
		},
		pre_run:function (){
			 $.messager.progress();
			 $('#btnSendSMS').linkbutton('disable');
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize()  + '&modvars=<?php echo $vars; ?>&function=pre_run';
		     $.post('./endpoints/crud/', fdata, function(data) {
             if (data.success === 1) {
                 <?php echo $cfg['appname']; ?>.send();
              } else {
				$.messager.progress('close');
				$('#btnSendSMS').linkbutton('enable');
                $.messager.alert('Error',data.message,'error');
                <?php echo $cfg['appname']; ?>.print_report_nophone();
             }
            }, "json");
		},
		send:function (){
		 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize()  + '&modvars=<?php echo $vars; ?>&function=send';
		 $.post('./endpoints/crud/', fdata, function(data) {
		       $.messager.progress('close');
		       $('#btnSendSMS').linkbutton('enable');
		       if (data.success === 1) {
                 $.messager.alert('Status',data.message,'info');
               } else {
                $.messager.alert('Error',data.message,'error');
               }
         }, "json");
		},
	    print_report_nophone:function (){
         var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
  	     var <?php echo ui::fi('w'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&'+fdata,'<?php echo ui::fi('pw'); ?>','height=800,width=1000,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('w'); ?>.focus();
        },
	}
			
	</script>

