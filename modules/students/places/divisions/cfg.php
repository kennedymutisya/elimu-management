<?php
/**
* auto created config file for modules/students/places/divisions
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:53:17
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Divisions';//user-readable formart
 $cfg['appname']       = 'divisions';//lower cased one word
 $cfg['datasrc']       = 'ADDIVISIONS';//where to get data
 $cfg['datatbl']       = 'ADDIVISIONS';//base data src [for updates & deletes]
 $cfg['form_width']    = 550;
 $cfg['form_height']   = 220;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 410;
 
 $cfg['pkcol']         = 'DIVISIONCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['byurd'] = array(
                      'dbcol'=>'DIVISIONCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['z4p7b'] = array(
                      'dbcol'=>'DIVISIONNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['yvwdr'] = array(
                      'dbcol'=>'DISTRICTCODE',
                      'title'=>'District',
                      'width'=>150,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'ADDISTRICTS|DISTRICTCODE|DISTRICTNAME|DISTRICTCODE',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['yvwdr']['columns']['DISTRICTCODE']  = array( 'field'=>'DISTRICTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['yvwdr']['columns']['DISTRICTNAME']  = array( 'field'=>'DISTRICTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['yvwdr']['source'] ='ADDISTRICTS';
  