<?php
/**
* auto created config file for modules/students/places/districts
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:53:00
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Districts';//user-readable formart
 $cfg['appname']       = 'districts';//lower cased one word
 $cfg['datasrc']       = 'ADDISTRICTS';//where to get data
 $cfg['datatbl']       = 'ADDISTRICTS';//base data src [for updates & deletes]
 $cfg['form_width']    = 550;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 410;
 
 $cfg['pkcol']         = 'DISTRICTCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['lajq1'] = array(
                      'dbcol'=>'COUNTYCODE',
                      'title'=>'County',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'ADCOUNTIES|COUNTYCODE|COUNTYNAME|COUNTYCODE',
                      );
                      



                      
$cfg['columns']['mhcoq'] = array(
                      'dbcol'=>'DISTRICTCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['nu0ih'] = array(
                      'dbcol'=>'DISTRICTNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['lajq1']['columns']['COUNTYCODE']  = array( 'field'=>'COUNTYCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['lajq1']['columns']['COUNTYNAME']  = array( 'field'=>'COUNTYNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['lajq1']['source'] ='ADCOUNTIES';
  