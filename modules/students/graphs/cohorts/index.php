<?php

require_once('cfg.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");	

 if(!class_exists($class)){
 	die('class--' .$classfile. 'not found');
 }
 
 $_class = new $class();
 $grid   = new grid();
 
 $module = array();
 
 $module['url']    =  DIR;
 $module['class']  =  $class;
 $module_packed    =  packvars($module);
 
 $cohort_stats = $db->GetAssoc("SELECT DISTINCT COHORTNAME,COUNT(DISTINCT(REGNO)) NUM FROM VIEWSP GROUP BY COHORTCODE, COHORTNAME ORDER BY COUNT(DISTINCT(REGNO))");
 
 $data        = array();
 $pie3d_data        = array();
 
 if(count($cohort_stats)>0){
 	foreach ($cohort_stats as $cohortname => $num){
 		
 		if(empty($cohortname)){
 		 $cohortname = "NA*";
 		}
 		
 		$data[$cohortname] = $num;
 		
 	}
 }
 
$max = max($data);

foreach ($data as $k=>$v){
	
	if($v==$max){
	 $pie3d_data[] = "{
name: '{$k}',
y: {$v},
sliced: true,
selected: true
}";	
	}else{
	$pie3d_data[]  = "['{$k}',   {$v}]";
	}
}

 $pie3d_data_str = implode(',',$pie3d_data);
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../public/css/themes/default/easyui.css">
	<script type="text/javascript" src="../../public/js/jquery.min.js"></script>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
				enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Student Per Cohort'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                showInLegend: true
            }
        },
        credits: {
                enabled: false
        },
        series: [{
            type: 'pie',
            name: 'Cohorts',
            data: [
              <?php echo $pie3d_data_str; ?>
            ]
        }]
    });
});
    
</script>
</head>
<body>

<script src="../../public/js/highcharts/highcharts.js"></script>
<script src="../../public/js/highcharts/highcharts-3d.js"></script>
<script src="../../public/js/highcharts/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

</body>
</html>