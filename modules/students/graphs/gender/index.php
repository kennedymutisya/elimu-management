<?php

require_once('cfg.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");	

 if(!class_exists($class)){
 	die('class--' .$classfile. 'not found');
 }
 
 $_class = new $class();
 $grid   = new grid();
 
 $module = array();
 
 $module['url']    =  DIR;
 $module['class']  =  $class;
 $module_packed    =  packvars($module);
 
 $school_stats = $db->GetArray("SELECT DISTINCT SCLCODE,SCLNAME,GNDCODE,COUNT(DISTINCT(REGNO)) NUM FROM VIEWSP GROUP BY SCLCODE,SCLNAME,GNDCODE");
 
 $data        = array();
 $categories  = array();
 $series_data = array();
 $series      = array();
 
 if(count($school_stats)>0){
 	foreach ($school_stats as $school_stat){
 		$sclcode  = valueof($school_stat, 'SCLCODE');
 		$sclname  = valueof($school_stat, 'SCLNAME');
 		$gndcode  = valueof($school_stat, 'GNDCODE');
 		$num      = valueof($school_stat, 'NUM');
 		$data[$sclcode]['name']  = $sclname;
 		$data[$sclcode]['data'][$gndcode] = $num;
 		
 		$categories[$sclcode] = $sclname;
 		
 		$series_data[$gndcode][$sclcode] =  $num;
 		
 	}
 }
 
 if(count($series_data)>0){
 	foreach ($series_data as $gndcode=>$serie_data){
 		
 		 $series[] = "{
              name: '{$gndcode}',
              data: [". implode(',',$serie_data) ."]}";
 	}
 }
 
 $series_str = '';
 
 if(count($series)>0){
 	$series_str = implode(',', $series);
 }
 
 $categories_str = "'" . implode("','", $categories ). "'";
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../public/css/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/themes/icon.css">
	<script type="text/javascript" src="../../public/js/jquery.min.js"></script>
<script>
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Gender Distribution / School'
            },
            subtitle: {
                text: 'Active Students Only'
            },
            xAxis: {
                categories: [<?php echo $categories_str; ?>],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'No. of Students',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ''
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [
            	<?php  echo $series_str ;?>
            ]
        });
    });
    
</script>
</head>
<body>

<script src="../../public/js/highcharts/highcharts.js"></script>
<script src="../../public/js/highcharts/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

</body>
</html>