<?php
/**
* auto created config file for modules/students/setup/streams
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-16 14:50:10
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Streams';//user-readable formart
 $cfg['appname']       = 'streams';//lower cased one word
 $cfg['datasrc']       = 'SASTREAMS';//where to get data
 $cfg['datatbl']       = 'SASTREAMS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 220;
 $cfg['window_width']    = 520;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['afuhy'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAFORMS|FORMCODE|FORMNAME|FORMCODE',
                      );
                      



                      
$cfg['columns']['cusfo'] = array(
                      'dbcol'=>'STREAMCODE',
                      'title'=>'Stream Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['ynna5'] = array(
                      'dbcol'=>'STREAMNAME',
                      'title'=>'Stream Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['afuhy']['columns']['FORMCODE']  = array( 'field'=>'FORMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['afuhy']['columns']['FORMNAME']  = array( 'field'=>'FORMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['afuhy']['source'] ='SAFORMS';
  