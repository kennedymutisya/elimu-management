<?php
/**
* auto created config file for modules/students/setup/studresponsibilities
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-07 07:06:06
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Responsibilities';//user-readable formart
 $cfg['appname']       = 'responsibilities';//lower cased one word
 $cfg['datasrc']       = 'SARSD';//where to get data
 $cfg['datatbl']       = 'SARSD';//base data src [for updates & deletes]
 $cfg['form_width']    = 600;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['kirx7'] = array(
                      'dbcol'=>'ADMNO',
                      'title'=>'Student',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASTUDENTS|ADMNO|FULLNAME|ADMNO',
                      );
                      



                      
$cfg['columns']['uspdo'] = array(
                      'dbcol'=>'RSCODE',
                      'title'=>'Responsibility',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SARS|RSCODE|RSCNAME|RSCODE',
                      );
                      



                      
$cfg['columns']['4sjdp'] = array(
                      'dbcol'=>'RSFROM',
                      'title'=>'From',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['yq0xk'] = array(
                      'dbcol'=>'RSEND',
                      'title'=>'To',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['kirx7']['columns']['ADMNO']  = array( 'field'=>'ADMNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['kirx7']['columns']['FULLNAME']  = array( 'field'=>'FULLNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['kirx7']['source'] ='SASTUDENTS';
	 
$combogrid_array['uspdo']['columns']['RSCODE']  = array( 'field'=>'RSCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['uspdo']['columns']['RSCNAME']  = array( 'field'=>'RSCNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['uspdo']['source'] ='SARS';
  