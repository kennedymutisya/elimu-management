<?php

/**
 * auto created config file for modules/students/setup/cleanbill
 * @author kenmsh@gmail.com
 *
 * @version 2.0
 * @since 2019-03-30 11:09:47
 */

final class cleanbill
{
    private $id;
    private $datasrc;
    private $primary_key;

    public function __construct()
    {
        global $cfg;

        $this->id = filter_input(INPUT_POST, 'id');
        $this->datasrc = valueof($cfg, 'datasrc');
        $this->primary_key = valueof($cfg, 'pkcol');
    }

    public function save()
    {
        global $db, $cfg;

        $grid = new grid();
        return $grid->grid_save_row_simple();
    }

    public function remove()
    {
        global $db, $cfg;

        $grid = new grid();
        return $grid->grid_remove_row();
    }


    public function export()
    {
        global $db, $cfg;

        $grid = new grid();
        return $grid->export();
    }

    public function import()
    {
        global $db, $cfg;

        $grid = new grid();
        return $grid->import();
    }

    public function rankkcpe()
    {
         $query = "SET @r=0; UPDATE sastudents SET kcperank= @r:= (@r+1) WHERE streamcode='3E' ORDER BY kcpemarks DESC";

    }
}
