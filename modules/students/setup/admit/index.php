<?php
?>
<style>
td.label {   font-size: 12px; }
#photo<?php echo MNUID; ?> {  min-width: 150px; min-height: 150px; max-width: 150px; max-height: 150px; }
#photo<?php echo MNUID; ?> {cursor:pointer;}
.textbox{
height:20px;
margin:0;
padding:0 2px;
box-sizing:content-box;
}
</style>
	
	<div style="margin:5px 0;">
	<table cellpadding="2" cellspacing="0" width="300px">
	 <tr>
	 <td nowrap class="label">Search</td>
	 <td><?php echo ui::form_input( 'text', 'find_admno', 20, '', '', '', '', '', '' , ""); ?></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.loadProfile();">View</a></td>
	 </tr>
	</table>
    </div>
    
	<form id="ff_student_profile<?php echo MNUID; ?>" method="post" enctype="multipart/form-data" novalidate>
	<div class="easyui-tabs" data-options="tabWidth:112" style="width:601px;height:295px">
	   
	   <div title="Official" style="padding:10px">
			
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    	
	    	    <tr>
	    			<td>Admission No:</td>
	    			<td><?php echo ui::form_input( 'text', 'admno', 15, '', '', '', 'data-options="required:true"', '', 'easyui-textbox' , ""); ?></td>
	    			<td rowspan="5">
	    			 <div id="photo<?php echo MNUID; ?>" title="click to replace photo" onclick="<?php echo $cfg['appname']; ?>.initiate_upload_photo();"><img src="./public/photos/profile-default.gif" border="0"></div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Full Name:</td>
	    			<td><?php echo ui::form_input( 'text', 'fullname', 30, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Admission Date:</td>
	    			<td><?php echo ui::form_input( 'text', 'admdate', 15, '', '', '', '', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td width="180px">Current Class</td>
	    			<td><?php echo dropdown::stream('stream','','data-options="required:true');  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td  width="180px">House:</td>
	    			<td><?php echo dropdown::house('house', $selected_code='',$options='', 1);  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td  width="180px">Dorm:</td>
	    			<td><?php echo dropdown::dorm('dorm', $selected_code='',$options='', 1);  ?></td>
	    		</tr>
	    		
	    	</table>
	    	
		</div>
		
		<div title="Personal" style="padding:10px">
		
	    	<table cellpadding="2" cellspacing="0" width="100%"  >
	    		
	    		<tr>
	    		 <td>Birth Date:</td>
	    		 <td><?php echo ui::form_input( 'text', 'dob', 15, '', '', '', '', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Gender:</td>
	    		 <td><?php echo dropdown::gender('gender','M');  ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Religion:</td>
	    		 <td><?php echo dropdown::religion('religion');  ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Ethinicity:</td>
	    		 <td><?php echo dropdown::ethnicities('etccode');  ?></td>
	    		</tr>
	    		
	    	</table>
		</div>
		
		
		<div title="Contacts" style="padding:10px">
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    		 <td>Mobile Phone:</td>
	    		 <td><?php echo ui::form_input( 'text', 'mobilephone', 20, '', '', '', '', '', 'easyui-validatebox textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Postal Code:</td>
	    		 <td><?php echo ui::form_input( 'text', 'postalcode', 20, '', '', '', 'data-options="required:false"', '', 'easyui-validatebox textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Address:</td>
	    		 <td><?php echo ui::form_input( 'text', 'address', 20, '', '', '', 'data-options="required:false"', '', 'easyui-validatebox textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Residence:</td>
	    		 <td><?php echo ui::form_input( 'text', 'residence', 20, '', '', '', 'data-options="required:false"', '', 'easyui-validatebox textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Email:</td>
	    		 <td><?php echo ui::form_input( 'text', 'email', 20, '', '', '', 'data-options="required:false",validType:\'email\'"', '', 'easyui-validatebox textbox' , ""); ?></td>
	    		</tr>
	    		
	    	</table>
		</div>
		
		<div title="KCPE" style="padding:10px">
		
	    <table cellpadding="2" cellspacing="0" width="100%">
	     <tr>
		  <td>KCPE Index:</td>
		  <td><?php echo ui::form_input( 'text', 'indexno', 10, '', '', '', ' data-options="required:false,validType:\'minLength[5]\'" ', '', 'easyui-validatebox textbox' , ""); ?></td>
		  <td>KCPE Marks:</td>
		  <td><?php echo ui::form_input( 'text', 'kcpemarks', 5, '', '', '', ' data-options="required:false,increment:1,min:200,max:500" ', '', 'easyui-numberspinner easyui-validatebox' , ""); ?></td>
		</tr>
		
	    <tr>
		 <td>KCPE Mean:</td>
		 <td><?php echo ui::form_input( 'text', 'kcpemean', 5, '', '', '', ' data-options="required:false, increment:1,min:0,max:100" ', '', 'easyui-numberspinner easyui-validatebox' , ""); ?></td>
		 <td>KCPE Grade:</td>
		 <td><?php echo dropdown::kcpegrades('kcpegrade', $selected_code='',$options='', 1);  ?></td>
		</tr>
	    					
		<?php
		 $subjects = $db->GetAssoc("SELECT KCPESUBCODE,KCPESUBNAME FROM SAKCPESUBJECT ORDER BY ID");
		 if($subjects){
		 	if(sizeof($subjects)>0){
		 		foreach ($subjects as $subjectcode=>$subjectname){
		 			$gradeselect = dropdown::kcpegrades("kcpe_subjects[{$subjectcode}]", $selected_code='',$options='', 1); 
		 			?>
		 			<tr>
	    			 <td style="width:150px;"><?php echo $subjectname; ?></td>
	    			 <td colspan="3"><?php echo $gradeselect; ?></td>
	    		   </tr>
		 			<?php
		 		}
		 	   }
		     }
		   ?>	
	    		
	    	</table>
	    	
		</div>
		
		<div title="Medical" style="padding:10px">
	    	<table cellpadding="2" cellspacing="0" width="100%">
				
	    		<tr>
	    			<td>Medical Status</td>
	    			<td><textarea  type="text" id="medstatus"  name="medstatus" cols="20" rows="2"></textarea></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Allergy 1:</td>
	    			<td><input  class="easyui-validatebox textbox" type="text" name="allergy1"  name="allergy1" ></input></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Allergy 2:</td>
	    			<td><input  class="easyui-validatebox textbox" type="text" name="allergy2"  name="allergy2" ></input></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Allergy 3:</td>
	    			<td><input  class="easyui-validatebox textbox" type="text" name="allergy3"  name="allergy3" ></input></td>
	    		</tr>
	    		
	    	</table>
		</div>
		
	</div>
   </form>
   
	<div style="margin:5px 0;"></div>
	<table cellpadding="2" cellspacing="0" width="300px">
	 <tr>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="<?php echo $cfg['appname']; ?>.saveProfile();">Save</a></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',disabled:true" onclick="<?php echo $cfg['appname']; ?>.Remove();" id="btnRemove<?php echo MNUID; ?>">Remove</a></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onclick="<?php echo $cfg['appname']; ?>.clearForm()">Reset</a></td>
	 </tr>
	</table>
	
<div id="dlg_upload<?php echo MNUID; ?>" class="easyui-dialog" title="Photo Upload" style="width:300px;height:120px;padding:5px" closed="true" >
 <form id="ff_student_photo<?php echo MNUID; ?>" class="form-horizontal" action="./" method="POST" enctype="multipart/form-data">
  <input  type="file" name="<?php echo ui::fi('student_photo'); ?>" id="<?php echo ui::fi('student_photo'); ?>" onchange="<?php echo $cfg['appname']; ?>.do_upload_photo()">
 </form>
 </div>
 
	<script>
	
       var <?php echo $cfg['appname']; ?> = {
		clearForm :function (){
			$('#ff_student_profile<?php echo MNUID; ?>').form('clear');
			$('#find_admno').val();
			$('.combo-text').val();
			$('#photo<?php echo MNUID; ?>').html('<img src="./public/photos/profile-default.gif" border="0">');
		    $('#btnRemove<?php echo MNUID; ?>').linkbutton('disable');
		},
		loadProfile :function (){
		  var admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
		  var rdata = 'admno='+admno+'&modvars=<?php echo $vars; ?>&function=data';	
		   $.post('./endpoints/crud/', rdata, function(data) {
             $('#ff_student_profile<?php echo MNUID; ?>').form('load',data);
           }, "json");
		  <?php echo $cfg['appname']; ?>.load_photo();
	      $('#btnRemove<?php echo MNUID; ?>').linkbutton('enable');
		},
		load_photo :function (){
		       var admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
			   var rdata = 'admno='+admno+'&modvars=<?php echo $vars; ?>&function=photo';	
		       $.post('./endpoints/crud/', rdata, function(data) {
               if (data.success === 1 && (typeof data.photo!=undefined)) {
               	$('#photo<?php echo MNUID; ?>').html(data.photo);
               }else{
				$.messager.alert('Error',data.message,'error');
               }
              }, "json");
		},
		initiate_upload_photo:function (){
	   	var admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
	   	if(admno===''){
		 $.messager.alert('Error','Select Student First','error');
		 return;	
		}else{
	   	$.messager.confirm('Confirm','Do you want to upload a photo now?',function(r){
         if (r){
	  	  $('#dlg_upload<?php echo MNUID; ?>').dialog('open');
         }
	   	});
		}
	   },
	   do_upload_photo:function(){
		var data = new FormData($('#ff_student_photo<?php echo MNUID; ?>')[0]);
		$.messager.progress();
		$.ajax({
        url : './endpoints/crud/?modvars=<?php echo $vars; ?>&function=upload_photo',
        type: 'POST',
        data: data,
        cache: false,
        async:'false',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR){
         <?php echo $cfg['appname']; ?>.load_photo();
         $('#dlg_upload<?php echo MNUID; ?>').dialog('close');
         $.messager.progress('close');
        },
        error: function(jqXHR, textStatus, errorThrown){

        }
        });
       },
	   saveProfile:function (){
			var validate =  $('#ff_student_profile<?php echo MNUID; ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 
	         $('#btnRemove<?php echo MNUID; ?>').linkbutton('disable');
		     $.messager.progress();
			 var fdata = $('#ff_student_profile<?php echo MNUID; ?>').serialize() +'&modvars=<?php echo $vars; ?>&function=save';	
		     $.post('./endpoints/crud/', fdata, function(data) {
             $.messager.progress('close');
            if (data.success === 1) {
            	$('#btnRemove<?php echo MNUID; ?>').linkbutton('enable');
                $.messager.show({title: 'Success',msg: data.message});
            } else {
				$.messager.alert('Error',data.message,'error');
            }
           }, "json");
           
		 }
		},
		Remove:function (){
			var admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
			$.messager.confirm('Confirm','Are you sure you want to remove student '+admno+'?',function(r){
            if (r){
			   var rdata = 'admno='+admno+'&modvars=<?php echo $vars; ?>&function=remove';	
		       $.post('./endpoints/crud/', rdata, function(data) {
               if (data.success === 1) {
            	<?php echo $cfg['appname']; ?>.clearForm();
                $.messager.show({title: 'Success',msg: data.message});
				$('#photo<?php echo MNUID; ?>').html('<img src="./public/photos/profile-default.gif" border="0">');
               }else{
				<?php echo $cfg['appname']; ?>.clearForm();
                $.messager.alert('Error',data.message,'error');
               }
              }, "json");
             }
			});
		}
	   }
	   
	<?php
      echo ui::ComboGrid($combogrid_array,'find_admno',"{$cfg['appname']}.loadProfile");
	?>
	</script>
