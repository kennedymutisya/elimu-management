<?php
/**
* auto created config file for modules/students/setup/termcalendar
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-06 17:59:23
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Term Calendar';//user-readable formart
 $cfg['appname']       = 'termcalendar';//lower cased one word
 $cfg['datasrc']       = 'SATCAL';//where to get data
 $cfg['datatbl']       = 'SATCAL';//base data src [for updates & deletes]
 $cfg['form_width']    = 500;
 $cfg['form_height']   = 300;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['sbqvg'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Year',
                      'width'=>50,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );
                      



                      
$cfg['columns']['wjghx'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Term',
                      'width'=>50,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SATERMS|TERMCODE|TERMNAME|TERMCODE',
                      );
                      



                      
$cfg['columns']['zlbxr'] = array(
                      'dbcol'=>'DATESTART',
                      'title'=>'Start',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['98hjv'] = array(
                      'dbcol'=>'DATEEND',
                      'title'=>'End',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['deyow'] = array(
                      'dbcol'=>'ACTIVE',
                      'title'=>'State',
                      'width'=>50,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAACTV|SATIVECODE|SATIVENAME|SATIVECODE',
                      );
                      



                      
$cfg['columns']['zly0h'] = array(
                      'dbcol'=>'ISNEXT',
                      'title'=>'Isnext',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'checkbox',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['sbqvg']['columns']['YEARCODE']  = array( 'field'=>'YEARCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['sbqvg']['columns']['YEARNAME']  = array( 'field'=>'YEARNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['sbqvg']['source'] ='SAYEAR';
	 
$combogrid_array['wjghx']['columns']['TERMCODE']  = array( 'field'=>'TERMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['wjghx']['columns']['TERMNAME']  = array( 'field'=>'TERMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['wjghx']['source'] ='SATERMS';
	 
$combogrid_array['deyow']['columns']['SATIVECODE']  = array( 'field'=>'SATIVECODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['deyow']['columns']['SATIVENAME']  = array( 'field'=>'SATIVENAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['deyow']['source'] ='SAACTV';
  