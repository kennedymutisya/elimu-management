<?php

$scriptname  = end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Bio Data';//user-readable formart
 $cfg['appname']       = 'biodata';//lower cased
 $cfg['datasrc']       = 'ACSP';//where to get data
 $cfg['datatbl']       = 'ACSP';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 280;
 
 $cfg['pkcol']         = 'code';//the primary key

$combogrid_array   = array();
$combogrid_array['regno']['columns']['REGNO']      = array( 'field'=>'regno', 'title'=>'Reg No', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['regno']['columns']['FULLNAME']   = array( 'field'=>'fullname', 'title'=>'Full Name', 'width'=> 100, 'isTextField'=>true);
$combogrid_array['regno']['columns']['PRGCODE']    = array( 'field'=>'prgcode', 'title'=>'Programme', 'width'=>100);
$combogrid_array['regno']['columns']['COHORTCODE'] = array( 'field'=>'cohort', 'title'=>'Cohort', 'width'=> 80);
$combogrid_array['regno']['source'] ='ACSP';
	