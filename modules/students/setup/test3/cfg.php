<?php
/**
* auto created config file for modules/students/setup/test3
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2015-10-26 08:37:15
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Test3';//user-readable formart
 $cfg['appname']       = 'test3';//lower cased one word
 $cfg['datasrc']       = 'ADLEVELS';//where to get data
 $cfg['datatbl']       = 'ADLEVELS';//base data src [for updates & deletes]
 $cfg['form_width']    = 690;
 $cfg['form_height']   = 390;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 230;
 
 $cfg['pkcol']         = 'LEVELCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 

$cfg['columns']['v7nv3'] = array(
                      'dbcol'=>'LEVELCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['h7x4z'] = array(
                      'dbcol'=>'LEVELNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      