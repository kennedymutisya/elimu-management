<?php

/**
* auto created config file for modules/students/setup/recordattendance
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-02-29 17:27:26
*/

 final class recordattendance {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }

 public function load_stage(){
		global $db,$cfg;
			
	 $school          =  new school();
	 $user            =  new user();
	 $years           = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
	 $terms           =  $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
	 $subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");
	 $streams_data    = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');

	 $my_subjects     =  $db->GetArray("
		SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
		ORDER BY YEARCODE,STREAMCODE ASC
	   ");

	 $my_list    = array();
	 $my_streams = array();
	 $streams    = array();
	 $subjects   = array();
	 
	 $forms = array();
	 if(isset($streams_data)){
	  if(sizeof($streams_data)>0){	
	   foreach ($streams_data as $streamcode=>$stream_data){
		$streamname = valueof($stream_data, 'STREAMNAME');
		$formcode   = valueof($stream_data, 'FORMCODE');
		$forms[$formcode] = "Form {$formcode}";
		$forms[$streamcode] = "Form {$formcode}-{$streamname}";
	   }
	  }
	 }
	 
       $yearcode     = isset($_SESSION['att']['yearcode']) ? $_SESSION['att']['yearcode'] : $school->active_yearcode;
       $termcode     = isset($_SESSION['att']['termcode']) ? $_SESSION['att']['termcode'] : $school->active_termcode;
       $streamcode   = isset($_SESSION['att']['streamcode']) ? $_SESSION['att']['streamcode'] : null;
       $attdate      = isset($_SESSION['att']['attdate']) ? $_SESSION['att']['attdate'] : date('Y-m-d');

       if( isset($_SESSION['att']['yearcode']) && isset($_SESSION['att']['streamcode']) ){

       $streams   =  array();
       $records   =  $db->GetArray("
		SELECT distinct STREAMCODE FROM SATSA
		WHERE YEARCODE='{$yearcode}'
		ORDER BY STREAMCODE ASC
       ");

	    if($records){
	  	 if(count($records)>0){
	  	  foreach ($records as $record){
	  	  	$streamcode_loop           = valueof($record,'STREAMCODE');
	  	  	$streams[$streamcode_loop] = $streamcode_loop;
	  	  }
	  	 }
	    }

       }

	?>
	<form id="<?php echo ui::fi('ff'); ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  
	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $yearcode," onchange=\"\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms, $termcode,"onchange=\"\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Stream</td>
			<td>
			 <div id="div_streams<?php echo MNUID; ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $forms, $streamcode,"onchange=\"\"",'',false,200);  ?>
			 </div>
			</td>
		</tr>

        <tr>
	    <td>Date:</td>
	    <td><?php echo ui::form_input( 'text', 'attdate', 15, $attdate, '', '', 'data-options="required:true,formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    </tr>
	    
		<tr>
			<td>&nbsp;</td>
			<td>
			 <button onclick="<?php echo $cfg['appname']; ?>.preload();return false;"><i class="fa check-square"></i> Enter Attendance</button>
			</td>
		</tr>

	</form>
	<script>
    $('#<?php echo ui::fi('attdate'); ?>').datebox({
        required:true
    });
    $('#<?php echo ui::fi('attdate'); ?>').datebox('setValue', '<?php echo $attdate; ?>');
	</script>
		<?php
	}

	public function list_streams(){
		global $db,$cfg;

	 $subjectcode  =  filter_input(INPUT_POST , ui::fi('subjectcode'));
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

	 $streams_array = array();
     $user          = new user();

     $records   =  $db->GetArray("
		SELECT distinct STREAMCODE FROM SASTUDSUB
		WHERE YEARCODE='{$yearcode}'
		ORDER BY STREAMCODE ASC
     ");


	  if($records){
	  	if(count($records)>0){
	  	  foreach ($records as $record){
	  	  	$streamcode  = valueof($record,'STREAMCODE');
	  	  	$streams_array[$streamcode] = $streamcode;
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('streamcode',$streams_array, ''," onchange=\"{$cfg['appname']}.list_subjects();\" ");

	}

	public function list_subjects(){
		global $db;
		
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $formcode     =  filter_input(INPUT_POST , ui::fi('streamcode'));
	 
	 if(strlen($streamcode)>1){
	 	$formstream_col = 'STREAMCODE';
	 }else{
        $formstream_col = 'FORMCODE';
	 }

     $subjects      =  $db->GetArray("
		SELECT DISTINCT SUBJECTCODE FROM SASTUDSUB
		WHERE YEARCODE='{$yearcode}'
		AND {$formstream_col}='{$streamcode}'
		ORDER BY SUBJECTCODE ASC
     ");

	  $subjects_array = array();

	  if($subjects){
	  	if(count($subjects)>0){
	  	  foreach ($subjects as $subject){
	  	  	$subjectcode  = valueof($subject,'SUBJECTCODE');
	  	  	$subjects_array[$subjectcode] = $subjectcode;
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('subjectcode',$subjects_array, ''," onchange=\"\" ");

	}

	public function preload(){
		global $db,$cfg;
   
	   $user         =  new user();

	   $subjectcode  = filter_input(INPUT_POST , ui::fi('subjectcode'));
       $streamcode   = filter_input(INPUT_POST , ui::fi('streamcode'));
	   $yearcode     = filter_input(INPUT_POST , ui::fi('yearcode'));
	   $termcode     = filter_input(INPUT_POST , ui::fi('termcode'));
	   $attdate      = filter_input(INPUT_POST , ui::fi('attdate'));

       $formcode     =  $db->GetOne("SELECT FORMCODE FROM SASTREAMS WHERE STREAMCODE='{$streamcode}'");

       $_SESSION['att'] = array();

       $_SESSION['att']['attdate']      = $attdate;
       $_SESSION['att']['formcode']     = $formcode;
       $_SESSION['att']['streamcode']   = $streamcode;
       $_SESSION['att']['yearcode']     = $yearcode;
       $_SESSION['att']['termcode']     = $termcode;

       $attdate_text  = textDate($attdate);

       /*
        *
        *make all as present
        **/
         
	    if(strlen($streamcode)>1){
	    $formstream_col = 'STREAMCODE';
	   }else{
        $formstream_col = 'FORMCODE';
	    }

        $students   = $db->Execute("
         select DISTINCT ADMNO, FULLNAME  from VIEWSP
         where YEARCODE='{$yearcode}'
         AND {$formstream_col}='{$streamcode}'
         ORDER BY ADMNO
         ");

        if($students){
         if($students->RecordCount()>0){
		   $user   = new user();	
		  foreach($students as $student){
		   $admno  =  $student['ADMNO'];
           $record = new ADODB_Active_Record('SASATT', array('ADMNO','STREAMCODE','YEARCODE','TERMCODE','ATTDATE'));
    
           $record->Load("
           ADMNO='{$admno}'
           AND STREAMCODE='{$streamcode}'
           AND YEARCODE='{$yearcode}'
           AND TERMCODE='{$termcode}'
           AND ATTDATE='{$attdate}'
          ");
    
          if(empty($record->_original)){
            $record->admno       = $admno;
            $record->streamcode  = $streamcode;
            $record->yearcode    = $yearcode;
            $record->termcode    = $termcode;
            $record->attdate     = $attdate;
            $record->statusid    = 1;
            $record->audituser   = $user->userid;
            $record->auditdate   = date('Y-m-d');
            $record->audittime   = time();
            @$record->Save();
           }
	      }
	     }
	    }
       
       return json_response(1,'ok', array('title' => "Form {$formcode} Date {$attdate_text} ") );

	}

	public function data(){
		global $db,$cfg;

	$this->page         = filter_input(INPUT_GET , 'page' , FILTER_VALIDATE_INT);
	$this->rows         = filter_input(INPUT_GET , 'rows' , FILTER_VALIDATE_INT);
	$this->id           = filter_input(INPUT_GET , 'id');
	$_sortcol           = filter_input(INPUT_GET , 'sortcol');
	$_sortorder         = filter_input(INPUT_GET , 'sortorder');

	$sortcol            = !empty($_sortcol)	? strtoupper($_sortcol) : 'ADMNO';
	$sortorder          = !empty($_sortorder)	? strtoupper($_sortorder) : 'ASC';

    $sort_asc_col1  = $_sortcol=='admno' && $_sortorder=='desc' ? 'active-sort' : 'inactive-sort';
    $sort_desc_col1 = $_sortcol=='admno' && $_sortorder=='asc' ? 'active-sort' : 'inactive-sort';

    $sort_asc_col2  = $_sortcol=='fullname' && $_sortorder=='desc' ? 'active-sort' : 'inactive-sort';
    $sort_desc_col2 = $_sortcol=='fullname' && $_sortorder=='asc' ? 'active-sort' : 'inactive-sort';

	$page    =  $this->page>0 ? $this->page : 1;
	$rows    =  $this->rows>0 ? $this->rows : 10;
	$offset  = ($page-1)*$rows;
	$count_start    = ($page*$rows);

    $formcode     =  isset($_SESSION['att']['formcode']) ? $_SESSION['att']['formcode'] : null;
    $streamcode   =  isset($_SESSION['att']['streamcode']) ? $_SESSION['att']['streamcode'] : null;
    $yearcode     =  isset($_SESSION['att']['yearcode']) ? $_SESSION['att']['yearcode'] : null;
    $termcode     =  isset($_SESSION['att']['termcode']) ? $_SESSION['att']['termcode'] : null;
    $attdate      =  isset($_SESSION['att']['attdate']) ? $_SESSION['att']['attdate'] : null;
	 
	if(strlen($streamcode)>1){
	  $formstream_col = 'STREAMCODE';
	}else{
       $formstream_col = 'FORMCODE';
	}

    $data_sql   = ("
select DISTINCT ADMNO, FULLNAME ,STATUSID 
from VIEWATTENDANCE
where YEARCODE='{$yearcode}'
and TERMCODE='{$termcode}'
and {$formstream_col}='{$streamcode}'
and ATTDATE='{$attdate}'
order by {$sortcol} {$sortorder}
");
     $data = $db->SelectLimit($data_sql,$rows, $offset);
     $students  = array();

     if ($data) {
	  $numRecords =  $data->RecordCount();
      if ( $numRecords>0) {
      	while (!$data->EOF){
      	 $students[] = $data->fields;
      	 $data->MoveNext();
      	}
      }
     }

     $table_id =  ui::fi('tblMarks');

     echo '<table id="'.$table_id.'" cellpadding="0" cellspacing="0" width="100%" border="0" class="datagrid-htable datagrid-btable datagrid-ftable">';
      echo '<thead>';

       echo '<tr>';
        echo '
        <td  class="panel-header" colspan="3">
        <a  href="javascript:void(0);"  class="marks-menu"  onclick="'.$cfg['appname'].'.load_stage();"><i class="fa fa-edit"></i> Open</a> |
        <a  href="javascript:void(0);"  class="marks-menu"  onclick="'.$cfg['appname'].'.print_subject();"><i class="fa fa-print"></i> Print</a>
        </td>
        ';

       echo "<td class=\"panel-header\" colspan=\"4\">&nbsp;</td>";
       echo "</tr>";

       echo '<tr>';
        echo '<td  class="header left right">No</td>';

        echo '<td  class="header right">
             <a  href="javascript:void(0);" class="'.$sort_asc_col1.'" title="Sort by No. Descending"  class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'admno\',\'desc\','.$page.');"><i class="fa fa-chevron-down"></i></a>
             Adm.No
             <a  href="javascript:void(0);" class="'.$sort_desc_col1.'"  title="Sort by No. Ascending"   class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'admno\',\'asc\','.$page.');"><i class="fa fa-chevron-up"></i></a>
        </td>';

        echo '<td  class="header right">
             <a  href="javascript:void(0);" class="'.$sort_asc_col2.'" title="Sort by Name Descending"  class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'fullname\',\'desc\','.$page.');"><i class="fa fa-chevron-down"></i></a>
             Name
             <a  href="javascript:void(0);" class="'.$sort_desc_col2.'" title="Sort by Name Ascending"   class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'fullname\',\'asc\','.$page.');"><i class="fa fa-chevron-up"></i></a>
        </td>';

       echo "<td class=\"header right\">Status</td>";
       echo "</tr>";

      echo '</thead>';

      if(count($students)>0){
       echo '<tbody>';

       if($page==1){
        $count = 1;
       }else{
        $count = $offset+1;
       }

       $statues    = array();
       $statues[1] = 'Present';
       $statues[2] = 'Absent With Permission';
       $statues[3] = 'Absent With-Out Permission';
       
       foreach ($students as $student){

       	$statusid     = valueof($student,'STATUSID');
       	$admno        = valueof($student,'ADMNO');
       	$fullname     = valueof($student,'FULLNAME','**missing**');
       	$fullname_cls = str_replace("'", '', $fullname);
        $select_id    = "status_{$admno}";
        $select       = ui::form_select_fromArray($select_id, $statues, $statusid," onchange=\"{$cfg['appname']}.save_attendance('{$admno}','{$fullname_cls}','{$select_id}');\" ",false,150);

        echo '<tr>';
         echo "<td class=\"input left\" >{$count}</td>";
         echo "<td class=\"input\" >{$admno}</td>";
         echo "<td class=\"input\" >{$fullname}</td>";
         echo "<td class=\"input\" >{$select}</td>";
        echo '</tr>';

        ++$count;
       }

       echo '</tbody>';
      }

     echo '</table>';


$data_pager_sql   = ("
select count(ADMNO) NUM
from VIEWATTENDANCE
where YEARCODE='{$yearcode}'
and TERMCODE='{$termcode}'
and {$formstream_col}='{$streamcode}'
and ATTDATE='{$attdate}'
");
     $numRecords = $db->GetOne($data_pager_sql);

      echo '<table width="100%" cellpadding="2" cellspacing="0" border="0">';
	   echo '<tr>';
	   echo '<td>';
	   echo '<ul id="pagination">';

				$pages = $numRecords/10;
				$pages = ceil($pages);

				for($i=1; $i<=$pages; $i++){
					$li_color = $i==$page ? "#000" : "#0063DC";
					$li_class = $i==$page ? "page-active" : "#page-inactive";
					echo "<li class=\"nav {$li_class}\" id=\"li_{$i}\" style=\"color:{$li_color}\" onclick=\"{$cfg['appname']}.paginate(this.id,{$i},'{$_sortcol}','{$_sortorder}');\">Page {$i}</li>";
				}

	   echo '</ul>';
	  echo '</td>';
	 echo '</tr>';
	echo '</table>';

       echo "
     <script>
$('#{$table_id}').tableNav();
$('#{$table_id} input').eq(1).click();
</script>
";
	}

 public function save(){
  global $db,$cfg;
  
    $id       = filter_input(INPUT_POST , 'id', FILTER_SANITIZE_STRING);
    $admno    = filter_input(INPUT_POST , 'admno', FILTER_SANITIZE_STRING);
    $status   = filter_input(INPUT_POST , 'status', FILTER_SANITIZE_STRING);
    $comments = filter_input(INPUT_POST , 'comments', FILTER_SANITIZE_STRING);

    $formcode     =  isset($_SESSION['att']['formcode']) ? $_SESSION['att']['formcode'] : null;
    $streamcode   =  isset($_SESSION['att']['streamcode']) ? $_SESSION['att']['streamcode'] : null;
    $yearcode     =  isset($_SESSION['att']['yearcode']) ? $_SESSION['att']['yearcode'] : null;
    $termcode     =  isset($_SESSION['att']['termcode']) ? $_SESSION['att']['termcode'] : null;
    $attdate      =  isset($_SESSION['att']['attdate']) ? $_SESSION['att']['attdate'] : null;
	 
	if(strlen($streamcode)>1){
	  $formstream_col = 'STREAMCODE';
	}else{
       $formstream_col = 'FORMCODE';
	}
   
    $user   = new user();	
    $record = new ADODB_Active_Record('SASATT', array('ADMNO','STREAMCODE','YEARCODE','TERMCODE','ATTDATE'));
    
    $record->Load("
    ADMNO='{$admno}'
    AND STREAMCODE='{$streamcode}'
    AND YEARCODE='{$yearcode}'
    AND TERMCODE='{$termcode}'
    AND ATTDATE='{$attdate}'
    ");
    
    if(empty($record->_original)){
     $record->admno      = $admno;
     $record->streamcode = $streamcode;
     $record->yearcode   = $yearcode;
     $record->termcode   = $termcode;
     $record->attdate    = $attdate;
    }

    $record->statusid    = $status;

    if(!empty($comments)){
    $record->comments    = $comments;
    }
    
    $record->audituser   = $user->userid;
    $record->auditdate   = date('Y-m-d');
    $record->audittime   = time();
    	
    $save   = $record->Save();

  if($save){
   return json_response(1,'ok');
  }else{
   return json_response(0,'Save Failed.Try later');
  }     
  
 }

 
}
