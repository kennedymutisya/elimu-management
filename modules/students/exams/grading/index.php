<?php


$school = new school();
$forms = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
$streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
$years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
$terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
$exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');

$forms = array();
if (isset($streams_data)) {
    if (sizeof($streams_data) > 0) {
        foreach ($streams_data as $streamcode => $stream_data) {
            $streamname = valueof($stream_data, 'STREAMNAME');
            $formcode = valueof($stream_data, 'FORMCODE');
            $forms[$formcode] = "Form {$formcode}";
            $forms[$streamcode] = "Form {$formcode}-{$streamname}";
        }
    }
}

?>

<div class="easyui-panel" title="" style="background-color:#fff;font-size:12px;font-family:Verdana;" fit="true">
    <div style="padding:10px">
        <form id="ff<?php echo MNUID; ?>" method="post" novalidate>

            <table cellpadding="4" cellspacing="0" width="100%">

                <tr>
                    <td>Year:</td>
                    <td>
                        <?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode, " onchange=\"{$cfg['appname']}.list_exams();\" "); ?>
                    </td>
                </tr>

                <tr>
                    <td>Term:</td>
                    <td>
                        <?php echo ui::form_select_fromArray('term', $terms, $school->active_termcode, "onchange=\"{$cfg['appname']}.list_exams();\""); ?>
                    </td>
                </tr>

                <tr>
                    <td width="180px">Form</td>
                    <td>
                        <?php echo ui::form_select_fromArray('form', $forms, '', "onchange=\"{$cfg['appname']}.list_exams();\""); ?>
                    </td>
                </tr>

                <tr>
                    <td>Exam:</td>
                    <td>
                        <div id="div_exams<?php echo MNUID; ?>">
                            <?php echo ui::form_select_fromArray('exam', $exams); ?>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>Positioning</td>
                    <td>
                        <input type="radio" name="positioning" id="posm" value="m" checked><label for="posm">By
                            Marks</label>
                        <input type="radio" name="positioning" id="posp" value="p"><label for="posp">By Points</label>
                    </td>
                </tr>

                <tr>
                    <td>Subject Score to Display</td>
                    <td>
                        <input type="radio" name="subjscore" id="subjscorer" value="r" checked><label for="subjscorer">as
                            Raw Marks</label>
                        <input type="radio" name="subjscore" id="subjscorep" value="p"><label for="subjscorep">Out of
                            100%</label>
                    </td>
                </tr>

                <!--
                                <tr>
                                    <td>*Optional</td>
                                    <td>
                                     <input type="checkbox" name="position_subjects" id="position_subjects"  value="1" >Position Subjects<label for="position_subjects"></label>
                                    </td>
                                </tr>
                -->

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <a href="javascript:void(0)" class="easyui-linkbutton"
                           onclick="<?php echo $cfg['appname']; ?>.grade();"><i class="fa fa-spinner"></i> Grade
                            Students</a>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <div id="div_grade<?php echo MNUID; ?>"></div>
                    </td>
                </tr>

            </table>

        </form>
    </div>
</div>

<script>

    var <?php echo $cfg['appname']; ?> =
    {
        clearForm:function () {
            $('#ff<?php echo MNUID; ?>').form('clear');
        }
    ,
        list_exams:function () {
            $.post('./endpoints/crud/', 'module=<?php echo $vars; ?>&function=list_exams&' + $('#ff<?php echo MNUID; ?>').serialize(), function (html) {
                $('#div_exams<?php echo MNUID; ?>').html(html);
            });
        }
    ,
        grade:function () {
            $.messager.progress();
            var fdata = $('#ff<?php echo MNUID; ?>').serialize() + '&module=<?php echo $vars; ?>&function=grade';
            $.post('./endpoints/crud/', fdata, function (data) {
                $.messager.progress('close');
                if (data.success === 1) {
                    $.messager.show({title: 'Success', msg: data.message});
                    <?php echo $cfg['appname']; ?>.
                    print_marks();
                } else {
                    $.messager.alert('Error', data.message, 'error');
                }
            }, "json");
        }
    ,
        print_marks:function () {
            var fdata = $('#ff<?php echo MNUID; ?>').serialize();
            var exam_name = $("#<?php echo ui::fi('exam');?> option:selected").text();
            var w = window.open('./endpoints/print/?module=<?php echo $vars; ?>&exam_name=' + exam_name + '&' + fdata, '<?php echo ui::fi('pw'); ?>', 'height=800,width=1000,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
            w.focus();
        }
    ,
    }

</script>
