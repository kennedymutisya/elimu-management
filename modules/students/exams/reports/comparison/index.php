<?php
//$exams = $db->GetAssoc("SELECT DISTINCT TERMCODE,YEARCODE FROM SASTUDSUB ORDER BY YEARCODE");
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$exams = $db->GetAll("SELECT DISTINCT yearcode,termcode,exam FROM sastmn order by yearcode,termcode");
$exams = $db->GetAll("SELECT DISTINCT yearcode,termcode,exam FROM sastmn order by yearcode,termcode");
//var_dump(unpackvars($vars));
$forms = array();
$formsas = $db->GetAssoc('SELECT FORMNAME,FORMCODE FROM SAFORMS ORDER BY FORMCODE');
foreach ($exams as $k => $exam) {
    $forms[$k] = valueof($exam, 'exam') . ' :Y' . valueof($exam, 'yearcode') . ' T' . valueof($exam, 'termcode');
}

?>
<p>Compare two exams</p>

<form id="<?php echo MNUID ?>" method="post" novalidate onsubmit="return false;">
    <table cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td width="">Form</td>
            <td width="">
                <div id="<?php echo ui::fi('form'); ?>">
                <?php echo ui::form_select_fromArray('form', $formsas, ''); ?>
                </div>
            </td>

        </tr>
        <tr>
            <td width="">Choose Exam</td>
            <td width="">
                <div id="<?php echo ui::fi('exama'); ?>">
                <?php echo ui::form_select_fromArray('exama', $forms, ''); ?>
                </div>
            </td>

        </tr>
        <tr>
            <td width="">Compare With</td>
            <td width="">
                <div id="<?php echo ui::fi('div_exams'); ?>">
                    <?php echo ui::form_select_fromArray('exam', $forms); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton"
                   onclick="<?php echo $cfg['appname']; ?>.print_report();"><i class="fa fa-print"></i> Open Report</a>
            </td>
        </tr>

    </table>
</form>


<script>

    var <?php echo $cfg['appname']; ?> =
    {
        clearForm:function () {
            $('#<?php echo ui::fi('ff'); ?>').form('clear');
        }
    ,
        list_exams:function () {
            $.post('./endpoints/crud/', 'modvars=<?php echo $vars; ?>&function=list_exams&' + $('#<?php echo ui::fi('ff'); ?>').serialize(), function (html) {
                $('#<?php echo ui::fi('div_exams'); ?>').html(html);
            });
        }
    ,
        print_report:function () {
            var fdata = $('#ff<?php echo MNUID; ?>').serialize();
            var form = $("#<?php echo ui::fi('form');?> option:selected").text();
            var examcurrent = $("#<?php echo ui::fi('exama');?> option:selected").text();
            var examcompare = $("#<?php echo ui::fi('div_exams');?> option:selected").text();
            var w = window.open('./endpoints/print/?module=<?php echo $vars; ?>&form=' + form +
            '&examcurrent=' + examcurrent +
                '&examcompare=' + examcompare +
                '&' + fdata, '<?php echo ui::fi('pw'); ?>', 'height=800,width=1000,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
            w.focus();
        }
    ,
    }

</script>
