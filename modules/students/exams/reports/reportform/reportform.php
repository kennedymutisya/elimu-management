<?php

  class reportform {
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
	  $this->admno        = filter_input(INPUT_POST , 'admno');
	}
	
	public function get_profile(){
		global $db,$cfg;
		//$db->debug=1;//remove	
         $student    = new student($this->admno,0);
         $school     = new school();
         $streamcode = $student->streamcode;
         $streamcode_alpha = preg_replace("/[^A-Za-z]/", '', $streamcode);
         $exams = array();
         $streams = $db->GetAssoc("SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS WHERE FORMCODE<='{$student->formcode}' AND STREAMCODE LIKE '%{$streamcode_alpha}%'");
		 $years   = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
		 $terms   = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
		 $exams   = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
		 
		 $forms = array();
		 if(isset($streams)){
		  if(sizeof($streams)>0){	
		   foreach ($streams as $streamcode=>$stream_data){
		   	$streamname = valueof($stream_data, 'STREAMNAME');
		   	$formcode   = valueof($stream_data, 'FORMCODE');
		   	$forms[$streamcode] = "Form {$streamname}";
		   }
		  }
		 }
		
		asort($years);
		asort($forms);
		asort($terms);
		?>
		<table cellpadding="2" cellspacing="0" width="100%" >
	    		
	             <tr>
	    			<td width="100px">Year:</td>
	    			<td><?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_exams();\" ");  ?></td>
	    		 </tr>
	    		 
	    		 <tr>
	    			<td>Term:</td>
	    			<td><?php echo ui::form_select_fromArray('term', $terms,$school->active_termcode,"onchange=\"{$cfg['appname']}.list_exams();\"");  ?></td>
	    		 </tr>
	    		
	    		<tr>
	    			<td>Form</td>
	    			<td><?php echo ui::form_select_fromArray('form', $forms, $student->streamcode,"onchange=\"{$cfg['appname']}.list_exams();\"");  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Exam:</td>
	    			<td>
	    			   <div id="<?php echo ui::fi('div_exams'); ?>">
	    				<?php echo ui::form_select_fromArray('exam', $exams);  ?>
	    				</div>
	    			</td>
	    		</tr>
	    		
				 <tr>
					<td>Positioning</td>
					<td>
					 <input type="radio" name="<?php echo ui::fi('positioning'); ?>" id="<?php echo ui::fi('posm'); ?>"  value="m" checked ><label for="posm">By Marks</label>
					 <input type="radio" name="<?php echo ui::fi('positioning'); ?>" id="<?php echo ui::fi('pop'); ?>"  value="p"><label for="posp">By Points</label>
					</td>
				  </tr>
				  
	    		<tr>
	    			<td>&nbsp;</td>
	    			<td><button onclick="<?php echo $cfg['appname']; ?>.print_report();"><i class="fa fa-print"></i> Open Report</button></td>
	    		</tr>
	    		
	  </table>
	
	  <script>
	  <?php echo $cfg['appname'];?>.list_exams();
	  </script>
	<?php
	}
	
	public function list_exams(){
		global $db;
	
	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));	
	 $examcode     = filter_input(INPUT_POST , ui::fi('exam'));	
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));
		
	 if(strlen($formcode)>1){
	 	$formcode = substr($formcode,0,1);
	 }
	 
	  $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");
	  
     $exams_array = array();
      
       if($exams){
       	if(sizeof($exams)>=1){
       		
       		for ($c=1;$c<=5;++$c){
       		 if(isset($exams["CATNAME{$c}"])){
       		 	 $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
       		 }
       		}
       		
       		for ($e=1;$e<=5;++$e){
       		 if(isset($exams["EXAMNAME{$e}"])){
       		 	 $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
       		 }
       		}
       		
       		$exams_array['TOTAL'] = 'Combined';
       		
       	}
       }
       
	  return ui::form_select_fromArray('exam',$exams_array, $examcode," onchange=\"\" ");
	  
	}
	
	
}
