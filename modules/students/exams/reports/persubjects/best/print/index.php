<?php

$dateNow     = textDate(null, true);
$school      = new school();

$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$formstream  = filter_input(INPUT_GET , ui::fi('form'));
$exam        = filter_input(INPUT_GET , ui::fi('exam'));


if(strlen($formstream)>1){
 $formcode            = $formstream;
 $formstream_code_col = "STREAMCODE";
}else{
 $formcode            = substr($formstream,0,1);
 $formstream_code_col = "FORMCODE";
 }

$subject_grades = array();

$subjects  = $db->CacheGetAssoc(1200,"
       select distinct SUBJECTCODE,SUBJECTNAME FROM VIEWSTUDENTSUBJECTS
       WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
        AND {$formstream_code_col}='{$formstream}'
        ORDER BY SUBJECTCODE
        ");

$best = array();

if($exam=='TOTAL'){
 $gradecode_col = 'GRADECODE';
 $pos_col       = 'TOTALPOS';
 $exam_col      = 'TOTAL';
}else{
 $gradecode_col = $exam.'GRADECODE';
 $pos_col       = $exam.'POSF';
 $exam_col      = $exam.'P';
}

 $top  = $db->GetArray("
	SELECT ADMNO,FULLNAME,STREAMCODE,SUBJECTCODE,{$exam_col} SCORE,{$gradecode_col}  GRADECODE
	FROM VIEWSTUDENTSUBJECTS
	WHERE YEARCODE='{$yearcode}'
	AND TERMCODE='{$termcode}'
	AND {$formstream_code_col}='{$formstream}'
	AND {$exam}>0
	AND {$pos_col}=1
    ");

 foreach($top as $top_students){
 $admno       = valueof($top_students, 'ADMNO');
 $fullname    = valueof($top_students, 'FULLNAME');
 $streamcode  = valueof($top_students, 'STREAMCODE');
 $subjectcode = valueof($top_students, 'SUBJECTCODE');
 $score       = valueof($top_students, 'SCORE','','round');
 $gradecode   = valueof($top_students, 'GRADECODE');

 $best[$subjectcode][] = array(
          'admno'       => $admno,
          'fullname'    => $fullname,
           'streamcode' => $streamcode,
           'score'      => $score,
           'gradecode'  => $gradecode
           );
 }

 ?>
 <html>
  <head>
   <title>Best Per Subject</title>
  <script language="JavaScript" type="text/javascript">
    setTimeout("window.print();", 10000);
</script>
<style>
 body {
 padding : 10px;
 margin : 10px;
 font-size:12px;
 }
    table {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    td {
        border:1px solid #ccc;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:12px;
    }

    td.bold {
        font-weight:bold;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }

    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  {
            display: block;
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  {
            display: block;
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

  </head>
 <body>
 <?php

         if(sizeof($best)==0) {
          print "First Rank Subjects in Form {$formstream} - Term {$termcode} {$yearcode}</b>";
		 }else{
 	 	  echo "
 	 	  <table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"3\" class=\"data\">
 	 	  <tr>
 	 	   <td colspan=\"7\">

 	 	   <table width=\"100%\"  style=\"border:0\"  cellspacing=\"0\" cellpadding=\"1\" class=\"data\">

 	 	    <tr>
 	 	     <td rowspan=\"6\" style=\"border:0\" ><img src=\"{$school->logo_path}\" ></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td valign=\"top\"  style=\"border:0\"  colspan=\"2\"><span class=\"title\">{$school->name}</span></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  colspan=\"2\" style=\"border:0\" ><b>Address : {$school->address}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" ><b>Tel :{$school->telephone}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" ><b>Vision :{$school->vision}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" >&nbsp;</td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  colspan=\"3\"  style=\"border:0\"   ><b>BEST PER SUBJECT - Form {$formstream} - Term {$termcode} {$yearcode}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  colspan=\"3\"  style=\"border:0\" align=\"right\"><b>Date : </b>{$dateNow}</td>
 	 	    </tr>

 	 	   </table>
 	 	 </td>
 	 	 </tr>";


		   $count = 1;

		 echo "<tr>";
 	 	  echo "<td class=\"bold\">S/N</td>";
 	 	  echo "<td class=\"bold\">Admo.No</td>";
 	 	  echo "<td class=\"bold\">Full Name</td>";
 	 	  echo "<td class=\"bold\">Class</td>";
 	 	  echo "<td class=\"bold\">Subject</td>";
 	 	  echo "<td class=\"bold\">Score</td>";
 	 	  echo "<td class=\"bold\">Grade</td>";
 	 	  //echo "<td>&nbsp;</td>";
 	 	  //echo "<td>&nbsp;</td>";
 	 	 echo "</tr>";

        foreach($best as $subjectcode => $best_students) {
         foreach($best_students as $best_student) {
          $admno       = valueof($best_student, 'admno');
          $fullname    = valueof($best_student, 'fullname');
          $streamcode  = valueof($best_student, 'streamcode');
          $admno       = valueof($best_student, 'admno');
          $score       = valueof($best_student, 'score');
          $gradecode   = valueof($best_student, 'gradecode');
          $subjectname = valueof($subjects, $subjectcode);

 	 	 echo "<tr>";
 	 	  echo "<td>{$count}</td>";
 	 	  echo "<td>{$admno}</td>";
 	 	  echo "<td>{$fullname}</td>";
 	 	  echo "<td>{$streamcode}</td>";
 	 	  echo "<td>{$subjectname}</td>";
 	 	  echo "<td>{$score}</td>";
 	 	  echo "<td>{$gradecode}</td>";
 	 	  //echo "<td>&nbsp;</td>";
 	 	  //echo "<td>&nbsp;</td>";
 	 	 echo "</tr>";

 	 	++$count;

 	   }
 	   }

 	  echo "</table>";

    }

 ?>
 </body>
  </html>

