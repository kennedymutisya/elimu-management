<?php

$dateNow     = textDate(null, true);
$yearcode    = filter_input(INPUT_GET , ui::fi('yearcode'));
$termcode    = filter_input(INPUT_GET , ui::fi('termcode'));
$exam        = filter_input(INPUT_GET , ui::fi('exam'));
$subjectcode = filter_input(INPUT_GET , ui::fi('subjectcode'));
$formstream  = filter_input(INPUT_GET , ui::fi('streamcode'));
$poscol_stream  = "{$exam}POS";
$poscol_form    = "{$exam}POSF";
$percentcol     = "{$exam}P";
    
 if(strlen($formstream)>1){
  $formcode            = substr($formstream,0,1);
  $formstream_code_col = "STREAMCODE";
  $class_name          = $formstream;
 }else{
  $formcode            = $formstream;
  $formstream_code_col = "FORMCODE";
  $class_name          = "ALL";
 }


$school = new school();

 
$subject_grades = array();
	
$data  = $db->Execute("
        SELECT * FROM VIEWSTUDENTSUBJECTS 
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}' 
        AND {$formstream_code_col}='{$formstream}' 
        AND SUBJECTCODE='{$subjectcode}' 
        ORDER BY {$poscol_form}
        ");

$subjects       = $db->GetAssoc("select SUBJECTCODE,SUBJECTNAME from SASUBJECTS ORDER BY  CODEOFFC");


$subjectname  = valueof($subjects, $subjectcode);
 ?>
 <html>
  <head>
   <title>Position Per Subject</title>
  <script language="JavaScript" type="text/javascript">
    //setTimeout("window.print();", 10000);
</script>

<style>
 body {
 padding : 10px;
 margin : 10px;
 font-size:12px;
 }
    table {
        font-family:Verdana;
        font-size:10px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    td {
        border:1px solid #ccc;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:10px;
    }

    td.bold {
        font-weight:bold;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }
    
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }
    
</style>

  </head>
 <body>
 <?php
 if($data) {
 	
     if(strstr($exam,'CAT') || strstr($exam,'PAPER')){
        $pointscol    = "{$exam}POINTS";
        $gradecodecol = "{$exam}GRADECODE";
       }else{
       	$pointscol    = "POINTS";
       	$gradecodecol = "GRADECODE";
       }
       
    $gradecodecol   = strtoupper($gradecodecol);
    
 	$subjects_data = array();
 	$distinct_subjects = array();
 	$students = array();
 	
 	if($data->RecordCount()>0) {
 		while (!$data->EOF) {
       	 	
       	 	$admno        = valueof($data->fields, 'ADMNO');	
       	 	$fullname     = valueof($data->fields, 'FULLNAME');	
       	 	$subjectcode  = valueof($data->fields, 'SUBJECTCODE');	
       	 	$codeoffc     = valueof($data->fields, 'CODEOFFC');	
       	 	$catcode      = valueof($data->fields, 'CATCODE');	
       	 	$formcode     = valueof($data->fields, 'FORMCODE');
       	 	$streamcode   = valueof($data->fields, 'STREAMCODE');
       	 	$sbccode      = valueof($data->fields, 'SBCCODE');
       	 	$position_st  = valueof($data->fields, $poscol_stream);
       	 	$position_fm  = valueof($data->fields, $poscol_form);
       	 	$gradecode    = valueof($data->fields, $gradecodecol);
       	 	$points       = valueof($data->fields, $pointscol);
       	 	$percent      = valueof($data->fields, $percentcol);
       	 	
       	 	$students[] = array($admno,$fullname,$streamcode,$percent,$gradecode,$position_st,$position_fm);
       	 	
       	 	if(!array_key_exists($subjectcode, $distinct_subjects)){
       	 		$distinct_subjects[$subjectcode] = $codeoffc;
       	 	}
       	 	
       	 	$subjects_gradecode    = valueof($data->fields, $gradecodecol);
       	 	$subjects_points       = valueof($data->fields, $pointscol);
       	 	
       	 	$subjects_data[$subjectcode][$subjects_gradecode][]     = $subjects_points;
       	 	
       	  $data->MoveNext();
       	 }
 	}
 	
 	 asort($distinct_subjects);
 	 
 	 if(sizeof($students)>0){
 	 	$page_rows    = 1;
 	 	$pageno       = 1;
 	 	$page_num_lines       = 30;
 	 	
 	 	$header = "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">";
 	 	 $header .= "<tr>";
 	 	  $header .= "<td colspan=\"8\">
 	 	 
 	 	   <table width=\"100%\"  style=\"border:0\"  cellspacing=\"0\" cellpadding=\"1\" class=\"data\">
 	 	   
 	 	    <tr>
 	 	     <td rowspan=\"6\" style=\"border:0\" ><img src=\"{$school->logo_path}\" ></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td valign=\"top\"  style=\"border:0\"  colspan=\"2\"><span class=\"title\">{$school->name}</span></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"2\" style=\"border:0\" ><b>Address : {$school->address}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" ><b>Tel :{$school->telephone}</b></td>
 	 	    </tr>
 	 	    
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" ><b>Motto :{$school->motto}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" ><strong>STUDENT POSITION PER SUBJECT</strong></td>
 	 	    </tr>

 	 	    
 	 	    <tr>
 	 	     <td colspan=\"1\" style=\"border:0\" ><b>{$subjectname}</b> <b>{$yearcode}</b> <b>T{$termcode}</b> <b>{$exam}</b></td>
 	 	     <td colspan=\"2\" style=\"border:0\"  align=\"right\"><b>Date : </b>{$dateNow}</td>
 	 	    </tr>
 	 	   
 	 	   </table>
 	 	    
 	 	 </td>";
 	 	$header .= "</tr>";
 	 	
 	 	echo $header;
 	 	
 	 	$header = "<tr>";
 	 	 $header .="<td class=\"bold\">#</td>";
 	 	 $header .="<td class=\"bold\">Adm. No</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>Stream</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>Name</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>Percent</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>Grade</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>Pos. Stream</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>Pos. Form</td>";
 	 	$header .="</tr>";
 	 	
 	 	echo $header;
	
 	 	$count = 1;
 	 	$count_cumm = 1;
 	 	foreach ($students as $student){
 	 	
 	 	echo "<tr>";
 	 	 echo "<td>".$count_cumm."</td>";
 	 	 echo "<td>".valueof($student,0)."</td>";
 	 	 echo "<td>".valueof($student,2)."</td>";
 	 	 echo "<td>".valueof($student,1)."</td>";
 	 	 echo "<td>".valueof($student,3)."</td>";
 	 	 echo "<td>".valueof($student,4)."</td>";
 	 	 echo "<td>".valueof($student,5)."</td>";
 	 	 echo "<td>".valueof($student,6)."</td>";
 	 	echo "</tr>";
    
       ++$count_cumm;
       
       if($count==25){
		$count = 1;
	    echo "</table>"; 
	    echo " \r\n<div class=\"page-break\"></div>  \r\n";
		echo "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">";
		echo $header;  
		
	   }
	   
	   ++$count;
 	 	
 	  }
 	 	
 	 	echo "</table>";
 	 
 	 }
 }
 
 
 ?>
 </body>
  </html>
 
