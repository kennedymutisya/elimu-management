<?php

ignore_user_abort(true);
set_time_limit(0);
ini_set("max_execution_time", 600000000);
ini_set("memory_limit", '128M');


$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$formstream  = filter_input(INPUT_GET , ui::fi('form'));
$admno       = filter_input(INPUT_GET , ui::fi('find_admno'));
$html_file   = ROOT."tmp_files/{$yearcode}_{$termcode}_{$formstream}.html";  

 if(strlen($formstream)>1){
  $formcode            = substr($formstream,0,1);
  $formstream_code_col = "STREAMCODE";
 }else{
  $formcode            = $formstream;
  $formstream_code_col = "FORMCODE";
 }
     
$school       = new school(120);
$dateNow      = textDate(null, true);
$dateY        = date('Y');
$where_am_i   = str_replace($_SERVER['DOCUMENT_ROOT'],'', ROOT);
$server_name  = $_SERVER['SERVER_NAME'];
$server_port  = $_SERVER['SERVER_PORT'];

$x = pathinfo(realpath($_SERVER['DOCUMENT_ROOT']));
$y = pathinfo(realpath(ROOT));
$z = '';

if(isset($x['dirname']) && isset($y['dirname'])){
 if($x != $y){
  $z = $y['filename'];
 }
}

$server_root =  "http://{$server_name}:{$server_port}/{$z}/";

if(is_readable($html_file)) unlink($html_file);

$css        = get_css();
$css_len    = strlen($css);
if(!$handle = fopen($html_file, 'a')){ die("could not open {$html_file} for write"); }
if(!$write  = fwrite($handle,$css, $hml_len)){ die("could save {$html_file}"); }
fclose($handle);

require_once ( ROOT.'lib/jpgraph/jpgraph.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_bar.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_line.php');

function prepare_studentperf($admno){
	global $db,$school,$html_file;
	
$student           = new student($admno);

$students_subjects = $db->Execute("
SELECT SUBJECTCODE,SUBJECTNAME,FORMCODE,TERMCODE,TOTAL,GRADECODE FROM VIEWSTUDENTSUBJECTS 
WHERE ADMNO='{$admno}'
ORDER BY FORMCODE ASC,TERMCODE ASC,SORTPOS ASC
");

$distinct_subjects  = array();
$subject_totals     = array();
$graph_files        = array();

if($students_subjects){
 if($students_subjects->RecordCount()>0){
  foreach($students_subjects as $subject){
	$subjectcode      = valueof($subject, 'SUBJECTCODE');
	$subjectname      = valueof($subject, 'SUBJECTNAME', $subjectcode);
	$formcode         = valueof($subject, 'FORMCODE');
	$termcode         = valueof($subject, 'TERMCODE');
	$gradecode        = valueof($subject, 'GRADECODE');
	$total            = valueof($subject, 'TOTAL');
	$total            = $total*1;
	
	$distinct_subjects[$subjectcode] = $subjectname;
	$subject_totals[$subjectcode][$formcode][$termcode]    =  array($total,$gradecode);
  }
 }
}

if(sizeof($distinct_subjects)>0){
 foreach($distinct_subjects as $subjectcode => $subjectname){
	$graph_files_path = graph_subject_performance($admno, $subjectcode, $subjectname);
	$graph_files[]    =  array( $subjectcode,$graph_files_path);
 }
}

$html = '<table cellpadding="2" class="plain" cellspacing="5"   >';

$html .= "<tr>";
  $html .= "<td colspan=\"3\" align=\"center\" class=\"bottom title\">{$school->name}</td>"; 
$html .= "</tr>";

$html .= "<tr>";
  $html .= "<td colspan=\"3\" align=\"left\" class=\"bottom sub-title\">ADM NO:{$student->admno}  NAME:{$student->fullname} FORM: {$student->streamname} {$student->yearcode} KCPE: {$student->kcpemarks}</td>"; 
$html .= "</tr>";

$html .= "<tr>";
 if(isset($graph_files[0][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[0][1]} ".year_totals($graph_files[0][0])." </td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
 if(isset($graph_files[1][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[1][1]} ".year_totals($graph_files[1][0])."</td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
 if(isset($graph_files[2][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[2][1]} ".year_totals($graph_files[2][0])."</td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
$html .= "</tr>";

$html .= "<tr>";
 if(isset($graph_files[3][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[3][1]} ".year_totals($graph_files[3][0])."</td>"; }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
 if(isset($graph_files[4][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[4][1]} ".year_totals($graph_files[4][0])."</td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
 if(isset($graph_files[5][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[5][1]} ".year_totals($graph_files[5][0])."</td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
$html .= "</tr>";

$html .= "<tr>";
 if(isset($graph_files[6][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[6][1]} ".year_totals($graph_files[6][0])."</td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
 if(isset($graph_files[7][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[7][1]} ".year_totals($graph_files[7][0])."</td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
 if(isset($graph_files[8][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[8][1]} ".year_totals($graph_files[8][0])."</td>";  }else{ $html .= "<td class=\"cell\" >&nbsp;</td>"; }
$html .= "</tr>";

$html .= "<tr>";
 if(isset($graph_files[9][1])){  $html .= "<td class=\"cell\" align=\"center\">{$graph_files[9][1]}  ".year_totals($graph_files[9][0])."</td>";  }else{ $html .= "<td class=\"\" >&nbsp;</td>"; }
 if(isset($graph_files[10][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[10][1]} ".year_totals($graph_files[10][0])."</td>";  }else{ $html .= "<td class=\"\" >&nbsp;</td>"; }
 if(isset($graph_files[11][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[11][1]} ".year_totals($graph_files[11][0])."</td>";  }else{ $html .= "<td class=\"\" >&nbsp;</td>"; }
$html .= "</tr>";

$html .= "<tr>";
 if(isset($graph_files[12][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[12][1]} ".year_totals($graph_files[12][0])."</td>";  }else{ $html .= "<td class=\"\" >&nbsp;</td>"; }
 if(isset($graph_files[13][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[13][1]} ".year_totals($graph_files[13][0])."</td>";  }else{ $html .= "<td class=\"\" >&nbsp;</td>"; }
 if(isset($graph_files[14][1])){ $html .= "<td class=\"cell\" align=\"center\">{$graph_files[14][1]} ".year_totals($graph_files[14][0])."</td>";  }else{ $html .= "<td class=\"\" >&nbsp;</td>"; }
$html .= "</tr>";

$html .= "</table>";
$html .= " \r\n<div class=\"page-break\"></div>  \r\n";

$hml_len    = strlen($html);
if(!$handle = fopen($html_file, 'a')){ die("could not open {$html_file} for write"); }
if(!$write  = fwrite($handle,$html, $hml_len)){ die("could save {$html_file}"); }
fclose($handle);

}

function graph_subject_performance($admno, $subjectcode, $subjectname){
 global $db,$server_root;
 
$subjects_data = array();

$data_sql  = ("
select FORMCODE,TERMCODE,SUBJECTCODE,TOTAL
from SASTUDSUB 
WHERE ADMNO='{$admno}'
AND SUBJECTCODE='{$subjectcode}'
ORDER BY FORMCODE,TERMCODE
");

$data   = $db->Execute($data_sql);
$forms  = range(1,4);
$terms  = range(1,3);

foreach($forms as $form){
 foreach($terms as $term){
	$subjects_data[$form][$term] =  null;
 }
}

if($data){
 if($data->RecordCount()>0){
	 foreach($data as $row){
		 $formcode     = isset($row['FORMCODE']) ? $row['FORMCODE'] : '';
		 $termcode     = isset($row['TERMCODE']) ? $row['TERMCODE'] : '';
		 $subjectcode  = isset($row['SUBJECTCODE']) ? $row['SUBJECTCODE'] : '';
		 $total        = isset($row['TOTAL']) ? $row['TOTAL']*1 : null;
		 
		 $subjects_data[$formcode][$termcode] = $total;
		 
	 }
 }
}

$data1y = array( $subjects_data[1][1] ,$subjects_data[2][1] ,$subjects_data[3][1] ,$subjects_data[4][1] );//T1
$data2y = array( $subjects_data[1][2] ,$subjects_data[2][2] ,$subjects_data[3][2] ,$subjects_data[4][2] );//T2
$data3y = array( $subjects_data[1][3] ,$subjects_data[2][3] ,$subjects_data[3][3] ,$subjects_data[4][3] );//T3


$avg_f1  = ($data1y[0]+$data2y[0]+$data3y[0]) ? round(($data1y[0]+$data2y[0]+$data3y[0])/3,0) : null;
$avg_f2  = ($data1y[1]+$data2y[1]+$data3y[1]) ? round(($data1y[1]+$data2y[1]+$data3y[1])/3,0) : null;
$avg_f3  = ($data1y[2]+$data2y[2]+$data3y[2]) ? round(($data1y[2]+$data2y[2]+$data3y[2])/3,0) : null;
$avg_f4  = ($data1y[3]+$data2y[3]+$data3y[3]) ? round(($data1y[3]+$data2y[3]+$data3y[3])/3,0) : null;

$l1datay = array( 
 $avg_f1,
 $avg_f2,
 $avg_f3,
 $avg_f4
);

// Create the graph. These two calls are always required
$graph = new Graph(350,200,'auto');
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;
$graph->SetTheme($theme_class);

//print_pre($l1datay);
//$graph->yaxis->SetTickPositions(array(0,30,60,90,120,150), array(15,45,75,105,135));
//$graph->yaxis->SetTickPositions(array(0,10,20,30,40,50,60,70,80,90,100,110), array(5,15,25,35,45,55,65,75,85,95,105));
$graph->SetBox(false);

$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array('Form 1','Form 2','Form 3','Form 4'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

// Create the bar plots
$b1plot = new BarPlot($data1y);
$b2plot = new BarPlot($data2y);
$b3plot = new BarPlot($data3y);

// Create the linear error plot - erax
$l1plot=new LinePlot($l1datay);
$l1plot->SetColor('black');//not working :(
$l1plot->SetWeight(1);
$l1plot->SetLegend('Average');

// Create the grouped bar plot
$gbplot = new GroupBarPlot(array($b1plot,$b2plot,$b3plot));
$gbplot->SetWidth(40);

// ...and add it to the graPH
$graph->Add($gbplot);
$graph->Add($l1plot);// - erax

$b1plot->SetColor("white");
$b1plot->SetFillColor("#cc1111");
$b1plot->value->Show();
$b1plot->value->SetFormat('%d');
$b1plot->value->SetFont(FF_FONT1,FS_NORMAL,8);
$b1plot->SetValuePos('top');//center

$b2plot->SetColor("white");
$b2plot->SetFillColor("#1111cc");
$b2plot->value->Show();
$b2plot->value->SetFormat('%d');
$b2plot->value->SetFont(FF_FONT1,FS_NORMAL,8);
$b2plot->SetValuePos('top');

$b3plot->SetColor("white");
$b3plot->SetFillColor("#29EA23");
$b3plot->value->Show();
$b3plot->value->SetFormat('%d');
$b3plot->value->SetFont(FF_FONT1,FS_NORMAL,8);
$b3plot->SetValuePos('top');

$graph->title->Set($subjectname);

$graph_path       = ROOT.'tmp_files/'.$admno.'-'.$subjectcode.".png";
$graph_path_web   = 'tmp_files/'.$admno.'-'.$subjectcode.".png";

$graph->Stroke($graph_path);

return  "<img src=\"{$server_root}{$graph_path_web}\">";	
}

function year_totals( $subjectcode  ){
 global $subject_totals;
 
$summary = '<table class="summary" cellpadding="2"   cellspacing="1"   >';

$summary .=  "<tr>";
  $summary .=  "<td colspan=\"3\">Form 1</td>"; 
  $summary .=  "<td colspan=\"3\">Form 2</td>"; 
  $summary .=  "<td colspan=\"3\">Form 3</td>"; 
  $summary .=  "<td colspan=\"2\">Form 4</td>";  
$summary .=  "</tr>";

$summary .=  "<tr>";
  $summary .=  "<td>T1</td>"; 
  $summary .=  "<td>T2</td>"; 
  $summary .=  "<td>T3</td>"; 
  $summary .=  "<td>T1</td>"; 
  $summary .=  "<td>T2</td>"; 
  $summary .=  "<td>T3</td>"; 
  $summary .=  "<td>T1</td>"; 
  $summary .=  "<td>T2</td>"; 
  $summary .=  "<td>T3</td>";
  $summary .=  "<td>T1</td>"; 
  $summary .=  "<td>T2</td>"; 
$summary .=  "</tr>";

$summary .=  "<tr>";
  $summary .=  "<td>".subject_score($subjectcode,1,1)."</td>";  //F1T1
  $summary .=  "<td>".subject_score($subjectcode,1,2)."</td>";  //F1T2
  $summary .=  "<td>".subject_score($subjectcode,1,3)."</td>";  //F1T2
  $summary .=  "<td>".subject_score($subjectcode,2,1)."</td>";  //F2T1
  $summary .=  "<td>".subject_score($subjectcode,2,2)."</td>";  //F2T2
  $summary .=  "<td>".subject_score($subjectcode,2,3)."</td>";  //F2T2 
  $summary .=  "<td>".subject_score($subjectcode,3,1)."</td>";  //F3T1
  $summary .=  "<td>".subject_score($subjectcode,3,2)."</td>";  //F3T2
  $summary .=  "<td>".subject_score($subjectcode,3,3)."</td>";  //F3T2
  $summary .=  "<td>".subject_score($subjectcode,4,1)."</td>";  //F4T1
  $summary .=  "<td>".subject_score($subjectcode,4,2)."</td>";  //F4T2
$summary .=  "</tr>";

$summary .= '</table>';

return $summary;
}

function subject_score($subjectcode,$formcode,$termcode){
	global $subject_totals;
	
	$total     = '';
	$gradecode = '';
	
	if(isset($subject_totals[$subjectcode][$formcode][$termcode][0])){
		$total     = $subject_totals[$subjectcode][$formcode][$termcode][0];
	}
	
	
	if(isset($subject_totals[$subjectcode][$formcode][$termcode][1])){
		$gradecode   = $subject_totals[$subjectcode][$formcode][$termcode][1];
	}
	
	return $total>0 ? ($total.':'.$gradecode) : '&nbsp;';
}

function get_css(){
 return <<<CSS
  <style>
	
	 body {
	  padding : 0px;
	  margin : 2px;
	  font-size:9px;
	 }
    
    table.report-form {
        font-family:Verdana;
        font-size:9px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    table.report-form td {
        border:1px solid #ccc;
        vertical-align: top;
    }
     
   table.plain {
        font-family:Verdana;
        font-size:9px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
        vertical-align: middle;
    }

    table.plain td {
        border:1px solid #fff;
        vertical-align: middle;
    }
    
   table.summary {
        font-family:Verdana;
        font-size:10px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    table.summary td {
        border:1px solid #ccc;
        text-align:center;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:9px;
    }

    td.bottom {
        border-bottom:1px solid #000;
    }

    td.cell {
        border:1px solid #555;
        width:350px;
        max-width:350px;
    }

    td.bold {
        font-weight:bolder;
    }

    td.title{
     font-size:16px;
     font-weight:bold;
    }

    td.sub-title{
     font-size:12px;
     font-weight:bold;
    }
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>
CSS;
}

//get students

$students = $db->Execute("
SELECT  ADMNO, FULLNAME FROM VIEWSP 
WHERE YEARCODE='{$yearcode}' 
AND FORMCODE='{$formcode}' 
order by ADMNO
");

if(!$students) die($db->ErrorMsg() );

if($students->RecordCount()==0) die("No Student Found");

foreach($students as $student){
  $admno    = valueof($student, 'ADMNO');
  $fullname = valueof($student, 'FULLNAME');
  
  prepare_studentperf($admno);
  
}

?>
