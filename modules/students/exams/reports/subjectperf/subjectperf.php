<?php

  class subjectperf {
	private $id;
	private $datasrc;
	private $primary_key;
	
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
	}
	
	public function list_exams(){
		global $db;
		
	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));	
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));
	 $examcode     = filter_input(INPUT_POST , ui::fi('exam'));	
	 
	 if(strlen($formcode)>1){
	 	$formcode = substr($formcode,0,1);
	 }
	 
	  $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");
	  
     $exams_array = array();
      
       if($exams){
       	if(sizeof($exams)>=1){
       		
       		for ($c=1;$c<=5;++$c){
       		 if(isset($exams["CATNAME{$c}"])){
       		 	 $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
       		 }
       		}
       		
       		for ($e=1;$e<=5;++$e){
       		 if(isset($exams["EXAMNAME{$e}"])){
       		 	 $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
       		 }
       		}
       		
       		$exams_array['TOTAL'] = 'Combined';
       		
       	}
       }
	  
       
	  return ui::form_select_fromArray('exam',$exams_array, $examcode," onchange=\"\" ");
	  
	}
}
