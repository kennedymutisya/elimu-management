<?php

class perstudent
{
    private $id;
    private $page;
    private $rows;
    private $admno;

    public function __construct()
    {
        global $cfg;

        $this->datasrc = valueof($cfg, 'datasrc');
        $this->primary_key = valueof($cfg, 'pkcol');

        $this->id = filter_input(INPUT_POST, 'id');
        $this->admno = filter_input(INPUT_POST, 'admno');

    }

    public function get_profile()
    {
        global $db, $cfg;

        $student = new student($this->admno);
        $school = new school();
        $distinct_data = $db->GetArray("SELECT DISTINCT YEARCODE,FORMCODE,FORMNAME,TERMCODE FROM VIEWSTUDENTSUBJECTS WHERE ADMNO='{$this->admno}' ORDER BY FORMCODE asc,TERMCODE asc");

        $terms = array();
        $forms = array();
        $years = array();

        if ($distinct_data) {
            foreach ($distinct_data as $distinct) {
                $yearcode = valueof($distinct, 'YEARCODE');
                $formcode = valueof($distinct, 'FORMCODE');
                $formname = valueof($distinct, 'FORMNAME');
                $termcode = valueof($distinct, 'TERMCODE');

                if (!empty($yearcode) && !array_key_exists($yearcode, $years)) {
                    $years[$yearcode] = $yearcode;
                }

                if (!empty($termcode) && !array_key_exists($termcode, $terms)) {
                    $terms[$termcode] = 'Term '.$termcode;
                }

                if (!empty($formcode) && !array_key_exists($formcode, $forms)) {
                    $forms[$formcode] = $formname;
                }

            }
        }

        asort($years);
        asort($forms);
        asort($terms);

        ?>
        <table cellpadding="2" cellspacing="0" width="100%">

            <tr>
                <td width="100px">Year:</td>
                <td><?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode,
                        " onchange=\"{$cfg['appname']}.list_subjects();\" "); ?></td>
                <td>Term:</td>
                <td><?php echo ui::form_select_fromArray('termcode', $terms, $school->active_termcode,
                        "onchange=\"{$cfg['appname']}.list_subjects();\""); ?></td>
                <td>Form</td>
                <td><?php echo ui::form_select_fromArray('form', $forms, $student->streamcode,
                        "onchange=\"{$cfg['appname']}.list_subjects();\""); ?></td>
            </tr>

            <tr>
                <td colspan="6">
                    <div id="div_subjects<?php echo MNUID; ?>"></div>
                </td>
            </tr>

        </table>

        <script>
            <?php echo $cfg['appname']; ?>.list_subjects();
        </script>

        <?php
    }

    public function list_subjects()
    {
        global $db, $cfg;


        $formcode = filter_input(INPUT_POST, ui::fi('form'));
        $yearcode = filter_input(INPUT_POST, ui::fi('yearcode'));
        $termcode = filter_input(INPUT_POST, ui::fi('termcode'));

        $data_sql = ("
select SUBJECTCODE,ID, SUBJECTNAME ,CAT1,CAT2,CAT3,CAT4,CAT5,AVGCAT,PAPER1,PAPER2,PAPER3,PAPER4,PAPER5,
CAT1GRADECODE,CAT2GRADECODE,CAT3GRADECODE,CAT4GRADECODE,CAT5GRADECODE,
PAPER1GRADECODE,PAPER2GRADECODE,PAPER3GRADECODE,PAPER4GRADECODE,PAPER5GRADECODE,
AVGEXAM,TOTAL,GRADECODE,POINTS,STATUSCODE
from VIEWSTUDENTSUBJECTS
where ADMNO='{$this->admno}'
and FORMCODE='{$formcode}'
and YEARCODE='{$yearcode}'
and TERMCODE='{$termcode}'
ORDER BY ADMNO,SORTPOS, SUBJECTCODE
");

        $data = $db->GetAssoc($data_sql);
        $subjects = array();

        if ($data) {
            if (sizeof($data) > 0) {
                foreach ($data as $subjectcode => $subject) {
                    $subjectname = valueof($subject, 'SUBJECTNAME');
                    $subjects[$subjectcode] = $subjectname;

                }
            }
        }

        $setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        and YEARCODE='{$yearcode}'
        and TERMCODE='{$termcode}'
       ");

        $subject_config = array();

        for ($c = 1; $c <= 5; ++$c) {
            if (isset($setup["CATMAX{$c}"])) {
                if ($setup["CATMAX{$c}"] > 0) {
                    $subject_config['cats'][$c]['max'] = $setup["CATMAX{$c}"];
                    $subject_config['cats'][$c]['name'] = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
                }
            }
        }

        for ($x = 1; $x <= 5; ++$x) {
            if (isset($setup["EXAMMAX{$x}"])) {
                if ($setup["EXAMMAX{$x}"] > 0) {
                    $subject_config['papers'][$x]['max'] = $setup["EXAMMAX{$x}"];
                    $subject_config['papers'][$x]['name'] = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
                }
            }
        }
        $mush = MNUID;
//        echo '<table id="marks'.MNUID.'" cellpadding="0" cellspacing="0" width="100%" border="0" class="datagrid-htable datagrid-btable datagrid-ftable">';
        echo "<table id='marks{$mush}' cellpadding='0' cellspacing='0' width='100%' border='0' class='datagrid-htable datagrid-btable datagrid-ftable'>";
        echo '<thead>';

        echo '<tr>';
        echo '<td   class="panel-header" >No.</td>';
        echo '<td   class="panel-header" >Subject</td>';

        if (isset($subject_config['cats']) && sizeof($subject_config['cats']) > 0) {
            foreach ($subject_config['cats'] as $cat_index => $cat) {
                $name = valueof($cat, 'name', '', 'Camelize');
                $max = valueof($cat, 'max');
                echo "<td   class=\"panel-header\">{$name}</td>";
            }
            echo "<td  class=\"panel-header\">&nbsp;</td>";
        }

        if (isset($subject_config['papers']) && sizeof($subject_config['papers']) > 0) {
            foreach ($subject_config['papers'] as $cat_index => $paper) {
                $name = valueof($paper, 'name', '', 'Camelize');
                $max = valueof($paper, 'max');
                echo "<td  class=\"panel-header\">{$name}</td>";
            }
        }

        echo "<td class=\"panel-header\" colspan=\"5\">&nbsp;</td>";
        echo "</tr>";

        echo '<tr>';
        echo '<td  class="header left right">&nbsp;</td>';
        echo '<td  class="header left right">&nbsp;</td>';

        if (isset($subject_config['cats']) && sizeof($subject_config['cats']) > 0) {
            foreach ($subject_config['cats'] as $cat_index => $cat) {
                $name = valueof($cat, 'name');
                $max = valueof($cat, 'max');
                echo "<td  class=\"header right\"><b>{$max}</b></td>";
            }
            echo "<td  class=\"header right\">CT.Avg</td>";
        }

        if (isset($subject_config['papers']) && sizeof($subject_config['papers']) > 0) {
            foreach ($subject_config['papers'] as $cat_index => $paper) {
                $name = valueof($paper, 'name');
                $max = valueof($paper, 'max');
                echo "<td  class=\"header right\"><b>{$max}</b></td>";
            }
            echo "<td  class=\"header\">PP.Avg</td>";
        }

        echo "<td class=\"header right\">Total</td>";
        echo "<td class=\"header right\">Grade</td>";
        echo "<td class=\"header right\" colspan=\"2\">Options</td>";
        echo "</tr>";

        echo '</thead>';

        if (count($subjects) > 0) {

            echo '<tbody>';

            $student = array();
            $count = 1;

            foreach ($subjects as $subjectcode => $subjectname) {

                $subject = isset($data[$subjectcode]) ? $data[$subjectcode] : array();

                $id = valueof($subject, 'ID');

                $cat1 = valueof($subject, 'CAT1');
                $cat1 = round($cat1, 0);
                $cat2 = valueof($subject, 'CAT2');
                $cat3 = valueof($subject, 'CAT3');
                $cat4 = valueof($subject, 'CAT4');
                $cat5 = valueof($subject, 'CAT5');
                $avgcat = valueof($subject, 'AVGCAT');

                $paper1 = valueof($subject, 'PAPER1');
                $paper2 = valueof($subject, 'PAPER2');
                $paper3 = valueof($subject, 'PAPER3');
                $paper4 = valueof($subject, 'PAPER4');
                $paper5 = valueof($subject, 'PAPER5');
                $avgexam = valueof($subject, 'AVGEXAM');
                $total = valueof($subject, 'TOTAL');
                $gradecode = valueof($subject, 'GRADECODE');

                $divAvgCat = "divavgcat_mps_{$id}";
                $divAvgExam = "divavgexam_mps_{$id}";
                $divGradeCode = "divgrade_mps_{$id}";
                $divTotal = "divtotal_mps_{$id}";
                $divStatus = "divstatus_mps_{$id}";

                $link_delete = ui::href("{$cfg['appname']}.subject_delete({$id},'{$subjectname}','{$yearcode}','{$termcode}');",
                    '<i class="fa fa-trash"></i> ', "Delete ".$subjectname, 'delete'.$subjectname, 'delete', 'delete');
                $link_comment = ui::href("{$cfg['appname']}.subject_comment({$id},'{$subjectcode}','{$yearcode}','{$termcode}');",
                    '<i class="fa fa-commenting-o"></i> ', "Comments for ".$subjectname, 'comments'.$subjectname,
                    'comments', 'comments');

                echo '<tr>';
                echo "<td class=\"input left\" >{$count}</td>";
                echo "<td class=\"input\" nowrap>{$subjectname}</td>";

                if (isset($subject_config['cats']) && sizeof($subject_config['cats']) > 0) {
                    foreach ($subject_config['cats'] as $cat_index => $cat) {

                        $value = valueof($subject, "CAT{$cat_index}");
                        $value = round($value, 1);
                        $name = valueof($cat, 'name');
                        $max = valueof($cat, 'max');

                        $textbox_catid = "cat{$cat_index}_{$id}";
                        $column = "cat{$cat_index}";
                        $textbox_cat = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_catid}\" size=\"5\" onfocus=\"\" onblur=\"{$cfg['appname']}.save({$id},'{$column}','{$textbox_catid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"  />";

                        echo "<td class=\"input\"  >{$textbox_cat}</td>";

                    }
                    echo "<td class=\"input\" ><div id=\"{$divAvgCat}\">{$avgcat}</div></td>";
                }

                if (isset($subject_config['papers']) && sizeof($subject_config['papers']) > 0) {
                    foreach ($subject_config['papers'] as $paper_index => $paper) {

                        $value = valueof($subject, "PAPER{$paper_index}");
                        $value = round($value, 1);
                        $name = valueof($paper, 'name');
                        $max = valueof($paper, 'max');
                        $textbox_paperid = "paper{$paper_index}_{$id}";
                        $column = "paper{$paper_index}";
                        $textbox_paper = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_paperid}\" size=\"5\" onfocus=\"\" onblur=\"{$cfg['appname']}.save({$id},'{$column}','{$textbox_paperid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"   />";

                        echo "<td class=\"input\" >{$textbox_paper}</td>";

                    }
                    echo "<td class=\"input\" ><div id=\"{$divAvgExam}\">{$avgexam}</div></td>";
                }

                echo "<td class=\"input\" ><div id=\"{$divTotal}\">{$total}</div></td>";
                echo "<td class=\"input\" ><div id=\"{$divGradeCode}\">{$gradecode}</div></td>";
                echo "<td class=\"input\">{$link_comment}</td>";
                echo "<td class=\"input\">{$link_delete}</td>";

                echo '</tr>';

                ++$count;
            }//each subject

            echo '</tbody>';
        }

        echo '</table>';


        echo "
     <script>
$('#marks".MNUID."').tableNav();
$('#marks".MNUID." input').eq(1).click();
</script>
";
    }

    private function get_global_set_up($formcode, $yearcode, $termcode)
    {
        global $db;

        $global_setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

        return $global_setup;
    }

    private function get_set_up($subjectcode, $streamcode, $yearcode, $termcode)
    {
        global $db;

        $setup = $db->CacheGetRow(1200, "
       SELECT * FROM SATSS
        WHERE SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

        return $setup;
    }

    public function save()
    {
        global $db, $cfg;

        $id = filter_input(INPUT_POST, 'id');
        $max = filter_input(INPUT_POST, 'max');
        $score = filter_input(INPUT_POST, 'score');
        $column = filter_input(INPUT_POST, 'column');
        $user = new user();

        $record = new ADODB_Active_Record('SASTUDSUB', array('ID'));
        $record->Load("ID={$id}");

        if (empty($record->_original)) {
            return json_response(0, "save failed : record missing in database");
        }

        $formcode = $record->formcode;
        $yearcode = $record->yearcode;
        $termcode = $record->termcode;
        $subjectcode = $record->subjectcode;
        $streamcode = $record->streamcode;

        $setup_global = self::get_global_set_up($formcode, $yearcode, $termcode);
        $setup = self::get_set_up($subjectcode, $streamcode, $yearcode, $termcode);

        if (sizeof($setup) == 0 || empty($setup)) {
            $setup = $setup_global;
        }


        if (empty($setup)) {
            return json_response(0,
                "Please Setup Required Subjects for Form {$formcode} , {$yearcode}- Term {$termcode} ");
        }

        $count_cats = 0;
        $count_exams = 0;

        for ($c = 1; $c <= 5; ++$c) {
            if (isset($setup["CATMAX{$c}"])) {
                if ($setup["CATMAX{$c}"] > 0) {
                    ++$count_cats;
                }
            }
        }

        for ($x = 1; $x <= 5; ++$x) {
            if (isset($setup["EXAMMAX{$x}"])) {
                if ($setup["EXAMMAX{$x}"] > 0) {
                    ++$count_exams;
                }
            }
        }

        $catmax1 = valueof($setup, 'CATMAX1');
        $catmax2 = valueof($setup, 'CATMAX2');
        $catmax3 = valueof($setup, 'CATMAX3');
        $catmax4 = valueof($setup, 'CATMAX4');
        $catmax5 = valueof($setup, 'CATMAX5');

        $exammax1 = valueof($setup, 'EXAMMAX1');
        $exammax2 = valueof($setup, 'EXAMMAX2');
        $exammax3 = valueof($setup, 'EXAMMAX3');
        $exammax4 = valueof($setup, 'EXAMMAX4');
        $exammax5 = valueof($setup, 'EXAMMAX5');

        $contc = valueof($setup, 'CONTC');
        $contct = valueof($setup, 'CONTCT');
        $conte = valueof($setup, 'CONTE');
        $cont = $contc + $conte + $contct;
        $gradesyscode = valueof($setup_global, 'GSYSCODE');

        $special_subjects = array('CHM', 'PHY', 'BIO', 'MAT');
        $subject_is_special = false;

        if ($formcode == 3 || $formcode == 4) {
            if (array_search($subjectcode, $special_subjects) || in_array($subjectcode, $special_subjects)) {
                $subject_is_special = true;
            }
        }

        if (empty($formcode)) {
            return json_response(0, " missing variable 'formcode'");
        }

        if (empty($gradesyscode)) {
            return json_response(0, " missing grading system for {$streamcode}");
        }

        if ($contc == 100 && $conte == 100 && $contct == 100) {

        } else {
            if ($cont != 100) {
                return json_response(0, "CAT & EXAM contributions should total 100");
            }
        }

        if (!empty($column)) {
            $record->$column = $score;
        }

        if (!empty($column)) {

            switch ($column) {
                case 'cat1':
                    $avg_this = ($score / $catmax1) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'cat2':
                    $avg_this = ($score / $catmax2) * $contct;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'cat3':
                    $avg_this = ($score / $catmax3) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'cat4':
                    $avg_this = ($score / $catmax4) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'cat5':
                    $avg_this = ($score / $catmax5) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'paper1':
                    $avg_this = ($score / $exammax1) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper2':
                    $avg_this = ($score / $exammax2) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper3':
                    $avg_this = ($score / $exammax3) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper4':
                    $avg_this = ($score / $exammax4) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper5':
                    $avg_this = ($score / $exammax5) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
            }

            $avg_raw = @($score / $catmax1) * 100;
            //echo "\$score={$score} <br>";//remove
            //echo "\$catmax1={$catmax1} <br>";//remove
            //echo "\$avg_raw={$avg_raw} <br>";//remove


            $avg_this = round($avg_this, 2);
            $perc_this = round($perc_this, 2);

            $exam_raw_column = "{$column}r";
            $exam_per_column = "{$column}p";
            $gradecode_column = "{$column}gradecode";
            $points_column = "{$column}points";
            $comments_column = "{$column}comments";
            $record->$column = $score;

            $Get_Grade_Exam = self::Get_Grade($gradesyscode, $perc_this);
            //print_pre($Get_Grade_Exam);//remove

            //$Get_Grade_Exam_Raw   = self::Get_Grade( $gradesyscode , $avg_raw);

            if (is_array($Get_Grade_Exam)) {
                $record->$exam_raw_column = $avg_this;
                $record->$exam_per_column = $perc_this;
                $record->$gradecode_column = valueof($Get_Grade_Exam, 'GRADECODE');
                //$record->$gradecode_column  = valueof($Get_Grade_Exam_Raw, 'GRADECODE');
                $record->$points_column = valueof($Get_Grade_Exam, 'POINTS');
            }

            $subject_Comments = self::Get_Comments($record->subjectcode, $record->$gradecode_column);
            $record->$comments_column = $subject_Comments;

        }

        if ($contc == 100 && $conte == 100 && $contct == 100) {

            if ($count_cats > 0) {
                $catcont1 = $record->cat1 > 0 && $catmax1 > 0 ? $record->cat1 : 0;
                $catcont2 = $record->cat2 > 0 && $catmax2 > 0 ? $record->cat2 : 0;
                $catcont3 = $record->cat3 > 0 && $catmax3 > 0 ? $record->cat3 : 0;
                $catcont4 = $record->cat4 > 0 && $catmax4 > 0 ? $record->cat4 : 0;
                $catcont5 = $record->cat5 > 0 && $catmax5 > 0 ? $record->cat5 : 0;
                $avgcat = ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) / $count_cats;
                $record->avgcat = round($avgcat, 0);
            }

            if ($count_exams > 0) {

                $examcont1 = $record->paper1 > 0 && $exammax1 > 0 ? $record->paper1 : 0;
                $examcont2 = $record->paper2 > 0 && $exammax2 > 0 ? $record->paper2 : 0;
                $examcont3 = $record->paper3 > 0 && $exammax3 > 0 ? $record->paper3 : 0;
                $examcont4 = $record->paper4 > 0 && $exammax4 > 0 ? $record->paper4 : 0;
                $examcont5 = $record->paper5 > 0 && $exammax5 > 0 ? $record->paper5 : 0;

                if (!$subject_is_special) {
                    $avgexam = ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5) / $count_exams;
                    $record->avgexam = round($avgexam, 0);
                    $total = (($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) + ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5)) / ($count_cats + $count_exams);
                } else {
                    $exammaxs = ($exammax1 + $exammax2);
                    $examcont_avg = ($examcont1 + $examcont2) / ($exammax1 + $exammax2);
                    $kamaa = ($examcont_avg * 60) + $examcont3;
                    $njooro = $kamaa * ($conte / 100);
                    $avgexam = $njooro;
                    $record->avgexam = round($avgexam, 0);
                    $total = (($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) + ($record->avgexam)) / ($count_cats + 1);

                }

            }

            $record->total = round($total);

        } else {//else not 100-100

            if ($count_cats > 0) {
                $catcont1 = $record->cat1 > 0 && $catmax1 > 0 ? $record->cat1 / $catmax1 * $contc : 0;
                $catcont2 = $record->cat2 > 0 && $catmax2 > 0 ? $record->cat2 / $catmax2 * $contct : 0;
                $catcont3 = $record->cat3 > 0 && $catmax3 > 0 ? $record->cat3 / $catmax3 * $contc : 0;
                $catcont4 = $record->cat4 > 0 && $catmax4 > 0 ? $record->cat4 / $catmax4 * $contc : 0;
                $catcont5 = $record->cat5 > 0 && $catmax5 > 0 ? $record->cat5 / $catmax5 * $contc : 0;
                $avgcat = ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) / $count_cats;
                $record->avgcat = round($avgcat, 0);
            }

            if ($count_exams > 0) {

                if ((!$subject_is_special) || ($count_exams == 1)) {
                    $examcont1 = $record->paper1 > 0 && $exammax1 > 0 ? $record->paper1 / $exammax1 * $conte : 0;
                    $examcont2 = $record->paper2 > 0 && $exammax2 > 0 ? $record->paper2 / $exammax2 * $conte : 0;
                    $examcont3 = $record->paper3 > 0 && $exammax3 > 0 ? $record->paper3 / $exammax3 * $conte : 0;
                    $examcont4 = $record->paper4 > 0 && $exammax4 > 0 ? $record->paper4 / $exammax4 * $conte : 0;
                    $examcont5 = $record->paper5 > 0 && $exammax5 > 0 ? $record->paper5 / $exammax5 * $conte : 0;

                    $avgexam = ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5) / $count_exams;
                    $record->avgexam = round($avgexam, 0);
                    $total = $record->avgcat + $record->avgexam;

                } else {

                    $examcont1 = $record->paper1 > 0 && $exammax1 > 0 ? $record->paper1 : 0;
                    $examcont2 = $record->paper2 > 0 && $exammax2 > 0 ? $record->paper2 : 0;
                    $examcont3 = $record->paper3 > 0 && $exammax3 > 0 ? $record->paper3 : 0;
                    $examcont4 = $record->paper4 > 0 && $exammax4 > 0 ? $record->paper4 : 0;
                    $examcont5 = $record->paper5 > 0 && $exammax5 > 0 ? $record->paper5 : 0;

                    $exammaxs = ($exammax1 + $exammax2);
                    $examcont_avg = ($examcont1 + $examcont2) / ($exammax1 + $exammax2);
                    $kamaa = ($examcont_avg * 60) + $examcont3;
                    $njooro = $kamaa * ($conte / 100);
                    $avgexam = $njooro;
                    $record->avgexam = round($avgexam, 0);
                    $total = $record->avgcat + $record->avgexam;

                }

                $record->total = round($total);

            }

        }

        $Get_Grade = self::Get_Grade($gradesyscode, $record->total);

        if (is_array($Get_Grade)) {
            $record->gradecode = valueof($Get_Grade, 'GRADECODE');
            $record->points = valueof($Get_Grade, 'POINTS');
        }

        $subject_Comments = self::Get_Comments($record->subjectcode, $record->gradecode);
        $record->comments = $subject_Comments;

        $record->audituser = $user->userid;
        $record->auditdate = date('Y-m-d');
        $record->audittime = time();

        if ($record->Save()) {
            return json_encode(array(
                'success'   => 1, 'message' => 'ok', 'avgcat' => $record->avgcat,
                'avgexam'   => $record->avgexam, 'total' => $record->total,
                'gradecode' => $record->gradecode, 'points' => 7, 'gradesyscode' => $gradesyscode
            ));
        } else {
            $error = @$db->ErrorMsg();
            return json_response(0, "save failed : {$error}");
        }

    }

    public function Get_Grade($gradesyscode = 'KN', $total)
    {
        global $db;
        $total = $total > 0 ? $total : 0;
        return $db->GetRow("SELECT GRADECODE,POINTS,COMMENTS FROM SAXGRD WHERE GSYSCODE='{$gradesyscode}' AND MIN<={$total} AND MAX>={$total}");
    }

    private function Get_Comments($subjectcode, $gradecode)
    {
        global $db;
        return $db->CacheGetOne(120,
            "SELECT COMMENTS FROM SASUBREMARKS WHERE SUBJECTCODE='{$subjectcode}' AND GRADECODE='{$gradecode}'");
    }

    public function delete()
    {
        global $db;

        $id = filter_input(INPUT_POST, 'id');
        $subjectname = filter_input(INPUT_POST, 'subjectname');
        $delete = $db->Execute("UPDATE SASTUDSUB SET DELETED=1  WHERE ID={$id}");

        if ($delete) {
            return json_response(1, "Subject {$subjectname} Deleted");
        } else {
            $error = @$db->ErrorMsg();
            return json_response(0, "Delete Failed : {$error}");
        }

    }

    public function get_custom_comments()
    {
        global $db, $cfg;

        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $record = new ADODB_Active_Record('SASTUDSUB', array('ID'));
        $record->Load("ID={$id}");

        $setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$record->formcode}'
        and YEARCODE='{$record->yearcode}'
        and TERMCODE='{$record->termcode}'
       ");

        $comments = array();

        for ($c = 1; $c <= 5; ++$c) {
            if (isset($setup["CATMAX{$c}"])) {
                if ($setup["CATMAX{$c}"] > 0) {
                    $comments["cat{$c}comments"] = "CAT {$c} Comments";
                }
            }
        }

        for ($x = 1; $x <= 5; ++$x) {
            if (isset($setup["EXAMMAX{$x}"])) {
                if ($setup["EXAMMAX{$x}"] > 0) {
                    $comments["paper{$x}comments"] = "PAPER {$x} Comments";
                }
            }
        }

        $comments["comments"] = "Combined";
        $subject_Comments = $record->comments;

        $f_id = ui::fi('id');
        $f_exam = 'exam';
        $f_comment = ui::fi('custom_comments');
        $div_exam = ui::fi('exam_custom_comments');


        $select = ui::form_select_fromArray($f_exam, $comments, 'comments',
            'onchange="'.$cfg['appname'].'.get_exam_custom_comment('.$id.')"', '', 200);
        $textarea = "<textarea name=\"".$f_comment."\" id=\"".$f_comment."\" cols=\"30\" rows=\"2\" data-options=\"\" class=\"\">".$subject_Comments."</textarea>";

        echo $input = ui::form_input('hidden', 'id', 30, $id);
        echo '<table cellpadding="0" cellspacing="0" width="100%" border="0" >';

        echo '<tr>';
        echo '<td>Exam</td>';
        echo '<td>'.$select.'</td>';
        echo '</tr>';


        echo '<tr>';
        echo '<td>Comment</td>';
        echo '<td><div id="'.$div_exam.'">'.$textarea.'</div></td>';
        echo '</tr>';

        echo '</table>';

    }

    public function get_exam_custom_comment()
    {
        global $db;

        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $exam = filter_input(INPUT_POST, 'exam');
        $record = new ADODB_Active_Record('SASTUDSUB', array('ID'));

        $record->Load("ID={$id}");

        $subject_Comments = isset($record->$exam) ? $record->$exam : $record->comments;

        $f_comment = ui::fi('custom_comments');

        echo "<textarea name=\"".$f_comment."\" id=\"".$f_comment."\" cols=\"30\" rows=\"2\" data-options=\"\" class=\"\">".$subject_Comments."</textarea>";

    }

    public function save_subject_comment()
    {
        global $db;

        $id = filter_input(INPUT_POST, ui::fi('id'), FILTER_VALIDATE_INT);
        $exam = filter_input(INPUT_POST, ui::fi('exam'), FILTER_SANITIZE_STRING);
        $comments = filter_input(INPUT_POST, ui::fi('custom_comments'), FILTER_SANITIZE_STRING);

        $record = new ADODB_Active_Record('SASTUDSUB', array('ID'));
        $record->Load("ID={$id}");

        if (isset($record->$exam)) {
            $record->$exam = $comments;
        } else {
            $record->comments = $comments;
        }

        if ($record->Save()) {
            return json_response(1, 'Comments Saved');
        } else {
            return json_response(1, 'Failed to Save Comments');
        }

    }

}
