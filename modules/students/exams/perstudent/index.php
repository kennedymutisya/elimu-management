<?php

$user = new user();

?>

<style>

    table#marks<?php echo MNUID;?> {
        font-size: 12px;
        font-family: Verdana
    }

    table.tblSubSetup {
        font-size: 12px;
        font-family: Verdana
    }

    table.tblSubSetup td.label {
        background-color: #95B8E7;
    }

    table.tblSubSetup td.cat {
        background-color: #E2EDFF;
    }

    table.tblSubSetup td.exam {
        background-color: #FFE48D;
    }

    table#marks<?php echo MNUID;?> a.delete {
        color: #555;
    }

    table#marks<?php echo MNUID;?> a.delete:hover {
        color: #D00;
    }

    table#marks<?php echo MNUID;?> a.comments {
        color: #555;
    }

    table#marks<?php echo MNUID;?> a.comments:hover {
        color: #0D0;
    }

    table#marks<?php echo MNUID;?> td.input input {
        width: 50px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
    }

    table#marks<?php echo MNUID;?> td.header {
        border-top: 1px solid #999;
        border-bottom: 1px dotted #999;
        padding-left: 5px;
        background-color: #EDECEB;
    }

    table#marks<?php echo MNUID;?> td.input {
        border-bottom: 1px dotted #999;
        border-right: 1px dotted #999;
        padding-left: 5px;
    }

    table#marks<?php echo MNUID;?> td.left {
        border-left: 1px dotted #999;
    }

    table#marks<?php echo MNUID;?> td.right {
        border-right: 1px dotted #999;
    }


</style>

<div class="easyui-panel" fit="true">
    <form id="form_marks<?php echo MNUID; ?>" method="post" novalidate onsubmit="return false;">

        <table cellpadding="2" cellspacing="0" width="100%">

            <tr>
                <td width="100px">Student</td>
                <td><?php echo ui::form_input('text', 'find_admno', 30, '', '', '', '', '', '', ""); ?> <a
                            href="javascript:void(0)" class="easyui-linkbutton"
                            onclick="<?php echo $cfg['appname']; ?>.loadProfile();"><i
                                class="fa fa-chevron-circle-down"></i> View</a></td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td colspan="3">
                    <div id="div_stage<?php echo MNUID; ?>">&nbsp;</div>
                </td>
            </tr>

        </table>
    </form>
</div>

<div id="<?php echo ui::fi('dlg'); ?>" class="easyui-dialog" style="width:350px;height:160px;padding:2px" closed="true"
     buttons="#<?php echo ui::fi('dlg-buttons'); ?>">
    <form id="<?php echo ui::fi('ff_comments') ?>" method="post" novalidate onsubmit="return false;">
        <div id="<?php echo ui::fi('div_comments'); ?>">&nbsp;</div>
    </form>
</div>

<div id="<?php echo ui::fi('dlg-buttons'); ?>">
    <a href="#" class="easyui-linkbutton" iconCls="icon-ok"
       onclick="<?php echo $cfg['appname']; ?>.save_subject_comment();">Save Comments</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel"
       onclick="javascript:$('#<?php echo ui::fi('dlg'); ?>').dialog('close')">Cancel</a>
</div>

<script>

    var <?php echo $cfg['appname']; ?> =
    {
        loadProfile:function () {
            var find_admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
            if (find_admno === '') {
                alert('Select a Student First');
            } else {
                $.post('./endpoints/crud/', 'modvars=<?php echo $vars; ?>&function=get_profile&admno=' + find_admno, function (html) {
                    $('#div_stage<?php echo MNUID; ?>').html(html);
                });
            }
        }
    ,
        list_subjects:function () {
            var find_admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
            if (find_admno === '') {
                alert('Select a Student First');
            } else {
                $.post('./endpoints/crud/', 'modvars=<?php echo $vars; ?>&function=list_subjects&' + $('#form_marks<?php echo MNUID; ?>').serialize() + '&admno=' + find_admno, function (html) {
                    $('#div_subjects<?php echo MNUID; ?>').html(html);
                });
            }
        }
    ,
        clearForm:function () {
            $('#form_marks<?php echo MNUID; ?>').form('clear');
            $('#find_admno').val('');
        }
    ,
        save:function (id, column, textboxId, max, origVal, name, divAvgCat, divAvgExam, divTotal, divGradeCode, divStatus) {
            var inputval = $('input#' + textboxId).val();
            if (typeof inputval < 0) {
                $.messager.alert(name, 'numbers only', 'error');
                $('input#' + textboxId).val(origVal);
                return;
            }

            inputval = inputval * 1;
            max = max * 1;
            origVal = origVal * 1;

            if (inputval > max) {
                alert('score greator for ' + name + ' than max ' + max, 'error');
                $('input#' + textboxId).val(origVal);
                return;
            }

            /*if(inputval != origVal){*/
            if (inputval >= 0) {

                var fdata = '&id=' + id + '&max=' + max + '&score=' + inputval + '&column=' + column + '&modvars=<?php echo $vars; ?>&function=save';
                $.post('./endpoints/crud/', fdata, function (data) {
                    // $('#div_stage').html("");
                    if (data.success === 1) {
                        $('#' + divAvgCat).html(data.avgcat);
                        $('#' + divAvgExam).html(data.avgexam);
                        $('#' + divTotal).html(data.total);
                        $('#' + divGradeCode).html(data.gradecode);
                    } else {
                        $.messager.alert('Error', data.message, 'error');
                    }
                }, "json");
            }
        }
    ,
        subject_delete:function (id, subjectname, yearcode, termcode) {
            if (confirm("Delete '" + subjectname + "' for Year '" + yearcode + "' Term '" + termcode + "' ")) {
                var fdata = '&id=' + id + '&subjectname=' + subjectname + '&modvars=<?php echo $vars; ?>&function=delete';
                $.post('./endpoints/crud/', fdata, function (data) {
                    if (data.success === 1) {
                        $.messager.show({title: 'Success', msg: data.message});
                        <?php echo $cfg['appname']; ?>.
                        list_subjects();
                    } else {
                        $.messager.alert('Error', data.message, 'error');
                    }
                }, "json");
            }
        }
    ,
        subject_comment:function (id, subjectode, yearcode, termcode) {
            $('#<?php echo ui::fi('dlg'); ?>').dialog({
                title: 'Custom Comments for ' + subjectode + ' ' + yearcode + '/T' + termcode + '',
                closed: false,
                resizable: true,
                modal: true,
            });
            ui.fc('modvars=<?php echo $vars; ?>&function=get_custom_comments&id=' + id, '<?php echo ui::fi('div_comments'); ?>');
        }
    ,
        get_exam_custom_comment:function (id) {
            ui.fc('modvars=<?php echo $vars; ?>&function=get_exam_custom_comment&id=' + id + '&exam=' + $('#<?php echo ui::fi('exam'); ?>').val(), '<?php echo ui::fi('exam_custom_comments'); ?>');
        }
    ,
        save_subject_comment:function (id) {
            var fdata = $('#<?php  echo ui::fi('ff_comments')?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save_subject_comment';
            $.post("./endpoints/crud/", fdata, function (data) {
                if (data.success === 1) {
                    $.messager.show({title: 'Success', msg: data.message});
                    $('#<?php echo ui::fi('dlg'); ?>').dialog('close')
                } else {
                    $.messager.alert('Save Comments', data.message, 'error');
                }
            }, "json");

        }
    ,
        print_marks:function (r) {
            var <?php echo ui::fi('w'); ?>=
            window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&r=' + r, '<?php echo ui::fi('pw'); ?>', 'height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
            <?php echo ui::fi('w'); ?>.
            focus();
        }
    }

    <?php
    echo ui::ComboGrid($combogrid_array, 'find_admno', "{$cfg['appname']}.loadProfile");
    ?>
</script>
</html>
