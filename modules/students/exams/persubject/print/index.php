<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Marks Print Out</title>
	   <style>
table#marks {font-size:12px;font-family:Verdana}
table#tblSubSetup {font-size:12px;font-family:Verdana}
table#tblSubSetup td.label{ background-color:#95B8E7;}
table#tblSubSetup td.cat{ background-color:#E2EDFF;}
table#tblSubSetup td.exam{ background-color:#FFE48D;}

td.input input {
    width: 50px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

td.header {
 border-top:1px solid #999;
 border-bottom:1px dotted #999;
 padding-left: 5px;
}

td.input {
 border-bottom:1px dotted #999;
 border-right:1px dotted #999;
 padding-left: 5px;
}

td.left {
 border-left:1px dotted #999;
}

td.right {
 border-right:1px dotted #999;
}
</style>
</head> 
<body>

<?php
    $subjectcode  =  isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null; 
    $subjectname  =  $db->CacheGetOne(1200,"SELECT SUBJECTNAME FROM SASUBJECTS WHERE SUBJECTCODE='{$subjectcode}'");
    $schoolname   =  $db->CacheGetOne(1200,"SELECT SCHOOLNAME FROM SASCHOOL");
    $formcode     =  isset($_SESSION['marks']['formcode']) ? $_SESSION['marks']['formcode'] : null;
    $streamcode   =  isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;  
    $yearcode     =  isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : null;
    $termcode     =  isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : null;
    $count_cats   =  isset($_SESSION['marks']['count_cats']) ? $_SESSION['marks']['count_cats'] : 5;
    $count_exams  =  isset($_SESSION['marks']['count_exams']) ? $_SESSION['marks']['count_exams'] : 5;
    $count_all    =  $count_cats + $count_exams;
    
    $data_sql   = ("
select ID, ADMNO, FULLNAME ,CAT1,CAT2,CAT3,CAT4,CAT5,AVGCAT,PAPER1,PAPER2,PAPER3,PAPER4,PAPER5,AVGEXAM,TOTAL,GRADECODE,POINTS,STATUSCODE
from VIEWSTUDENTSUBJECTS 
where YEARCODE='{$yearcode}' 
AND TERMCODE='{$termcode}' 
AND FORMCODE='{$formcode}'
AND STREAMCODE='{$streamcode}'
AND SUBJECTCODE='{$subjectcode}'
ORDER BY ADMNO
");
     $data = $db->Execute($data_sql);
     $students  = array();
     
     if ($data) {
      if ($data->RecordCount()>0) {
      	while (!$data->EOF){
      	 $students[] = $data->fields;	
      	 $data->MoveNext();
      	}
      }
     }
     
   $setup = $db->GetRow("
       SELECT * FROM SATSS
        WHERE SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");
       
     $subject_config = array();
     
     $subject_config['cont']['cat']  = 40;
     $subject_config['cont']['exam'] = 60;
     
     for ($c=1;$c<=5;++$c){
      if(isset($setup["CATMAX{$c}"])){
      	if($setup["CATMAX{$c}"]>0){
      		$subject_config['cats'][$c]['max']   = $setup["CATMAX{$c}"];
            $subject_config['cats'][$c]['name']  = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
      	}
      }
     }
     
     for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
      		$subject_config['papers'][$x]['max']   = $setup["EXAMMAX{$x}"];
            $subject_config['papers'][$x]['name']  = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
      	}
      }
     }
     
     echo '<table id="marks" cellpadding="0" cellspacing="0" width="100%" border="0" class="datagrid-htable datagrid-btable datagrid-ftable">';
      echo '<thead>';
      
       $count_all_all = $count_all+7;
       
       echo '<tr>';
        echo "<td align=\"center\" colspan=\"{$count_all_all}\" class=\"left header right\"> <b>{$schoolname}</b></td>";
       echo '</tr>';
       
       echo '<tr>';
        echo "<td colspan=\"{$count_all}\" class=\"left header \"><b>Subject :</b></td>";
        echo "<td colspan=\"7\" class=\"left header right\"> <b>{$subjectcode} - {$subjectname}</b></td>";
       echo '</tr>';
       
       echo '<tr>';
        echo "<td colspan=\"{$count_all}\" class=\"left input\"><b>Stream :</b></td>";
        echo "<td colspan=\"7\" class=\"input\"><b>{$streamcode}</b></td>";
       echo '</tr>';
       
       echo '<tr>';
        echo "<td colspan=\"{$count_all}\" class=\"left input\"><b>Year :</b></td>";
        echo "<td colspan=\"7\" class=\"input\"><b>{$yearcode}</b></td>";
       echo '</tr>';
       
       echo '<tr>';
        echo "<td colspan=\"{$count_all}\" class=\"left input\"><b>Term :</b></td>";
        echo "<td colspan=\"7\" class=\"input\"><b>{$termcode}</b></td>";
       echo '</tr>';
       
       echo '<tr>';
        echo "<td colspan=\"{$count_all}\" class=\"left input\">&nbsp;</td>";
        echo "<td colspan=\"7\" class=\"input\">&nbsp;</td>";
       echo '</tr>';
       
       echo '<tr>';
        echo '<td  class="" colspan="3">&nbsp;</td>';
        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name','','Camelize');
        		$max  = valueof($cat,'max');
               echo "<td  class=\" \"><b>{$name}</b></td>";
        	}
        	 echo "<td  class=\" \">&nbsp;</td>";
        }
        
        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name','','Camelize');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"\"><b>{$name}</b></td>";
        	}
        }
        
       
       echo "<td class=\"\" colspan=\"3\">&nbsp;</td>";  
       echo "</tr>";
       
       echo '<tr>';
        echo '<td  class="header left right">No</td>';
        echo '<td  class="header right">Adm.No</td>';
        echo '<td  class="header right">Name</td>';
        
        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	 echo "<td  class=\"header right\">CT.Avg</td>";
        }
        
        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	echo "<td  class=\"header\">PP.Avg</td>";
        }
        
       echo "<td class=\"header right\">Total</td>";  
       echo "<td class=\"header right\">Grade</td>";  
       echo "</tr>";
       
      echo '</thead>';
      
      if(count($students)>0){
       echo '<tbody>';
      
       $count = 1;	
       
       foreach ($students as $student){
       	
       	$id          = valueof($student,'ID');
       	$admno       = valueof($student,'ADMNO');
       	$fullname    = valueof($student,'FULLNAME','**missing**');
       	
       	$cat1        = valueof($student,'CAT1');
       	$cat1        = round($cat1,0);
       	$cat2        = valueof($student,'CAT2');
       	$cat3        = valueof($student,'CAT3');
       	$cat4        = valueof($student,'CAT4');
       	$cat5        = valueof($student,'CAT5');
       	$avgcat      = valueof($student,'AVGCAT');
       	
       	$paper1      = valueof($student,'PAPER1');
       	$paper2      = valueof($student,'PAPER2');
       	$paper3      = valueof($student,'PAPER3');
       	$paper4      = valueof($student,'PAPER4');
       	$paper5      = valueof($student,'PAPER5');
       	$avgexam     = valueof($student,'AVGEXAM');
       	$total       = valueof($student,'TOTAL');
       	$gradecode   = valueof($student,'GRADECODE');
       	
       	$divAvgCat     = "divavgcat_{$id}";
       	$divAvgExam    = "divavgexam_{$id}";
       	$divGradeCode  = "divgrade_{$id}";
       	$divTotal      = "divtotal_{$id}";
       	$divStatus     = "divstatus_{$id}";
       	
        echo '<tr>';
         echo "<td class=\"input left\" >{$count}</td>";
         echo "<td class=\"input\" >{$admno}</td>";
         echo "<td class=\"input\" >{$fullname}</td>";
         
         if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
         foreach ($subject_config['cats'] as $cat_index => $cat){
         	
          $value = valueof($student,"CAT{$cat_index}");
          $value        = round($value,1);
          $name  = valueof($cat,'name');
          $max   = valueof($cat,'max');
          
       	  $textbox_catid = "cat{$cat_index}_{$id}";
       	  $column = "cat{$cat_index}";
       	  $textbox_cat   = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_catid}\" size=\"5\" onfocus=\"\" onblur=\"save({$id},'{$column}','{$textbox_catid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"  />";
       	
          echo "<td class=\"input\"  >{$value}</td>";
          
          }
         }
         
         echo "<td class=\"input\" ><div id=\"{$divAvgCat}\">{$avgcat}</div></td>";
         
         if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
         foreach ($subject_config['papers'] as $paper_index => $paper){
         	
          $value = valueof($student,"PAPER{$paper_index}");
          $value        = round($value,1);
          $name  = valueof($paper,'name');
          $max   = valueof($paper,'max');
       	  $textbox_paperid = "paper{$paper_index}_{$id}";
       	  $column = "paper{$paper_index}";
       	  $textbox_paper   = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_paperid}\" size=\"5\" onfocus=\"\" onblur=\"save({$id},'{$column}','{$textbox_paperid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"   />";
       	
          echo "<td class=\"input\" >{$value}</td>";
          
          }
         }
         
         echo "<td class=\"input\" ><div id=\"{$divAvgExam}\">{$avgexam}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divTotal}\">{$total}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divGradeCode}\">{$gradecode}</div></td>";
        echo '</tr>';
        
        ++$count;
       }
       
       echo '</tbody>';
      }
      
     echo '</table>';
     
?>
</body>
</html>