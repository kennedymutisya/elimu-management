<?php

/**
* auto created config file for modules/students/setup/globalsetup
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-09-13 16:29:29
*/

 final class globalsetup {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }

 public function data(){
		global $db;
		
		$data = array();
		/**********/
        $id      = filter_input(INPUT_GET, 'id');
        
        $settings = new ADODB_Active_Record('SATSSG', array('ID'));
        
        $settings->Load("ID={$id} ");
      
		$data['xgs_form']          = valueof( $settings, 'formcode');
		$data['xgs_year']          = valueof( $settings, 'yearcode');
		$data['xgs_term']          = valueof( $settings, 'termcode');
		
		$data['xgs_grading']       = valueof( $settings, 'gsyscode');
		
		$data['xgs_numsubjects']   = valueof( $settings, 'numsubjects');
		$data['xgs_minsubjects']   = valueof( $settings, 'minsubjects');
		
		$data['xgs_contc_global']  = valueof( $settings, 'contc');
		$data['xgs_conte_global']  = valueof( $settings, 'conte');
		
		$max_c = 5;
		
		for ($c=1;$c<=$max_c;++$c){
		 $data["xgs_cat[{$c}]"]           = valueof( $settings, "catmax{$c}");
		 $data["xgs_title_cat[{$c}]"]     = valueof( $settings, "catname{$c}");
		 $data["xgs_mute_cat[{$c}]"]    = valueof( $settings, "catmute{$c}");
		}
		
		$max_x = 5;
		
		for ($x=1;$x<=$max_x;++$x){
		 $data["xgs_exam[{$x}]"]           = valueof( $settings, "exammax{$x}");
		 $data["xgs_title_exam[{$x}]"]     = valueof( $settings, "examname{$x}");
		 $data["xgs_mute_exam[{$x}]"]    = valueof( $settings, "exammute{$x}");
		}
		
		return json_encode($data);
		
	}
  
 public function data_copy(){
		global $db;



     $data    = array();
        $id      = filter_input(INPUT_GET, 'id');
        
        $settings = new ADODB_Active_Record('SATSSG', array('ID'));
        
        $settings->Load("ID={$id} ");
      
		$data['xgs_form']          = valueof( $settings, 'formcode');
		$data['xgs_year']          = valueof( $settings, 'yearcode');
		$data['xgs_term']          = valueof( $settings, 'termcode');
		
		$data['xgs_form_to']        = valueof( $settings, 'formcode');
		$data['xgs_year_to']        = valueof( $settings, 'yearcode');
		$data['xgs_term_to']        = null;
		
		return json_encode($data);
		
	}
  
 public function save(){
		global $db;

	   $user         =  new user();
       $formcode     =  filter_input(INPUT_POST , 'xgs_form');
       $yearcode     =  filter_input(INPUT_POST , 'xgs_year');
       $termcode     =  filter_input(INPUT_POST , 'xgs_term');
       $gsyscode     =  filter_input(INPUT_POST , 'xgs_grading');
//     $m =   filter_input(INPUT_POST , 'xgs_second_contc_global');
//     $s   	  = filter_input(INPUT_POST , 'xgs_contc_global');

     if(empty($formcode)){
	    return json_response(0,"Select Form");	
	   }else if(empty($yearcode)){
	    return json_response(0,"Select Year");	
	   }else if(empty($termcode)){
	    return json_response(0,"Select Term");	
	   }else if(empty($gsyscode)){
	    return json_response(0,"Select Grading System");	
	   }
	   
       $record = new ADODB_Active_Record('SATSSG', array('FORMCODE','YEARCODE','TERMCODE'));
       
       $record->Load("
        FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");
       
       if(empty($record->_original)){
       	$record->id	          = generateID($record->_table);
       	$record->formcode	  = $formcode;
       	$record->yearcode	  = $yearcode;
       	$record->termcode	  = $termcode;
       }
       
        $record->gsyscode     = $gsyscode;
        $record->numsubjects  = filter_input(INPUT_POST , 'xgs_numsubjects');
        $record->minsubjects  = filter_input(INPUT_POST , 'xgs_minsubjects');
        
        $record->catmax1	  = isset($_POST['xgs_cat'][1]) && is_numeric($_POST['xgs_cat'][1]) ? $_POST['xgs_cat'][1] : null;
        $record->catmax2	  = isset($_POST['xgs_cat'][2]) && is_numeric($_POST['xgs_cat'][2]) ? $_POST['xgs_cat'][2] : null;
        $record->catmax3	  = isset($_POST['xgs_cat'][3]) && is_numeric($_POST['xgs_cat'][3]) ? $_POST['xgs_cat'][3] : null;
        $record->catmax4	  = isset($_POST['xgs_cat'][4]) && is_numeric($_POST['xgs_cat'][4]) ? $_POST['xgs_cat'][4] : null;
        $record->catmax5	  = isset($_POST['xgs_cat'][5]) && is_numeric($_POST['xgs_cat'][5]) ? $_POST['xgs_cat'][5] : null;
        $record->contc   	  = filter_input(INPUT_POST , 'xgs_contc_global');
        $record->contct   	  = filter_input(INPUT_POST , 'xgs_second_contc_global');

        $record->exammax1	  = isset($_POST['xgs_exam'][1]) && is_numeric($_POST['xgs_exam'][1]) ? $_POST['xgs_exam'][1] : null;
        $record->exammax2	  = isset($_POST['xgs_exam'][2]) && is_numeric($_POST['xgs_exam'][2]) ? $_POST['xgs_exam'][2] : null;
        $record->exammax3	  = isset($_POST['xgs_exam'][3]) && is_numeric($_POST['xgs_exam'][3]) ? $_POST['xgs_exam'][3] : null;
        $record->exammax4	  = isset($_POST['xgs_exam'][4]) && is_numeric($_POST['xgs_exam'][4]) ? $_POST['xgs_exam'][4] : null;
        $record->exammax5	  = isset($_POST['xgs_exam'][5]) && is_numeric($_POST['xgs_exam'][5]) ? $_POST['xgs_exam'][5] : null;
        $record->conte   	  = filter_input(INPUT_POST , 'xgs_conte_global');
       
        $record->catname1	  = isset($_POST['xgs_title_cat'][1]) && is_string($_POST['xgs_title_cat'][1]) && $record->catmax1>0 ? $_POST['xgs_title_cat'][1] : null;
        $record->catname2	  = isset($_POST['xgs_title_cat'][2]) && is_string($_POST['xgs_title_cat'][2]) && $record->catmax2>0? $_POST['xgs_title_cat'][2] : null;
        $record->catname3	  = isset($_POST['xgs_title_cat'][3]) && is_string($_POST['xgs_title_cat'][3]) && $record->catmax3>0 ? $_POST['xgs_title_cat'][3] : null;
        $record->catname4	  = isset($_POST['xgs_title_cat'][4]) && is_string($_POST['xgs_title_cat'][4]) && $record->catmax4>0 ? $_POST['xgs_title_cat'][4] : null;
        $record->catname4	  = isset($_POST['xgs_title_cat'][5]) && is_string($_POST['xgs_title_cat'][5]) && $record->catmax5>0 ? $_POST['xgs_title_cat'][5] : null;
       
        $record->examname1	  = isset($_POST['xgs_title_exam'][1]) && is_string($_POST['xgs_title_exam'][1]) && $record->exammax1>0  ? $_POST['xgs_title_exam'][1] : null;
        $record->examname2	  = isset($_POST['xgs_title_exam'][2]) && is_string($_POST['xgs_title_exam'][2]) && $record->exammax2>0  ? $_POST['xgs_title_exam'][2] : null;
        $record->examname3	  = isset($_POST['xgs_title_exam'][3]) && is_string($_POST['xgs_title_exam'][3]  && $record->exammax3>0 ) ? $_POST['xgs_title_exam'][3] : null;
        $record->examname4	  = isset($_POST['xgs_title_exam'][4]) && is_string($_POST['xgs_title_exam'][4]) && $record->exammax4>0  ? $_POST['xgs_title_exam'][4] : null;
        $record->examname4	  = isset($_POST['xgs_title_exam'][5]) && is_string($_POST['xgs_title_exam'][5]) && $record->exammax5>0  ? $_POST['xgs_title_exam'][5] : null;
        
        $record->catmute1	  = isset($_POST['xgs_mute_cat'][1]) && is_string($_POST['xgs_mute_cat'][1]) && $record->catmax1>0 ? $_POST['xgs_mute_cat'][1] : null;
        $record->catmute2	  = isset($_POST['xgs_mute_cat'][2]) && is_string($_POST['xgs_mute_cat'][2]) && $record->catmax2>0? $_POST['xgs_mute_cat'][2] : null;
        $record->catmute3	  = isset($_POST['xgs_mute_cat'][3]) && is_string($_POST['xgs_mute_cat'][3]) && $record->catmax3>0 ? $_POST['xgs_mute_cat'][3] : null;
        $record->catmute4	  = isset($_POST['xgs_mute_cat'][4]) && is_string($_POST['xgs_mute_cat'][4]) && $record->catmax4>0 ? $_POST['xgs_mute_cat'][4] : null;
        $record->catmute4	  = isset($_POST['xgs_mute_cat'][5]) && is_string($_POST['xgs_mute_cat'][5]) && $record->catmax5>0 ? $_POST['xgs_mute_cat'][5] : null;
       
        $record->exammute1	  = isset($_POST['xgs_mute_exam'][1]) && is_string($_POST['xgs_mute_exam'][1]) && $record->exammax1>0  ? $_POST['xgs_mute_exam'][1] : null;
        $record->exammute2	  = isset($_POST['xgs_mute_exam'][2]) && is_string($_POST['xgs_mute_exam'][2]) && $record->exammax2>0  ? $_POST['xgs_mute_exam'][2] : null;
        $record->exammute3	  = isset($_POST['xgs_mute_exam'][3]) && is_string($_POST['xgs_mute_exam'][3]  && $record->exammax3>0 ) ? $_POST['xgs_mute_exam'][3] : null;
        $record->exammute4	  = isset($_POST['xgs_mute_exam'][4]) && is_string($_POST['xgs_mute_exam'][4]) && $record->exammax4>0  ? $_POST['xgs_mute_exam'][4] : null;
        $record->exammute4	  = isset($_POST['xgs_mute_exam'][5]) && is_string($_POST['xgs_mute_exam'][5]) && $record->exammax5>0  ? $_POST['xgs_mute_exam'][5] : null;
        
        $record->audituser	  = $user->userid;
        $record->auditdate	  = date('Y-m-d');
        $record->audittime	  = time();

      if( $record->Save() ){
         return json_response(1,"Form {$formcode} {$yearcode} {$termcode} settings saved");
       }else{
       	 $error = @$db->ErrorMsg();
         return json_response(0,"save failed : {$error}");
       }
       
	}
  
 public function save_copy(){
    global $db;
	    
	   $user         =  new user();
       $formcode     =  filter_input(INPUT_POST , 'xgs_form');
       $formcode_to  =  filter_input(INPUT_POST , 'xgs_form_to');
       $yearcode     =  filter_input(INPUT_POST , 'xgs_year');
       $yearcode_to  =  filter_input(INPUT_POST , 'xgs_year_to');
       $termcode     =  filter_input(INPUT_POST , 'xgs_term');
       $termcode_to  =  filter_input(INPUT_POST , 'xgs_term_to');
       $gsyscode     =  filter_input(INPUT_POST , 'xgs_grading');
			
	   if(empty($yearcode_to)){
	    return json_response(0,"Select Year to copy to");	
	   }else if(empty($termcode_to)){
	    return json_response(0,"Select Term  to copy to");	
	   }
	   
       $record_from = new ADODB_Active_Record('SATSSG', array('FORMCODE','YEARCODE','TERMCODE'));
       
       $record_from->Load("
        FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");
       
       if(empty($record_from->_original)){
        return json_response(0,"Copy From record Not Found");
	   }

	    $record_to = new ADODB_Active_Record('SATSSG', array('FORMCODE','YEARCODE','TERMCODE'));
       
        $record_to->Load("
        FORMCODE='{$formcode_to}'
        AND YEARCODE='{$yearcode_to}'
        AND TERMCODE='{$termcode_to}'
       ");
       
       if(empty($record_to->_original)){
       	$record_to->id	          = generateID($record_to->_table);
       	$record_to->formcode	  = $formcode_to;
       	$record_to->yearcode	  = $yearcode_to;
       	$record_to->termcode	  = $termcode_to;
       }

        $skip_cols            = array('id','formcode','yearcode','termcode','_dbat','_table','_tableat','_where','_saved','_lasterr','_original');

        foreach ($record_from as $k=>$v){
  	     if(!in_array($k, $skip_cols)){
  	      $record_to->$k	  = $v;
  	     }
  	    }
  	    
        $record_to->audituser	  = $user->userid;
        $record_to->auditdate	  = date('Y-m-d');
        $record_to->audittime	  = time();
	   
        if( $record_to->Save() ){
         return json_response(1,"Form {$formcode_to} {$yearcode_to} {$termcode_to} settings saved");
        }else{
       	 $error = @$db->ErrorMsg();
         return json_response(0,"save failed : {$error}");
        }
       
	}
	
 public function remove(){
  global $db, $cfg;
   $grid = new grid();
  return $grid->grid_remove_row();
 }
 	
 
}
