<?php
 $forms = $db->CacheGetAssoc(1200,'SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
 $years = $db->CacheGetAssoc(1200,'SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->CacheGetAssoc(1200,'SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $sbc   = $db->CacheGetAssoc(0,'SELECT SBCCODE,SBCNAME FROM SASBC ORDER BY SBCCODE');
?>

    <div style="padding:2px">
	<form id="frmMain" method="post" onsubmit="return false;">
		
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    	
	    		<tr>
	    			<td width="180px">Year:</td>
	    			<td colspan="3">
	    				<?php echo ui::form_select_fromArray('year', $years, date('Y')," onchange=\"{$cfg['appname']}.list_subjects();\" ");  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Term:</td>
	    			<td colspan="3">
	    				<?php echo ui::form_select_fromArray('term', $terms,'',"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td >Form</td>
	    			<td colspan="3">
	    			<?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
	    			</td>
	    		</tr>
	    			
	    		<tr>
	    			<td >Category</td>
	    			<td colspan="3">
	    			<?php echo ui::form_select_fromArray('sbc', $sbc,'',"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
	    			</td>
	    		</tr>
	    			
	    		<tr>
	    			<td colspan="4"><hr></td>
	    		</tr>
	    			
	    		<tr>
	    			<td >&nbsp;</td>
	    			<td colspan="3">
	    			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onclick="<?php echo $cfg['appname']; ?>.list_subjects();">List Subjects</a>
	    			</td>.
	    		</tr>
	    		
	    		<tr>
	    			<td  colspan="4"><div id="div_subjects">&nbsp;</div></td>
	    		</tr>
	    		
	    	</table>
		
     </form>
    </div>
	<script>
		
		var <?php echo $cfg['appname']; ?> = {
		 clearForm:function (){
			$('#fProfile').form('clear');
		  },
		 popupItems:function (){
		  $('#dlgSubjects').dialog({
             closed: false,
             resizable: true,
             modal: true,
            });
		 },
		 list_subjects:function (){
            $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#frmMain').serialize()   , function( html ){
  		 	$('#div_subjects').html(html);
  		    });
		  },
		 save_setup:function (){
			 var fdata = $('#frmMain').serialize()  + '&modvars=<?php echo $vars; ?>&function=save';
		     $.post('./endpoints/crud/', fdata, function(data) {
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
				<?php echo $cfg['appname']; ?>.list_subjects();
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		}
	  }
		
	</script>
	
</body>
</html>
