<?php

  class requiredsubjects {
	private $id;
	private $datasrc;
	private $primary_key;
	private $regno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function list_subjects(){
		global $db,$cfg;
      
      $yearcode      = filter_input(INPUT_POST , ui::fi('year'));
      $formcode      = filter_input(INPUT_POST , ui::fi('form'));
      $termcode      = filter_input(INPUT_POST , ui::fi('term'));
      $sbccode       = filter_input(INPUT_POST , ui::fi('sbc'));
      
      $settings = $db->GetRow("select REQUIRED,OFFERED,MANDATORY from SAFCS WHERE YEARCODE='{$yearcode}' 
       AND TERMCODE='{$termcode}' 
       AND FORMCODE='{$formcode}'
       AND SBCCODE='{$sbccode}'
       ");
      
      $saved_required   = isset($settings['REQUIRED'])  && !empty($settings['REQUIRED']) ? unserialize(base64_decode( $settings['REQUIRED'] )) : array();
      $saved_offered    = isset($settings['OFFERED']) && !empty($settings['OFFERED']) ? unserialize(base64_decode( $settings['OFFERED'] )) : array();
      $saved_mandatory  = isset($settings['MANDATORY']) && !empty($settings['MANDATORY']) ? unserialize(base64_decode( $settings['MANDATORY'] )) : array();
      
      $subjects = $db->GetAssoc("
      SELECT S.SUBJECTCODE,C.CATCODE,C.CATNAME,S.SUBJECTNAME,S.CODEOFFC, S.OFFERED 
      FROM SASUBJECTS S
      LEFT JOIN SASUBJCATS C ON C.CATCODE=S.CATCODE
      order by C.SORTPOS,S.SUBJECTCODE asc
     ");
    
    
    $subject_cats  = array();
    $data  = array();
    
    if(count($subjects)>=1){
    	foreach ($subjects as $subjectcode=>$subject){
    		$catcode      =  valueof($subject, 'CATCODE');
    		$catname      =  valueof($subject, 'CATNAME');
    		$subjectname  =  valueof($subject, 'SUBJECTNAME');
    		$codeoffc     =  valueof($subject, 'CODEOFFC');
    		$offered     =  valueof($subject, 'OFFERED');
    		
    		if(!array_key_exists($catcode, $subject_cats)){
    			$subject_cats[$catcode] = $catname;
    		}
    		
    		$data[$catcode][] = array(
    		 'subjectcode'=>$subjectcode,
    		 'subjectname'=>$subjectname,
    		 'codeoffc'=>$codeoffc,
    		 'offered'=>$offered
    		 );
    	}
    }
    
    echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\" class=\"easyui-datagrid\">";
      
     if($subject_cats && sizeof($subject_cats)>=1){
     	
		 foreach ($subject_cats as $catcode=>$catname){
		 	
		 $num_subjects	            = isset($data[$catcode]) ? sizeof($data[$catcode]) : 1;
		 $selected_numcore_subjects = isset($saved_required[$catcode]) ? $saved_required[$catcode] : sizeof($data[$catcode]); 	
		 
		 $select_required = ui::numbers_select("required[$catcode]",1,$num_subjects,1,$selected_numcore_subjects);
		 	
		 echo "<tr>";
		  echo "<th colspan=\"2\">{$catcode} - {$catname}</th>";
		  echo "<th colspan=\"2\">Required - {$select_required}</th>";
		 echo "</tr>";
		 
		 if(isset($data[$catcode]) && sizeof($data[$catcode])>=1){

		 //<td>Offered</td>
		  echo " 
		   <td width=\"100px\">Subject Code</td>
		   <td>Subject Name</td>
		   
		   <td>Mandatory</td>
		   ";
		
		  foreach ($data[$catcode] as $subject){
		  	$subjectcode = valueof($subject,'subjectcode');
		  	$subjectname = valueof($subject,'subjectname');
		  	$offered     = valueof($subject,'offered');
		  	
		  	if(empty($settings)){
		  	 $offered_state = $offered==1 ? 'checked' : '';
		  	}else{
		  	 $offered_state = isset($saved_offered[$subjectcode]) && $saved_offered[$subjectcode]==1 ? 'checked' : '';	
		  	}
		  	
		  	$mandatory_state = isset($saved_mandatory[$subjectcode]) && $saved_mandatory[$subjectcode]==1 ? 'checked' : '';	
		  	
		  	$checkbox_offered      =  ui::form_checkbox("offered[{$subjectcode}]",$offered_state,'','',1);
		  	$checkbox_mandatory    =  ui::form_checkbox("mandatory[{$subjectcode}]",$mandatory_state,'','',1);

		  	//<td>{$checkbox_offered}</td>
		  echo "<tr>";
		   echo " 
		   <td width=\"40px\">{$subject['subjectcode']}</td>
		   <td>{$subject['subjectname']}</td>
		   
		   <td>{$checkbox_mandatory}</td>
		   ";
		  echo "</tr>";
		  
		  }
		  }
		  
		  echo "<tr>";
		   echo "<td colspan=\"4\"><hr></td>";
		  echo "</tr>";
		 
		 }//cat
		 
		  echo "<tr>";
		   echo "<td colspan=\"4\"><button  onclick=\"{$cfg['appname']}.save_setup();return false;\">Save</button></td>";
		  echo "</tr>";
		 
	 }
		 
     echo "</table>";
     
	}
	
	public function save(){
		global $db,$cfg;
//		$db->debug=1;
		
      //$yearcode      = filter_input(INPUT_POST , 'year');
      //$formcode      = filter_input(INPUT_POST , 'form');
      //$termcode      = filter_input(INPUT_POST , 'term');
      //$sbccode       = filter_input(INPUT_POST , 'sbc');
      
      $yearcode      = filter_input(INPUT_POST , ui::fi('year'));
      $formcode      = filter_input(INPUT_POST , ui::fi('form'));
      $termcode      = filter_input(INPUT_POST , ui::fi('term'));
      $sbccode       = filter_input(INPUT_POST , ui::fi('sbc'));
      
      $required      = isset($_POST['required']) ? $_POST['required'] : array();
      $offered       = isset($_POST['offered']) ? $_POST['offered'] : array();
      $mandatory     = isset($_POST['mandatory']) ? $_POST['mandatory'] : array();
      
      $exam_config =  $db->GetRow("
      SELECT NUMSUBJECTS,MINSUBJECTS 
      FROM SATSSG 
      WHERE FORMCODE='{$formcode}' 
      AND YEARCODE='{$yearcode}' 
      AND TERMCODE='{$termcode}'
      ");
      
      $numsubjects = valueof($exam_config,'NUMSUBJECTS');
      $numRequired = array_sum($required);
       
      if( $numRequired != $numsubjects){
//      	return json_response(0,'Required Subjects Must Equal '.$numsubjects);
      }
      
      if(sizeof($required)>0){
  	   $required = base64_encode(serialize($required));
      }else{
  	   $required = null;
      }
  
      if(sizeof($offered)>0){
  	   $offered = base64_encode(serialize($offered));
      }else{
  	   $offered = null;
      }
      
      if(sizeof($mandatory)>0){
  	   $mandatory = base64_encode(serialize($mandatory));
      }else{
  	   $mandatory = null;
      }
  
//      echo "\$required={$required} <br>";
//      echo "\$offered={$offered} <br>";
//      echo "\$mandatory={$mandatory} <br>";
      
      $record = new ADODB_Active_Record('SAFCS', array('YEARCODE','TERMCODE','FORMCODE'));
      
      $record->Load("
       YEARCODE='{$yearcode}' 
       AND TERMCODE='{$termcode}' 
       AND FORMCODE='{$formcode}' 
      ");
      
    if(empty($record->_original)){
     $record->id           =  generateID($record->_tableat);// 	
     $record->yearcode     =  $yearcode;
     $record->termcode     =  $termcode;
     $record->formcode     =  $formcode;
    }
    
     $record->sbccode      =  $sbccode;
     $record->required     =  $required;
     $record->offered      =  1;
     $record->mandatory    =  $mandatory;
         
//     print_pre($record);
//     exit();
//     $db->debug=1;
     
    if($record->Save()){
     return json_response(1,'record saved');
    }else{
     return json_response(0,'save failed');
    }

	}
	
	public function delete(){
		global $db, $cfg;
		
		$id  = filter_input(INPUT_POST , 'id');
		$del = $db->Execute("DELETE FROM SASUBSRC  WHERE ID={$id}");
		
		return  self::list_subjects();
		
		
	}
	
}
