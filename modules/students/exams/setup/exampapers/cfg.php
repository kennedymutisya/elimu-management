<?php
/**
* auto created config file for modules/students/setup/exampapers
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-09-11 22:14:34
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter   = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Exam Papers';//user-readable formart
 $cfg['appname']       = 'exam papers';//lower cased one word
 $cfg['datasrc']       = 'SAXPF';//where to get data
 $cfg['datatbl']       = 'SAXPF';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 250;
 
 $cfg['pkcol']         = 'FORMCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['zandq'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAFORMS|FORMCODE|FORMNAME|FORMCODE',
                      );
                      
$cfg['columns']['iilkl'] = array(
                      'dbcol'=>'SUBJECTCODE',
                      'title'=>'Subject',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASUBJECTS|SUBJECTCODE|SHORTNAME|SUBJECTCODE',
                      );
                      
$cfg['columns']['h4rvp'] = array(
                      'dbcol'=>'NUMCATS',
                      'title'=>'Cats',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SANL10|NMBCODE|NMBNAME|NMBCODE',
                      );
                      
$cfg['columns']['mflz2'] = array(
                      'dbcol'=>'NUMEXAMS',
                      'title'=>'Exams',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SANL10|NMBCODE|NMBNAME|NMBCODE',
                      );
                      
