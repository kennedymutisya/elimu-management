<?php
/**
* auto created config file for modules/students/exams/setup/examgrades
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-16 14:17:09
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Exam Grades';//user-readable formart
 $cfg['appname']       = 'examgrades';//lower cased one word
 $cfg['datasrc']       = 'VIEWXGRD';//where to get data
 $cfg['datatbl']       = 'SAXGRD';//base data src [for updates & deletes]
 $cfg['form_width']    = 430;
 $cfg['form_height']   = 315;
 $cfg['window_width']    = 515;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['klbu0'] = array(
                      'dbcol'=>'GSYSCODE',
                      'title'=>'Grading System',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAXGSYS|GSYSCODE|GSYSNAME|GSYSCODE',
                      );
                      



                      
$cfg['columns']['kywha'] = array(
                      'dbcol'=>'GSYSNAME',
                      'title'=>'Grading Sys.',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['w3qxv'] = array(
                      'dbcol'=>'GRADECODE',
                      'title'=>'Grade',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['9fnvt'] = array(
                      'dbcol'=>'MIN',
                      'title'=>'Min',
                      'width'=>60,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['btrmh'] = array(
                      'dbcol'=>'MAX',
                      'title'=>'Max',
                      'width'=>60,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['v4zgw'] = array(
                      'dbcol'=>'POINTS',
                      'title'=>'Points',
                      'width'=>60,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['fjfkx'] = array(
                      'dbcol'=>'COMMENTS',
                      'title'=>'Tchr Comments',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['sgukw'] = array(
                      'dbcol'=>'COMMENTS2',
                      'title'=>'Princ. Comm',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['klbu0']['columns']['GSYSCODE']  = array( 'field'=>'GSYSCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['klbu0']['columns']['GSYSNAME']  = array( 'field'=>'GSYSNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['klbu0']['source'] ='SAXGSYS';
  