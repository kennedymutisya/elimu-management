<?php
?>
<style>
	
div#add_subjects table#marks {font-size:12px;font-family:Verdana}
div#add_subjects table#tblSubSetup {font-size:12px;font-family:Verdana}
div#add_subjects table#tblSubSetup td.label{ background-color:#95B8E7;}
div#add_subjects table#tblSubSetup td.cat{ background-color:#E2EDFF;}
div#add_subjects table#tblSubSetup td.exam{ background-color:#FFE48D;}
div#add_subjects a.delete { color: #555;}
div#add_subjects a.delete:hover { color: #D00;}

div#add_subjects td.input input {
    width: 50px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

div#add_subjects td.header {
 
 padding-left: 5px;
 background-color:#EDF3FF;
}

div#add_subjects td.input {
 border-bottom:1px dotted #95B8E7;
 border-right:1px dotted #95B8E7;
 padding-left: 5px;
}

div#add_subjects td.top {
 border-top:1px dotted #999;
}

div#add_subjects td.left {
 border-left:1px dotted #999;
}

div#add_subjects td.right {
 border-right:1px dotted #999;
}

div#add_subjects #loading { 
width: 100%; 
position: absolute;
}

div#add_subjects #pagination{
text-align:center;
margin-left:100px;
}

div#add_subjects li.nav{	
list-style: none; 
float: left; 
margin-left: 5px; 
padding:4px; 
border:solid 1px #dddddd;
color:#0063DC;
font-size:12px;
}

div#add_subjects li.nav:hover{ 
color:#FF0084; 
cursor: pointer; 
}

</style>

    <div id="add_subjects">
    <form id="form_subjects<?php echo MNUID; ?>" method="post" novalidate onsubmit="return false;">
	<table cellpadding="2" cellspacing="0" width="590px">
	 <tr>
	 <td width="180px">Student</td>
	 <td><?php echo ui::form_input( 'text', 'find_admno', 20, '', '', '', '', '', '' , ""); ?></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.loadProfile();">View</a></td>
	 <td>&nbsp;</td>
	 </tr>
	 <tr>
	 <td colspan="3"><div id="div_stage">&nbsp;</div></td>
	 </tr>
	  <tr>
	 <td colspan="3"><div id="div_subjects">&nbsp;</div></td>
	 </tr>
	 
	</table> 
	</form>
	</div>
    		
    <script>
  
       var <?php echo $cfg['appname']; ?> = {
		clearForm :function (){
			$('#form_subjects<?php echo MNUID; ?>').form('clear');
			$('#find_admno').val();
		},
		loadProfile :function(){
		  var find_admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
		  $.post('./endpoints/crud/',  'modvars=<?php echo $vars; ?>&function=get_profile&admno='+find_admno,function( html ){
  		   $('#div_stage').html(html);
  		  });
		},
		list_subjects:function(){
			var find_admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
			if(find_admno===''){
		 	 alert('Select a Student First');
		    }else{
             $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#form_subjects<?php echo MNUID; ?>').serialize()+'&admno='+find_admno   , function( html ){
  		 	  $('#div_subjects').html(html);
  		     });
		    }
		},
		add_subject:function (){
			 var subject = $('#<?php echo ui::fi('subject'); ?>').val();
			 if(subject==='' || subject=='undefined'){
			 	$.messager.alert('Error','Select Subject to Add','error');
			 	return;
			 }
			 if(confirm("Add '"+subject+"'? ")){
			   var fdata = $('#form_subjects<?php echo MNUID; ?>').serialize() + '&module=<?php echo $vars; ?>&function=add_subject';	
		       $.post('./endpoints/crud/', fdata, function(data) {
               if (data.success === 1) {
               	 $.messager.show({title: 'Success',msg: data.message});
                 <?php echo $cfg['appname']; ?>.list_subjects();
                } else {
				 $.messager.alert('Error',data.message,'error');
              }
             }, "json");
		   }
		},
		delete_subject:function (id,subjectname,yearcode,termcode){
			 if(confirm("Delete '"+subjectname+"' for '"+yearcode+"' '"+termcode+"' ")){
			  var fdata = '&id='+ id + '&subjectname='+subjectname+'&module=<?php echo $vars; ?>&function=delete';	
		      $.post('./endpoints/crud/', fdata, function(data) {
               if (data.success === 1) {
               	 $.messager.show({title: 'Success',msg: data.message});
                 <?php echo $cfg['appname']; ?>.list_subjects();
                } else {
				 $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
	   },
		print_marks:function (r){
  	     var w=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&r='+r,'p','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          w.focus();
        }
	}
		 
	<?php
      echo ui::ComboGrid($combogrid_array,'find_admno',"{$cfg['appname']}.loadProfile");
	?>
    </script>
