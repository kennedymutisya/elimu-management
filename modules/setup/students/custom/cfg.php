<?php
/**
* auto created config file for modules/setup/students/custom
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2015-10-26 08:22:23
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Custom';//user-readable formart
 $cfg['appname']       = 'custom';//lower cased one word
 $cfg['datasrc']       = 'ADCATEGORIES';//where to get data
 $cfg['datatbl']       = 'ADCATEGORIES';//base data src [for updates & deletes]
 $cfg['form_width']    = 510;
 $cfg['form_height']   = 230;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 230;
 
 $cfg['pkcol']         = 'CATGCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 
