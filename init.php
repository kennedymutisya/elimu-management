<?php

define('ROOT', realpath(dirname(__FILE__)).'/');

require_once(ROOT.'include/errors.inc.php');
require_once(ROOT.'include/functions.php');
require_once(ROOT.'include/defs.inc.php');
require_once(ROOT.'include/academic_functions.php');

ini_set('date.timezone', 'Africa/Nairobi');

spl_autoload_register(null, false);

spl_autoload_extensions('.php, .class.php');

 function classLoader($class) {

        $classfile =  ROOT.'classes/' . strtolower($class) .'/class.'.strtolower($class).'.php';
        
        if (is_readable($classfile)){
         require_once($classfile);
        }
        
 }

 spl_autoload_register('classLoader');
 
 $protocol =  'http';
 
 if(isset($_SERVER['SERVER_PROTOCOL'])){
 $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')  === FALSE ? 'http' : 'https';
 }

 session_start();
