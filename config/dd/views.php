<?php


$db->debug=0;

$views = array();

$views['VIEWUSERS'] = "
select U.ID, U.GROUPCODE,G.GROUPNAME,G.IST,U.USERID,U.USERNAME, U.DEPTCODE,D.DEPTNAME,U.EMAIL,U.PASSWORD
from USERS U 
LEFT JOIN USERGROUPS G ON G.GROUPCODE=U.GROUPCODE
LEFT JOIN SADEPT D ON D.DEPTCODE=U.DEPTCODE
 ";

//principles
$views['VIEWPRC'] = "
select P.ID, T.SLTCODE, P.TCHNO, T.FULLNAME, P.YEARSTART , P.YEAREND  
from SAPRC P
LEFT JOIN SATEACHERS T ON T.TCHNO=P.TCHNO
 ";

//Teacher Responsibilities Data
$views['VIEWRTD'] = "
select P.ID, T.SLTCODE, P.TCHNO, T.FULLNAME, P.RTSTART , P.RTEND ,P.RTCODE,R.RTNAME 
from SARTD P
left join SATEACHERS T ON T.TCHNO=P.TCHNO
left join SART R ON R.RTCODE=P.RTCODE
 ";

//Teacher Subject allocation
$views['VIEWTSA'] = "
select A.ID, T.SLTCODE, A.TCHNO, T.FULLNAME, A.SUBJECTCODE,S.SUBJECTNAME,S.SHORTNAME,S.CODEOFFC,A.STREAMCODE,R.STREAMNAME, R.FORMCODE, A.YEARCODE
from SATSA A
inner join SATEACHERS T ON T.TCHNO=A.TCHNO
inner join SASTREAMS R ON R.STREAMCODE=A.STREAMCODE
inner join SASUBJECTS S ON S.SUBJECTCODE=A.SUBJECTCODE
 ";

//Class Teacher
$views['VIEWCT'] = "
select C.ID, T.SLTCODE, C.TCHNO, T.FULLNAME, C.YEARCODE , C.STREAMCODE,R.STREAMNAME, R.FORMCODE
from SACT C
inner join SATEACHERS T ON T.TCHNO=C.TCHNO
inner join SASTREAMS R ON R.STREAMCODE=C.STREAMCODE
 ";

//PTA
$views['VIEWPTA'] = "
SELECT P.ID, P.PTACATCODE ,C.PTACATNAME,P.STREAMCODE, P.MEMBERID, P.FULLNAME, P.SLTCODE, P.GNDCODE, P.MOBILEPHONE, P.RESIDENCE
FROM SAPTA P
INNER JOIN SAPTACAT C ON C.PTACATCODE=P.PTACATCODE
inner join SASTREAMS R ON R.STREAMCODE=P.STREAMCODE
 ";

//BOG
$views['VIEWBOG'] = "
SELECT B.ID, B.BOGCATCODE ,C.BOGCATNAME, B.MEMBERID, B.FULLNAME, B.SLTCODE, B.GNDCODE, B.MOBILEPHONE, B.RESIDENCE
FROM SABOG B
INNER JOIN SABOGCAT C ON C.BOGCATCODE=B.BOGCATCODE
 ";

//subjects
$views['VIEWSUBJECTS'] = "
SELECT  S.ID, S.CATCODE,C.CATNAME,S.SUBJECTCODE, S.SUBJECTNAME , S.SHORTNAME, S.CODEOFFC, S.OFFERED,C.SORTPOS
FROM SASUBJECTS S
LEFT JOIN SASUBJCATS C ON C.CATCODE = S.CATCODE
 ";

//school info
$views['VIEWSCHOOL'] = "
select S.ID,S.SCHOOLCODE,S.SCHOOLNAME , S.MOECODE, S.TSCCODE, S.KNECCODE,
S.TELEPHONE, S.MOBILEPHONE,S.POSTALCODE, S.ADDRESSPOST, S.ADDRESSPHYS,S.EMAIL , S.WEBSITE,
S.COUNTYCODE,C.COUNTYNAME,
S.CONSTITUENCYCODE,Y.CONSTITUENCYNAME,
S.DISTRICTCODE, D.DISTRICTNAME,
S.DIVISIONCODE, V.DIVISIONNAME,
S.ZONECODE, Z.ZONENAME,
S.LOCATIONCODE, L.LOCATIONNAME,
S.SUBLOCATIONCODE, B.SUBLOCATIONNAME,
S.ENROLLTYPECODE, ET.ENROLLTYPENAME,
S.GENDERTYPECODE , GT.GENDERTYPENAME,
S.CATGCODE , CT.CATGNAME,
S.LEVELCODE , LV.LEVELNAME,
S.SSTATUSCODE, ST.SSTATUSNAME,
S.MISSION,S.VISION,S.MOTTO,S.LOGO
 from SASCHOOL S
 left join ADCOUNTIES C ON C.COUNTYCODE=S.COUNTYCODE
 LEFT JOIN ADDISTRICTS D ON D.DISTRICTCODE=S.DISTRICTCODE
 LEFT JOIN ADCONSTITUENCIES Y ON Y.CONSTITUENCYCODE=S.CONSTITUENCYCODE
 LEFT JOIN ADDIVISIONS V ON V.DIVISIONCODE=S.DIVISIONCODE
 LEFT JOIN ADZONES Z ON Z.ZONECODE = S.ZONECODE
 LEFT JOIN ADLOCATIONS L ON L.LOCATIONCODE=S.LOCATIONCODE
 LEFT JOIN ADSUBLOCATIONS B ON B.SUBLOCATIONCODE = S.SUBLOCATIONCODE
 LEFT JOIN ADENROLLTYPES ET ON ET.ENROLLTYPECODE = S.ENROLLTYPECODE
 LEFT JOIN ADGENDERTYPES GT ON GT.GENDERTYPECODE =S.GENDERTYPECODE
 LEFT JOIN ADCATEGORIES CT ON CT.CATGCODE=S.CATGCODE
 LEFT JOIN ADLEVELS LV ON LV.LEVELCODE=S.LEVELCODE
 LEFT JOIN ADSTATUSES ST ON ST.SSTATUSCODE=S.SSTATUSCODE
 ";

//streams
//SASTREAMS,SAFORMS 
$views['VIEWSTREAMS'] = "
select x.ID,x.FORMCODE,q.FORMNAME,q.STREAMS,x.STREAMCODE,x.STREAMNAME
from SASTREAMS x 
left join SAFORMS q on q.FORMCODE=x.FORMCODE
 ";
 
$views['VIEWSP'] = "
SELECT P.ID,P.ADMNO,P.ADMNO ACCOUNTNO,P.FULLNAME, P.DOB, P.GNDCODE, P.RLGCODE , 
P.HOUSECODE, H.HOUSENAME, P.DORMCODE, D.DORMNAME, 
P.STREAMCODE, T.STREAMNAME, T.FORMCODE ,F.FORMNAME,
P.SBCCODE, P.KCPEGRADE, P.KCPEMEAN, P.KCPEPOINT, P.KCPEMARKS, P.INDEXNO, P.ADMDATE,
P.MOBILEPHONE, P.RESIDENCE, P.POSTALCODE, P.ADDRESS, P.EMAIL, P.MEDSTATUS, P.YEARCODE
FROM  SASTUDENTS P
left join SASTREAMS T ON T.STREAMCODE = P.STREAMCODE
left join SAFORMS F ON F.FORMCODE = T.FORMCODE
left join SADORMS D ON D.DORMCODE = P.DORMCODE
left join SAHOUSES H ON H.HOUSECODE = P.HOUSECODE
WHERE (P.DELETED IS NULL OR P.DELETED=0)
AND (P.COMPLETED IS NULL OR P.COMPLETED=0)
AND (P.DISCON IS NULL OR P.DISCON=0)
AND (P.TRANSFERED IS NULL OR P.TRANSFERED=0)
";

$views['VIEWSPC'] = "
SELECT P.ID,P.ADMNO,P.ADMNO ACCOUNTNO,P.FULLNAME, P.DOB, P.GNDCODE, P.RLGCODE , P.HOUSECODE, 
P.STREAMCODE, T.STREAMNAME, T.FORMCODE ,F.FORMNAME,
P.SBCCODE, P.KCPEGRADE, P.KCPEMEAN, P.KCPEPOINT, P.KCPEMARKS, P.INDEXNO, P.ADMDATE,
P.MOBILEPHONE, P.RESIDENCE, P.POSTALCODE, P.ADDRESS, P.EMAIL, P.MEDSTATUS, P.YEARCODE
FROM  SASTUDENTS P
left join SASTREAMS T ON T.STREAMCODE = P.STREAMCODE
left join SAFORMS F ON F.FORMCODE = T.FORMCODE
WHERE (P.DELETED IS NULL OR P.DELETED=0)
AND (P.COMPLETED=1)
AND (P.DISCON IS NULL OR P.DISCON=0)
AND (P.TRANSFERED IS NULL OR P.TRANSFERED=0)
 ";

//SASATT,SASTUDENTS 
$views['VIEWATTENDANCE'] = "
select w.ADMNO,m.FULLNAME,w.STREAMCODE,T.STREAMNAME,T.FORMCODE ,F.FORMNAME,w.YEARCODE,w.TERMCODE,w.ATTDATE,w.STATUSID,w.COMMENTS,m.GNDCODE
from SASATT w 
left join SASTUDENTS m on m.ADMNO=w.ADMNO
left join SASTREAMS T ON T.STREAMCODE = w.STREAMCODE
left join SAFORMS F ON F.FORMCODE = T.FORMCODE
 ";

//exam grades
$views['VIEWXGRD'] = "
select G.ID, G.GSYSCODE,S.GSYSNAME,G.GRADECODE,G.MIN,G.MAX,G.POINTS,G.COMMENTS,G.COMMENTS2
from SAXGRD G
LEFT JOIN SAXGSYS S ON S.GSYSCODE=G.GSYSCODE
 ";


$views['VIEWSTUDENTSUBJECTS'] = "
select E.ID, E.ADMNO ,S.FULLNAME, S.KCPEMARKS, S.KCPERANK, S.MOBILEPHONE, F.SBCCODE, E.SUBJECTCODE , J.SUBJECTNAME, 
J.SHORTNAME, J.CODEOFFC, J.CATCODE, C.CATNAME, C.SORTPOS, E.FORMCODE , F.FORMNAME, E.STREAMCODE,
E.YEARCODE , E.TERMCODE,
E.CAT1 ,E.CAT2,E.CAT3,E.CAT4,E.CAT5,
E.CAT1P ,E.CAT2P,E.CAT3P,E.CAT4P,E.CAT5P,
E.CAT1R,E.CAT2R,E.CAT3R,E.CAT4R,E.CAT5R,
E.AVGCAT,
E.PAPER1,E.PAPER2,E.PAPER3,E.PAPER4,E.PAPER5,
E.PAPER1P,E.PAPER2P,E.PAPER3P,E.PAPER4P,E.PAPER5P,
E.PAPER1R,E.PAPER2R,E.PAPER3R,E.PAPER4R,E.PAPER5R,
E.AVGEXAM,
E.CAT1GRADECODE,
E.CAT2GRADECODE,
E.CAT3GRADECODE,
E.CAT4GRADECODE,
E.CAT5GRADECODE,
E.PAPER1GRADECODE,
E.PAPER2GRADECODE,
E.PAPER3GRADECODE,
E.PAPER4GRADECODE,
E.PAPER5GRADECODE,
E.CAT1POINTS,
E.CAT2POINTS,
E.CAT3POINTS,
E.CAT4POINTS,
E.CAT5POINTS,
E.PAPER1POINTS,
E.PAPER2POINTS,
E.PAPER3POINTS,
E.PAPER4POINTS,
E.PAPER5POINTS,
E.CAT1COMMENTS,E.CAT2COMMENTS,E.CAT3COMMENTS,E.CAT4COMMENTS,E.CAT5COMMENTS,
E.PAPER1COMMENTS,E.PAPER2COMMENTS,E.PAPER3COMMENTS,E.PAPER4COMMENTS,E.PAPER5COMMENTS,
E.CAT1POS,E.CAT2POS,E.CAT3POS,E.CAT4POS,E.CAT5POS,
E.CAT1POSF,E.CAT2POSF,E.CAT3POSF,E.CAT4POSF,E.CAT5POSF,
E.PAPER1POS,E.PAPER2POS,E.PAPER3POS,E.PAPER4POS,E.PAPER5POS,
E.PAPER1POSF,E.PAPER2POSF,E.PAPER3POSF,E.PAPER4POSF,E.PAPER5POSF,
E.TOTALPOS,
E.TOTAL, E.TOTALP, E.GRADECODE , E.POINTS , E.STATUSCODE, E.COMMENTS, E.PICKED,
S.DORMCODE,S.HOUSECODE
from SASTUDSUB E
inner join SASTUDENTS S ON S.ADMNO=E.ADMNO
left join SAFORMS F ON F.FORMCODE = E.FORMCODE
left join SASTREAMS T ON T.STREAMCODE = E.STREAMCODE
left join SASUBJECTS J ON J.SUBJECTCODE = E.SUBJECTCODE
left join SASUBJCATS C ON C.CATCODE = J.CATCODE
WHERE (E.DELETED IS NULL OR E.DELETED=0)
AND (J.OFFERED=1)
AND (S.DELETED IS NULL OR S.DELETED=0)
AND (S.COMPLETED IS NULL OR S.COMPLETED=0)
AND (S.DISCON IS NULL OR S.DISCON=0)
AND (S.TRANSFERED IS NULL OR S.TRANSFERED=0)
";

$views['VIEWSTUDENTSMEAN'] = "
select M.ADMNO ,S.FULLNAME,S.KCPEGRADE, S.KCPEMEAN, S.KCPEPOINT, S.KCPEMARKS , S.KCPERANK,
F.SBCCODE,M.YEARCODE, M.TERMCODE, M.EXAM,
M.FORMCODE , F.FORMNAME, M.STREAMCODE,
M.NUMSUBJ, M.TOTAL , M.TOTALMAX, M.TOTALP, M.TOTALM, M.TOTALPC,
M.GRADECODEP, M.GRADECODEM, M.POSTREAM_MARKS, M.POSFORM_MARKS, M.POSTREAM_POINTS , M.POSFORM_POINTS,M.POSDORM_POINTS , M.POSHSE_POINTS,
M.COMMENTS,M.COMMENTS2,S.DORMCODE,S.HOUSECODE
from SASTMN M
inner join SASTUDENTS S ON S.ADMNO=M.ADMNO
left join SAFORMS F ON F.FORMCODE = M.FORMCODE
WHERE (S.DELETED IS NULL OR S.DELETED=0)
";


//FINFSTR,SATERMS,SAFORMS,FINITEMS 
$views['VIEWFS'] = "
select g.ID,g.YEARCODE,g.TERMCODE,p.TERMNAME,g.FORMCODE,b.FORMNAME,g.FITEMCODE,t.FITEMNAME,t.MUCODE,g.AMOUNT,t.ACCTGL,t.ISTUITION 
from FINFSTR g 
  left join SATERMS p on p.TERMCODE=g.TERMCODE
  left join SAFORMS b on b.FORMCODE=g.FORMCODE
  left join FINITEMS t on t.FITEMCODE=g.FITEMCODE
";

$views['VIEWSFRCTS'] = "
select R.RECEIPTNO , R.BATCHID , R.ADMNO , 
P.FULLNAME , R.YEARCODE , R.TERMCODE , R.FORMCODE , R.DATERECEIPT , 
R.PYMDCODE , M.PYMDDESC , R.DOCUMENTNO , R.DOCUMENTDATE , R.DOCUMENTAMOUNT , R.APPROVED, R.PROCESSED
from FINRCTHD R
inner join SASTUDENTS P ON R.ADMNO = P.ADMNO
inner join FINPYMD M ON M.PYMDCODE = R.PYMDCODE
";

//SLMTF,SLFS,SLMTP 
$views['VIEWLIBFINES'] = "
select c.ID,c.MTPCODE,b.MTPNAME,c.TYPECODE,l.TYPENAME,c.MUCODE,c.AMOUNT
from SLMTF c 
  left join SLFS l on l.TYPECODE=c.TYPECODE
  left join SLMTP b on b.MTPCODE=c.MTPCODE
 ";
 
 //SLMBR,SLMTP 
$views['VIEWLIBMEMBERS'] = "
select e.ID,e.MEMBERID,e.FULLNAME,e.MTPCODE,v.MTPNAME,v.ISSTUD ,e.MOBILE,e.BORROWMAX,e.DATEJOIN,e.DATEEXPIRE,v.MAXBOOKS,v.MAXDAYS
from SLMBR e 
  left join SLMTP v on v.MTPCODE=e.MTPCODE
 ";
 
 
 //books list
//SLBK,SLBCT,SASUBJECTS,SAFORMS 
$views['VIEWLIBBOOKS'] = "
select o.ID,o.BOOKCODE,o.BOOKTITLE,o.ISBN,o.SUBJECTCODE,o.BCTCODE,o.DESCRIPTION,o.FORMCODE,o.PBLCODE,o.EDITION,o.SERIES,o.COPIES,o.AVAILABLE,
o.DATERCV,o.YRCPR,o.LOCATION,o.SLVCODE,o.COST,o.DATEPRC,g.BCTNAME,c.CATCODE,c.SUBJECTNAME,c.SHORTNAME,c.CODEOFFC,c.OFFERED,w.FORMNAME,w.STREAMS,w.SBCCODE 
from SLBK o 
  left join SLBCT g on g.BCTCODE=o.BCTCODE
  left join SASUBJECTS c on c.SUBJECTCODE=o.SUBJECTCODE
  left join SAFORMS w on w.FORMCODE=o.FORMCODE
 ";
 
 //stationery list
 //SLST,SLSCT 
$views['VIEWLIBSTATIONERY'] = "
select f.ID,f.SCTCODE,v.SCTNAME ,f.STCODE,f.STNAME,f.QTY
from SLST f 
  left join SLSCT v on v.SCTCODE=f.SCTCODE
 ";
 
 //SLTRN, SLMBR,SLMTP
$views['VIEWLIBTRANS'] = "
select x.ID,x.TRANSNO,c.MEMBERID,c.FULLNAME,c.MTPCODE,v.MTPNAME,c.BORROWMAX,c.DATEJOIN,c.DATEEXPIRE ,v.ISSTUD,
x.BOOKCODE,b.BOOKTITLE,b.SUBJECTCODE,x.DATEISSUE,x.LENDDAYS,x.DATEDUE,x.RETURNED,x.FINE,x.DAMAGE
from SLTRN x
left join SLMBR c  on c.MEMBERID=x.MEMBERID
left join SLMTP v on v.MTPCODE=c.MTPCODE
left join SLBK b on b.BOOKCODE=x.BOOKCODE
 ";
 
 
 //SLSIH,SLMBR,SLST
$views['VIEWLIBSTI'] = "
select c.ID,c.ISSUENO,k.MEMBERID,c.QTY,c.DESCR,c.DATEISSUE,k.FULLNAME,k.MTPCODE,v.MTPNAME,v.ISSTUD,k.MOBILE,
k.DATEJOIN,k.DATEEXPIRE
from SLSIH c 
left join SLMBR k  on k.MEMBERID=c.MEMBERID
left join SLMTP v on v.MTPCODE=k.MTPCODE
 ";
 
 //SLAQS,SLST,SLSPL 
$views['VIEWLIBAQS'] = "
select m.ID,k.SCTCODE,m.STCODE,k.STNAME,m.QTY,m.UNITCOST,m.DATEACQ,m.SPLCODE,o.SPLNAME,o.MOBILEPHONE,o.EMAIL,o.TOWN 
from SLAQS m 
  left join SLST k on k.STCODE=m.STCODE
  left join SLSPL o on o.SPLCODE=m.SPLCODE
 ";
 
 
 //SLAQB,SLBK,SLSPL 
$views['VIEWLIBAQB'] = "
select m.ID,m.BOOKCODE,m.SPLCODE,m.QTY,m.UNITCOST,m.DATEACQ,l.BOOKTITLE,l.ISBN,l.SUBJECTCODE,l.BCTCODE,l.DESCRIPTION,l.FORMCODE,
l.AUTHOR1,l.AUTHOR2,l.AUTHOR3,l.PBLCODE,l.EDITION,l.SERIES,l.COPIES,l.DATERCV,l.YRCPR,l.LOCATION,l.SLVCODE,
l.COST,l.DATEPRC,g.SPLNAME,g.MOBILEPHONE,g.EMAIL,g.TOWN 
from SLAQB m 
  left join SLBK l on l.BOOKCODE=m.BOOKCODE
  left join SLSPL g on g.SPLCODE=m.SPLCODE
 ";
 
//FININVD,SATERMS,SAFORMS,FINITEMS,SASTUDENTS
//sf invoices
$views['VIEWSFINV'] = "
select e.ID,e.HEADERID,e.YEARCODE,e.TERMCODE,r.TERMNAME,e.FORMCODE,e.FITEMCODE,t.FITEMNAME,e.ADMNO,q.FULLNAME,e.AMOUNT,e.INVNO,e.INVDESC,
e.ENTRYTYPE,e.FYRPRCODE,l.FORMNAME,t.MUCODE,t.ISTUITION,q.GNDCODE
from FININVD e 
  left join SATERMS r on r.TERMCODE=e.TERMCODE
  left join SAFORMS l on l.FORMCODE=e.FORMCODE
  left join FINITEMS t on t.FITEMCODE=e.FITEMCODE
  left join SASTUDENTS q on q.ADMNO=e.ADMNO
 ";
 
/*


$views['VIEWINVRPT'] = '
SELECT DISTINCT S.CMPCODE "Campus Code", C.CMPNAME  "Campus Name", S.PRCODE  "Prog Code", P.PRDESC  "prog Name", F.REGNO  "Student Number" , S.FULLNAME  "Student name",  SUM(F.AMOUNT) INVOICETOTAL
FROM FININVC F
LEFT JOIN ACSP S ON S.REGNO=F.REGNO
LEFT JOIN ACPRG  P ON S.PRCODE = P.PRCODE
LEFT JOIN ACCMP C ON S.CMPCODE=C.CMPCODE
LEFT JOIN FINITEMS G ON G.FITEMCODE=F.FITEMCODE
GROUP BY S.CMPCODE, C.CMPNAME, S.PRCODE, P.PRDESC, F.REGNO , S.FULLNAME
';

$views['VIEWFEEFINS'] = "
SELECT F.ACCOUNTNO , S.REGNO , S.FULLNAME, F.BATCHNO , F.BATCHDESC , F.ENTRY , F.SOURCE , F.ENTRYDATE , F.ENTRYTOTAL, F.DOCNO , F.DOCTYPE , F.REFERENCE,F.ACCSETCODE
FROM FINOBL F
INNER JOIN ACSP S ON S.STUDACCOUNTNO=F.ACCOUNTNO
";

*/

echo "<table cellspacing=0 cellpadding=0>";

$error_replace = array('"',"'");

foreach ($views as $view_name=>$view_sql) {
$view_name_sm = strtolower($view_name);

echo "<tr>\r\n";
    $test = 1;
    $error = $db->ErrorMsg();
    $error = '';
    if(isset($test) && empty($error)){

	   @$db->Execute("DROP VIEW {$view_name}");
	   @$db->Execute("DROP TABLE {$view_name}");
	   @$db->Execute("DROP VIEW {$view_name_sm}");

	   echo  "<td>Creating view {$view_name} </td> \r\n";

	   if(!$db->Execute("CREATE VIEW {$view_name} AS {$view_sql}")){

	   	 $error  = "Error Creating view {$view_name} \\n ";
	   	 $error  .= str_replace($error_replace,'',$db->ErrorMsg());

	   	echo  "<td style=\"cursor:pointer\" onclick=\"javascript:alert('{$error}');\">-X</td>\r\n ";
	   }else{

	    echo  "<td>-OK</td>\r\n ";
	   }

     }
echo "<tr>\r\n";

}//foreach

echo "<table>";

?>
