<?php

$db->debug=1;

$recommeded_export_records = 100000;

$mask = '%';

//$tables  = $db->MetaTables('TABLES', $showSchema = false, $mask);
//
//foreach($tables as $table){
	//echo "\$tables[] = '{$table}'; <br>";
//}
//
//exit();
$tables[] = 'ADCATEGORIES';
$tables[] = 'ADCLASSROOMITEMS';
$tables[] = 'ADCLASSROOMTYPES';
$tables[] = 'ADCONSTITUENCIES';
$tables[] = 'ADCOUNTIES';
$tables[] = 'ADDISTRICTS';
$tables[] = 'ADDIVISIONS';
$tables[] = 'ADENROLLTYPES';
$tables[] = 'ADEQUIPMENTS';
$tables[] = 'ADFACILITIES';
$tables[] = 'ADFINANCESRCS';
$tables[] = 'ADGENDERTYPES';
$tables[] = 'ADLEVELS';
$tables[] = 'ADLOCATIONS';
$tables[] = 'ADMATERIALTYPES';
$tables[] = 'ADSPECIALNEEDS';
$tables[] = 'ADSPONSORS';
$tables[] = 'ADSTATUSES';
$tables[] = 'ADSUBLOCATIONS';
$tables[] = 'ADTEACHINGAIDS';
$tables[] = 'ADZONES';
$tables[] = 'FINCURR';
$tables[] = 'FINITEMS';
$tables[] = 'FINMU';
$tables[] = 'FINPYMD';
$tables[] = 'FINSTS';
$tables[] = 'FINYRPR';
$tables[] = 'SAACTV';
$tables[] = 'SABOGCAT';
$tables[] = 'SAETC';
$tables[] = 'SAFCS';
$tables[] = 'SAFORMS';
$tables[] = 'SAFSC';
$tables[] = 'SAGND';
$tables[] = 'SAKCPE';
$tables[] = 'SAKCPEGRADES';
$tables[] = 'SAKCPESUBJECT';
$tables[] = 'SAMGRD';
$tables[] = 'SAMST';
$tables[] = 'SANL10';
$tables[] = 'SAPTACAT';
$tables[] = 'SARLG';
$tables[] = 'SARLS';
$tables[] = 'SARS';
$tables[] = 'SART';
$tables[] = 'SASBC';
$tables[] = 'SASLT';
$tables[] = 'SASTCAT';
$tables[] = 'SASTREAMS';
$tables[] = 'SASTS';
$tables[] = 'SASUBJCATS';
$tables[] = 'SASUBJECTS';
$tables[] = 'SASUBREMARKS';
$tables[] = 'SATCAL';
$tables[] = 'SATCHP';
$tables[] = 'SATERMS';
$tables[] = 'SATQF';
$tables[] = 'SATSA';
$tables[] = 'SATSS';
$tables[] = 'SATSSG';
$tables[] = 'SAXFMTST';
$tables[] = 'SAXGRD';
$tables[] = 'SAXGSYS';
$tables[] = 'SAXOPTS';
$tables[] = 'SAXPF';
$tables[] = 'SAXSTS';
$tables[] = 'SAYEAR';
$tables[] = 'SAYN';
$tables[] = 'SECTMV';
$tables[] = 'SYSAPPS';
$tables[] = 'SYSMNUAUTH';
$tables[] = 'SYSMODCFG';
$tables[] = 'SYSMODMNU';
$tables[] = 'SYSMODS';


$skip_tables = array();
$tables = array();
$tables[] = 'USERS';

foreach ($tables as $table){
  $table = strtoupper($table);
  if(!in_array($table,$skip_tables)) {
  	
     $columns = $db->MetaColumns($table,$notcasesensitive=true);
     
echo "dumping data for {$table}...<br>";

$xml   = "<?xml version=\"1.0\"?> \r\n";
$xml  .= "<schema version=\"0.3\"> \r\n";
$xml  .= "\r\n";
  #--make insert data sql
  
     $table_data  = $db->SelectLimit("SELECT * FROM {$table}",$recommeded_export_records);
     
     $num_records = $table_data->RecordCount();
     
     if( ($num_records>0) ) {
     	
	  $skip_table = false;
	  
      $sql_part1      = " INSERT INTO {$table} ";

      #-----------get first part of insert sql
       $num_columns = count($columns);
       $pos  =  1;
       $sql_part1 .= "(";
       
       foreach ($columns as $column_name=>$column_properties){
  	
        $sql_part1 .= $column_name;
        $sql_part1 .= $pos < $num_columns? ',' :'';
        ++$pos;
       }
       $sql_part1 .= ")";

       $sql_part2 = 'VALUES';

       #------------------make insert data sql
       $xml .= "<sql> \r\n";
       $xml .= "  <descr>Insert some data into the {$table} table.</descr>\r\n";
      
       $tables_rows = 0;
       while (!$table_data->EOF){//loop table data
   	     $pos  = 1;
   	     //print_pre($table_data->fields);
	     $sql_part3 = "(";
	        foreach ($columns as $column_name=>$column_properties){
	          $column_value  = trim($table_data->fields[$column_name]);
	          $column_type   = get_standard_coltype($column_properties->type); 
	          
	          //handle special characters in data
	          if(isset($special_chars) && is_array($special_chars) && sizeof($special_chars)>0){
	          	foreach ($special_chars as $characterRef=>$NumericRef){
	          		$column_value = str_replace($characterRef,$NumericRef,$column_value);
	          	}
	          }
	          
	         	
	         if($column_type=='I' && $column_value==0) {
	         	$sql_part3 .= '0';
	         }elseif(is_null($column_value) || empty($column_value)){
	         	$sql_part3 .= 'NULL';
	         }else{
	         	switch ($column_type){
	         	case 'I': 	         		
     	          
     	          if($column_value==0){
     	          	$sql_part3 .= '0';
     	          }else{
     	          	$sql_part3 .= $column_value;	
     	          }
     	          
     	        break;
     	        case 'D': 	         		
     	          $sql_part3 .= "'";
     	          $sql_part3 .= date("Y-m-d",strtotime($column_value));
     	         $sql_part3 .= "'";
     	        break;
     	        default:
     	         $sql_part3 .= "'";
     	          $sql_part3 .= str_replace('&','&amp;',$column_value);
     	         $sql_part3 .= "'";
     	        break;
	           }//end switch coltype
	         }
	         
             $sql_part3 .= $pos < $num_columns? ',' :'';
             
             ++$pos;
	        }//foreach column in $table_data->fields
	     $sql_part3 .= ")";
	   
	    $xml .= "   <query>".$sql_part1.' '.$sql_part2.' '.$sql_part3."</query> \r\n";
		
	    $table_data->MoveNext();
      }//while $table_data
        $xml .= "</sql>\r\n";               
     }else{
     	$skip_table = true;
     }
  
		$xml .= "</schema>";
		
		if(!$skip_table){
		 $handle = fopen( ROOT ."data/default/{$table}.xml",'w');
         fwrite($handle,$xml);
         fclose($handle);
         $xml = null;
		}
		
  #--end insert sql
 }//if !skip
}//for each table

function get_standard_coltype ($column_type){
	
	/*
  C:  Varchar capped to 255 characters.
  X:  Larger varchar capped to 4000 characters (to be compatible with Oracle). 
  XL: For Oracle returns CLOB otherwise the largest varchar size.

  C2: Multibyte varchar
  X2: Multibyte varchar (largest size)

  B:  BLOB (binary large object)

  D:  D (some databases do not support this and we return a Dtime type)
  T:  Dtime or Timestamp
  L:  Integer field suitable for storing booleans (0 or 1)
  I:  Integer (mapped to I4)
  I1: 1-byte integer
  I2: 2-byte integer
  I4: 4-byte integer
  I8: 8-byte integer
  F:  Floating point N
  N:  Numeric or decimal N
*/
    $return      = $column_type;
    $column_type = strtolower($column_type);
	switch ($column_type){
		#--------------------------------
		case 'varchar':	
		case 'varchar2':
		 $return = 'C';
		break;
		#--------------------------------
		case 'nvarchar':
		 $return = 'X';
		 break;
		#--------------------------------
		case 'int':	
		 $return = 'I';
		break;
		#--------------------------------
		case 'date':	
		case 'datetime':	
		 $return = 'D';
		break;
		#--------------------------------
		case 'float':
		 $return = 'I';
		 break;
		#--------------------------------
		case 'nchar':
		case 'char':
		 $return = 'C';
		 break;
		case 'numeric':
		case 'number':
		 $return = 'N';
		 break;
		#--------------------------------
		case 'clob':
		 $return = 'XL';
		 break;
		#--------------------------------
		case 'blob':
		case 'image':
		 $return = 'B';
		 break;
		#--------------------------------
		
	}
	
  return $return;
 }
 
?>
