<?php

  class mm{
	private $id;
	private $primary_key;
	
	private $menu_parents   = array();
	private $menu_children  = array();
	
	public function __construct(){
		global $cfg;
		
		
	}
	
	public function list_modules(){
	 $appid =  filter_input(INPUT_POST , 'appid');
	 return  ui::form_select('modid','SYSMODS','MODID','MODNAME'," APPID='{$appid}' ",'',"onchange=\"mm.list_menu();\" data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ",'','MODPOS');
	}
	
	public function list_menu(){
		global $db;
		
	 $appid =  filter_input(INPUT_POST , 'appid');
	 $modid =  filter_input(INPUT_POST , 'modid');
     
	 $modulename = $db->GetOne("SELECT MODNAME FROM SYSMODS WHERE APPID='{$appid}' AND MODID='{$modid}'  order by MODPOS ");
	 $menu       = $db->GetAssoc("SELECT MNUID,MNUNAME,PARENTID,MODPATH,MODTYPE,POSITION,ACTIVE , PINHOME FROM SYSMODMNU WHERE APPID='{$appid}' AND MODID='{$modid}' ORDER BY POSITION ASC ");
	 
	 if(count($menu)>0){
	  foreach ($menu as $mnuid=>$menuitem){
 	   
 	   	$mnuname   =  valueof($menuitem, 'MNUNAME');
 	   	$modpath   =  valueof($menuitem, 'MODPATH');
 	   	$parentid  =  valueof($menuitem, 'PARENTID');
 	   	$modtype   =  valueof($menuitem, 'MODTYPE');
 	   	$position  =  valueof($menuitem, 'POSITION');
 	   	$active    =  valueof($menuitem, 'ACTIVE');
 	   	$pinhome   =  valueof($menuitem, 'PINHOME');
 	   	
 	   	if(empty($parentid)){
 	   	 $this->menu_parents[$mnuid]             = array($mnuname, $modpath, $modtype, $position, $active , $pinhome);
 	   	}else{
 	   	 $this->menu_children[$parentid][$mnuid] = array($mnuname, $modpath, $modtype, $position, $active , $pinhome);	
        }
 	   	
      }
     }
     
     header('Content-type: text/json');
  	 
     $return =  "[";
     
     $num_parents = count($this->menu_parents);
      
     if($num_parents>0){
     	$count = 1;
     	
     
     	
     	 foreach ($this->menu_parents as $parentid=>$parent){
     	 	
     	   $parentname = valueof($parent,0);
     	   $modpath    = valueof($parent,1); 
     	   $modtype    = valueof($parent,2); 
     	   $position   = valueof($parent,3);
     	   $active     = valueof($parent,4);
     	   $pinhome    = valueof($parent,5);
     	   
     	    $return .=  "{\r\n";
     	     $return .=  "\"id\":{$parentid},\r\n";
     	     $return .= "\"text\": \"{$parentname}\",\r\n";
     	     $return .= "\"state\":\"closed\", \r\n";
     	     
     	    if(array_key_exists($parentid, $this->menu_children)){
				//erax 08dec15
                $return .= "\"attributes\":{ \"url\":\"./module.php?id={$parentid}&pid=0\", \"parentid\":\"{$parentid}\",\"position\":\"{$position}\", \"active\":\"{$active}\", \"pinhome\":\"{$pinhome}\", \"modpath\":\"{$modpath}\" , \"modtype\":\"{$modtype}\" },";
     	    	$return .= "\"children\":[";
     	         $return .= self::list_menu_children($parentid);
     	        $return .= "]";
     	        
     	     }else{
     	    	$return .= "\"attributes\":{ \"url\":\"./module.php?id={$parentid}&pid=0\", \"parentid\":\"{$parentid}\",\"position\":\"{$position}\",\"active\":\"{$active}\", \"pinhome\":\"{$pinhome}\", \"modpath\":\"{$modpath}\" , \"modtype\":\"{$modtype}\" }";
             }
     	    
     	    $return .=  "}";//close leaf
     	   
     	   if($count<$num_parents) {
     	   	 $return .=  ",\r\n";
     	   }
     	   
     	   ++$count;
         }
         
     	
     }
     
     $return .=  "]\r\n";
      
     return $return;
     
	}
	
	public function list_menu_children( $parentid ){
		
      $num_children = count( $this->menu_children[$parentid] );
      
     if($num_children>0){
     	$count = 1;
     	$tree = "";
     	
     	 foreach ($this->menu_children[$parentid] as $childid=>$child){
     	 	
     	   $childname = valueof($child,0);
     	   $modpath   = valueof($child,1);
     	   $modtype   = valueof($child,2);
     	   $position  = valueof($child,3);
     	   $active    = valueof($child,4);
     	   $pinhome   = valueof($child,5);
     	     
     	    if(!array_key_exists($childid, $this->menu_children)){
     	     $state = 'open';	
     	    }else{
     	     $state = 'closed';	
     	    }
     	    
     	   $tree .= "{\r\n";
     	    $tree .=  "\"id\":{$childid},\r\n";
     	    $tree .=  "\"text\":\"{$childname}\",\r\n";
     	  
     	    $tree .= "\"state\":\"{$state}\", \r\n";
     	    
     	    $tree .=  "\"attributes\":{ \"url\":\"./module.php?id={$childid}&pid={$parentid}\", \"parentid\":\"{$parentid}\",\"position\":\"{$position}\",\"active\":\"{$active}\", \"pinhome\":\"{$pinhome}\", \"modpath\":\"{$modpath}\", \"modtype\":\"{$modtype}\"  }\r\n";
     	    
     	    //children
     	    if(array_key_exists($childid, $this->menu_children)){
     	    	$tree .= ",\r\n";//terminate  attributes
     	    	$tree .=  "\"children\":[\r\n";
     	         $tree .=  self::list_menu_children( $childid );
     	        $tree .=  "]\r\n";
     	    }
     	    
     	     $tree .=  "}";//close leaf
     	   
     	   if($count<$num_children) {
     	   	$tree .= ",\r\n";
     	   }
     	   
     	   ++$count;
         }
         
      }
      
     return $tree;
     
	}
	
	public function select_modtype() {
     $modtype = filter_input(INPUT_POST , 'modtype', FILTER_SANITIZE_STRING);
     $module_types         = array();
     $module_types['dga']  = 'Data Grid - Auto Created';
     $module_types['dgc']  = 'Data Grid - Custom Made';
     $module_types['cmd']  = 'Custom Made Module';
     return ui::form_select_fromArray('modtype',$module_types,$modtype,"onchange=\"\" data-options=\"required:true\"  class=\"easyui-validatebox textbox\"",false, 200, 5000);
 	 
}
 
	public function get_window_properties(){
	 global $db;
	 $mnuid      = filter_input(INPUT_POST , 'mnuid');	
	 
	 $properties = $db->GetRow("SELECT DESCR,ATTRIBS FROM SYSMODMNU WHERE MNUID='{$mnuid}'");
	 $properties_vars = array();
	 
	 if(isset($properties['ATTRIBS']) && !empty($properties['ATTRIBS'])){
	   $properties_vars = json_decode($properties['ATTRIBS']);
     }
     
     $minimizable  = valueof($properties_vars,'min')==1 ? 'checked' : '';
     $maximizable  = valueof($properties_vars,'max')==1 ? 'checked' : '';
     $collapsible  = valueof($properties_vars,'col')==1 ? 'checked' : '';
     $closable     = valueof($properties_vars,'clo')==1 ? 'checked' : '';
     $resizable    = valueof($properties_vars,'res')==1 ? 'checked' : '';
     
	$html = '<table cellpadding="0" cellspacing="0">';
 
  $html .= "<tr>";
   $html .=  "<td>Width</td>";
   $html .= "<td><input type=\"text\" name=\"win_width\"  id=\"win_width\" size=\"5\"   value=\"".valueof($properties_vars,'win',690)."\"  class=\"easyui-validatebox textbox\" ></td>";
  $html .=  "</tr>";
   
  $html .=  "<tr>";
   $html .=  "<td>Height</td>";
   $html .=  "<td><input type=\"text\" name=\"win_height\"  id=\"win_height\" size=\"5\" value=\"".valueof($properties_vars,'hei',690)."\"  class=\"easyui-validatebox textbox\" ></td>";
  $html .=  "</tr>";
  
 $html .= '<tr>';
  $html .= '<td><label for="win_minimizable">Minimizable</label></td>';
  $html .= '<td><input type="checkbox" id="win_minimizable" name="win_minimizable" value="1"  '.$minimizable.' ></td>';
 $html .= '</tr>';
  
 $html .= '<tr>';
  $html .= '<td><label for="win_maximizable">Maximizable</label></td>';
  $html .= '<td><input type="checkbox" id="win_maximizable" name="win_maximizable" value="1" '.$maximizable.' ></td>';
 $html .= '</tr>';
 
 $html .= '<tr>';
  $html .= '<td><label for="win_closable">Closable</label></td>';
  $html .= '<td><input type="checkbox" id="win_closable" name="win_closable" value="1"  '.$closable.'></td>';
 $html .= '</tr>';
 
 $html .= '<tr>';
  $html .= '<td><label for="win_resizable">Resizable</label></td>';
  $html .= '<td><input type="checkbox" id="win_resizable" name="win_resizable" value="1" '.$resizable.' ></td>';
 $html .= '</tr>';
 
 $html .= '<tr>';
  $html .= '<td><label for="win_collapsible">Collapsible</label></td>';
  $html .= '<td><input type="checkbox" id="win_collapsible" name="win_collapsible" value="1" '.$collapsible.' ></td>';
 $html .= '</tr>';
 
 $html .= '</table>';	

 return $html; 
}
	
	public function get_modules_properties(){
	 global $db;
	 
	 $mnuid      = filter_input(INPUT_POST , 'mnuid');	
	 $properties = array();
	 $data       = array();
	 
	  $properties = $db->GetRow("SELECT DESCR,MODTYPE,ATTRIBS FROM SYSMODMNU WHERE MNUID='{$mnuid}'");
	  
	  $attribs = valueof($properties,'ATTRIBS','','json_decode');
	  
	  $data['descr']            = valueof($properties,'DESCR');
	  $data['modtype']          = valueof($properties,'MODTYPE');
	  $data['win_width']        = valueof($attribs,'win');
	  $data['win_height']       = valueof($attribs,'hei');
	  $data['win_minimizable']  = valueof($attribs,'min');
	  $data['win_maximizable']  = valueof($attribs,'max');
	  $data['win_collapsible']  = valueof($attribs,'col');
	  $data['win_closable']     = valueof($attribs,'clo');
	  $data['win_resizable']    = valueof($attribs,'res');
	  
	  return json_encode($data);
	  
	}
	
	public function save_menu(){
		global $db;
		
	  $appid       = filter_input(INPUT_POST , 'appid'); 
      $modid       = filter_input(INPUT_POST , 'modid'); 
      $modpath     = filter_input(INPUT_POST , 'modpath');
      $modtype     = filter_input(INPUT_POST , 'modtype');
      
      $modpath     = centerTrim($modpath);
      $modpath     = strtolower($modpath);
      $modpath     = str_replace(' ', '' ,$modpath);
      
      $menuid      = filter_input(INPUT_POST , 'menuid'); 
      $menuname    = filter_input(INPUT_POST , 'menuname'); 
      $descr       = filter_input(INPUT_POST , 'descr', FILTER_SANITIZE_STRING);
      $descr       = !empty($descr) ? $descr : $menuname;
      $parentname  = filter_input(INPUT_POST , 'parentname'); 
      $parentid    = filter_input(INPUT_POST , 'parentid'); 
      $position    = filter_input(INPUT_POST , 'position');
      $active      = filter_input(INPUT_POST , 'active');
      $pinhome     = filter_input(INPUT_POST , 'pinhome');
      $modkey      = md5($modpath);
      
	   //print_pre($_POST);
	   
      $modpathExists = $db->GetOne("SELECT MNUID FROM SYSMODMNU WHERE MODKEY='{$modkey}' ");
	  
      if($modpathExists>0 && $modpathExists != $menuid){
		return json_response(0,  "Use a unique module path." );
	  }
	  
      if(empty($menuid)){
      	$menuid = generateID('SYSMODMNU','MNUID');
      }
      
      $record = new ADODB_Active_Record('SYSMODMNU', array('APPID','MODID','MNUID'));
      $record->Load("APPID='{$appid}' and MODID='{$modid}' and MNUID={$menuid} ");
      
      if(empty($record->_original)){
      $record->appid     =  $appid;
      $record->modid     =  $modid;
      $record->mnuid     =  $menuid;
      }
      
      $record->mnuname   =  $menuname;
      $record->modpath   =  $modpath;
      $record->modkey    =  $modkey;
      $record->modtype   =  $modtype;
      $record->parentid  =  $parentid;
      $record->descr     =  $descr;
      $record->position  =  $position;
      $record->active    =  $active;
      $record->pinhome   =  $pinhome;
      
      //print_pre($record);
      if($record->Save()){

      return  json_encode(
	    array(
	    'success' => 1,
	    'message' => "<b>{$menuname}</b> saved",
	    'mnuid'=> $record->mnuid,
	    'modpath'=>$modpath
	   )
	   );
	   
      }else{
      	echo json_response(0,  $db->ErrorMsg() );
      }
      
	}
	
	public function delete_menu(){
		global $db;
		
		$menuid = filter_input(INPUT_POST , 'menuid');
		
		$numChildren = $db->GetOne("SELECT COUNT(MNUID) NUL FROM SYSMODMNU WHERE PARENTID={$menuid} ");
		
		if($numChildren>0){
			return json_response(0,  "Not Allowed. {$numChildren} Attached child menu." );
		}
		
		$menu  = $db->GetRow("SELECT * FROM SYSMODMNU WHERE MNUID={$menuid} ");
		
		if(empty($menu)){
		  return json_response(0,  "Menu not found. Try saving first" );	
		}
		
		$appid  = valueof($menu, 'APPID');
		$modid  = valueof($menu, 'MODID');
		
		$del   = $db->Execute("DELETE FROM SYSMODMNU WHERE MNUID={$menuid} ");
		
      if($del){

      //clear module config
      $db->Execute("DELETE FROM SYSMODCFG WHERE APPID='{$appid}' and MODID='{$modid}' and MNUID={$menuid} ");
      	
      return  json_encode(
	    array(
	    'success' => 1,
	    'message' => 'menu deleted'
	   )
	   );
	   
      }else{
      	return json_response(0,  $db->ErrorMsg() );
      }
      
	}
	
	public function suggest_names(){
	 $modpath  = filter_input(INPUT_POST , 'modpath');
	 
	 if(strstr($modpath,'/')){
	 	$modpath_array = explode('/', $modpath);
	 	
	 	$modpath_array_rv = array_reverse($modpath_array);
	 	
	 	if(isset($modpath_array_rv[0]) && !empty($modpath_array_rv[0])){
	 		return json_encode(array(
	 		 'menuname'  => Camelize($modpath_array_rv[0]),
	 		 'apptitle' => Camelize($modpath_array_rv[0]),
	 		));
	 	}
	 }
	}
	
	public function list_tables(){
    global $db;
    
     $tables_array = array();
     $tables = $db->MetaTables('TABLES');
     $views  = $db->MetaTables('VIEWS');
     
     if($tables && sizeof($tables)>0){
 	  foreach ($tables as $table){
 		$tables_array[$table]  = $table;
 	  }
     }
     
     
     if($views && sizeof($views)>0){
 	  foreach ($views as $view){
 		$tables_array[$view]  = $view;
 	  }
     }
     
	 return ui::form_select_fromArray('table_col_datasrc',$tables_array,'',"onchange=\"mm.list_columns_select_src();\"  class=\"easyui-validatebox textbox\" ",false, 200, 5000);
	 
	}
	
	public function list_columns_pkey(){
		global $db;
		$table_datatbl = filter_input(INPUT_POST , 'table_datatbl');
		
		$colums        = $db->MetaColumns($table_datatbl);
		$keycolums     = $db->MetaPrimaryKeys($table_datatbl);
		$skipcols      = array('AUDITUSER','AUDITDATE','AUDITTIME','IPADDRESS','AUDITIP','IPADDR');
		$colums_array  = array();
		
		if(($colums)){
			foreach ($colums as $column => $properties){
//				$colums_array[$column] = $column;
                if(!in_array($column, $skipcols)){
				  $colums_array[$column] = $column;
                }
			}
		}
		
		$default_column = isset($keycolums[0]) ? $keycolums[0] : '';
		
		return ui::form_select_fromArray('primary_key_col', $colums_array , $default_column , '', false, 200);
		
	}
	
	public function list_columns_select_src(){
		global $db;
		
		$table_col_datasrc  = filter_input(INPUT_POST , 'table_col_datasrc');
		$col_linked_to_foreign_col  = filter_input(INPUT_POST , 'col_linked_to_foreign_col');
		$coldetype          = filter_input(INPUT_POST , 'coldetype');
		$linked_tbl_col     = $coldetype=='code' ? 'linked_tbl_col_code' : 'linked_tbl_col_name';
		$skipcols           = array('ID','AUDITUSER','AUDITDATE','AUDITTIME','IPADDRESS','AUDITIP','IPADDR');
		$skipcols           = array('AUDITUSER','AUDITDATE','AUDITTIME','IPADDRESS','AUDITIP','IPADDR');
		
		$colums         = $db->MetaColumns($table_col_datasrc);
		
		$colums_array = array();
		
		$default_column =  $col_linked_to_foreign_col;
		
		if(($colums)){
			foreach ($colums as $column => $properties){
				if(!in_array($column, $skipcols)){
				 $colums_array[$column] = $column;
				 
				 if($coldetype=='code' && strstr($column,'CODE') && $col_linked_to_foreign_col==$column){
				 	$default_column = $column;
				 }elseif($coldetype=='name' && strstr($column,'NAME')){
				 	$default_column = $column;
				 }
				 
                }
			}
		}
		
		if(!empty($col_linked_to_foreign_col)){
//		 $default_column = $col_linked_to_foreign_col;
		}
		
		
		return ui::form_select_fromArray($linked_tbl_col, $colums_array , $default_column , 'class="easyui-combobox"', false, 200 );
	 	
	}
	
 public function data_apps(){
  global $db, $cfg;
  
	$page     = isset($_POST['page']) ? intval($_POST['page']) : 1;
    $rows     = isset($_POST['rows']) ? intval($_POST['rows']) : 200;
    $offset   = ($page-1)*$rows;
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		 $sort_col_name  = strtoupper($sort_col_name);
		 $sort_col_order = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]       = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = 'order by ' .implode(',', $sortbys);
	 }
   }else{
	 $sortby   =  'order by ID asc';  
   }
	
	 $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
		
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = @$filterRule->value;
	  
	  $filterRuleDBCol = strtoupper($filterRuleCol);
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
         default  : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	 
	}else{
	    $filters_conditions_str =  '';
	}
	
    $sql_where   = " WHERE 1=1 {$filters_conditions_str}";
	
	$numRecords    = $db->GetOne("select count(*) from SYSAPPS {$sql_where} ");
	
	$result["total"] = $numRecords;
	   
	$sql = "
	SELECT * FROM SYSAPPS
    {$sql_where}
    {$sortby}
    ";
	
	$rs = $db->SelectLimit($sql,$rows, $offset);
		
	$items = array();
	
	if($rs){
	 
	 while(!$rs->EOF){
	 	
	  foreach ($rs->fields as $dbCol=>$val){	
	    $formCol = strtolower($dbCol);
	    $rs->fields[$formCol]  = $val;
	    unset($rs->fields[$dbCol]);
	  }
	  
	  array_push($items, $rs->fields);
	  
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
		
 }
 
 public function save_app(){
	global $db;
	
	$appid   = filter_input(INPUT_POST , 'appid', FILTER_SANITIZE_STRING);
	$appname = filter_input(INPUT_POST , 'appname', FILTER_SANITIZE_STRING);
    $record  = new ADODB_Active_Record('SYSAPPS', array('APPID'));
    
    if(!empty($appid)){
    $record->Load("APPID='{$appid}'");
    }
    
    if(empty($record->_original)){
     $record->id     =  generateID( $record->_tableat);
     $record->appid  =  $appid;
    }
    	
    $record->appname =  $appname;
    
    if($record->Save()){
    
     return json_response(1,'record saved');
    }else{
     $error = $db->ErrorMsg();	
     return json_response(0,"save failed : {$error}");
    }
 
 }
 
 public function remove_app(){
	global $db;
	
	$id = filter_input(INPUT_POST , 'id', FILTER_SANITIZE_STRING);
    $record = new ADODB_Active_Record('SYSAPPS', array('ID'));
    
    if($id>0) {
    $record->Load("ID={$id}");
    
    if(!empty($record->_original)) {
    
    if($record->Delete()){
     return json_response(1,'Record Deleted');
    }else{
     $error = $db->ErrorMsg();	
     return json_response(0,"Delete Failed : {$error}");
    }
    
    }else{
     return json_response(0,"Record Not Found ");
    }
 
   }else{
     return json_response(0,"Record ID Not Found ");
   }
 
 }
 
 public function data_modules(){
  global $db, $cfg;
  
	$page     = isset($_POST['page']) ? intval($_POST['page']) : 1;
    $rows     = isset($_POST['rows']) ? intval($_POST['rows']) : 200;
    $offset   = ($page-1)*$rows;
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		 $sort_col_name  = strtoupper($sort_col_name);
		 $sort_col_order = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]       = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = 'order by ' .implode(',', $sortbys);
	 }
   }else{
	 $sortby   =  'order by ID asc';  
   }
	
	 $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
		
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = @$filterRule->value;
	  
	  $filterRuleDBCol = strtoupper($filterRuleCol);
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
         default  : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	 
	}else{
	    $filters_conditions_str =  '';
	}
	
    $sql_where   = " WHERE 1=1 {$filters_conditions_str}";
	
	$numRecords    = $db->GetOne("select count(*) from SYSMODS {$sql_where} ");
	
	$result["total"] = $numRecords;
	   
	$sql = "
	SELECT M.ID,M.APPID,A.APPNAME,M.MODID,M.MODNAME,M.COMMENTS,M.MODPOS,M.MODICON, M.MOBPAGE
	 FROM SYSMODS M
    LEFT JOIN SYSAPPS A ON A.APPID=M.APPID
    {$sql_where}
    {$sortby}
    ";
	   
	$rs = $db->SelectLimit($sql,$rows, $offset);
		
	$items = array();
	
	if($rs){
	 
	 while(!$rs->EOF){
	 	
	  foreach ($rs->fields as $dbCol=>$val){	
	    $formCol = strtolower($dbCol);
	    $rs->fields[$formCol]  = $val;
	    unset($rs->fields[$dbCol]);
	  }
	  
	  array_push($items, $rs->fields);
	  
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
		
 }
 
 public function save_module(){
	global $db;
	
	$appid   = filter_input(INPUT_POST , 'appid', FILTER_SANITIZE_STRING);
	$modid   = filter_input(INPUT_POST , 'modid', FILTER_SANITIZE_STRING);
	$modname = filter_input(INPUT_POST , 'modname', FILTER_SANITIZE_STRING);
	$modpos  = filter_input(INPUT_POST , 'modpos', FILTER_SANITIZE_STRING);
	$modicon = filter_input(INPUT_POST , 'modicon', FILTER_SANITIZE_STRING);
	$mobpage = filter_input(INPUT_POST , 'mobpage', FILTER_SANITIZE_STRING);
	
    $record  = new ADODB_Active_Record('SYSMODS', array('APPID','MODID'));
    
    if(!empty($appid)){
    $record->Load("APPID='{$appid}' and MODID='{$modid}'");
    }
    
    if(empty($record->_original)){
     $record->id     =  generateID( $record->_tableat);
     $record->appid  =  $appid;
     $record->modid  =  $modid;
    }
    	
    $record->modname =  $modname;
    $record->modpos  =  $modpos;
    $record->modicon =  $modicon;
    $record->mobpage =  $mobpage;
	
    if($record->Save()){
     /**
      */
     if(!empty($mobpage)){
      
	 }
	 
     return json_response(1,'record saved');
    }else{
     $error = $db->ErrorMsg();	
     return json_response(0,"save failed : {$error}");
    }
 
 }
 
 public function remove_module(){
	global $db;
	
	$id = filter_input(INPUT_POST , 'id', FILTER_SANITIZE_STRING);
    $record = new ADODB_Active_Record('SYSMODS', array('ID'));
    
    if($id>0) {
    $record->Load("ID={$id}");
    
    if(!empty($record->_original)) {
    
    if($record->Delete()){
     return json_response(1,'Record Deleted');
    }else{
     $error = $db->ErrorMsg();	
     return json_response(0,"Delete Failed : {$error}");
    }
    
    }else{
     return json_response(0,"Record Not Found ");
    }
 
   }else{
     return json_response(0,"Record ID Not Found ");
   }
 
 }
 
	
	public function module_columns_config(){
		global $db;
		 $appid   = filter_input(INPUT_POST , 'appid');
		 $modid   = filter_input(INPUT_POST , 'modid');
		 $menuid  = filter_input(INPUT_POST , 'menuid');
		 
         $tables_array = array();
         $tables_array_all = array();
         $modcfg       = array();
         $table_keycol_select  = ui::form_select_fromArray('primary_key_col', array(0=>'...'),0);
 
         $tables = $db->MetaTables('TABLES');
         $views  = $db->MetaTables('VIEWS');
 
         if(count($tables)){
 	      foreach ($tables as $table){
 		   $tables_array[$table]  = $table;
 		   $tables_array_all[$table]  = $table;
 	      }
         }
         
         if($views && sizeof($views)>0){
 	      foreach ($views as $view){
 		   $tables_array_all[$view]  = $view;
 	      }
         }
         
         $module = $db->GetRow("SELECT * FROM SYSMODMNU WHERE APPID='{$appid}' and MODID='{$modid}' and MNUID={$menuid} ");
         $vars   = $db->GetOne("SELECT VARS FROM SYSMODCFG WHERE APPID='{$appid}' and MODID='{$modid}' and MNUID={$menuid} ");
         
         if(!empty($vars)){
          $modcfg = unserialize( base64_decode( $vars ) );
         }
         
       
         $modtype            = valueof($module, 'MODTYPE');
         $table_datatbl      = valueof($modcfg, 'table_datatbl');
         $table_datasrc      = valueof($modcfg, 'table_datasrc');

         $form_width         = valueof($modcfg, 'form_width',400);
         $form_height        = valueof($modcfg, 'form_height',200);

         $window_width       = valueof($modcfg, 'window_width',500);
         $window_height      = valueof($modcfg, 'window_height',400);

         $primary_key_col    = valueof($modcfg, 'primary_key_col');
         
         switch($modtype){
			 case 'dga':
			  $allow_config_cols  = true;
			  $allow_reconfig_mod = true;
			 break;
			 case 'dgc':
			  $allow_config_cols  = true;
			  $allow_reconfig_mod = false;
			 break;
			 case 'cmd':
			  $allow_config_cols  = false;
			  $allow_reconfig_mod = false;
			 break;
		 }
		 
		 if(!empty($vars)){
		  $allow_reconfig_mod = true;
         }
         
         if(sizeof($modcfg)>0){
			 
          $chk_minimizable    = valueof($modcfg, 'chk_minimizable');
          $chk_maximizable    = valueof($modcfg, 'chk_maximizable');
          $chk_collapsible    = valueof($modcfg, 'chk_collapsible');
          $chk_closable       = valueof($modcfg, 'chk_closable');
          $chk_resizable      = valueof($modcfg, 'chk_resizable');
          
          $chk_button_add     = valueof($modcfg, 'chk_button_add');
          $chk_button_edit    = valueof($modcfg, 'chk_button_edit');
          $chk_button_save    = valueof($modcfg, 'chk_button_save');
          $chk_button_delete  = valueof($modcfg, 'chk_button_delete');
          $chk_button_import  = valueof($modcfg, 'chk_button_import');
          $chk_button_export  = valueof($modcfg, 'chk_button_export');
          
         }else{
		  $chk_minimizable    = 1;
          $chk_maximizable    = 0;
          $chk_collapsible    = 1;
          $chk_closable       = 1;
          $chk_resizable      = 0;
          
          $chk_button_add     = 1;
          $chk_button_edit    = 1;
          $chk_button_save    = 1;
          $chk_button_delete  = 1;
          $chk_button_import  = 1;
          $chk_button_export  = 1;
          
         }
         
         $button_add         = $chk_button_add==1 ? 'checked' : '';
         $button_edit        = $chk_button_edit==1 ? 'checked' : '';
         $button_save        = $chk_button_save==1 ? 'checked' : '';
         $button_delete      = $chk_button_delete==1 ? 'checked' : '';
         $button_import      = $chk_button_import==1 ? 'checked' : '';
         $button_export      = $chk_button_export==1 ? 'checked' : '';
         
         $minimizable      = $chk_minimizable==1 ? 'checked' : '';
         $maximizable      = $chk_maximizable==1 ? 'checked' : '';
         $collapsible      = $chk_collapsible==1 ? 'checked' : '';
         $closable         = $chk_closable==1 ? 'checked' : '';
         $resizable        = $chk_resizable==1 ? 'checked' : '';
         $openAnimation    = 'show';
         $closeAnimation   = 'hide';
         $winAnimations    =  array(
             'hide'=>'hide', 
             'slide'=>'slide',
             'fade'=>'fade'
         );
         $methods       = array('post'=>'post','get'=>'get');
         $method        = 'post';
         $queryParams   = '{}';//{a:1,b:2}
    
         if($allow_config_cols){
		  $allow_config_cols_handler = "mm.list_columns_properties();";
		 }else{
		  $allow_config_cols_handler = "";
		 }
		 
		 $tables_save_select = ui::form_select_fromArray('table_datatbl',$tables_array,$table_datatbl,"onchange=\"mm.set_same_source();mm.list_columns_pkey();{$allow_config_cols_handler}\" class=\"easyui-validatebox easyui-combobox\" ",false, 200, 5000);
         $tables_list_select = ui::form_select_fromArray('table_datasrc',$tables_array_all,$table_datasrc,"onchange=\"mm.set_same_source();mm.list_columns_pkey();{$allow_config_cols_handler}\"  class=\"easyui-validatebox easyui-combobox\" ",false, 200,5000 );
 
 
         if(!empty($table_datatbl)){
         
         //get primary key col**********************************************************************************
		 $skipcols  = array('AUDITUSER','AUDITDATE','AUDITTIME','IPADDRESS','AUDITIP','IPADDR');
		 $colums    = $db->MetaColumns($table_datatbl);
		 
		 $keycolums = $db->MetaPrimaryKeys($table_datatbl);
		
		  $colums_array = array();
		
		 if(($colums)){
			foreach ($colums as $column => $properties){
               if(!in_array($column, $skipcols)){
				$colums_array[$column] = $column;
                }
			}
		 }
		
        if(!empty($primary_key_col)){
         $default_column = $primary_key_col;	
        }else{
		 $default_column = isset($keycolums[0]) ? $keycolums[0] : '';
        }
        
		$table_keycol_select = ui::form_select_fromArray('primary_key_col', $colums_array , $default_column , '', false, 200);
		
	 	//***********************************************************************************************************
         }
	 	
 
              $html =  '<form id="frmMMcfg" name="frmMMcfg"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
                            
              $html .=  "<table width=\"100%\" border=\"0\" cellpadding=\"2\"  cellspacing=\"1\" >";
 
              $html .=  "<tr>";
               $html .=  "<td  width=\"130px\">Data Table</td>";
               $html .=  "<td>{$tables_save_select}</td>";
              $html .=  "</tr>";
 
              $html .=  "<tr>";
               $html .=  "<td>Data Source</td>";
               $html .=  "<td>{$tables_list_select}</td>";
              $html .=  "</tr>";
 
              $html .=  "<tr>";
               $html .=  "<td>Window Dimensions</td>";
               $html .=  "<td>
                Width <input type=\"text\" name=\"window_width\"  id=\"window_width\" size=\"5\"   value=\"{$window_width}\"  class=\"easyui-validatebox textbox\" >
                Height<input type=\"text\" name=\"window_height\"  id=\"window_height\" size=\"5\" value=\"{$window_height}\"  class=\"easyui-validatebox textbox\" >
               </td>";
              $html .=  "</tr>";
 
              $html .=  "<tr>";
               $html .=  "<td>Form Dimensions</td>";
               $html .=  "<td>
                Width <input type=\"text\" name=\"form_width\"  id=\"form_width\" size=\"5\"   value=\"{$form_width}\"  class=\"easyui-validatebox textbox\" >
                Height<input type=\"text\" name=\"form_height\"  id=\"form_height\" size=\"5\" value=\"{$form_height}\"  class=\"easyui-validatebox textbox\" >
               </td>";
              $html .=  "</tr>";
 
              $html .=  "<tr>";
               $html .=  "<td>Window</td>";
               $html .=  "<td>
                  <input type=\"checkbox\" id=\"chk_minimizable\" name=\"chk_minimizable\" value=\"1\" {$minimizable} ><label for=\"chk_minimizable\">Minimizable</label>
                  <input type=\"checkbox\" id=\"chk_maximizable\" name=\"chk_maximizable\"  value=\"1\" {$maximizable}><label for=\"chk_maximizable\">Maximizable</label>
                  <input type=\"checkbox\" id=\"chk_closable\" name=\"chk_closable\" value=\"1\"  {$closable}><label for=\"chk_closable\">Closable</label>
                  <input type=\"checkbox\" id=\"chk_resizable\" name=\"chk_resizable\" value=\"1\" {$resizable}><label for=\"chk_resizable\">Resizable</label>
                  <input type=\"checkbox\" id=\"chk_collapsible\" name=\"chk_collapsible\" value=\"1\" {$collapsible}><label for=\"chk_collapsible\">Collapsible</label>
               </td>";
              $html .=  "</tr>";
             
              $html .=  "<tr>";
               $html .=  "<td>ToolBar Buttons</td>";
               $html .=  "<td>
                  <input type=\"checkbox\" id=\"chk_button_add\" name=\"chk_button_add\" value=\"1\" {$button_add} ><label for=\"chk_button_add\">Add</label>
                  <input type=\"checkbox\" id=\"chk_button_edit\" name=\"chk_button_edit\"  value=\"1\" {$button_edit}><label for=\"chk_button_edit\">Edit</label>
                  <input type=\"checkbox\" id=\"chk_button_save\" name=\"chk_button_save\" value=\"1\" {$button_save}><label for=\"chk_button_save\">Save</label>
                  <input type=\"checkbox\" id=\"chk_button_delete\" name=\"chk_button_delete\" value=\"1\" {$button_delete}><label for=\"chk_button_delete\">Delete</label>
                  <input type=\"checkbox\" id=\"chk_button_import\" name=\"chk_button_import\" value=\"1\"  {$button_import}><label for=\"chk_button_import\">Import</label>
                  <input type=\"checkbox\" id=\"chk_button_export\" name=\"chk_button_export\" value=\"1\" {$button_export}><label for=\"chk_button_export\">Export</label>
               </td>";
              $html .=  "</tr>";
             
              $html .=  "<tr>";
               $html .=  "<td>Primary Key Col</td>";
               $html .=  "<td id=\"tdPkey\">{$table_keycol_select}</td>";
              $html .=  "</tr>";
 
              $html .=  "<tr>";
               $html .=  "<td id=\"tdColumns\" colspan=\"2\">&nbsp;</td>";
              $html .=  "</tr>";
 
              $html .=  "<tr>";
               $html .=  "<td>&nbsp;</td>";
               $html .=  "<td>";
                $html .=  "<button type=\"submit\" id=\"btnSave\" onclick=\"mm.save();\">Save Config File</button>";
                $html .=  "<button onclick=\"$('#dlg').dialog('close');\">Close</button>";
               $html .=  "</td>";
              $html .=  "</tr>";
 
              $html .=  "</table>";
              $html .=  "</form>";
//              $html .=  "</div>";

              $html .=  "<script>
              {$allow_config_cols_handler}
              
			  //\$('#table_datatbl').combo({ });
			 
			
			 
              </script>";
              
              return $html;
	}
	
	public function list_columns_properties(){
		global $db;
		
		 $table_datatbl = filter_input(INPUT_POST , 'table_datatbl');
		 $table_datasrc = filter_input(INPUT_POST , 'table_datasrc');
		 
		 $appid         = filter_input(INPUT_POST , 'appid');
		 $modid         = filter_input(INPUT_POST , 'modid');
		 $menuid        = filter_input(INPUT_POST , 'menuid');
		 $modpath       = filter_input(INPUT_POST , 'modpath');

        if(!isset($_POST['title'])){
		 $vars           = $db->GetOne("SELECT VARS FROM SYSMODCFG WHERE APPID='{$appid}' and MODID='{$modid}' and MNUID={$menuid} ");
        
		 $cfg_column_selectsrc = array() ;
        
         if(!empty($vars)){
          $modcfg             = unserialize( base64_decode( $vars ) );
          $table_datatbl      = valueof($modcfg, 'table_datatbl');
          $modpath            = valueof($modcfg, 'modpath');
         }
        }
        
		$modpath_arr  = explode('/', $modpath);
		$modpath_arrv = array_reverse($modpath_arr);
		$dir          = isset($modpath_arrv[1]) ? $modpath_arrv[1] : '';
		$modpath_prnt = str_replace($dir.'/', '', $modpath);
		$file_config  = ROOT . $modpath. '/cfg.php';
		
		if( isset($_SESSION['linked_to_foreign_cols']) ){
		  unset($_SESSION['linked_to_foreign_cols']);
		}
		
		if(file_exists($file_config) && is_readable($file_config)){
			require_once($file_config);
			
			if(isset($cfg['columns']) && count($cfg['columns'])){
				foreach ($cfg['columns'] as $cfg_column ){
					
					$dbcol      = valueof($cfg_column,'dbcol');
					$inputType  = valueof($cfg_column,'inputType');
					$selectsrc  = valueof($cfg_column,'selectsrc');
					
					$cfg_column_selectsrc[$dbcol] = array($inputType ,$selectsrc);
					
					if($inputType=='select' || $inputType=='combogrid'){
					
					 if(strstr($selectsrc,'|')){
					  $selectsrc_array           = explode('|', $selectsrc);
					  $table_col_datasrc          = valueof($selectsrc_array, 0);
	                  $linked_tbl_col_code        = valueof($selectsrc_array, 1);
	                  $linked_tbl_col_name        = valueof($selectsrc_array, 2);
	  
	                  $_SESSION['linked_to_foreign_cols'][$dbcol]['table']     = $table_col_datasrc; 
	                  $_SESSION['linked_to_foreign_cols'][$dbcol]['col_code']  = $linked_tbl_col_code; 
	                  $_SESSION['linked_to_foreign_cols'][$dbcol]['col_name']  = $linked_tbl_col_name; 
	                                
					 }
					}
				}
			}
		}
		
		$alignments            = array('left'=>'left','center'=>'center','right'=>'right');
		$visibles              = array('1'=>'Form & Grid','2'=>'Grid Only','3'=>'Form Only','4'=>'Not Visible');
		$validation_classes    = array('easyui-validatebox'=>'default');
		$requiredArray         = array(0=>'No',1=>'Yes');
		$sortableArray         = array(0=>'No',1=>'Yes');
		$validtypes            = array('0'=>'none','email'=>'email','url'=>'url','ln05'=>'ln[min=0,max=5]','ln10'=>'ln[min=0,max=10]','ln20'=>'ln[min=0,max=20]');
		$forminputs_select     = array('text'=>'text','numberbox'=>'Number Box','numberspinner'=>'Number Spinner','timespinner'=>'Time Spinner','monthspinner'=>'Month Spinner','email'=>'email','textarea'=>'textarea','select'=>'select','combogrid'=>'combogrid','yearselect'=>'Year Select','radio'=>'radio','checkbox'=>'checkbox','date'=>'date','password'=>'password');
		$forminputs_noselect   = array('text'=>'text','numberbox'=>'Number Box','numberspinner'=>'Number Spinner','timespinner'=>'Time Spinner','monthspinner'=>'Month Spinner','email'=>'email','textarea'=>'textarea','select'=>'select','combogrid'=>'combogrid','yearselect'=>'Year Select','radio'=>'radio','checkbox'=>'checkbox','date'=>'date','password'=>'password');
		$forminputs_numbers    = array('text'=>'text','numberbox'=>'Number Box','numberspinner'=>'Number Spinner','timespinner'=>'Time Spinner','monthspinner'=>'Month Spinner');
		$forminputs_checkbox   = array('checkbox'=>'checkbox');
		
		$filters    =  array(
             'none'=>'No Filter', 
             'contains'=>'Contains',
             'equal'=>'Equal',
             'notequal'=>'Not Equal',
             'beginwith'=>'Begin With',
             'endwith'=>'End With',
             'less'=>'Less',
             'lessorequal'=>'Less Or Equal',
             'greater'=>'Greater',
             'greaterorequal'=>'Greater Or Equal',
         );
         
		$colums    = $db->MetaColumns($table_datasrc);
		
		if(!$colums){
			
        }
        
		$keycolums = $db->MetaPrimaryKeys($table_datatbl);
		
		if(count($colums)>15){
        }

		$skipcols = array('ID','AUDITUSER','AUDITDATE','AUDITTIME','IPADDRESS','AUDITIP','IPADDR');
		$colums_array = array();
		
		if($colums){
		 foreach ($colums as $column => $properties){
               if(!in_array($column, $skipcols)){
				$colums_array[$column] = $column;
               }
	     }
	    }
			 
		if(count($colums_array)){
			echo "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
			
			echo "<tr>";
			echo "<td>Property</td>";
			 foreach ($colums_array as $column => $columnname){
				echo "<td><small><b>{$column}</b></small></td>";
			 }
			echo "</tr>";
			
			
			//**************************************************
			echo "<tr>";
			echo "<td><label>Title:</label></td>";
			
			foreach ($colums_array as $column => $properties){
				
				if(strstr($column,'CODE')){
				 $title_default = 'Code';
				}elseif(strstr($column,'NAME')){
				 $title_default = 'Name';
				}else{
				 $title_default = $column;
				}
				
				$title      = isset($modcfg['title'][$column]) ? $modcfg['title'][$column] : Camelize($title_default);
				
				echo "<td>".ui::form_input('text',"title[$column]",10, $title,'','','class="easyui-validatebox textbox"')."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			echo "<tr>";
			echo "<td><label>Description:</label></td>";
			
			foreach ($colums_array as $column => $properties){
				
				if(strstr($column,'CODE')){
				 $title_default = 'Code';
				}elseif(strstr($column,'NAME')){
				 $title_default = 'Name';
				}else{
				 $title_default = $column;
				}
				
				$descr   = isset($modcfg['descr'][$column]) ? $modcfg['descr'][$column] : Camelize($title_default);
				echo "<td>".ui::form_input('text',"descr[$column]",10, strtolower($descr),'','','class="easyui-validatebox textbox"')."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			//style=\"display:none\"
			echo "<tr >";
			echo "<td><label>Alias:</label></td>";
			
			foreach ($colums_array as $column => $properties){
				$alias   = isset($modcfg['alias'][$column]) ? $modcfg['alias'][$column] : generateRandomString("5");
				echo "<td>".ui::form_input('text',"alias[$column]",10, strtolower($alias),'','','class="easyui-validatebox textbox"')."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			echo "<tr>";
			echo "<td>Width</td>";
			
			foreach ($colums_array as $column => $properties){
				$width      = isset($modcfg['width'][$column]) ? $modcfg['width'][$column] : 100;
				echo "<td>".ui::form_input('text',"width[$column]",5,$width,'','','class="easyui-validatebox textbox"')."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			echo "<tr>";
			echo "<td>Visible</td>";
			
			foreach ($colums_array as $column => $properties){
				$visible      = isset($modcfg['visible'][$column]) ? $modcfg['visible'][$column] : '1';
				echo "<td>".ui::form_select_fromArray("visible[$column]",$visibles,$visible,'','','class="easyui-validatebox textbox"')."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			echo "<tr>";
			echo "<td>Align</td>";
			
			foreach ($colums_array as $column => $properties){
				$colalign      = isset($modcfg['colalign'][$column]) ? $modcfg['colalign'][$column] : 'left';
				echo "<td>".ui::form_select_fromArray("colalign[$column]",$alignments,$colalign,'','','class="easyui-validatebox textbox"')."</td>";
			 }
			 
			echo "</tr>";
			
			
			//**************************************************
			echo "<tr>";
			echo "<td>Required</td>";
			
			foreach ($colums_array as $column => $properties){
				$required      = isset($modcfg['required'][$column]) ? $modcfg['required'][$column] : '1';
				echo "<td>".ui::form_select_fromArray("required[$column]",$requiredArray,$required,'class="easyui-validatebox textbox"','',80)."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			echo "<tr>";
			echo "<td>Sortable</td>";
			
			foreach ($colums_array as $column => $properties){
				$sortable      = isset($modcfg['sortable'][$column]) ? $modcfg['sortable'][$column] : 0;
				echo "<td>".ui::form_select_fromArray("sortable[$column]",$sortableArray,$sortable,'','','class="easyui-validatebox textbox"')."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			echo "<tr>";
			echo "<td>Validation</td>";
			
			foreach ($colums_array as $column => $properties){
				$validation_class      = isset($modcfg['validation_class'][$column]) ? $modcfg['validation_class'][$column] : 'easyui-validatebox';
				echo "<td>".ui::form_select_fromArray("validation_class[$column]",$validation_classes,$validation_class,'class="easyui-validatebox textbox"','',140)."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
			echo "<tr>";
			echo "<td>Validation Type</td>";
			
			foreach ($colums_array as $column => $properties){
				$validtype      = isset($modcfg['validtype'][$column]) ? $modcfg['validtype'][$column] : '0';
				echo "<td>".ui::form_select_fromArray("validtype[$column]",$validtypes,$validtype,'class="easyui-validatebox textbox"','',140)."</td>";
			 }
			 
			echo "</tr>";
			
			//**************************************************
		   if(count($colums)>2){	
			echo "<tr>";
			echo "<td>Form Input</td>";
			
			foreach ($colums_array as $column => $properties){
				
				if(strstr($column,'CODE') || strstr($column,'ACTIVE')){
					$forminputs = $forminputs_select;
					$defaultType = 'text';
				}elseif(strstr($column,'DATE')){
					$forminputs  = $forminputs_select;
					$defaultType = 'date';
				}elseif(strstr($column,'AMT') || strstr($column,'AMOUNT')){
					$forminputs  = $forminputs_numbers;
					$defaultType = 'numberbox';
				}elseif(substr($column,0,2)=='IS'){
					$forminputs = $forminputs_checkbox;
					$defaultType = 'text';
				}elseif(strtolower($column=='password')){
					$forminputs = $forminputs_select;
					$defaultType = 'password';
				}else{
					$forminputs = $forminputs_noselect;
					$defaultType = 'text';
				}
				
				//default
				//date
                $forminput_type = isset($cfg_column_selectsrc[$column][0]) ? $cfg_column_selectsrc[$column][0] : $defaultType;
				
				echo "<td>".ui::form_select_fromArray("forminput_{$column}",$forminputs,$forminput_type,"onchange=\"mm.forminput_type_check('forminput_{$column}','{$column}');\" class=\"easyui-validatebox textbox\" ",'',140)."</td>";
			 }
			 
			echo "</tr>";
		   }
			//**************************************************
			echo "</table>";
		}
		
		
	}
	
	public function pre_save_select_source(){
		
	  $table_col_datasrc          = filter_input(INPUT_POST , 'table_col_datasrc');
	  $col_linked_to_foreign_col  = filter_input(INPUT_POST , 'col_linked_to_foreign_col');
	  $linked_tbl_col_code        = filter_input(INPUT_POST , 'linked_tbl_col_code');
	  $linked_tbl_col_name        = filter_input(INPUT_POST , 'linked_tbl_col_name');
	  
	  $_SESSION['linked_to_foreign_cols'][$col_linked_to_foreign_col]['table']     = $table_col_datasrc; 
	  $_SESSION['linked_to_foreign_cols'][$col_linked_to_foreign_col]['col_code']  = $linked_tbl_col_code; 
	  $_SESSION['linked_to_foreign_cols'][$col_linked_to_foreign_col]['col_name']  = $linked_tbl_col_name; 
	  
	}
	
    private function get_saved_opts(){
 	 if (!empty($this->record)) {
 		if(isset($this->record->fieldopts)){
 		 $fieldopts = unserialize($this->record->fieldopts);
 		 foreach ($fieldopts as $field=>$opt){
 		  	$this->record->$field = $opt;
         }
       }
 	 }
    }
   
	public function save(){
		global $db;
		//print_pre($_POST);
		
	   $vars      = '';
  
       if(sizeof($_POST)>0){
  	   $vars = base64_encode( serialize($_POST) );
       }
     
        ini_set('error_reporting',0);
        ini_set('display_errors',0);
        
		$modpath_post = filter_input(INPUT_POST , 'modpath');
		$modpath      = ROOT . $modpath_post . '/';
		$modpath_arr  = explode('/', $modpath);
		$modpath_arrv = array_reverse($modpath_arr);
		$dir          = isset($modpath_arrv[1]) ? $modpath_arrv[1] : '';
		$modpath_prnt = str_replace($dir.'/', '', $modpath);
		$file_config  = $modpath. 'cfg.php';
		$file_index   = $modpath. 'index.php';
		$file_class   = $modpath. $dir .'.php';

		$appid        = filter_input(INPUT_POST , 'appid');// st
		$modid        = filter_input(INPUT_POST , 'modid');// GS
		$menuname     = filter_input(INPUT_POST , 'menuname');// Institution
		$menuid       = filter_input(INPUT_POST , 'menuid');// 11
		$parentid     = filter_input(INPUT_POST , 'parentid');// 1
		$modtype      = filter_input(INPUT_POST , 'modtype'); 

		$chk_button_add = isset($_POST['chk_button_add']) && $_POST['chk_button_add']==1 ? 1 : 0;
		$chk_button_edit = isset($_POST['chk_button_edit']) && $_POST['chk_button_edit']==1 ? 1 : 0;
		$chk_button_delete = isset($_POST['chk_button_delete']) && $_POST['chk_button_delete']==1 ? 1 : 0;
		$chk_button_import = isset($_POST['chk_button_import']) && $_POST['chk_button_import']==1 ? 1 : 0;
		$chk_button_export = isset($_POST['chk_button_export']) && $_POST['chk_button_export']==1 ? 1 : 0;

		$chk_minimizable = isset($_POST['chk_minimizable']) && $_POST['chk_minimizable']==1 ? 1 : 0;
		$chk_maximizable = isset($_POST['chk_maximizable']) && $_POST['chk_maximizable']==1 ? 1 : 0;
		$chk_collapsible = isset($_POST['chk_collapsible']) && $_POST['chk_collapsible']==1 ? 1 : 0;
		$chk_closable = isset($_POST['chk_closable']) && $_POST['chk_closable']==1 ? 1 : 0;
		$chk_resizable = isset($_POST['chk_resizable']) && $_POST['chk_resizable']==1 ? 1 : 0;

		$attributes  = array();

		$attributes['win']  = filter_input(INPUT_POST , 'window_width', FILTER_VALIDATE_INT);
		$attributes['hei']  = filter_input(INPUT_POST , 'window_height', FILTER_VALIDATE_INT);
		$attributes['min']  = $chk_minimizable;
		$attributes['max']  = $chk_maximizable;
		$attributes['col']  = $chk_collapsible;
		$attributes['clo']  = $chk_closable;
		$attributes['res']  = $chk_resizable;
		$attributes['bta']  = $chk_button_add;
		$attributes['bte']  = $chk_button_edit;
		$attributes['btd']  = $chk_button_delete;
		$attributes['bti']  = $chk_button_import;
		$attributes['btx']  = $chk_button_export;

		$apptitle     = Camelize($menuname);
		$appname      = strtolower(centerTrim($menuname));
		$appname_cls  = str_replace(' ','',$appname);

		$datasrc      = filter_input(INPUT_POST , 'table_datasrc');
		$datatbl      = filter_input(INPUT_POST , 'table_datatbl');

		$form_width   = filter_input(INPUT_POST , 'form_width');
		$form_height  = filter_input(INPUT_POST , 'form_height');

		$window_width   = filter_input(INPUT_POST , 'window_width');
		$window_height  = filter_input(INPUT_POST , 'window_height');

		$primarykey   = filter_input(INPUT_POST , 'primary_key_col');

		if(empty($datasrc) && !empty($datatbl)){
			$datasrc = $datatbl;
		}elseif(empty($datatbl) && !empty($datasrc)){
			$datatbl = $datasrc;
		}
		
//--------------------------------------------------------------------------------------------------------
     
      $modcfg = new ADODB_Active_Record('SYSMODCFG', array('APPID','MODID','MNUID'));
      $modcfg->Load("APPID='{$appid}' and MODID='{$modid}' and MNUID={$menuid} ");
      
      if(empty($modcfg->_original)){
      $modcfg->appid     =  $appid;
      $modcfg->modid     =  $modid;
      $modcfg->mnuid     =  $menuid;
      }
      
      $modcfg->vars      =  $vars;
      $modcfg->auditdate =  date('Y-m-d');
      $modcfg->audittime =  time();
      
      if($modcfg->Save()){
		  
		//make dir
		if(!is_dir($modpath)){
			
			if(!mkdir($modpath, 0777, true)){
				return json_response(0,'unable to create '.$modpath);
			}
			
            chmod($modpath_prnt, 0777);
            chmod($modpath, 0777);
		}
		
		  //update menu attributes
		  $attribs    =  json_encode($attributes);
	      $db->Execute("update SYSMODMNU set ATTRIBS='{$attribs}' WHERE APPID='{$appid}' and MODID='{$modid}' and MNUID={$menuid} ");
	  }

    //no over-wrtite in modtype is custom & module files already exists
          switch($modtype){
			 case 'dga':
			  $allow_reconfig_mod = true;
			 break;
			 case 'dgc':
			  
			  if(!is_readable($file_config) || !is_readable($file_index) || !is_readable($file_class)){
			   $allow_reconfig_mod = true;  
			  }else{
				$allow_reconfig_mod = false;  
			  }
			  
			 break;
			 case 'cmd':
			 
			  if(!is_readable($file_config) || !is_readable($file_index) || !is_readable($file_class)){
			   $allow_reconfig_mod = true;  
			  }else{
				$allow_reconfig_mod = false;  
			  }
			  
			 break;
		 }
		 
   if(!$allow_reconfig_mod){
		 return  json_response(1,"{$appname} configuration saved");
   }else{//over-write
		
//-------------make index.php code
$date = date('Y-m-d H:i:s');
		
$comment_index = "/**
* auto created index file for {$modpath_post}
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since {$date}
*/";
		
 $comment_cfg = "/**
* auto created config file for {$modpath_post}
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since {$date}
*/";
		
 $comment_classs = "/**
* auto created config file for {$modpath_post}
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since {$date}
*/";
		
		
$index_php_code = <<<PHPCODE
<?php

{$comment_index}

require_once('cfg.php');
//require_once(BASEPATH.'init.php');

\$class     = CLASSFILE;
\$classfile = CLASSFILE.'.php';

require_once("{\$classfile}");	

 if(!class_exists(\$class)){
 	die('class--' .\$classfile. 'not found');
 }
 
 \$_class = new \$class();
 \$grid   = new grid();
 
 /*
 \$module = array();
 \$module['url']    =  DIR;
 \$module['class']  =  \$class;
 \$module_packed    =  packvars(\$module);
 */
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<?php
 \$grid->draw_page_header( );
?>
<body>
<?php
 \$grid->draw_grid( \$module_packed );
 \$grid->draw_form_simple(  );
// \$_class->form();
?>
</body>
</html>
PHPCODE;
		
	
//*******************************************cfg.php**********************************************************
$cfg_php_code = <<<PHPCODE
<?php
{$comment_cfg}

\$scriptname  = @end(explode('/',\$_SERVER['PHP_SELF']));
\$scriptpath  = str_replace(\$scriptname,'',\$_SERVER['PHP_SELF']);
\$root        = \$_SERVER['DOCUMENT_ROOT'].\$scriptpath;
\$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\\' : '/'; 

define('BASEPATH',\$root);
define('DIR',     dirname( __FILE__ ) . \$delimeter); 
    
\$break   = explode(\$delimeter, DIR);

define('CLASSFILE',  \$break[count(\$break) - 2] ); 

 \$cfg                  = array();
 \$cfg['apptitle']      = '{$apptitle}';//user-readable formart
 \$cfg['appname']       = '{$appname_cls}';//lower cased one word
 \$cfg['datasrc']       = '{$datasrc}';//where to get data
 \$cfg['datatbl']       = '{$datatbl}';//base data src [for updates & deletes]
 \$cfg['form_width']    = {$form_width};
 \$cfg['form_height']   = {$form_height};
 \$cfg['window_width']    = {$window_width};
 \$cfg['window_height']   = {$window_height};
 
 \$cfg['pkcol']         = '{$primarykey}';//the primary key
 
 \$cfg['tblbns']['chk_button_add']    = {$chk_button_add};
 \$cfg['tblbns']['chk_button_edit']   = {$chk_button_edit};
 \$cfg['tblbns']['chk_button_delete'] = {$chk_button_delete};
 \$cfg['tblbns']['chk_button_import'] = {$chk_button_import};
 \$cfg['tblbns']['chk_button_export'] = {$chk_button_export};
 

PHPCODE;
            
if(isset($_POST['title'])){
$combogrid_array_cols =  '';
foreach ($_POST['title'] as $dbcol => $title){	
	
$alias             = isset($_POST['alias'][$dbcol]) ? $_POST['alias'][$dbcol] : 'NAN';
$title             = isset($_POST['title'][$dbcol]) ? $_POST['title'][$dbcol] : 'NAN';
$width             = isset($_POST['width'][$dbcol]) ? $_POST['width'][$dbcol] : 'NAN';
$visible           = isset($_POST['visible'][$dbcol]) ? $_POST['visible'][$dbcol] : '1';
$import            = isset($_POST['import'][$dbcol]) ? $_POST['import'][$dbcol] : '1';
$colalign          = isset($_POST['colalign'][$dbcol]) ? $_POST['colalign'][$dbcol] : 'left';
$validation_class  = isset($_POST['validation_class'][$dbcol]) ? $_POST['validation_class'][$dbcol] : 'easyui-validatebox';
$sortable          = isset($_POST['sortable'][$dbcol]) && $_POST['sortable'][$dbcol]==1 ? 'true' : 'false';
$required          = isset($_POST['required'][$dbcol]) ? $_POST['required'][$dbcol] : '0';
$validType_raw     = isset($_POST['validtype'][$dbcol]) ? $_POST['validtype'][$dbcol] : '';
$forminput_type    = isset($_POST["forminput_{$dbcol}"]) ? $_POST["forminput_{$dbcol}"] : 'text';

//if inputype = select , save data src table & cols
$selectsrc =  '';

if($forminput_type=='select'  || $forminput_type=='combogrid' ){
      $table_col_datasrc    = isset($_SESSION['linked_to_foreign_cols'][$dbcol]['table']) ? $_SESSION['linked_to_foreign_cols'][$dbcol]['table'] : null;
	  $linked_tbl_col_code  = isset($_SESSION['linked_to_foreign_cols'][$dbcol]['col_code']) ? $_SESSION['linked_to_foreign_cols'][$dbcol]['col_code'] :  null; 
	  $linked_tbl_col_name  = isset($_SESSION['linked_to_foreign_cols'][$dbcol]['col_name']) ? $_SESSION['linked_to_foreign_cols'][$dbcol]['col_name'] : null; 
	 
	 $selectsrc = "{$table_col_datasrc}|{$linked_tbl_col_code}|{$linked_tbl_col_name}|{$linked_tbl_col_code}";
	 
	 $combogrid_array_cols .= <<<CCG
	 
\$combogrid_array['{$alias}']['columns']['{$linked_tbl_col_code}']  = array( 'field'=>'{$linked_tbl_col_code}', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
\$combogrid_array['{$alias}']['columns']['{$linked_tbl_col_name}']  = array( 'field'=>'{$linked_tbl_col_name}', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
\$combogrid_array['{$alias}']['source'] ='{$table_col_datasrc}';

CCG;
}

switch ($validType_raw){
	case 'ln05':
	 $validType='length[min=0,max=5]';
	break;
	case 'ln10':
	 $validType='length[min=0,max=10]';
	break;
	case 'ln20':
	 $validType='length[min=0,max=20]';
	break;
	case '0':
	 $validType="''";
	break;
	default:
	 $validType = "'{$validType_raw}'";
	break;
}


$cfg_php_code .= <<<PHPCODE

\$cfg['columns']['{$alias}'] = array(
                      'dbcol'=>'{$dbcol}',
                      'title'=>'{$title}',
                      'width'=>{$width},
                      'sortable'=> {$sortable},
                      'import'=>{$import},
                      'visible'=>{$visible},
                      'colalign'=>'{$colalign}',
                      'validation_class'=> '{$validation_class}',
                      'required'=> {$required},
                      'validType'=> {$validType},
                      'inputType'=> '{$forminput_type}',
                      'selectsrc'=> '{$selectsrc}',
                      );
                      



                      
PHPCODE;
}

$cfg_php_code .= <<<CG
\$combogrid_array   = array();
{$combogrid_array_cols}  
CG;
            
}

//class file code
$class_php_code = <<<PHPCODE
<?php

{$comment_classs}

 final class {$dir} {
 private \$id;
 private \$datasrc;
 private \$primary_key;
	
 public function __construct(){
  global \$cfg;
		
     \$this->id           = filter_input(INPUT_POST , 'id');
     \$this->datasrc      = valueof(\$cfg,'datasrc');
     \$this->primary_key  = valueof(\$cfg,'pkcol');
 }
	
 public function save(){
  global \$db,\$cfg;
  
   \$grid = new grid();
   return \$grid->grid_save_row_simple();
 }
	
 public function remove(){
  global \$db, \$cfg;
  
   \$grid = new grid();
  return \$grid->grid_remove_row();
 }
	
 	
 public function export(){
  global \$db, \$cfg;
  
   \$grid = new grid();
  return \$grid->export();
 }
 	
 public function import(){
  global \$db, \$cfg;
  
   \$grid = new grid();
  return \$grid->import();
 }
 
}

PHPCODE;
//*******************************************save files**********************************************************

        //index.php
		$index_php_code_len = strlen($index_php_code);
		
			if(!$handle = fopen($file_index, 'w+')){
				return json_response(0,"could not open {$file_index} for write");
			}
			
			if(!$write = fwrite($handle,$index_php_code, $index_php_code_len)){
				return json_response(0,"could save {$file_index}");
			}
			
		  fclose($handle);	
			
		 //cfg.php	
		 $cfg_php_code_len = strlen($cfg_php_code);
		
			if(!$handle = fopen($file_config, 'w+')){
				return json_response(0,"could not open {$file_config} for write");
			}
			
			if(!$write = fwrite($handle,$cfg_php_code, $cfg_php_code_len)){
				return json_response(0,"could save {$file_config}");
			}
			
		  fclose($handle);
			
		 //$appname.php	
		 $class_php_code_len = strlen($class_php_code);
		
			if(!$handle = fopen($file_class, 'w+')){
				return json_response(0,"could not open {$file_config} for write");
			}
			
			if(!$write = fwrite($handle,$class_php_code, $class_php_code_len)){
				return json_response(0,"could save {$file_class}");
			}
			
			fclose($handle);
			
		return  json_response(1,"{$appname} created");
		
   }	
  }
	
    
    public function save_db_config(){
 	$dbtype = filter_input(INPUT_POST , 'dbtype');
 	$dbhost = filter_input(INPUT_POST , 'dbhost');
 	$dbuser = filter_input(INPUT_POST , 'dbuser');
 	$dbpass = filter_input(INPUT_POST , 'dbpass');
 	$dbname = filter_input(INPUT_POST , 'dbname');
 	$dbrpass = filter_input(INPUT_POST , 'dbrpass');
 	
 	if(!empty($dbrpass)){
 		@$db_admin = ADONewConnection($dbtype);
 		
        if(@$db_admin->Connect($dbhost,'root',$dbrpass,'mysql')){
        	if(!@$db_admin->Execute("create database {$dbname}")){ return json_response(0,$db_admin->ErrorMsg()); } 
        	if(!@$db_admin->Execute("create user {$dbuser} identified by '{$dbpass}'")){ return json_response(0,$db_admin->ErrorMsg()); }
        	if(!@$db_admin->Execute("GRANT ALL PRIVILEGES ON {$dbname}.* TO '{$dbuser}'@'{$dbhost}' IDENTIFIED BY '{$dbpass}'")){ return json_response(0,$db_admin->ErrorMsg()); }
        	if(!@$db_admin->Execute("FLUSH PRIVILEGES;")){ return json_response(0,$db_admin->ErrorMsg()); }
        }else{
        	return json_response(0,"Access denied for user 'root'@'{$dbhost}' ");
        }
 	}
    /**
     * test connection
     */
 	if(!empty($dbpass)){
 		$db_test = ADONewConnection($dbtype);
        if(!@$db_test->Connect($dbhost, $dbuser, $dbpass, $dbname)){
        	return json_response(0,"Access denied for user '{$dbuser}'@'{$dbhost}' ");
        }
 	}
	
	
 $date = date('Y-m-d H:i:s');
 
 $comments = "/**
* auto created config file for db.php
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since {$date}
*/";

//class file code
$db_php_code = <<<PHPCODE
<?php

{$comments}

define('ADODB_ERROR_LOG_TYPE',3);
define('ADODB_ERROR_LOG_DEST',ROOT.'logs/db_errors.log');

require_once(ROOT.'lib/adodb/adodb.inc.php');
require_once(ROOT.'lib/adodb/adodb-active-record.inc.php');
//require_once(ROOT.'lib/adodb/adodb-errorhandler.inc.php');
require_once(ROOT .'lib/adodb/adodb-active-record.inc.php');

\$db_type = '{$dbtype}';
\$db_host = '{$dbhost}';
\$db_user = '{$dbuser}';
\$db_pass = '{$dbpass}';
\$db_name = '{$dbname}';

\$db = ADONewConnection(\$db_type);
if(! @\$db->Connect(\$db_host,\$db_user,\$db_pass,\$db_name)){
 
 if(isset(\$_SERVER['HTTP_X_REQUESTED_WITH']) && \$_SERVER['HTTP_X_REQUESTED_WITH']=='XMLHttpRequest'){	
  echo json_response(0,"Could Not Connect to {\$db_host}/{\$db_name} ");
 }else{
  echo("Could Not Connect to {\$db_host}/{\$db_name} ");
 }
 
}else{
\$db->SetFetchMode(ADODB_FETCH_ASSOC);

ADODB_Active_Record::SetDatabaseAdapter( \$db );

\$ADODB_ASSOC_CASE = 0;
\$ADODB_CACHE_DIR = ROOT . 'cache';
\$ADODB_ACTIVE_CACHESECS = 30;

//\$db->debug=1;
}

PHPCODE;

//db.php	
		 $file_config     = ROOT.'config/db.php';
		 $db_php_code_len = strlen($db_php_code);
		
			if(!$handle = @fopen($file_config, 'w+')){
				return json_response(0,"could not open {$file_config} for write");
			}
			
			if(!$write = @fwrite($handle,$db_php_code, $db_php_code_len)){
				return json_response(0,"could save {$file_config}");
			}
			
		  fclose($handle);
			
		return  json_response(1,"db config created");  
 }
	
  public function schema_load(){
      global $db;
     $db->debug=0;
     
    require( ROOT  . "lib/adodb/adodb-xmlschema03.inc.php");
    
    $dd_xml_files = isset($_POST['load_xml']) ? $_POST['load_xml'] : array();
    $dd_views     = isset($_POST['load_view']) ? $_POST['load_view'] : array();
    
   if(count($dd_xml_files)>=1){
 	foreach ($dd_xml_files as $dd_xml_file => $dd_xml_file_path ){
 		
		$schema = new adoSchema( $db );
		$schema->debug = false;
		$schema->continueOnError = true;
		$schema->ExecuteInline( true );
		$schema->SetUpgradeMethod('BEST');
		
		$sql = $schema->ParseSchema( $dd_xml_file_path );
		$result = $schema->ExecuteSchema();
		
		//print $schema->PrintSQL( 'HTML' );
	    
 	}
   }
 
   $db->debug=0;
    if(count($dd_views)>=1){
 	  foreach ($dd_views as $dd_view_file => $dd_view_file_path ){
 		 require_once($dd_view_file_path);
 	  }
     }

   }

  public function schema_data(){
      global $db;
     $db->debug=0;
         
    require( ROOT  . "lib/adodb/adodb-xmlschema03.inc.php");
    
    $load_data_files     = isset($_POST['load_data']) ? $_POST['load_data'] : array();
    $db->debug=1;
    
    $count =0;
    
   if(count($load_data_files)>=1){
 	foreach ($load_data_files as $load_data_file_path ){
       
       $load_data_file_path_info = pathinfo($load_data_file_path);
       $table      = valueof($load_data_file_path_info,'filename');
       $extension  = valueof($load_data_file_path_info,'extension');
       
       
      if(strtolower($extension) == 'xml'){
      	if($count%2 > 0) sleep(.7);
      	
      	 $test = true;
      	
      	 if(isset($test)) {
      	  echo "importing data for table <b>{$table}</b>...<br>";	
      	
      	  $schema = new adoSchema( $db );
          $schema->debug = true;

      	  $schemaFile  = $load_data_file_path;
      	  $sql         = $schema->ParseSchemaFile($schemaFile);
      	  
      	  foreach ($sql as $sqlstatement){
      	  	$db->Execute($sqlstatement);
      	  }
        
      	 }
      	}
        ++$count;   
 	 }
    }
   }
   
  public function import_data_mysql(){
    	global $db;
    	
    	$mysql_dump_path_posted  = filter_input(INPUT_POST , 'mysql_dump_path');
    	$mysql_dump_path         = str_replace('\\','\\\\',$mysql_dump_path_posted);
    	
		if( (!is_file($mysql_dump_path))){
		return  "
		<script>
		 parent.$.messager.alert('Error','{$mysql_dump_path} is not a valid file','error');
		</script>
				";	
		}
		
		
		if( (is_file($mysql_dump_path) && !is_readable($mysql_dump_path))){
		return "
		<script>
		 parent.$.messager.alert('Error','{$mysql_dump_path} is not readable or does not exist','error');
		</script>
				";	
		}
		
		$pathinfo = pathinfo($mysql_dump_path);
		$basename = isset($pathinfo['basename']) ? $pathinfo['basename'] : '';
		
		if(isset($pathinfo['extension']) && $pathinfo['extension']!=='sql'){
		return "
		<script>
		 parent.$.messager.alert('Error','{$mysql_dump_path} is not a valid sql file','error');
		</script>
				";		
		}
		
		$command = "mysql -u {$db->user} -p{$db->password} {$db->database} -h {$db->host} --verbose < {$mysql_dump_path} ";

        //exec($command,$output=array(),$worked);
        
        @system($command);
        
        switch($worked){
        case 0:
         echo '<b>' .$basename .'</b> imported successfully to db <b>' .$db->database .'</b>';
        break;
        case 1:
         echo 'There was an error during import.<table><tr><td>MySQL Database Name:</td><td><b>' .$db->database.'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$db->user.'</b></td></tr><tr><td>MySQL Password:</td><td>*****</td></tr><tr><td>MySQL Host Name:</td><td><b>' .$db->host .'</b></td></tr><tr><td>MySQL Import Filename:</td><td><b>' .$mysql_dump_path .'</b></td></tr></table>';
        break;
       }
		
    }
    
  public function list_tables_for_tools(){
    global $db;
    
     $tables_array = array();
     $tables = $db->MetaTables('TABLES');
     if($tables && sizeof($tables)>0){
 	  foreach ($tables as $table){
 		$tables_array[$table]  = $table;
 	  }
     }
     
	 return ui::form_select_fromArray('tools_table',$tables_array,'',' class="easyui-combobox "',false, 200,1000);
	 
  }
	
  public function makecode(){
	  global $db;
	  
		$tool_for      = filter_input(INPUT_POST , 'tool_for', FILTER_SANITIZE_STRING);
		$tools_table   = filter_input(INPUT_POST , 'tools_table', FILTER_SANITIZE_STRING);
		$tools_action  = filter_input(INPUT_POST , 'tools_action', FILTER_SANITIZE_STRING);
		$tool_for      = str_replace(' ','_', $tool_for);
		$tool_for_sm   = strtolower($tool_for);

		$colums        = $db->MetaColumns($tools_table);
		$keycolums     = $db->MetaPrimaryKeys($tools_table);
		$skipcols      = array('AUDITUSER','AUDITDATE','AUDITTIME','IPADDRESS','AUDITIP','IPADDR');
		$colums_array  = array();
		$col_code  = '';
		$col_name  = '';

		if(($colums)){
			foreach ($colums as $column => $properties){
                if(!in_array($column, $skipcols)){
					
				  $colums_array[$column] = $column;
				  
				 if( (strstr($column,'CODE') || strstr($column,'code')) && empty($col_code) ){
				 	$col_code = $column;
				 }elseif( (strstr($column,'NAME') || strstr($column,'name'))  && empty($col_name) ){
				 	$col_name = $column;
				 }
				 
                }
			}
		}
		
		switch($tools_action){
		 case 'dropdown':
		 $code =  ' 
 public static function '.$tool_for_sm.' ($name,$selected_code=\'\',$options=\'\',$forcedb = false){
   return self::form_select ($name,\''.$tools_table.'\',\''.$col_code.'\',\''.$col_name.'\',\'\',$selected_code,$options,false,\'\', $forcedb );
 }
';
		 break;
		 case 'combobox':
		 $code =  ' 
 echo ui::combobox(\''.$tool_for_sm.'\',\''.$tools_table.'\',\''.$col_code.'\',\''.$col_name.'\',\'console.log\');
';
		 break;
		 case 'combogrid':
		 //do
		 $code =   '
$combogrid_array[\''.$tool_for_sm.'\'][\'columns\'][\''.$col_code.'\']  = array( \'field\'=>\'code\', \'title\'=>\'Code\', \'width\'=> 80, \'isIdField\' => true );
$combogrid_array[\''.$tool_for_sm.'\'][\'columns\'][\''.$col_name.'\']  = array( \'field\'=>\'name\', \'title\'=>\'Name\', \'width\'=> 150, \'isTextField\'=>true);
$combogrid_array[\''.$tool_for_sm.'\'][\'source\'] =\''.$tools_table.'\'; 

/*
<?php
  echo ui::ComboGrid($combogrid_array,\''.$tool_for_sm.'\',"{$cfg[\'apptitle\']}.onchange'.$tool_for_sm.'");
?>
*/
  ';
		 break;
		 case 'customgrid':
		 //do
$code = '
$grid_'.$tool_for_sm.' = new gridcustom(\''.$tool_for_sm.'\');
	
	$grid_'.$tool_for_sm.'->title       = \''.$tool_for_sm.'\';
	$grid_'.$tool_for_sm.'->grid_width  = 510;
	$grid_'.$tool_for_sm.'->grid_height = 300;
	$grid_'.$tool_for_sm.'->form_width  = 370;
	$grid_'.$tool_for_sm.'->form_height = 260;
	';
	
	if(sizeof($colums_array)>0){
	 foreach($colums_array as $col_name=>$col_title){
	  if($col_name != 'ID'){
	 $col_name_sm = strtolower($col_name);
	 $col_name_cm = ucwords($col_name_sm);
	 
	 if( strstr($col_name_sm,'code') ){
	  $visible = 2;
     }elseif( strstr($col_name_sm,'name') ){
	  $visible = 3;
     }else{
	  $visible = 1;
     }
	 
	 $code .= '
	$grid_'.$tool_for_sm.'->columns[\''.$col_name_sm.'\'] = array(
					  \'dbcol\'=>\''.$col_name.'\',
					  \'title\'=>\''.$col_name_cm.'\',
					  \'width\'=>100,
					  \'sortable\'=> true,
					  \'import\'=>1,
					  \'visible\'=>'.$visible.',
					  \'colalign\'=>\'left\',
					  \'validation_class\'=> \'easyui-validatebox\',
					  \'required\'=> 1,
					  \'inputType\'=> \'text\',
					  );
					  ';
	 }
	}
   }
					  
					  
$code .= '	
 //$grid_'.$tool_for_sm.'->render_grid(); 
 //$grid_'.$tool_for_sm.'->render_form(); 
';
		 break;
		}
		
		return '<textarea name="table_code" id="table_code" cols="80" rows="20" >'.$code .'</textarea>';
		
  }
	
  public function mkv(){
	  global $db;
	  
	    $alphas     = range('a', 'z');
		$alphas_array =  array();
		$alphas_used =  array();
 
		foreach ($alphas as $index=>$alpha){
			$index_next = $index+1;
			$alphas_array[$index_next] = $alpha;
		}
		
		$alphas_indexes = range(1, count($alphas_array) );
		
		$mkv_tables      = filter_input(INPUT_POST , 'mkv_tables', FILTER_SANITIZE_STRING);
		$mkv_name        = filter_input(INPUT_POST , 'mkv_name', FILTER_SANITIZE_STRING);
		$mkv_name        = str_replace(' ','',$mkv_name);
		$mkv_name        = strtoupper($mkv_name);
		$mkv_tables      = centerTrim($mkv_tables);
		$mkv_tables_arr  = explode(',',$mkv_tables);
		
		print_pre($mkv_tables_arr);

        $src_table       = valueof($mkv_tables_arr, 0);
        
		$src_table_cols  = $db->MetaColumns($src_table);
		
		if(!$src_table_cols){
		 return $db->ErrorMsg();
		}
		
		$src_table_alias_index = rand(1, count($alphas_array)  );
		$src_table_alias       = valueof( $alphas_array, $src_table_alias_index  );
		
		$alphas_used[$src_table_alias_index]  = $src_table_alias_index;
		
		$selects    =  array();
		$joinables  =  array();
		$joins      =  array();
		$skipcols   =  array('AUDITUSER','AUDITDATE','AUDITTIME','IPADDRESS','AUDITIP','IPADDR');
		
		foreach ($src_table_cols as $column => $column_properties){
		 if(!in_array($column, $skipcols)){
		  $selects[$column] =  "{$src_table_alias}.{$column}";
		  
		  //joinable?
		  if(strstr($column,'CODE') || strstr($column,'code') || substr($column,-2,2)=='NO' && !array_key_exists($column,$joinables) ){
			$joinables[$column] = 1;
		  }
		  
		 }
		}
		
		if(sizeof($mkv_tables_arr)>1){
		 foreach($mkv_tables_arr as $index => $mkv_table){
		   if($index>0){
			   $mkv_table      = trim($mkv_table);
			   $mkv_tables_cols  = $db->MetaColumns($mkv_table);
			   
			   //try get unsed alias index 5 times
			    $mkv_table_alias_index  = rand(1, count($alphas_array)  );
		        
		        if(array_key_exists($mkv_table_alias_index,$alphas_used)){
				 $mkv_table_alias_index  = rand(1, count($alphas_array)  );	
				}
		        
		        if(array_key_exists($mkv_table_alias_index,$alphas_used)){
				 $mkv_table_alias_index  = rand(1, count($alphas_array)  );	
				}
		        
		        if(array_key_exists($mkv_table_alias_index,$alphas_used)){
				 $mkv_table_alias_index  = rand(1, count($alphas_array)  );	
				}
		        
		        if(array_key_exists($mkv_table_alias_index,$alphas_used)){
				 $mkv_table_alias_index  = rand(1, count($alphas_array)  );	
				}
				
		        if(array_key_exists($mkv_table_alias_index,$alphas_used)){
				 $mkv_table_alias_index  = rand(1, count($alphas_array)  );	
				}
				
				$mkv_table_alias        = valueof( $alphas_array, $mkv_table_alias_index  );
				
		        $alphas_used[$mkv_table_alias_index] = $mkv_table_alias_index;
		          
		        
			   foreach ($mkv_tables_cols as $column => $column_properties) 
			   {
				  
		       if(!in_array($column, $skipcols) && !array_key_exists($column, $joinables) && $column!='ID'){
		        $selects[$column] =  "{$mkv_table_alias}.{$column}";
			   }
			   
			   
		        //create join
		        if(array_key_exists($column, $joinables) && !array_key_exists($mkv_table, $joins)){
				 $joins[$mkv_table] =  "  left join {$mkv_table} {$mkv_table_alias} on {$mkv_table_alias}.{$column}={$src_table_alias}.{$column}";
				}
		        
		      }
			  
		   }
		 }
	    }
		
		//print_pre($selects);
		print_pre($joins);
		//ksort($selects);
		
		/**
		 * test view
		 */
		$view  = " select " . implode(',', $selects). " from {$src_table} {$src_table_alias} " . implode(" ", $joins) ."  where 1=1 ";
		
		if(!$db->Execute($view)){
		 echo 'Error:<br>';
		 $db->debug=1;
		  $db->Execute($view);
		 $db->debug=0;
			
		 exit($db->ErrorMsg());
		 
		}
		
		$view_sql = "//{$mkv_tables} \r\n\$views['VIEW{$mkv_name}'] = \"\r\nselect " . implode(',', $selects). " \r\nfrom {$src_table} {$src_table_alias} \r\n" . implode("\r\n", $joins) ."\r\n \";";
		
		return '<textarea name="mkv_code" id="mkv_code" cols="80" rows="20" >'.$view_sql .'</textarea>';
		
  }
    
 }
     
$function = filter_input(INPUT_POST , 'function');

if(!empty($function)){
    //session_start();
	require_once('../init.php');
  
  if($function !=='save_db_config'){ 
    require_once('../config/db.php');
  }else{
   require_once(ROOT.'lib/adodb/adodb.inc.php');
   require_once(ROOT.'lib/adodb/adodb-active-record.inc.php');
   require_once(ROOT.'lib/adodb/adodb-errorhandler.inc.php');
   require_once(ROOT .'lib/adodb/adodb-active-record.inc.php');
   
   define('ADODB_ERROR_LOG_TYPE',3);
   define('ADODB_ERROR_LOG_DEST',ROOT.'logs/db_errors.log');
  }
 $mm = new mm();
 if(method_exists($mm,$function)){
   echo $mm->$function();	
 }else{
   echo  json_response(0,":(");
 }
}
