<?php

$auth_realm = 'Restricted area';

require_once('../init.php');
require_once 'getin.php';
require_once('../config/db.php');

define('DIR', realpath(dirname(__FILE__)) . '/');

$module = array();
$module['url'] = DIR;
$module_packed = packvars($module);

$tables_array = array();

$tables = $db->MetaTables('TABLES');

if ($tables && sizeof($tables) > 0) {
    foreach ($tables as $table) {
        $tables_array[$table] = $table;
    }
}

$tables_save_select = ui::form_select_fromArray('table_datatbl', $tables_array, '', "onchange=\"mm.set_same_source();mm.list_columns_pkey();mm.list_columns_properties();\"  class=\"easyui-validatebox textbox\" ", false, 200, 5000);
$tables_list_select = ui::form_select_fromArray('table_datasrc', $tables_array, '', "  class=\"easyui-validatebox textbox\" ", false, 200, 5000);
$tables_selectsrcs_select = ui::form_select_fromArray('table_col_datasrc', $tables_array, '', "onchange=\"mm.list_columns_select_src();\"  class=\"easyui-validatebox textbox\" ", false, 200, 5000);


$system_applications = ui::form_select('appid', 'SYSAPPS', 'APPID', 'APPNAME', '', "", "onchange=\"mm.list_modules();\"  data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ", '', 'APPID');
$system_applications_module = ui::form_select('modid', 'SYSMODS', 'MODID', 'MODNAME', '', '', " data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ", '', 'MODPOS');


class IgnorantRecursiveDirectoryIterator extends RecursiveDirectoryIterator
{

    function getChildren()
    {
        try {
            return new IgnorantRecursiveDirectoryIterator($this->getPathname());
        } catch (UnexpectedValueException $e) {
            return new RecursiveArrayIterator(array());
        }
    }

}

function database_config()
{
    global $db_type, $db_host, $db_user, $db_pass, $db_name;
    $dbtype = !empty($db_type) ? $db_type : 'mysql';
    $dbhost = !empty($db_host) ? $db_host : 'localhost';
    $dbuser = !empty($db_user) ? $db_user : 'secsol';
    $dbpass = !empty($db_type) ? $db_pass : 'secsol';
    $dbname = !empty($db_name) ? $db_name : 'secsol';

    echo '<div  class="easyui-panel"  title="Database Config" style="width:100%;height:685px;padding:2px">';
    echo '<form id="frmDBCFG" name="frmDBCFG"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"1\" >";

    echo "<tr>";
    echo "<td>Type</td>";
    echo "<td >" . databasetype_select('dbtype', $dbtype) . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Database Host</td>";
    echo "<td><input type=\"text\" name=\"dbhost\"  id=\"dbhost\" size=\"20\" value=\"{$dbhost}\"   data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Database User Name</td>";
    echo "<td><input type=\"text\" name=\"dbuser\"  id=\"dbuser\" size=\"20\" value=\"{$dbuser}\"   data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Database Password</td>";
    echo "<td><input type=\"password\" name=\"dbpass\"  id=\"dbpass\" size=\"20\" value=\"{$dbpass}\"   data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Database Name</td>";
    echo "<td><input type=\"text\" name=\"dbname\"  id=\"dbname\" size=\"20\" value=\"{$dbname}\"   data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td colspan=\"2\">If Above <b>Database User Name</b> does not exist, provive mysql root password to autocreate above user</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Root Password</td>";
    echo "<td><input type=\"password\" name=\"dbrpass\"  id=\"dbrpass\" size=\"20\" value=\"\"   data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ></td>";
    echo "</tr>";


    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td><button id=\"btndbSave\" onclick=\"db_config.save();return false;\">Save</button></td>";
    echo "</tr>";

    echo "</table>";
    echo "</form>";
    echo "</div>";

}

function schema_loading()
{
    $fileDirectory = ROOT . 'config/dd/';

    if (!is_readable($fileDirectory)) exit("Directory <b>'{$fileDirectory}'</b> does not exist or is not readable!");

    $dd_xml_files = array();
    $skip = array();

    try {
        $it = new RecursiveIteratorIterator(new IgnorantRecursiveDirectoryIterator($fileDirectory), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($it as $fullFileName => $fileSPLObject)
            if (!is_dir($fullFileName)) {
                if (@eregi("^\.{1,2}$|\.(xml)$", $fullFileName)) {
                    $pathinfo = pathinfo($fullFileName);
                    $filename = $pathinfo['basename'];
                    $dd_xml_files[$filename] = $fullFileName;
                }
            }
    } catch (UnexpectedValueException $e) {
        printf("Directory [%s] contained a directory we can not recurse into", $dir);
    }

    asort($dd_xml_files);

    $dd_views = array();
    $skip = array();

    try {
        $it = new RecursiveIteratorIterator(new IgnorantRecursiveDirectoryIterator($fileDirectory), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($it as $fullFileName => $fileSPLObject)
            if (!is_dir($fullFileName)) {
                if (
                    (@eregi("^\.{1,2}$|\.(php)$", $fullFileName) && strstr($fullFileName, 'view'))
                    ||
                    (@eregi("^\.{1,2}$|\.(php)$", $fullFileName) && strstr($fullFileName, 'task_'))
                ) {
                    $pathinfo = pathinfo($fullFileName);
                    $filename = $pathinfo['basename'];
                    $dd_views[$filename] = $fullFileName;
                }
            }
    } catch (UnexpectedValueException $e) {
        printf("Directory [%s] contained a directory we can not recurse into", $dir);
    }

    asort($dd_views);

    echo '<div  class="easyui-panel"  title="Load Data Dictionaries" style="width:100%;height:685px;padding:2px">';
    echo '<form id="frmSchemaLoad" name="frmSchemaLoad"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"1\" >";

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td>
  <a href=\"javascript:;\" onclick=\"tg1();\">Select Toggle</a>
  <button id=\"btnSchemaLoad\" onclick=\"schema.load();return false;\">Execute Dictionary</button>
  </td>";
    echo "</tr>";


    if (count($dd_xml_files) >= 1) {
        $count = 1;
        foreach ($dd_xml_files as $dd_xml_file => $dd_xml_file_path) {
            $checkboxId = "load_xml[{$count}]";
            echo "<tr>";
            echo "<td  colspan=\"2\"><input type=\"checkbox\" id=\"{$checkboxId}\" name=\"{$checkboxId}\" value=\"{$dd_xml_file_path}\"  class=\"datadict\"> <label for=\"{$checkboxId}\">{$dd_xml_file}</label></td>";
            echo "</tr>";

            ++$count;
        }
    }


    if (count($dd_views) >= 1) {
        $count = 1;
        foreach ($dd_views as $dd_view_file => $dd_view_file_path) {
            $checkboxId = "load_view[{$count}]";
            echo "<tr>";
            echo "<td colspan=\"2\"><input type=\"checkbox\" id=\"{$checkboxId}\" name=\"{$checkboxId}\" value=\"{$dd_view_file_path}\" > <label for=\"{$checkboxId}\">{$dd_view_file}</label></td>";
            echo "</tr>";

            ++$count;
        }
    }


    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td><div id=\"divSchemaLoad\" ></div></td>";
    echo "</tr>";

    echo "</table>";
    echo "</form>";
    echo "</div>";

}

function data_loading()
{

    $fileDirectory = ROOT . 'data/default/';

    if (!is_readable($fileDirectory)) exit("Directory <b>'{$fileDirectory}'</b> does not exist or is not readable!");

    $dd_xml_files = array();
    $skip = array();

    try {
        $it = new RecursiveIteratorIterator(new IgnorantRecursiveDirectoryIterator($fileDirectory), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($it as $fullFileName => $fileSPLObject)
            if (!is_dir($fullFileName)) {
                if (@eregi("^\.{1,2}$|\.(xml)$", $fullFileName)) {
                    $pathinfo = pathinfo($fullFileName);
                    $filename = $pathinfo['basename'];
                    $dd_xml_files[$filename] = $fullFileName;
                }
            }
    } catch (UnexpectedValueException $e) {
        printf("Directory [%s] contained a directory we can not recurse into", $dir);
    }

    asort($dd_xml_files);

    echo '<div  class="easyui-panel"  title="Default Data Loading" style="width:100%;height:685px;padding:2px">';
    echo '<form id="frmDataLoad" name="frmDataLoad"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"1\" >";

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td>
  <a href=\"javascript:;\" onclick=\"tg2();\">Select Toggle</a>
  <button id=\"btnDataLoad\" onclick=\"schema.data();return false;\">Load Default Data</button>
  </td>";
    echo "</tr>";


    if (count($dd_xml_files) >= 1) {
        $count = 1;
        foreach ($dd_xml_files as $dd_xml_file => $dd_xml_file_path) {
            $checkboxId = "load_data[{$count}]";
            echo "<tr>";
            echo "<td  colspan=\"2\"><input type=\"checkbox\" id=\"{$checkboxId}\" name=\"{$checkboxId}\" value=\"{$dd_xml_file_path}\" checked class=\"defdata\"> <label for=\"{$checkboxId}\">{$dd_xml_file}</label></td>";
            echo "</tr>";

            ++$count;
        }
    }

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td><div id=\"divDataLoad\" ></div></td>";
    echo "</tr>";

    echo "</table>";
    echo "</form>";
    echo "</div>";

}

function data_import()
{
    ?>
    <div class="easyui-panel" title="Data Import" style="width:635px;height:685px;padding:5px">
        <div style="padding:10px">
            <form id="frmData" method="post" novalidate>

                <fieldset>
                    <legend> from MS Access Database</legend>
                    <table cellpadding="2" cellspacing="0" width="100%">

                        <tr>
                            <td>Access Db Path</td>
                            <td>
                                <input type="text" id="db_path" name="db_path" value="" placeholder="c:\school.db"
                                       size="40">
                            </td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" id="copy_classes" name="copy_classes" value="1"> <label
                                        for="copy_classes">copy classes</label></td>
                            <td>
                                <div id="prg_copy_classes" class="easyui-progressbar" data-options="value:0"
                                     style="width:400px;"></div>
                            </td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" id="copy_teachers" name="copy_teachers" value="1"> <label
                                        for="copy_teachers">copy teachers</label></td>
                            <td>
                                <div id="prg_copy_teachers" class="easyui-progressbar" data-options="value:0"
                                     style="width:400px;"></div>
                            </td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" id="copy_teachers_subject" name="copy_teachers_subject"
                                       value="1"> <label for="copy_teachers_subject">copy teachers Subjects</label></td>
                            <td>
                                <div id="prg_copy_teachers_subject" class="easyui-progressbar" data-options="value:0"
                                     style="width:400px;"></div>
                            </td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" id="copy_students" name="copy_students" value="1"> <label
                                        for="copy_students">copy students</label></td>
                            <td>
                                <div id="prg_copy_students" class="easyui-progressbar" data-options="value:0"
                                     style="width:400px;"></div>
                            </td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" id="copy_student_houses" name="copy_student_houses" value="1">
                                <label for="copy_student_houses">student houses</label></td>
                            <td>
                                <div id="prg_copy_student_houses" class="easyui-progressbar" data-options="value:0"
                                     style="width:400px;"></div>
                            </td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" id="copy_exam" name="copy_exam" value="1"> <label
                                        for="copy_exam">copy exam</label></td>
                            <td>
                                <div id="prg_copy_exam" class="easyui-progressbar" data-options="value:0"
                                     style="width:400px;"></div>
                            </td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" id="copy_cats" name="copy_cats" value="1"> <label
                                        for="copy_cats">copy cats</label></td>
                            <td>
                                <div id="prg_copy_cats" class="easyui-progressbar" data-options="value:0"
                                     style="width:400px;"></div>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <a href="javascript:void(0)" id="btnImportAccess" class="easyui-linkbutton"
                                   data-options="iconCls:'icon-save'" onclick="schema.import_data_access();">Import from
                                    Access</a>
                            </td>
                        </tr>

                    </table>
                </fieldset>
                <div style="margin-top:20px"></div>
                <fieldset>
                    <legend> from MySQL Dump</legend>
                    <table cellpadding="2" cellspacing="0" width="100%">

                        <tr>
                            <td>MySQL Dump Path</td>
                            <td>
                                <input type="text" id="mysql_dump_path" name="mysql_dump_path" value=""
                                       placeholder="c:\elimu.sql" size="40">
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <a href="javascript:void(0)" id="btnImportMysql" class="easyui-linkbutton"
                                   data-options="iconCls:'icon-save'" onclick="schema.import_data_mysql();">Import from
                                    Dump</a>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" valign="top">
                                <div id="div_import_status">&nbsp;</div>
                            </td>
                        </tr>
                    </table>

                </fieldset>

            </form>
            <iframe name="i" id="i" frameborder="0" height="0px" width="100%" src="" scrolling="no"></iframe>
        </div>
    </div>

    <?php
}

function modules_config()
{
    global $system_applications, $tables_selectsrcs_select;

    $module_types = array();
    $module_types['dga'] = 'Data Grid - Auto Created';
    $module_types['dgc'] = 'Data Grid - Custom Made';
    $module_types['cmd'] = 'Custom Made Module';
    $module_types_select = ui::form_select_fromArray('modtype', $module_types, '', "onchange=\"\" data-options=\"required:true\"  class=\"easyui-validatebox textbox\"", false, 200, 5000);


    echo '<div  class="easyui-panel"  title="Module Config" style="width:100%;height:685px;padding:2px">';

    echo '<form id="frmMM" name="frmMM"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table width=\"100%\" border=\"0\" cellpadding=\"2\"  cellspacing=\"0\" >";

    echo "<tr>";
    echo "<td>Application</td>";
    echo "<td  colspan=\"3\">{$system_applications}</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Module</td>";
    echo "<td id=\"tdModules\"  colspan=\"3\">&nbsp;</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td valign=\"top\">Menu</td>";
    echo "<td id=\"tdMenu\" >
    <div style=\"width:200px;height:auto;border:1px solid #ccc;\">
    <ul id=\"tt\" class=\"easyui-tree\" ></ul>
    </div></td>";
    echo "<td id=\"tdProperties\" colspan=\"2\" valign=\"bottom\"><div id=\"divProperties\"></div></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Module Type</td>";
    echo "<td colspan=\"3\"><div id=\"divmodtype\">{$module_types_select}</div></td>";
    echo "</tr>";


    echo "<tr>";
    echo "<td>Module Path</td>";
    echo "<td colspan=\"3\"><input type=\"text\" name=\"modpath\"  id=\"modpath\" size=\"40\" value=\"modules/setup/\" onkeyup=\"mm.suggest_names();\" type=\"text\" name=\"name\" data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Menu Mame</td>";
    echo "<td colspan=\"3\"><input type=\"text\" name=\"menuname\"  id=\"menuname\" size=\"20\"  type=\"text\" name=\"name\" data-options=\"required:true\"  class=\"easyui-validatebox textbox\" ><input type=\"hidden\" name=\"menuid\"  id=\"menuid\" size=\"5\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Short Description</td>";
    echo "<td colspan=\"3\"><textarea name=\"descr\"  id=\"descr\" size=\"30\" data-options=\"required:true\"  class=\"easyui-validatebox textbox\"></textarea></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Position</td>";
    echo "<td colspan=\"3\"><input type=\"text\" name=\"position\"  id=\"position\" class=\"easyui-numberspinner\"  style=\"width:80px;\" data-options=\"min:1,max:20,precision:0\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Parent Menu</td>";
    echo "<td colspan=\"3\"><input type=\"text\" name=\"parentname\"  id=\"parentname\" size=\"20\" readonly=\"readonly\" ><input type=\"hidden\" name=\"parentid\"  id=\"parentid\" size=\"5\" ></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Menu is Visible</td>";
    echo "<td colspan=\"3\"><input type=\"checkbox\" id=\"active\" name=\"active\" value=\"1\" ><label for=\"active\">Visible?</label></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>PIN to Home</td>";
    echo "<td colspan=\"3\"><input type=\"checkbox\" id=\"pinhome\" name=\"pinhome\" value=\"1\" ><label for=\"pinhome\">Pinned?</label></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td colspan=\"3\">
  <a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"mm.save_menu();\"  id=\"btnSave\" >Save menu</a> |
  <a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"mm.module_columns_config();\">Make Module</a> |
  <a href=\"javascript:void(0)\" id=\"btnDelete\" class=\"easyui-linkbutton\" onclick=\"mm.delete_menu();\" >Delete menu</a> |
  </td>";
    echo "</tr>";

    echo "</table>";
    echo "</form>";
    echo "</div>";

    echo '<div id="dlg" class="easyui-dialog" title="Module Configuration" style="width:700px;height:600px;padding:5px" closed="true" buttons="#dlg-buttons">';
    echo '<div id="dlg_inner"> ';
    echo("please wait...loading");
    echo '</div>';
    echo '</div>';

    echo '<div id="dlgSlcfg" class="easyui-dialog" title="Select Column Data Source" style="width:500px;height:400px;padding:2px 2px" closed="true" buttons="#dlg-buttons">';
    echo '<form id="frmMMcfgFcols" name="frmMMcfg"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table width=\"100%\" border=\"0\" cellpadding=\"2\"  cellspacing=\"1\" >";
    echo "<input type=\"hidden\" id=\"col_linked_to_foreign_col\" name=\"col_linked_to_foreign_col\" >";

    echo "<tr>";
    echo "<td  width=\"180px\">Select Src </td>";
    echo "<td id=\"tdTables\">{$tables_selectsrcs_select}</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Select-Opt-Code Col</td>";
    echo "<td id=\"tdFKCode\">&nbsp;</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Select-Opt-Name Col</td>";
    echo "<td id=\"tdFKName\" >&nbsp;</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td ><button type=\"submit\" id=\"btnPreSetSSR\" onclick=\"mm.pre_save_select_source();\">Save & Close</button></td>";
    echo "</tr>";

    echo "</table>";
    echo "</form>";
    echo "</div>";

}

function rdt()
{
    global $db;

    $actions_array = array();
    $actions_array['dropdown'] = 'Make dropdown  code';
    $actions_array['combobox'] = 'Make combobox  code';
    $actions_array['combogrid'] = 'Make combogrid  code';
    $actions_array['customgrid '] = 'Make custom grid code';

    $tables_actions = ui::form_select_fromArray('tools_action', $actions_array, '', "", false, 200, 1000);

    $tables = $db->MetaTables('TABLES');

    if ($tables && sizeof($tables) > 0) {
        foreach ($tables as $table) {
            $tables_array[$table] = $table;
        }
    }

    $tables_select = ui::form_select_fromArray('tools_table', $tables_array, '', "onchange=\"\"  class=\"easyui-validatebox\" ", false, 200, 1000);

    echo '<div  class="easyui-panel"  title="Rapid Development Tools" style="width:100%;height:685px;padding:2px">';
    echo '<form id="frmTools"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"1\" >";

    echo "<tr>";
    echo "<td>Control Name</td>";
    echo "<td><input id=\"tool_for\" id=\"tool_for\" class=\"easyui-textbox\" required=\"true\" ></td>";
    echo "<td><a href=\"javascript:void(0)\" class=\"easyui-linkbutton\"  plain=\"true\" onclick=\"mm.call('list_tables_for_tools','all=1','divToolsTables')\">refresh</a></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Table/View Name</td>";
    echo "<td colspan=\"2\"><div id=\"divToolsTables\">{$tables_select}</div></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Do</td>";
    echo "<td colspan=\"2\">{$tables_actions}</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td  colspan=\"2\"><a href=\"javascript:void(0)\" class=\"easyui-linkbutton\"  onclick=\"mm.makecode()\">Make Code</a></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td valign=\"top\">PHP Code</td>";
    echo "<td  colspan=\"2\"><div id=\"divMadeCode\"><textarea name=\"table_code\" id=\"table_code\" cols=\"50\" rows=\"10\" ></textarea></div></td>";
    echo "</tr>";


    echo "</table>";
    echo "</div>";

}

function mkv()
{
    global $db;

    echo '<div  class="easyui-panel"  title="Make View" style="width:100%;height:685px;padding:2px">';
    echo '<form id="frmMKV"   method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"1\" >";

    echo "<tr>";
    echo "<td>View Name</td>";
    echo "<td><input id=\"mkv_name\" id=\"mkv_name\" class=\"easyui-textbox\" required=\"true\" ></td>";
    echo "<td>&nbsp;</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>Tables</td>";
    echo "<td><input id=\"mkv_tables\" id=\"mkv_tables\" class=\"easyui-textbox\" required=\"true\" ></td>";
    echo "<td>*separated by ,</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td  colspan=\"2\"><a href=\"javascript:void(0)\" class=\"easyui-linkbutton\"  onclick=\"mm.mkv()\">Make View</a></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>View Code</td>";
    echo "<td  colspan=\"2\"><div id=\"divMadeView\"><textarea name=\"mkv_code\" id=\"mkv_code\" cols=\"50\" rows=\"10\" ></textarea></div></td>";
    echo "</tr>";


    echo "</table>";
    echo "</div>";

}

function db_admin()
{
    ?>
    <iframe name="a" id="a" frameborder="0" width="635px" height="685px" src="da.php" scrolling="yes"></iframe>
    <?php
}

function manage_apps()
{
    ?>

    <table id="dg_apps" title="Manage Apps" style="width:695px;height:370px;border:0" toolbar="#toolbar_apps">
        <thead>
        <tr>
            <th field="appid" width="50" sortable="true">App ID</th>
            <th field="appname" width="50">App Name</th>
        </tr>
        </thead>
    </table>

    <div id="toolbar_apps">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="apps.add()">New
            App</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="apps.edit()">Edit
            App</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
           onclick="apps.remove()">Remove App</a>
    </div>

    <div
            id="dlg_apps"
            class="easyui-dialog"
            style="width:350px;height:160px;padding:5px 0"
            closed="true"
            buttons="#dlg_buttons_apps"
            modal="true"
    >

        <div title="App Config" style="padding:10px">

            <form id="fm_apps" method="post" novalidate>
                <input type="hidden" id="id" name="id" value="0">

                <div class="fitem">
                    <label>App ID</label>
                    <input id="appid" name="appid" class="easyui-textbox" required="true"
                           style="width:80px;padding:2px">
                </div>

                <div class="fitem">
                    <label>App Name:</label>
                    <input id="appname" name="appname" class="easyui-textbox" required="true"
                           style="width:150px;padding:2px">
                </div>

            </form>
        </div>

    </div>

    <div id="dlg_buttons_apps">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="apps.save();"
           style="width:90px">Save App</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#dlg_apps').dialog('close');" style="width:90px">Cancel</a>
    </div>

    <script>
        $(function () {
            var dg = $('#dg_apps').datagrid({
                url: 'mm.php',
                pagination: 'true',
                rownumbers: 'true',
                fitColumns: 'true',
                singleSelect: 'true',
                queryParams: {function: 'data_apps'},

            });
            dg.datagrid('enableFilter');
        });

    </script>

    <?php
}

function manage_modules()
{
    $system_applications = ui::form_select('appid', 'SYSAPPS', 'APPID', 'APPNAME', '', "", "", '', 'APPID');
    ?>

    <table id="dg_modules" title="Manage Modules" style="width:695px;height:400px;border:0"

           toolbar="#toolbar_modules"
    >
        <thead>
        <tr>
            <th field="appname" width="100" sortable="true">App Name</th>
            <th field="modid" width="50" sortable="true">Module ID</th>
            <th field="modname" width="150" sortable="true">Module Name</th>
            <th field="modpos" width="50" sortable="true">Position</th>
            <th field="modicon" width="50">Icon</th>
            <th field="mobpage" width="50">Mobile Page</th>
        </tr>
        </thead>
    </table>

    <div id="toolbar_modules">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="modules.add()">New</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
           onclick="modules.edit()">Edit</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
           onclick="modules.remove()">Remove</a>
    </div>

    <div
            id="dlg_modules"
            class="easyui-dialog"
            style="width:450px;height:290px;padding:5px 0"
            closed="true"
            buttons="#dlg_buttons_modules"
            modal="true"
    >

        <div title="Modules Config" style="padding:10px">

            <form id="fm_modules" method="post" novalidate>
                <input type="hidden" id="id" name="id" value="0">

                <div class="fitem">
                    <label>App ID</label>
                    <?php echo $system_applications; ?>
                </div>

                <div class="fitem">
                    <label>Module ID</label>
                    <input id="modid" name="modid" class="easyui-textbox" required="true"
                           style="width:100px;padding:2px">
                </div>

                <div class="fitem">
                    <label>Module Name:</label>
                    <input id="modname" name="modname" class="easyui-textbox" required="true"
                           style="width:250px;padding:2px">
                </div>

                <div class="fitem">
                    <label>Module Pos:</label>
                    <input id="modpos" name="modpos" class="easyui-textbox" required="true"
                           style="width:80px;padding:2px">
                </div>

                <div class="fitem">
                    <label>Module Icon:</label>
                    <input id="modicon" name="modicon" class="easyui-textbox" required="true"
                           style="width:80px;padding:2px">
                </div>

                <div class="fitem">
                    <label>Mobile Page:</label>
                    <input id="mobpage" name="mobpage" class="easyui-textbox" required="false"
                           style="width:80px;padding:2px">
                </div>

            </form>
        </div>

    </div>

    <div id="dlg_buttons_modules">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="modules.save();"
           style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#dlg_modules').dialog('close');" style="width:90px">Cancel</a>
    </div>

    <script>

        $(function () {
            var dg = $('#dg_modules').datagrid({
                url: 'mm.php',
                pagination: 'true',
                rownumbers: 'true',
                fitColumns: 'true',
                singleSelect: 'true',
                queryParams: {function: 'data_modules'},

            });
            dg.datagrid('enableFilter');
        });
    </script>

    <?php
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>System Admin</title>
    <link rel="stylesheet" type="text/css" href="../public/css/themes/metro-blue/easyui.css">
    <link rel="stylesheet" type="text/css" href="../public/css/themes/icon.css">
    <script type="text/javascript" src="../public/js/jquery.min.js"></script>
    <script type="text/javascript" src="../public/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../public/js/datagrid-filter.js"></script>
    <style type="text/css">
        #frmMM {
            margin: 0;
            padding: 10px 30px;
        }

        .ftitle {
            font-size: 14px;
            font-weight: bold;
            color: #666;
            padding: 5px 0;
            margin-bottom: 10px;
            border-bottom: 1px solid #ccc;
        }

        .fitem {
            margin-bottom: 5px;
        }

        .fitem label {
            display: inline-block;
            width: 120px;
        }
    </style>
</head>
<body>

<div id="config_tabs" class="easyui-tabs" style="width:900px;height:800px">

    <div title="1.Database Setup" style="padding:2px">
        <?php echo database_config(); ?>
    </div>

    <div title="2.Data Dictionary" style="padding:2px">
        <?php echo schema_loading(); ?>
    </div>

    <div title="3.System Data" style="padding:2px">
        <?php echo data_loading(); ?>
    </div>

    <div title="4.Data Import" style="padding:2px">
        <?php echo data_import(); ?>
    </div>

    <div title="5.Apps" style="padding:2px">
        <?php echo manage_apps(); ?>
    </div>

    <div title="6.Modules" style="padding:2px">
        <?php echo manage_modules(); ?>
    </div>

    <div title="7.Modules Config" style="padding:2px">
        <?php echo modules_config(); ?>
    </div>

    <div title="8.Database Admin" style="padding:2px">
        <?php echo db_admin(); ?>
    </div>

    <div title="9.RDT" style="padding:2px">
        <?php echo rdt(); ?>
    </div>

    <div title="10.MKV" style="padding:2px">
        <?php echo mkv(); ?>
    </div>


</div>

<script type="text/javascript">


    $(function () {
        $('#config_tabs').tabs({tabPosition: 'left'})
    });

    function tg1() {
        $('input[type="checkbox"].datadict').each(function () {
            this.checked = !this.checked
        });
    }

    function tg2() {
        $('input[type="checkbox"].defdata').each(function () {
            this.checked = !this.checked
        });
    }

    var schema = {
        load: function () {
            $('#divSchemaLoad').html('<img src="../public/images/loading.gif">');
            mm.call('schema_load', $('#frmSchemaLoad').serialize(), 'divSchemaLoad');
        },
        data: function () {
            $('#divDataLoad').html('<img src="../public/images/loading.gif">');
            mm.call('schema_data', $('#frmDataLoad').serialize(), 'divDataLoad');
        },
        import_data_access: function () {
            var fdata = $('#frmData').serialize() + '&module=<?php echo $module_packed; ?>&function=init';
            $('#i').attr('src', './import.php?module=<?php echo $module_packed; ?>&' + fdata);
        },
        import_data_mysql: function () {
            $('#btnImportAccess').linkbutton('disable');
            $('#btnImportMysql').linkbutton('disable');
            $('#div_import_status').html('<img src="../public/images/loading.gif">');

            $.ajax({
                type: 'POST',
                url: './mm.php',
                data: 'function=import_data_mysql&' + $('#frmData').serialize(),
                success: function (responseText) {
                    $('#btnImportAccess').linkbutton('enable');
                    $('#btnImportMysql').linkbutton('enable');
                    $('#div_import_status').html('&nbsp;');
                    $.messager.alert('Done', responseText, 'info');
                }
            });

        }
    }

    var db_config = {
        save: function () {
            $.messager.progress();
            $.post('mm.php', $('#frmDBCFG').serialize() + '&function=save_db_config' + '&', function (data) {
                $.messager.progress('close');
                $("#btndbSave").attr("disabled", "disabled");

                if (data.success === 1) {

                    $("#btndbSave").removeAttr("disabled");

                    $.messager.show({
                        title: 'Success',
                        msg: data.message
                    });

                } else {
                    $("#btndbSave").removeAttr("disabled");
                    $.messager.alert('DB Config Save', data.message, 'error');
                }
            }, "json");

        }
    }

    var apps = {
        add: function () {
            $('#dlg_apps').dialog({title: 'New App', closed: false, resizable: true, modal: true});
            $('#fm_apps').form('clear');
        },
        edit: function () {
            var row = $('#dg_apps').datagrid('getSelected');
            if (row) {
                $('#dlg_apps').dialog({title: 'Edit App', closed: false, resizable: true, modal: true});
                $('#fm_apps').form('load', row);
            }
        },
        save: function () {
            var validate = $('#fm_apps').form('validate');
            var fdata = $('#fm_apps').serialize() + '&function=save_app';
            $.post('mm.php', fdata, function (data) {
                if (data.success === 1) {
                    $('#dlg_apps').dialog('close');
                    $('#dg_apps').datagrid('reload');
                } else {
                    $.messager.alert('Error', data.message, 'error');
                }
            }, "json");

        },
        remove: function () {
            var row = $('#dg_apps').datagrid('getSelected');
            if (row) {
                $.messager.confirm('Confirm', 'Are you sure you want to remove this application?', function (r) {
                    if (r) {
                        var rdata = 'id=' + row.id + '&function=remove_app';
                        $.post('mm.php', rdata, function (data) {
                            if (data.success === 1) {
                                $('#dlg_apps').dialog('close');
                                $('#dg_apps').datagrid('reload');
                            } else {
                                $.messager.alert('Error', data.message, 'error');
                            }
                        }, "json");
                    }
                });
            }
        },
    };

    var modules = {
        add: function () {
            $('#dlg_modules').dialog({title: 'New Module', closed: false, resizable: true, modal: true});
            $('#fm_modules').form('clear');
        },
        edit: function () {
            var row = $('#dg_modules').datagrid('getSelected');
            if (row) {
                $('#dlg_modules').dialog({title: 'Edit Module', closed: false, resizable: true, modal: true});
                $('#fm_modules').form('load', row);
            }
        },
        save: function () {
            var validate = $('#fm_modules').form('validate');
            var fdata = $('#fm_modules').serialize() + '&function=save_module';
            $.post('mm.php', fdata, function (data) {
                if (data.success === 1) {
                    $('#dlg_modules').dialog('close');
                    $('#dg_modules').datagrid('reload');
                } else {
                    $.messager.alert('Error', data.message, 'error');
                }
            }, "json");

        },
        remove: function () {
            var row = $('#dg_modules').datagrid('getSelected');
            if (row) {
                $.messager.confirm('Confirm', 'Are you sure you want to remove this Module?', function (r) {
                    if (r) {
                        var rdata = 'id=' + row.id + '&function=remove_module';
                        $.post('mm.php', rdata, function (data) {
                            if (data.success === 1) {
                                $('#dlg_modules').dialog('close');
                                $('#dg_modules').datagrid('reload');
                            } else {
                                $.messager.alert('Error', data.message, 'error');
                            }
                        }, "json");
                    }
                });
            }
        },
    };

    var mm = {
        call: function (_function, _params, update_element) {
            $.ajax({
                type: 'POST',
                url: './mm.php',
                data: 'function=' + _function + '&' + _params,
                success: function (responseText) {
                    $('#' + update_element + '').html(responseText);
                }
            });
        },
        save: function () {
            var appid = $('#appid').val();
            var modid = $('#modid').val();
            var modpath = $('#modpath').val();
            var menuid = $('#menuid').val();
            var menuname = $('#menuname').val();
            var parentid = $('#parentid').val();
            var modtype = $('#modtype').val();

            var params = 'appid=' + appid + '&modid=' + modid + '&modpath=' + modpath + '&menuid=' + menuid + '&menuname=' + menuname + '&parentid=' + parentid + '&modtype=' + modtype;

            if (confirm("'cfg.php' & index.php will be created in '/modules/" + modpath + "'. make sure it has apache write-rights")) {
                $.post('mm.php', $('#frmMMcfg').serialize() + '&function=save' + '&' + params, function (data) {

                    $("#btnSave").attr("disabled", "disabled");

                    if (data.success === 1) {

                        /*$('#divStatus').html('Record Saved');*/
                        $("#btnSave").removeAttr("disabled");

                        $.messager.show({
                            title: 'Success',
                            msg: data.message
                        });

                    } else {
                        $("#btnSave").removeAttr("disabled");
                        /*$('#divStatus').html(data.message);*/
                        $.messager.alert('Menu Save', data.message, 'error');
                    }
                }, "json");

            }
        },
        populate_fields: function (_function, Params) {
            $.post('mm.php', 'function=' + _function + '&' + Params, function (data) {

                $.each(data, function (field, value) {

                    var elm = $('input#' + field);
                    var is_input = $(elm).is("input");
                    var is_select = $(elm).is("select");
                    var is_textarea = $('#' + field).is("textarea");
                    var is_checkbox = $('#' + field).is("checkbox");
                    var elementType = $('input#' + field).prop('tagName') ? 1 : 0;
                    var tagName = $('input#' + field).prop('tagName');
                    var nodeType = $('input#' + field).prop('type');
                    var nodeName = $('input#' + field).prop('nodeName');
                    var type = $('input#' + field).type;

                    if (is_textarea === true) {
                        $('#' + field).val(value);
                    } else if ((elementType == 1)) {
                        switch (nodeType) {
                            case 'text':
                                $('#' + field).val(value);
                                break;
                            case 'checkbox':
                                if (value == 1) {
                                    $("#" + field).attr("checked", "checked");
                                }
                                break;
                        }

                    } else {
                        jQuery("select#" + field + " option[value='" + value + "']").attr("selected", "selected");
                    }
                });

            }, "json");
        },
        suggest_names: function () {
            var modpath = $('#modpath').val();
            this.populate_fields('suggest_names', 'modpath=' + modpath);
        },
        set_same_source: function () {
            var table_datatbl = $('#table_datatbl').val();
            jQuery("select#table_datasrc option[value='" + table_datatbl + "']").attr("selected", "selected");
        },
        list_columns_pkey: function () {
            data = $('#frmMMcfg').serialize();
            mm.call('list_columns_pkey', data, 'tdPkey');
        },
        module_columns_config: function () {
            var appid = $('#appid').val();
            var modid = $('#modid').val();
            var modpath = $('#modpath').val();
            var menuid = $('#menuid').val();
            var menuname = $('#menuname').val();
            var parentid = $('#parentid').val();

            if (appid === '') {
                $.messager.alert('Module Config', 'Select an Application', 'error');
            } else if (modid === '') {
                $.messager.alert('Module Config', 'Select a Module', 'error');
            } else if (menuid === '') {
                $.messager.alert('Module Config', 'Select a module from the tree menu', 'error');
            } else {
                var params = 'appid=' + appid + '&modid=' + modid + '&modpath=' + modpath + '&menuid=' + menuid + '&menuname=' + menuname + '&parentid=' + parentid;
                $('#dlg').dialog('open');
                $.ajax({
                    type: 'POST',
                    url: './mm.php',
                    data: 'function=module_columns_config&' + params,
                    success: function (responseText) {
                        $('#dlg_inner').html(responseText);
                        //mm.list_columns_properties();
                    }
                });
            }
        },
        list_columns_properties: function () {
            var appid = $('#appid').val();
            var modid = $('#modid').val();
            var modpath = $('#modpath').val();
            var menuid = $('#menuid').val();
            var menuname = $('#menuname').val();
            var parentid = $('#parentid').val();

            var params = 'appid=' + appid + '&modid=' + modid + '&modpath=' + modpath + '&menuid=' + menuid + '&menuname=' + menuname + '&parentid=' + parentid + '&modpath=' + modpath;

            data = $('#frmMMcfg').serialize() + '&' + params;

            mm.call('list_columns_properties', data, 'tdColumns');
        },
        list_columns_select_src: function () {
            var table_col_datasrc = $('#table_col_datasrc').val();
            var col_linked_to_foreign_col = $('#col_linked_to_foreign_col').val();
            mm.call('list_columns_select_src', 'table_col_datasrc=' + table_col_datasrc + '&coldetype=code&col_linked_to_foreign_col=' + col_linked_to_foreign_col, 'tdFKCode');
            mm.call('list_columns_select_src', 'table_col_datasrc=' + table_col_datasrc + '&coldetype=name&col_linked_to_foreign_col=' + col_linked_to_foreign_col, 'tdFKName');
        },
        forminput_type_check: function (inputid, columnname) {
            mm.call('list_tables', 'all=1', 'tdTables');
            var inputval = $('#' + inputid).val();
            $('input#col_linked_to_foreign_col').val('');
            if (inputval === 'select' || inputval === 'combogrid') {
                $('input#col_linked_to_foreign_col').val(columnname);
                $('#dlgSlcfg').dialog({
                    title: "Select linked table for column '" + columnname + "'",
                    closed: false,
                    resizable: true,
                    modal: true,
                });
            }
        },
        pre_save_select_source: function () {
            var table_col_datasrc = $('#table_col_datasrc').val();
            var col_linked_to_foreign_col = $('#col_linked_to_foreign_col').val();
            var linked_tbl_col_code = $('#linked_tbl_col_code').val();
            var linked_tbl_col_name = $('#linked_tbl_col_name').val();
            var params = 'table_col_datasrc=' + table_col_datasrc + '&col_linked_to_foreign_col=' + col_linked_to_foreign_col + '&linked_tbl_col_code=' + linked_tbl_col_code + '&linked_tbl_col_name=' + linked_tbl_col_name;

            $.ajax({
                type: 'POST',
                url: './mm.php',
                data: 'function=pre_save_select_source&' + params,
                success: function (responseText) {
                    $('#dlgSlcfg').dialog('close');
                }
            });

        },
        list_modules: function () {
            data = $('#frmMM').serialize();
            mm.call('list_modules', data, 'tdModules');
        },
        list_menu: function () {
            data = $('#frmMM').serialize();
            $('#tt').html('&nbsp;');
            $('input#parentname').val('');
            $('input#parentid').val(0);
            $('input#menuname').val('');
            $('input#menuid').val(0);
            //$("#active").removeAttr("checked");
            //$("#pinhome").removeAttr("checked");

            var t;
            $.ajax({
                type: 'POST',
                url: './mm.php',
                data: 'function=list_menu&' + data,
                success: function (treeJson) {
                    t = $('#tt').tree(
                        {
                            data: treeJson,
                            dnd: true,
                            onDragOver: function (target, source) {
                                if ($(target).hasClass('tree-node-append')) {
                                    setTimeout(function () {
                                        $(target).droppable('enable');
                                    }, 0);
                                    return false;
                                } else {

                                }
                            },
                            onDrop: function (target, source, point) {
                                //console.log(target);
                                //console.log(source);
                                //console.log(point);
                            },
                            onStopDrag: function (node) {
                                //console.log(node);
                                //console.log('node.target='+node.target);

                                if (node.attributes) {
                                    //var parentid = node.attributes.parentid;
                                    //console.log('parentid='+parentid);
                                }
                                //var root = t.tree('getRoot');    // get the root node
                                //console.log(root);
                                //var parent = $('#tt').tree('getData', root.target);    // get the Parent node
                                //console.log(parent);
                                //
                                //var parent = $('#tt').tree('getParent', node.target);    // get the Parent node
                                //console.log(parent);


                            },
                            onClick: function (node) {
                                console.log(node);
                                $('input#parentname').val('');
                                $('#modtype option[value="0"]').attr("selected", true);
                                $('input#parentid').val(0);
                                $('input#menuname').val(node.text);
                                $('input#menuid').val(node.id);
                                $('#position').numberspinner('setValue', 100);
                                $('#descr').val(node.text);
                                $("#active").prop("checked", false);
                                $("#pinhome").prop("checked", false);

                                if (node.attributes) {
                                    var modtype = node.attributes.modtype;
                                    $('input#modpath').val(node.attributes.modpath);
                                    $('input#modtype').val(node.attributes.modtype);
                                    $('#position').numberspinner('setValue', node.attributes.position);

                                    if (node.attributes.active == 1) {
                                        $("#active").prop("checked", true);
                                    }

                                    if (node.attributes.pinhome == 1) {
                                        $("#pinhome").prop("checked", true);
                                    }

                                    mm.call('select_modtype', 'modtype=' + modtype, 'divmodtype');
                                    mm.populate_fields('get_modules_properties', 'mnuid=' + node.id);
                                }

                                var pnode = t.tree('getParent', node.target);

                                if (pnode) {
                                    $("#btnDelete").removeAttr("disabled");
                                    $('input#parentname').val(pnode.text)
                                    $('input#parentid').val(pnode.id)
                                } else {
                                    $("#btnDelete").attr("disabled", "disabled");
                                }

                                if (node.id > 0) {
                                    $('#descr').val('');
                                    $("#descr").attr("disabled", "disabled");
                                    //mm.populate_fields('get_descr', 'mnuid='+node.id);
                                    $("#descr").removeAttr("disabled");
                                }

                            },
                            onDblClick: function (node) {

                                $('input#menuname').val('');
                                $('input#parentid').val(node.id);
                                $('input#menuid').val('');
                                $('input#menuname').val('');
                                mm.call('select_modtype', 'modtype=0', 'divmodtype');

                                if (node.attributes) {
                                    $('input#modpath').val(node.attributes.modpath);
                                }

                                var pnode = t.tree('getParent', node.target);

                                if (pnode) {
                                    $("#btnDelete").removeAttr("disabled");
                                    $.messager.show({
                                        title: 'New Menu',
                                        msg: 'Add new menu to <b>' + pnode.text + '</b>',
                                        showType: 'show'
                                    });
                                } else {
                                    $("#btnDelete").attr("disabled", "disabled");
                                    $.messager.show({
                                        title: 'New Menu',
                                        msg: 'Add new menu ',
                                        showType: 'show'
                                    });
                                }

                                $('input#menuname').select();

                            },
                        });
                }
            });
        },
        save_menu: function () {

            var modpath = $('#modpath').val();
            var appid = $('#appid').val();
            var modtype = $('#modtype').val();
            var menuname = $('#menuname').val();
            var descr = $('#descr').val();

            var validate = $('#frmMM').form('validate');
            if (!validate || appid == '' || modtype == '' || modtype == '0' || modpath == '' || menuname == '' || descr == '') {
                $.messager.alert('Error', 'Fill In All Required Fields', 'error');
                return;
            } else {
                $.messager.confirm('Menu', 'Save Now?', function (r) {
                    if (r) {
                        $.post('mm.php', $('#frmMM').serialize() + '&function=save_menu', function (data) {
                            $("#btnSave").attr("disabled", "disabled");
                            $("#btnDelete").attr("disabled", "disabled");
                            if (data.success === 1) {
                                $('input#menuid').val(data.mnuid);
                                $('input#modpath').val(data.modpath);
                                $('#position').numberspinner('setValue', '');
                                $("#btnSave").removeAttr("disabled");
                                $("#btnDelete").removeAttr("disabled");
                                $.messager.show({title: 'Success', msg: data.message});
                                mm.list_menu();
                            } else {
                                $("#btnSave").removeAttr("disabled");
                                $("#btnDelete").removeAttr("disabled");
                                $.messager.alert('Menu Save', data.message, 'error');
                            }
                        }, "json");
                    }
                });
            }
        },
        delete_menu: function () {

            var menuname = $('#menuname').val();
            var parentname = $('#parentname').val();

            if (confirm("Delete '" + parentname + "->" + menuname + "' Now?")) {
                $.post('mm.php', $('#frmMM').serialize() + '&function=delete_menu', function (data) {

                    $("#btnSave").attr("disabled", "disabled");
                    $("#btnDelete").attr("disabled", "disabled");

                    if (data.success === 1) {
                        $('input#parentname').val('');
                        $('input#parentid').val('');
                        $('input#menuname').val('');
                        $('input#menuid').val('');
                        $("#btnSave").removeAttr("disabled");
                        $("#btnDelete").removeAttr("disabled");
                        mm.list_menu();
                    } else {
                        $("#btnSave").removeAttr("disabled");
                        $("#btnDelete").removeAttr("disabled");
                        $.messager.alert('Menu Save', data.message, 'error');
                    }
                }, "json");
            }
        },
        makecode: function () {
            var tool_for = $('#tool_for').val();
            var tools_action = $('#tools_action').val();
            var tools_table = $('#tools_table').val();
            mm.call('makecode', 'tool_for=' + tool_for + '&tools_action=' + tools_action + '&tools_table=' + tools_table, 'divMadeCode');
        },
        mkv: function () {
            var mkv_tables = $('#mkv_tables').val();
            var mkv_name = $('#mkv_name').val();
            mm.call('mkv', 'mkv_tables=' + mkv_tables + '&mkv_name=' + mkv_name, 'divMadeView');
        }

    }

</script>

</body>
</html>
