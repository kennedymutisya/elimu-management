<?php

/**
* auto created config file for db.php
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2017-03-27 13:39:02
*/

define('ADODB_ERROR_LOG_TYPE',3);
define('ADODB_ERROR_LOG_DEST',ROOT.'logs/db_errors.log');

require_once(ROOT.'lib/adodb/adodb.inc.php');
require_once(ROOT.'lib/adodb/adodb-active-record.inc.php');
//require_once(ROOT.'lib/adodb/adodb-errorhandler.inc.php');
require_once(ROOT .'lib/adodb/adodb-active-record.inc.php');


$db_type = 'mysql';
$db_host = 'localhost';
$db_user = 'root';
$db_pass = '';
$db_name = 'moi';

$db = ADONewConnection($db_type);
if(! @$db->Connect($db_host,$db_user,$db_pass,$db_name)){
 
 if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=='XMLHttpRequest'){	
  echo json_response(0,"Could Not Connect to {$db_host}/{$db_name} ");
 }else{
  echo("Could Not Connect to {$db_host}/{$db_name} ");
 }
 
}else{
$db->SetFetchMode(ADODB_FETCH_ASSOC);

ADODB_Active_Record::SetDatabaseAdapter( $db );

$ADODB_ASSOC_CASE = 0;
$ADODB_CACHE_DIR = ROOT . 'cache';
$ADODB_ACTIVE_CACHESECS = 30;

//$db->debug=1;
}
