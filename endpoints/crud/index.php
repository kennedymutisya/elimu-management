<?php

require_once('../../init.php');
require_once(ROOT.'config/db.php');

	
if(isset($_POST['module']) && isset($_POST['function'])){
 $modulestr      =  filter_input(INPUT_POST , 'module');
 $function       =  filter_input(INPUT_POST , 'function');
}elseif(isset($_POST['modvars']) && isset($_POST['function'])){
 $modulestr      =  filter_input(INPUT_POST , 'modvars');
 $function       =  filter_input(INPUT_POST , 'function');
}elseif(isset($_GET['modvars']) && isset($_GET['function'])){
 $modulestr      =  filter_input(INPUT_GET , 'modvars');
 $function       =  filter_input(INPUT_GET , 'function');
}elseif(isset($_GET['module']) && isset($_GET['function'])){
 $modulestr      =  filter_input(INPUT_GET , 'module');
 $function       =  filter_input(INPUT_GET , 'function');
}else{
  header('Content-type : text/json');
  echo json_response( 0 , 'invalid end point');
  die;
}
 
 $module_unpack  =  unpackvars($modulestr);
 
 $appid         =  valueof($module_unpack, 'appid');
 $modid         =  valueof($module_unpack, 'modid');
 $mnuid         =  valueof($module_unpack, 'mnuid');
 $modpath       =  valueof($module_unpack, 'modpath');
 
 $modpath_arr   = explode('/', $modpath);
 
 foreach($modpath_arr as $k=>$v){
  if(empty($v)){
	unset($modpath_arr[$k]);
  }
 }
 
define('MNUID',$mnuid);

 $modpath_arrv = array_reverse($modpath_arr);
 $class        = isset($modpath_arrv[0]) ? $modpath_arrv[0] : '';
 $file_config  =  ROOT . $modpath. '/cfg.php';
 $file_class   =  ROOT . $modpath. '/'. $class .'.php';
 
 if(!is_readable($file_class)){
 	header('Content-type : text/json');
 	echo json_response(  0 , 'invalid end point');
 	die;
 }elseif(!is_readable($file_config)){
 	header('Content-type : text/json');
 	echo json_response(  'success' , 'invalid end point config file');
 	die;
 }
 
 require_once($file_class);
 require_once($file_config);
 
 $_class = new $class();
 
 if(empty($function)){
  header('Content-type : text/json');
  echo json_response(0, 'missing class method of object' .$class);
  die;
 }
 
 if(!method_exists($_class, $function)){
 	
  switch ($function){
  	case 'export':
  		$_class = new grid();
  	break;
  	case 'import':
  		$_class = new grid();
  	break;
    default:	
     header('Content-type : text/json');
     echo json_response(0, 'method '.$class.'::'.$function. ' not defined');
     die;
  }
 }
 
 $respose  = $_class->$function();
 
 
 echo $respose ;
