<?php

require_once('../../init.php');
require_once('../../config/db.php');

$mnuid = filter_input(INPUT_POST , 'mnuid');
$title = filter_input(INPUT_POST , 'title');
$descr = "No Help Content Added for <b>{$title}</b>";

if($mnuid>0){
$get_descr = $db->GetOne("SELECT DESCR FROM SYSMODMNU WHERE MNUID={$mnuid}");
$descr     = !empty($get_descr) ? $get_descr : $descr;
}

echo $descr;