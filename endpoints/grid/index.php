<?php

require_once('../../init.php');
require_once(ROOT.'config/db.php');
 
if(isset($_POST['modvars']) && isset($_POST['function'])){
 $modulestr      =  filter_input(INPUT_POST , 'modvars');
 $function       =  filter_input(INPUT_POST , 'function');
}elseif(isset($_GET['modvars']) && isset($_GET['function'])){
 $modulestr      =  filter_input(INPUT_GET , 'modvars');
 $function       =  filter_input(INPUT_GET , 'function');
}else{
  header('Content-type : text/json');
  echo json_response( 0 , 'invalid end point');
  die;
}
 $module_unpack  =  unpackvars($modulestr);
 
 $class          =  valueof($module_unpack, 'class');
 $modpath        =  valueof($module_unpack, 'modpath');
 $module_config  =  ROOT.$modpath .'/cfg.php';
 
 if(!is_readable($module_config)){
 	echo json_encode( array('success' =>0 , 'invalid end point config file'));
 	die;
 }
 
 require_once($module_config);
 
$grid = new grid();

$grid->data();
