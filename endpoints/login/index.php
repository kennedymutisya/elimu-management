<?php

require_once('../../init.php');

header('Content-type: text/json');

 $replace = array('--',"'");
 $userid        =   filter_input(INPUT_POST , 'login',FILTER_SANITIZE_STRING);
 $password      =   filter_input(INPUT_POST , 'password',FILTER_SANITIZE_STRING);
 
 $userid        =   trim(str_replace($replace,'',$userid));
 $password      =   trim(str_replace($replace,'',$password));
 
 if(empty($userid)){
 	die(json_response(0, 'Enter your User Name'));
 }elseif(empty($password)){
 	die(json_response(0, 'Enter your Password'));
 }
 
 $password_str   =  md5( strtolower($userid).$password.strtolower($userid));

 require_once(ROOT.'config/db.php');

 $sql = "SELECT U.ID,  U.USERID , U.USERNAME ,U.EMAIL, U.GROUPCODE, G.GROUPNAME
FROM USERS U
LEFT JOIN USERGROUPS G ON G.GROUPCODE = U.GROUPCODE
WHERE U.USERID='{$userid}'
AND PASSWORD ='{$password_str}'
";
 
 $rs = $db->GetRow($sql);
 
  if(!$rs){
  	sleep(1);//kwanini unasahau password ? ..throttling you
  	die(json_response(0, 'Wrong User ID or Password'));
  }else{
  	
  	$_SESSION['user']['id']         = valueof($rs, 'ID');
  	$_SESSION['user']['userid']     = valueof($rs, 'USERID');
  	$_SESSION['user']['username']   = valueof($rs, 'USERNAME');
  	$_SESSION['user']['email']      = valueof($rs, 'EMAIL');
  	$_SESSION['user']['groupcode']  = valueof($rs, 'GROUPCODE');
  	$_SESSION['user']['groupname']  = valueof($rs, 'GROUPNAME');
  	$_SESSION['user']['deptcode']   = valueof($rs, 'DEPTCODE');
  	$_SESSION['user']['deptname']   = valueof($rs, 'DEPTNAME');
  	
  	echo json_response(1, 'login successful');
  	
  }
