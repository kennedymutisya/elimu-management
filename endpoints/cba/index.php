<?php

require_once('../../init.php');
require_once(ROOT.'config/db.php');

 $q             =  filter_input(INPUT_POST , 'q');
 $vars          =  filter_input(INPUT_POST , 'vars');
 $vars_unpack   =  unpackvars($vars);
 
 //print_pre($vars_unpack);
 //$db->debug=1;
 
 $skip          =  array('IdField','TextField','source');
 $select_cols   =  array();
 $json_cols     =  array();
 
 $IdField       =  valueof($vars_unpack, 'IdField');
 $TextField     =  valueof($vars_unpack, 'TextField');
 $source        =  valueof($vars_unpack, 'source');
 
 //echo "\$IdField={$IdField} <br>";
 //echo "\$TextField={$TextField} <br>";
 //echo "\$source={$source} <br>";
 
 $select_cols[]    = " {$IdField} id ";
 $select_cols[]    = " {$TextField} text ";
 
 
 $select_cols_str = implode("," ,$select_cols);
 //$rs  = $db->SelectLimit("select {$select_cols_str} from {$source} where 1=1",20);
 $rs  = $db->SelectLimit("select {$select_cols_str} from {$source} where {$IdField} like '%{$q}%' or {$TextField} like '%{$q}%'",20);
 
$str = '';
if($rs){
	$numRecs = $rs->RecordCount();
	$numCols = count($json_cols);
	if($numRecs>0){
		$count = 1;
		$count_cols = 0;

		$str = '';
		while (!$rs->EOF) {
			//print_pre($rs->fields);exit();
			
			 $col_value_id   = isset($rs->fields['id']) ? $rs->fields['id'] : null;
			 $col_value_text = isset($rs->fields['text']) ? $rs->fields['text'] : null;
			 
			 $str .= "{ \"id\":\"{$col_value_id}\",\"text\":\"{$col_value_text}\" }";
			 
            $str .=  $count <$numRecs ? ",\r\n" : '';			 
			
		 ++$count;	
		 $rs->MoveNext();
		}
	}
}
 
 header('Content-type: text/json');
 
 echo '['.$str.']';
