<?php

 
 function queue_sms (  $to_number,  $message ){
    global $db;

    $next_id           = generateID('SYSQSMS');

    $message           = trim($message);
    $message           = preg_replace("/\s+/", " ", $message);

    $sms['ID']         = $next_id;
    $sms['QUEUEREF']   = $refno;
    $sms['MOBILE']     = $to_number;
    $sms['SMS']        = strtoupper($message);
    $sms['QUEUEDATE']  = time();

    if(!empty($sms['SENDTO'])){
      if($db->AutoExecute('SYSQSMS' ,$sms, 'INSERT')){
        return  true;
      }
    }else{
        //
    }

   return false;

 }

 function queue_mail( $refno , $to_name, $to_email, $subject, $message, $fromemail ,  $fromname , $replyto , $attachmentpath='' ){
    global $db;

    $check             = $db->GetOne("select QUEUEREF from SYSQMAIL where QUEUEREF='{$refno}' AND TOEMAIL='{$to_email}' ");

    if(!empty($check)) return  true;

    $next_id             = generateID('SYSQMAIL');

    $email['ID']         = $next_id;
    $email['QUEUEREF']   = $refno;
    $email['TOEMAIL']    = $to_email;
    $email['TONAME']     = $to_name;
    $email['SUBJECT']    = $subject;
    $email['MESSAGE']    = $message;
    $email['FROMEMAIL']  = $fromemail;
    $email['FROMNAME']   = $fromname;
    $email['REPLYTO']    = $replyto;
    $email['ATTACHMENTPATH']    = $attachmentpath;
    $email['QUEUEDATE']  = time();


    if(!empty($email['TOEMAIL'])){
      if($db->AutoExecute('SYSQMAIL' ,$email, 'INSERT')){
        return  true;
      }else{
      }
    }

   return false;

 }


 function html_to_pdf($reportname, $html, $fontsize=12, $orientation='P'){


//require_once( ROOT .'/lib/tcpdf/config/lang/eng.php');
require_once( ROOT.'/lib/tcpdf/tcpdf.php');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Report');
$pdf->SetAuthor('Report');
$pdf->SetTitle('Report');
$pdf->SetSubject('Report');
$pdf->SetKeywords('Report');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins - set_margins(left,top,right
$pdf->SetMargins(10, 10, 5, true);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// add a page
$pdf->AddPage( $orientation );

// set default font subsetting mode
$pdf->setFontSubsetting(true);

$pdf->SetFont('courier', '', $fontsize, '', false); 

// set color for background
$pdf->SetFillColor(255, 255, 200);

// set color for text
$pdf->SetTextColor(0, 0, 0,'','','black');

// set color for background
$pdf->SetFillColor(215, 235, 255);

// set color for text
$pdf->SetTextColor(0, 0, 0);

//$y = $pdf->getY();

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->Output("{$reportname}.pdf", 'I');	
}

 function compress_image($source_url, $destination_url, $quality) {
	
	if(is_readable( $destination_url)){
		return  $destination_url;
	}
	
	$info = getimagesize($source_url);

	if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
	elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
	elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
 
	//save file
	imagejpeg($image, $destination_url, $quality);
 
	//return destination file
	return $destination_url;
}

 function generateRandomChars($length = 10) {
    return substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

 function generateRandomString($length = 10) {
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

 function json_response($success , $message, $extra = array() ){
 	
 	$_array =   array(
	 'success' => $success,
	  'message' => $message,
	 );
	 
 	if(sizeof($extra)>0){
 	 $_array = array_merge($_array, $extra);	
 	}
 	
	return  json_encode(
	  $_array
	);
 }
 
 function microtime_float() { 
    if (version_compare(PHP_VERSION, '5.0.0', '>'))  return microtime(true); 
    list($u,$s)=explode(' ',microtime()); return ((float)$u+(float)$s);  
  } 
  
 function depluralize($word){
    $rules = array( 
        'ss' => false, 
        'os' => 'o', 
        'ies' => 'y', 
        'xes' => 'x', 
        'oes' => 'o', 
        'ies' => 'y', 
        'ves' => 'f', 
        's' => '');
    foreach(array_keys($rules) as $key){
        if(substr($word, (strlen($key) * -1)) != $key) 
            continue;
        if($key === false) 
            return $word;
        return substr($word, 0, strlen($word) - strlen($key)) . $rules[$key]; 
    }
    return $word;
}

 function get_mime($filename, $mode=0){

    // mode 0 = full check
    // mode 1 = extension check only

    $mime_types = array(

        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',


        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

    $ext = strtolower(array_pop(explode('.',$filename)));



     if(function_exists('mime_content_type')&&$mode==0){
        $mimetype = mime_content_type($filename);
        return $mimetype;
        }

    if(function_exists('finfo_open')&&$mode==0){
        $finfo = finfo_open(FILEINFO_MIME);
        $mimetype = finfo_file($finfo, $filename);
        finfo_close($finfo);
        return $mimetype;
        } elseif(array_key_exists($ext, $mime_types)){
        return $mime_types[$ext];
        }else {
        return 'application/octet-stream';
        }
     }

 function packvars($array){
	global $root_path;
	
	 if(!is_array($array)) return null;
	 
	 $urlcrypt = new urlcrypt('vx10');
	 $encdata = $urlcrypt->encode($array);
	
	 return $encdata;
	 
}

 function unpackvars($str){
	global $root_path;
	
	 if(empty($str)) return null;
	 
	 $urlcrypt = new urlcrypt('vx10');
	 $decdata  = $urlcrypt->decode($str);
	
	 return $decdata;
}


/**
 * get strings similarity as a percentage
 *
 * @param string $text1
 * @param string $text2
 * @return float
 */
 function similarity($text1 , $text2 ){

 $var_1 = $text1;
 $var_2 = $text2;

 similar_text($var_1, $var_2, $percent1);

 similar_text($var_2, $var_1, $percent2);

 /**
  * percentage
  */
 return  ($percent1+$percent2)/2;

 }

 /**
  * get array value using key
  *
  * @param array $var
  * @param string $key
  * @param string $default_return_value
  * @param string $run_value_in_this_function
  * @return string
  */
 function valueof($var, $key , $default_return_value = null , $run_value_in_this_function='' ){
 	
 	$return_value =  $default_return_value;
 	
 	 if(is_object($var)){
 	 	if(isset($var->$key)){
 	 		$return_value = trim($var->$key);
 	 	}
 	 }elseif(is_array($var)){
 	 	if(isset($var[$key])){
 	 		$value =  $var[$key];
 	 		$return_value =  is_string($value) ? trim($value) : $value;
 	 	}
 	}else{
 		 $return_value = $default_return_value;
 	}

  if(!empty($return_value) && !empty($run_value_in_this_function)){
  	if(function_exists($run_value_in_this_function)){
  		$return_value = $run_value_in_this_function($return_value);
  	}
  }
  
  return $return_value;

 }

 /**
 * safely get value of array key or object->var
 * @author kenmsh@gmail.com
 * @param object $var
 * @param string $key
 * @return key value
 */


 function inline_image($path){
    	if(is_readable($path)){
    		$contents = base64_encode( file_get_contents( $path ) );
            return "data:image/png;base64,{$contents}";
    	}else{
    		return $path;
    	}
 }

 function isAjax(){
	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=='XMLHttpRequest';
 }

 function get_user_ip()
 {
	
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$remote_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}else{
		$remote_ip = $_SERVER['REMOTE_ADDR'];
	}
	return $remote_ip;
}

 function get_user_ip2lng()
 {
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$remote_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}else{
		$remote_ip = $_SERVER['REMOTE_ADDR'];
	}
	return ip2long($remote_ip);
}

 function file_icon($file_name){

	$ext=strrchr($file_name, ".");//Get file extention
	switch(strtolower($ext)){
		case ".xls": $file_icon = "page_excel.png"; break;
		case ".xlsx": $file_icon = "page_excel.png"; break;
		case ".doc": $file_icon = "page_word.png"; break;
		case ".docx": $file_icon = "page_word.png"; break;
		case ".pdf": $file_icon = "page_white_acrobat.png"; break;
		case ".png": $file_icon = "page_white_picture.png"; break;
		case ".jpg": $file_icon = "page_white_picture.png"; break;
		case ".png": $file_icon = "page_white_picture.png"; break;
		case ".zip": $file_icon = "compress.png"; break;
		case ".rar": $file_icon = "compress.png"; break;
		case ".gz": $file_icon = "compress.png"; break;
		case ".gz": $file_icon = "compress.png"; break;
		case ".txt": $file_icon = "page_white_text.png"; break;
		case ".php": $file_icon = "page_white_code_red.png"; break;
		case ".htm": $file_icon = "page_white_code_red.png"; break;
		case ".html": $file_icon = "html.png"; break;
		case ".xml": $file_icon = "xml.png"; break;
		case ".xsl": $file_icon = "xsl.png"; break;
		case ".csv": $file_icon = "page_white_excel.png"; break;
		case ".asp": $file_icon = "page_code.png"; break;
		case ".js": $file_icon = "js.png"; break;
		case ".mp3": $file_icon = "mp3.png"; break;
		case ".waw": $file_icon = "waw.png"; break;
		case ".cgi": $file_icon = "script.png"; break;
		case ".swi": $file_icon = "swi.png"; break;
		case ".swf": $file_icon = "swf.png"; break;
		case ".mov": $file_icon = "movie.png"; break;
		case ".qt": $file_icon = "movie.png"; break;
		case ".mpg": $file_icon = "movie.png"; break;
		case ".mpeg": $file_icon = "movie.png"; break;
		case ".sql": $file_icon = "database.png"; break;
		default: $file_icon = "page_attach.png";
	}
	return gui::icon($file_icon);
}

 function InstanceTimeToTime($strtime){
$time   = strlen($strtime)==5 ? '0'.$strtime : $strtime;
$array  = str_split($time);
$count  = 1;
$len    = count($array);
$pos    = 1;
$return = '';

foreach ($array as $time_digit){

 $return .= $time_digit;

 if($count==2){
 	$return.= $pos<$len ? ':' : '';
    $count = 1;
 }else{
 	$count++;
 }

  ++$pos;

}
 return $return;
}

function centerTrim($str){
	 $str = trim($str);
     return preg_replace("/\s+/", " ", $str);
}

 function Camelize($text){
  return ucwords(centerTrim(strtolower($text)));
 }

function Shorten($string, $width, $postfix_str='...') {
  if(strlen($string) > $width) {
    $string = wordwrap($string, $width);
    $string = substr($string, 0, strpos($string, "\n"));
    $string = $string.$postfix_str;
  }

  return $string;
}

/**
 *
 * @param string $string
 * @param string $slug
 * @return string
 */
function create_slug($string, $slug='-'){
 $string = strtolower($string);
   $slug=preg_replace('/[^a-z0-9-]+/', $slug, $string);
   return $slug;
}
 

/**
 * human readble date eg. 1hr ago
 * @param string $d
 * @param boolean $d_istime
 * @return string
 */
 function fuzzyDate($d, $d_istime=false) {
 	
 	    if($d_istime){
	     $ts = time() - $d;
 	    }else{
         $ts = time() - strtotime(str_replace("-","/",$d));
 	    }
 	    
 	    
        if($ts>31536000){
        	$val = round($ts/31536000,0).' year';
        }else if($ts>2419200){
          $val = round($ts/2419200,0).' month';
        }else if($ts>604800){
        	$val = round($ts/604800,0).' week';
        }else if($ts>86400){
        	$val = round($ts/86400,0).' day';
        }else if($ts>3600){
        	$val = round($ts/3600,0).' hour';
        }else if($ts>60){
        	$val = round($ts/60,0).' minute';
        }else{
        	$val = $ts.' second';
        }

        if($val>1){
        	$val .= 's ago';
        }else{
        	$val .= ' ago';
        }

        return $val;
 }

 function days_count_down($x){
	return 10;//haha guaranteed to return days_count_down 
}

 function textDate($strDate='',$supp=true){
 $return = '';

 if(is_numeric($strDate)){
  $strDate  = date('Y-m-d',$strDate);
 }else{
  $strDate  = isset($strDate) && !empty($strDate) ? $strDate : date('Y-m-d');
 }

 $return = date("D j",strtotime($strDate));

 $return .=  $supp==true ? "<sup>" : '';
 $return .=   date("S",strtotime($strDate));
 $return .=  $supp==true ? "</sup> " : '';

 $return .= date(" M Y",strtotime($strDate));

 return $return;

}//function

/*
 	$now  = strtotime(date('Y-m-d H:i'));
    $jana  = strtotime('2010-09-26 22:00');
    echo $start = distanceOfTimeInWords($now,$jana).' ago';
 	*/
 function distanceOfTimeInWords($fromTime, $toTime = 0, $showLessThanAMinute = false) {


    $distanceInSeconds = round(abs($toTime - $fromTime));
    $distanceInMinutes = round($distanceInSeconds / 60);

        if ( $distanceInMinutes <= 1 ) {
            if ( !$showLessThanAMinute ) {
                return ($distanceInMinutes == 0) ? 'less than a minute' : '1 minute';
            } else {
                if ( $distanceInSeconds < 5 ) {
                    return 'less than 5 seconds';
                }
                if ( $distanceInSeconds < 10 ) {
                    return 'less than 10 seconds';
                }
                if ( $distanceInSeconds < 20 ) {
                    return 'less than 20 seconds';
                }
                if ( $distanceInSeconds < 40 ) {
                    return 'about half a minute';
                }
                if ( $distanceInSeconds < 60 ) {
                    return 'less than a minute';
                }

                return '1 minute';
            }
        }
        if ( $distanceInMinutes < 45 ) {
            return $distanceInMinutes . ' minutes';
        }
        if ( $distanceInMinutes < 90 ) {
            return 'about 1 hour';
        }
        if ( $distanceInMinutes < 1440 ) {
            return 'about ' . round(floatval($distanceInMinutes) / 60.0) . ' hours';
        }
        if ( $distanceInMinutes < 2880 ) {
            return '1 day';
        }
        if ( $distanceInMinutes < 43200 ) {
            return 'about ' . round(floatval($distanceInMinutes) / 1440) . ' days';
        }
        if ( $distanceInMinutes < 86400 ) {
            return 'about 1 month';
        }
        if ( $distanceInMinutes < 525600 ) {
            return round(floatval($distanceInMinutes) / 43200) . ' months';
        }
        if ( $distanceInMinutes < 1051199 ) {
            return 'about 1 year';
        }

        return 'over ' . round(floatval($distanceInMinutes) / 525600) . ' years';
 }

 function fnc_date_calc($this_date,$num_days){

    $my_time = strtotime ($this_date); //converts date string to UNIX timestamp
    $timestamp = $my_time + ($num_days * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
     $return_date = date("Y/m/d",$timestamp);  //puts the UNIX timestamp back into string format

    return $return_date;//exit function and return string
 }//end of function

 function getMondays($year) {
  $newyear = $year;
  $week = 0;
  $day = 0;
  $mo = 1;
  $mondays = array();
  $i = 1;
  while ($week != 1) {
    $day++;
    $week = date("w", mktime(0, 0, 0, $mo,$day, $year));
  }
  array_push($mondays,date("r", mktime(0, 0, 0, $mo,$day, $year)));
  while ($newyear == $year) {
    $test =  strtotime(date("r", mktime(0, 0, 0, $mo,$day, $year)) . "+" . $i . " week");
    $i++;
    if ($year == date("Y",$test)) {
      array_push($mondays,date("r", $test));
    }
    $newyear = date("Y",$test);
  }
  return $mondays;
}

 function addWorkingDaysToDate($timeStamp,$days){
	$weekends = array('Sat','Sun');
	$dates = array();
    $i = 1;
  while ($i<=$days) {
 	$next = addDayToDate($timeStamp,$i);
 	$day  = date("Y-m-d",$next);
 	$day_name = date('D',strtotime($day));

 	if(in_array($day_name,$weekends)){
 	 ++$days;
 	}else{
 	 $dates[] = $day;
 	}
 	++ $i;
 }//while

 $reverse_dates = array_reverse($dates);
 return strtotime($reverse_dates[0]);
}

 function returnNextWorkingDay($timeStamp){
	$weekends = array('Sat','Sun');
	$dates = array();
    $i     = 0;
    $days  = 6;
  while ($i<=$days) {
 	$next = addDayToDate($timeStamp,$i);
 	$day  = date("Y-m-d",$next);
 	$day_name = date('D',strtotime($day));

 	if(in_array($day_name,$weekends)){
 	 ++$days;
 	}else{
 	 $dates[] = $day;
 	}
 	++ $i;
 }//while

 return strtotime($dates[0]);

}

 function addDayToDate($timeStamp, $totalDays=1){
        // You can add as many days as you want. mktime will accumulate to the next month / year.
        $thePHPDate = getdate($timeStamp);
        $thePHPDate['mday'] = $thePHPDate['mday']+$totalDays;
        $timeStamp = mktime($thePHPDate['hours'], $thePHPDate['minutes'], $thePHPDate['seconds'], $thePHPDate['mon'], $thePHPDate['mday'], $thePHPDate['year']);
        return $timeStamp;
 }

 function addMonthToDate($timeStamp, $totalMonths=1){
        // You can add as many months as you want. mktime will accumulate to the next year.
        $thePHPDate = getdate($timeStamp); // Covert to Array
        $thePHPDate['mon'] = $thePHPDate['mon']+$totalMonths; // Add to Month
        $timeStamp = mktime($thePHPDate['hours'], $thePHPDate['minutes'], $thePHPDate['seconds'], $thePHPDate['mon'], $thePHPDate['mday'], $thePHPDate['year']); // Convert back to timestamp
        return $timeStamp;
 }

 function addYearToDate($timeStamp, $totalYears=1){
        $thePHPDate = getdate($timeStamp);
        $thePHPDate['year'] = $thePHPDate['year']+$totalYears;
        $timeStamp = mktime($thePHPDate['hours'], $thePHPDate['minutes'], $thePHPDate['seconds'], $thePHPDate['mon'], $thePHPDate['mday'], $thePHPDate['year']);
        return $timeStamp;
 }

 function dateRangeArray($start,$end, $return_timestamps = true) {
  $range = array();

 if (is_string($start) === true) $start = strtotime($start);
 if (is_string($end) === true ) $end = strtotime($end);

  do {
	  
	if($return_timestamps){
     $range[] = $start;
    }else{
     $range[] = date('Y-m-d', $start);
    }
    
   $start = strtotime("+ 1 day", $start);
 } while($start <= $end);

 return $range;

}

 function reverseInputDate($input_date){
$array_date = explode('/',$input_date);
if(strlen($array_date[2])==4){
 $date  = $array_date[2].'/'.$array_date[1].'/'.$array_date[0];
}else{
 $date =  $array_date[0].'/'.$array_date[1].'/'.$array_date[2];;
}
return $date;
}//function reverseInputDate

 function getDays($start, $end , $input_is_raw = true){

   if($input_is_raw){
    $uts['start']      =    strtotime( $start );
    $uts['end']        =    strtotime( $end );
   }else{
   	$uts['start']      =    ( $start );
    $uts['end']        =    ( $end );
   }
   
//   print_pre($uts);
   
   if( $uts['start']!==-1 && $uts['end']!==-1 ){
       if( $uts['end'] >= $uts['start'] )
       {
               $diff  =  $uts['end'] - $uts['start'];
               $val   =  intval((floor($diff/86400)));
           if( $days= $val)
               $diff  = $diff % 86400;
               $val   = intval((floor($diff/3600)));
           if( $hours= $val)
               $diff  = $diff % 3600;
               $val   = intval((floor($diff/60))) ;
           if( $minutes=$val)
               $diff  = $diff % 60;
               $diff  =    intval( $diff );
           return($days);
       }else{
//         trigger_error( "Ending date/time {$end} is earlier than the startdate/time {$start}", E_USER_WARNING );
        return null;
       }
   }else{
//       trigger_error( "Invalid date/time data detected", E_USER_WARNING );
       return null;
   }

   return( false );

}//function

 function getWeekEnds($start_date,$days){
	$num_weekends = 0;
	$days         = $days-1;
	$weekends   = array('Sat','Sun');
		for($i=0;$i<=$days;$i++){

			$next_date = mktime(0, 0, 0, date("m",strtotime($start_date))  , date("d",strtotime($start_date))+$i, date("Y",strtotime($start_date)));

			$day = date('D',$next_date);
			if(in_array($day,$weekends)){
			  $num_weekends ++;
			}
  }//for

  return $num_weekends;

}


 function ordSuffix($num) {
$sLastChar = substr($num,-1,1);
switch($sLastChar) {
case 1:
$suffix = "st";
break;
case 2:
$suffix = "nd";
break;
case 3:
$suffix = "rd";
break;
default:
$suffix = "th";
break;
}
return $suffix;
}


 function table_value_exists($table, $col, $val){
	 global $db;
   $exists = $db->CacheGetOne(1200,"select {$col} from {$table} where {$col}='{$val}' ");
   
   if(!empty($exists)){
	   return true;
   }
   
   return false;
 }


 function table_value_name($table, $colVal, $colName, $val){
	 global $db;
   $name = $db->CacheGetOne(1200,"select {$colName} from {$table} where {$colVal}='{$val}' ");
   
   if($name){
	   return $name;
   }
   
   return false;
 }


 function generateID($table,$column='ID', $db=null){
	if(is_null($db))
	{
 	 global $db;
	}
    
	 $column = !empty($column) ? $column : 'ID';
	 $return ='';
       $record_id  = $db->GetRow("SELECT MAX(".$column.") AS RID FROM ".$table);
       if (!$record_id){
       	$return = $db->ErrorMsg();
       }else{
       	if($record_id['RID']<=0)$record_id['RID']=0;
        $return = $record_id['RID']+1;
       }
   return $return;
}

 function generateUniqueCode($prefix,$current_id,$padding=6){
 	$id =  str_pad($current_id, $padding, "0", STR_PAD_LEFT);
 	return $prefix.$id;
 }

 function emailIsValid($email_address){
 	return (preg_match(
		'/^[-!#$%&\'*+\\.\/0-9=?A-Z^_`{|}~]+'.   // the user name
		'@'.                                     // the ubiquitous at-sign
		'([-0-9A-Z]+\.)+' .                      // host, sub-, and domain names
		'([0-9A-Z]){2,4}$/i',                    // top-level domain (TLD)
		trim($email_address)));
 }

 function getFileExtension($filename){
 	 $pieces = explode('.', $filename);
     $extension = array_pop($pieces);
     return $filename;
 }

 function getMonthName($month) {

 	$months = array(
 	 1=>'January',
 	 2=>'February',
 	 3=>'March',
 	 4=>'April',
 	 5=>'May',
 	 6=>'June',
 	 7=>'July',
 	 8=>'August',
 	 9=>'September',
 	 10=>'October',
 	 11=>'November',
 	 12=>'December'
 	);

 	if(array_key_exists($month,$months)) return $months[$month]; else return $month;

 }

 function hourmin($hid = "hour", $mid = "minute", $pid = "pm", $hval = "", $mval = "", $pval = "")
      {
      if(empty($hval)) $hval = date("h");
      if(empty($mval)) $mval = date("i");
      if(empty($pval)) $pval = date("a");
      $hours = array(12, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11);
      $out = "<select name='$hid' id='$hid'>";
      foreach($hours as $hour)
      if(intval($hval) == intval($hour)) $out .= "<option value='$hour' selected>$hour</option>";
      else $out .= "<option value='$hour'>$hour</option>";
      $out .= "</select>";

      $minutes = array("00", 15, 30, 45);
      $out .= "<select name='$mid' id='$mid'>";
      foreach($minutes as $minute)
      if(intval($mval) == intval($minute)) $out .= "<option value='$minute' selected>$minute</option>";
      else $out .= "<option value='$minute'>$minute</option>";
      $out .= "</select>";

      $out .= "<select name='$pid' id='$pid'>";
      $out .= "<option value='am'>am</option>";
      if($pval == "pm") $out .= "<option value='pm' selected>pm</option>";
      else $out .= "<option value='pm'>pm</option>";
      $out .= "</select>";
      return $out;
      }

 function get_hex_from_file($tmp_file_name){
	if(filesize($tmp_file_name)>0){
		$fp = fopen($tmp_file_name, "r");
		$bin_contents = fread($fp, filesize($tmp_file_name));
		fclose($fp);
	 return ($bin_contents);
	}else{
	 return false;
	}
}

 function print_pre($s) {
 	print "<pre>";print_r($s);print "</pre>";
 }

 function delmagic( $value ){
	if( get_magic_quotes_gpc() ) return stripslashes( $value );
	else return $value;
}

 function databasetype_select($select_name,$default=''){
	$config_databases = array(
                          'mysql'=>'MySQL',
                          'mysqli'=>'MySQLi',
                          'mssql'=>'MSSQL',
                          'odbc_mssql'=>'Odbc Mssql',
                          'oci8'=>'Oracle',
                          'odbc_oracle'=>'Odbc Oracle',
                          'postgres'=>'Postgres',
                          'postgres8'=>'Postgres8',
                          'sybase'=>'Sybase',
                          'db2'=>'DB2',
                          'odbc_db2'=>'Odbc DB2',
                          'firebird'=>'Firebird',
                          'informix'=>'Informix',
                          );

 $cfg_db  = "<select name=\"{$select_name}\" id=\"{$select_name}\"> \n";
 $cfg_db .= empty($default)?'<option><?option>':'';
  foreach($config_databases as $database=>$database_desc){
   $cfg_db .= "<option value=\"{$database}\" ".($default==$database?'selected':'').">{$database_desc}</option> \n";
  }
 $cfg_db .= "</select> \n";
 return $cfg_db;
}


 function compress_html($buffer){
  $search = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
  $replace = array('>','<','\\1');
  $compr_buffer = preg_replace($search, $replace, $buffer);
  return $compr_buffer;
 }

 function randomizer($length) {
 	$str = '';
	$arr = array("1","2","3","4","5","6","7","8","9","0","q","w","e","r","t",
					"y","u","i","o","p","a","s","d","f","g","h","j","k","l",
					"z","x","c","v","b","n","m","Q","W","E","R","T","Y","U",
					"I","O","P","A","S","D","F","G","H","J","K","L","Z","X",
					"C","V","B","N","M");
	srand((float) microtime() * 1000000);
	for($i = $length; $i > 0; $i--) {
		$rand = rand(0, sizeof($arr));
		if(isset($arr[$rand])){
		$str .= $arr[$rand];
		}
	}
	return $str;
}

?>
