<?php

 define('CACHE_TIME',60000);
 define('STATIC_CONTENT_URL','./');
 
 function combine_css($cssfiles = array() , $force_reload = false ){
 	global $ROOT;

 	if(count($cssfiles)<0) return ;

      $unique_filename  = '/';
      $search           = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
      $replace          = array('>','<','\\1');

 	 foreach ($cssfiles as $index=>$cssfile)
 	 {
 	   $unique_filename  .= $cssfile;
 	 }
 	 
    $filename_md5       = md5($unique_filename);

    $filename_combined  =  './public/cache/'.$filename_md5.'.css';

    $output =  "/******Cached ".date('jS F Y H:i')."*****/\r\n";

    if(!is_readable($filename_combined) || $force_reload){
 	 foreach ($cssfiles as $index=>$cssfile_name){

 	  $compress        = true;

 	  $cssfile         =  $cssfile_name.'.css';

 		if(is_readable($cssfile) ){
 			$contents = file_get_contents($cssfile);

           if($compress){
 			 $output .= PHP_EOL;
             $output  .= preg_replace($search, $replace, $contents);
           }else{
           	 $output .= $contents;
           }
           
 		}else{
 			print ("Unable to load stylesheet {$cssfile} ");
 		}
 	  }//foreach

 	  if(!file_put_contents($filename_combined, $output))
 	  {
 	  	die("Unable to create CSS Cache File(s) in ./public/cache/ directory. Make sure it has httpd write rights and that you have at least 50MB of free disk space");
 	  }

    }

    ob_start();
    echo "<!--Expires ".date('jS F Y H:i', time()+CACHE_TIME)."-->";

    echo "<style type=text/css>@import url( ./public/cache/".$filename_md5.".css);</style> \n";

    ob_end_flush();

 }

 function  combine_js( $jsfiles = array()  , $refresh = false){
 	global $ROOT;
    require_once(ROOT.'classes/jspacker/class.jspacker.php');
    
 	$unique_filename = '';

 	if(count($jsfiles)<0) return ;

 	 foreach ($jsfiles as $index=>$jsfile) {
 	   $unique_filename  .= $jsfile;
 	 }

    $filename_md5       = md5($unique_filename);

    $filename_combined  =  './public/cache/'.$filename_md5.'.js';

    if($refresh && is_readable($filename_combined)) @unlink($filename_combined);

 	if(!is_readable($filename_combined)){

     $output = '';

 	 foreach ($jsfiles as $index=>$jsfile){
 	    $jsfile =   $jsfile.'.js';
 		if(is_readable($jsfile)){
 			
 		 $contents = file_get_contents($jsfile);	
 		 
 		 if(!strstr($jsfile,'.min')){
 		  $parker  = new jspacker($contents, 0  );
 	      $output .= PHP_EOL;
 		  $output .= $parker->pack();
 		 }else{
 		  $output .= $contents;
 		 }
 		 $output .= ';';
 		 
 		}else{
// 			echo("unable to read ...");
 		}
 	  }//foreach

 	  if(!file_put_contents($filename_combined, $output)){
 	  	die("Unable to create Javascript Cache Files. Make sure it has httpd write rights");
 	  }

 	}

    ob_start();
    echo "<!--Expires ".date('jS F Y H:i', time()+CACHE_TIME)."-->";
    echo "<script type=\"text/javascript\" src=\"./public/cache/".$filename_md5.".js\"></script> \n";
    ob_end_flush();

 }
