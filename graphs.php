<?php

$function = filter_input(INPUT_GET , 'function', FILTER_SANITIZE_STRING);

if(isset($_GET['appid'])){
 $appid       = filter_input(INPUT_GET , 'appid', FILTER_SANITIZE_STRING);
}else{
 $appid       = 'ST';
}

if(function_exists($function)){

 require_once('./init.php');
 require_once('./config/db.php');

 $function();
}

function population_pc(){
	global $db;
	$data = array();
	$population = $db->GetAssoc("select FORMCODE,count(ADMNO) num from viewsp WHERE FORMCODE IS NOT NULL group by  FORMCODE ");
	if($population){
	 foreach($population as $formcode=>$num){
	  $sliced    =  ($formcode  == 1) ? 'true' : 'false';
	  $selected  =  ($formcode  == 4) ? 'true' : 'false';
	  $data[]    = "{name: \"{$formcode}\", y: {$num},sliced: {$sliced} ,selected: {$selected} }";
	 }
	}

?>

<div id="graph_population" style="min-width: 120px; height: 170px; margin: 0 auto"></div>

	<script>
	$(function () {
    $('#graph_population').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Population",
            colorByPoint: true,
            data: [<?php print implode(',', $data); ?>]
        }]
    });
});
	</script>
	<?php
}

function population_num(){
	global $db;
	$data = array();
	$population = $db->GetAssoc("select FORMCODE,count(ADMNO) num from viewsp WHERE FORMCODE IS NOT NULL group by  FORMCODE ");
	if($population){
	 foreach($population as $formcode=>$num){
	 $categories[]  = "'F{$formcode}'";
	 $data[]        = $num;
	 }
    }
	?>

<div id="population_num" style="min-width: 150px; height: 170px; margin: 0 auto"></div>

	<script>
	$(function () {
    $('#population_num').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: 'By Form'
        },
        xAxis: {
            categories: [<?php echo implode(',', $categories); ?>],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Students'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -130,
            y: 120,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Students',
            data: [<?php echo implode(',', $data); ?>]
        }]
    });
});
	</script>
	<?php
}

function population_all(){
	global $db;
	$students = $db->GetOne("select count(ADMNO) num from viewsp  WHERE FORMCODE IS NOT NULL ");
	?>

<div id="div_sponsors" style="height: 10em;position: relative">
	<p style=" margin: 0;position: absolute;top: 50%;left: 50%;margin-right: -50%; transform: translate(-50%, -50%)">
	 <span style="font-size:30px;color:#000"  class="timer" data-from="0" data-to="<?php echo $students; ?>" data-speed="2000"><?php echo number_format($students,0); ?></span> <span>Students</span><br>
	</p>
</div>
	<?php
}

function tasks(){
	global $db,$appid;

	 $user         = new user();

	 if(strtolower($user->userid)=='admin'){
	  $menu         = $db->GetAssoc("SELECT MNUID,MODID,MNUNAME,PARENTID,MODPATH,MODKEY,MODTYPE,ATTRIBS,POSITION FROM sysmodmnu WHERE APPID='{$appid}'  AND upper(MNUNAME) not like '%SMS%' AND ACTIVE=1 AND PINHOME=1 ORDER BY MODID,PARENTID,POSITION ASC ");
	 }else{
	  $menu         = $db->GetAssoc("
	  SELECT MNUID,MNUNAME,MODID,PARENTID,MODPATH,MODKEY,MODTYPE,ATTRIBS,POSITION FROM sysmodmnu
	  WHERE APPID='{$appid}'  AND upper(MNUNAME) not like '%SMS%'
	  AND ACTIVE=1
	  AND PINHOME=1
	  AND MNUID IN (
	     SELECT MNUID FROM sysmnuauth WHERE GROUPCODE='{$user->groupcode}' AND  APPID='{$appid}'
	     )
	  ORDER BY MODID,PARENTID,POSITION ASC
	   ");
	 }

     if(!$menu) return;
     if(sizeof($menu)==0) return;

     $count = 1;

     echo "<ul style=\"list-style:none\">";
	 foreach($menu as $menuid => $menuitem){

	  if($count<=8) {

        $modid      =  valueof($menuitem, 'MODID');
        $mnuname    =  valueof($menuitem, 'MNUNAME');
 	   	$modpath    =  valueof($menuitem, 'MODPATH');
 	   	$modkey     =  valueof($menuitem, 'MODKEY');
 	   	$parentid   =  valueof($menuitem, 'PARENTID');
 	   	$modtype    =  valueof($menuitem, 'MODTYPE');
 	   	$attribs    =  valueof($menuitem, 'ATTRIBS');
 	   	$attribsd   =  json_decode($attribs);
 	   	$attributes =  array();

 	   	if(sizeof($attribsd)>0){
 	   	 foreach($attribsd as $k=>$v){
		  $attributes[$k] = $v;
		 }
		}

		$vars['appid']    = $appid;
		$vars['modid']    = $modid;
		$vars['mnuid']    = $menuid;
		$vars['modpath']  = $modpath;
		$vars['modkey']   = $modkey;
		$vars_str         = packvars($vars);

         $onclick = "openWindow( {$menuid},
                                  '{$mnuname}',
		                          '{$modkey}',
		                          '{$modtype}',
		                          '{$vars_str}',
		                          ".valueof($attributes,'win').",
		                          ".valueof($attributes,'hei').",
		                          ".valueof($attributes,'min').",
		                          ".valueof($attributes,'max').",
		                          ".valueof($attributes,'clo').",
		                          ".valueof($attributes,'col').",
		                          ".valueof($attributes,'res').",
		                          ".valueof($attributes,'bta').",
		                          ".valueof($attributes,'bte').",
		                          ".valueof($attributes,'btd').",
		                          ".valueof($attributes,'bti').",
		                          ".valueof($attributes,'btx')."
		                );";

		 echo "<li style=\"padding:2px;\"><a class=\"\" style=\"text-decoration:none;\" href=\"javascript:void(0);\" onclick=\"{$onclick}\"><i class=\"fa fa-arrow-circle-o-right\"></i> {$mnuname}</a> <br></li>";

	    }

		++$count;
	 }

	 echo "</ul>";

    $exec     = exec( "hostname" ); //the "hostname" is a valid command in both windows and linux
    $hostname = trim( $exec ); //remove any spaces before and after
    $ip       = gethostbyname( $hostname );
    echo "<p style=''>";
    echo "Current System IP:  ";
    echo '<span style="font-weight: bold;color: blue">';
    echo $ip;
    echo "</span>";
    echo "</p>";

}

function employees(){
global $db;
	$teachers = $db->GetOne("select count(TCHNO) num from sateachers ");

	?>

<div id="div_employees" style="height: 10em;position: relative">
	<p style=" margin: 0;position: absolute;top: 50%;left: 50%;margin-right: -50%; transform: translate(-50%, -50%)">
	 <span style="font-size:28px;color:#00f"  class="timer" data-from="0" data-to="<?php echo $teachers; ?>" data-speed="2500"><?php echo number_format($teachers,0); ?></span> <span>Teachers</span><br>
	</p>
</div>
	<?php
}

function sms(){
    echo "<p style='padding-left: 10px;padding-bottom: 0;margin: 0'> </p>";
    echo "<br>";
    echo "<p style='padding: 0;padding-left: 10px;margin: 0'> NO SENDER ID</p>";
}?>
<script type="text/javascript">
  $('.timer').countTo();
</script>
