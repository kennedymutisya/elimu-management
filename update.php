<?php

//updates server
$update_base_url  = "http://secondary.sims.co.ke/updatenow/";

@ignore_user_abort(true);
@set_time_limit(0);
@ini_set("max_execution_time", 1200000);

require_once('./init.php');
require_once(ROOT.'config/db.php');
require ROOT."lib/adodb/adodb-xmlschema03.inc.php";

$school           =  $db->GetRow("select SCHOOLCODE,SCHOOLNAME from SASCHOOL");
$schoolcode       =  valueof($school ,'SCHOOLCODE');
$schoolname       =  valueof($school ,'SCHOOLNAME');
$schoolname       =  urlencode($schoolname);

$updated      = false;
$found        = false;
$run_upgrade  = false;
$datadict_xml = array();
$datadict_php = array();
$_ENV         = array();

$_ENV['site']['files']['includes-dir'] = ROOT.'updates/';
$_ENV['site']['files']['server-root']  = ROOT;

 function get_system_version(){
  global $db;
  $version = $db->GetOne("select VERSION from SASVER ");

  if(empty($version)){
   $version = '1.0';
   $db->Execute("insert into SASVER (VERSION)values('{$version}')");
  }

  return $version;

 }

 function set_setting($version){
  global $db;
  $db->Execute("update SASVER set VERSION='{$version}'");
 }


//set_setting( 1 );

 $getVersions = file_get_contents($update_base_url."current-release-versions.php?schoolcode={$schoolcode}&schoolname={$schoolname}") or die ('DOWNLOAD ERROR');

 if ($getVersions != '') {
    $current_version = get_system_version();
    echo '<h3>Elimu Dynamic Update System</h3>';
    echo '<p style="color:blue">CURRENT VERSION: '.$current_version.'</p>';
    echo '<p>Reading Current Releases List</p>';
    $versionList = explode("\n", $getVersions);
    $version_client = str_replace('.','', $current_version);
    foreach ($versionList as $available_version){
        $available_version =  trim($available_version);
        $version_server = str_replace('.','', $available_version);
        //if ( $available_version > $current_version ) {
        if ( $version_server > $version_client ) {
            echo '<p style="color:red">New Update Found: '.$available_version.'</p>';
            $found = true;

            //Download The File If We Do Not Have It
            if ( !is_file(  $_ENV['site']['files']['includes-dir'].'/ELU-V-'.$available_version.'.zip' )) {
                echo '<p>Downloading New Update</p>';
                $newUpdate = file_get_contents($update_base_url.'ELU-V-'.$available_version.'.zip');
                if ( !is_dir( $_ENV['site']['files']['includes-dir'] ) ) mkdir ( $_ENV['site']['files']['includes-dir']);
                $dlHandler = fopen($_ENV['site']['files']['includes-dir'].'/ELU-V-'.$available_version.'.zip', 'w');
                if ( !fwrite($dlHandler, $newUpdate) ) { echo '<p>Could not save new update. Operation aborted.</p>'; exit(); }
                fclose($dlHandler);
                echo '<p>Update Downloaded And Saved</p>';
            } else echo '<p>Update already downloaded.</p>';

            if ( isset($_GET['doUpdate']) && ($_GET['doUpdate'] == true) ) {
                //Open The File And Do Stuff
                $zipHandle = zip_open($_ENV['site']['files']['includes-dir'].'/ELU-V-'.$available_version.'.zip');
                //echo '<ul style="color:#555">';
                while ($aF = zip_read($zipHandle) )
                {
                    $thisFileName = zip_entry_name($aF);
                    $thisFileDir  = dirname($thisFileName);

                    //Continue if its not a file
                    if ( substr($thisFileName,-1,1) == '/') continue;


                    //Make the directory if we need to...
                    if ( !is_dir ( ROOT.$thisFileDir ) ){

                         if( mkdir( ROOT.$thisFileDir, 0775, true)){
                          //echo '<li>Created Directory '.$thisFileDir.'</li>';
                         }else{
                          die("Unable to create : ".ROOT.$thisFileDir);
                         }

                    }

                    //Overwrite the file
                    if ( !is_dir(ROOT.''.$thisFileName) ) {

                        //echo '<li>'.$thisFileName.'...........';

                        //echo "\$thisFileName={$thisFileName} <br>";//remove

                        $contents = zip_entry_read($aF, zip_entry_filesize($aF));
                        $contents = str_replace("\r\n", "\n", $contents);
                        $updateThis = '';

                        //If we need to run commands, then do it.
                        if ( $thisFileName == 'upgrade.php' ){
                            //$db->debug=1;
                            $upgradeExec = fopen ('upgrade.php','w');
                            fwrite($upgradeExec, $contents);
                            fclose($upgradeExec);
                            //echo' CREATED</li>';
                            $run_upgrade = true;
                        }elseif ( $thisFileName == 'systables.sql' ){
                            $systables = fopen ('systables.sql','w');
                            fwrite($systables, $contents);
                            fclose($systables);

                            ////////////////////////////////////////////////////////////////////
                            // Temporary variable, used to store current query
                            $templine = '';
                            // Read in entire file
                            $lines = file($thisFileName);
                            // Loop through each line
                            foreach ($lines as $line)
                            {
                            // Skip it if it's a comment
                            if (substr($line, 0, 2) == '--' || $line == '')
                                continue;

                            // Add this line to the current segment
                            $templine .= $line;
                            // If it has a semicolon at the end, it's the end of the query
                            if (substr(trim($line), -1, 1) == ';')
                            {
                                // Perform the query
                                if(!$db->Execute($templine)){
                                 echo $db->ErrorMsg();
                                }
                                // Reset temp variable to empty
                                $templine = '';
                            }
                            }

                            //echo' IMPORTED</li>';
                            unlink('systables.sql');
                            flush();
                        }else{

                            $updateThis = @fopen( ROOT.''.$thisFileName, 'w');

                            if($updateThis){
                             if( fwrite($updateThis, $contents)){
                              fclose($updateThis);

                              if(substr($thisFileName,-3,3)=='xml' && strstr($thisFileName,'config/dd/') ){
                                $datadict_xml[] = $thisFileName;
                              }

                              if(substr($thisFileName,-3,3)=='php' && strstr($thisFileName,'config/dd/') ){
                                $datadict_php[] = $thisFileName;
                              }

                             }
                           }

                            unset($contents);

                            //echo' UPDATED</li>';
                            flush();
                        }
                    }
                }//while

                if(sizeof($datadict_xml)>0){
                 foreach($datadict_xml as $xml_file_name){
                    $schemaFile  = ROOT."{$xml_file_name}";
                    $schema      = new adoSchema( $db );
                    $schema->continueOnError = true;
                    $schema->debug = false;
                    $schema->SetUpgradeMethod('BEST');
                    $sql = @$schema->ParseSchema($schemaFile);
                    $result = @$schema->ExecuteSchema();

                    //echo '<li style="color:#EA671A">'.$xml_file_name.'....executed.....</li>';
                    flush();
                 }
                }

                if(sizeof($datadict_php)>0){
                 foreach($datadict_php as $php_file_name){
                    $incFile  = ROOT."{$php_file_name}";
                    include($incFile);
                    //echo '<li style="color:#EA671A">'.$php_file_name.'....executed.....</li>';
                    flush();
                 }
                }


                if($run_upgrade){
                 include ('upgrade.php');
                 unlink('upgrade.php');
                 //echo '<li style="color:#EA671A">upgrade.php....executed.....</li>';
                 flush();
                }

                //echo '</ul>';

                $updated = true;
            }
            else echo '<p>Update ready. <a href="?doUpdate=true">&raquo; Install Now?</a></p>';
            break;
        }
    }

    if ($updated == true){
        set_setting( $available_version );

        echo '<p class="success"  style="color:green">&raquo; System Updated to version '.$available_version.'</p>';
    }
    else if ($found != true) echo '<p>&raquo; No update is available.</p>';


}
else echo '<p  style="color:red">Could not find latest realeases.</p>';
